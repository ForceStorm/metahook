#ifndef _HUD_H
#define _HUD_H


typedef struct {
	int x, y;
} POSITION;


#define HUD_ACTIVE					(1<<0)
#define HUD_INTERMISSION			(1<<1)

enum GameMode_e
{
	GAME_MODE_SURVIVOR = 1,
	GAME_MODE_GHOST,
};


enum 
{ 
	MAX_PLAYERS = 64,
	MAX_TEAMS = 64,
	MAX_TEAM_NAME = 16,
};



class CHudBase
{
public:
	POSITION  m_pos;
	int   m_type;
	int	  m_iFlags; // active, moving, 

	CHudBase();

	virtual		~CHudBase() {}
	virtual void Init( void ) {}
	virtual void VidInit( void ) {}
	virtual void Draw( void ) {}
	virtual void Think(void) {}
	virtual void Reset(void) {}
	virtual void InitHUDData( void ) {}		// called every time a server is connected to

public:
	virtual void Show(void){}
	virtual void Hide(void) {}

public:
	bool m_bHasVidInit;

};

// ------------------------------------------------- [SayText] ------------------------------------------
class CHudSayText : public CHudBase
{
public:
	void Init( void );
	void InitHUDData( void );
	void VidInit( void );
	void Draw( void );
	int MsgFunc_SayText( const char *pszName, int iSize, void *pbuf );
	void SayTextPrint( const char *pszBuf, int iBufSize, int clientIndex = -1 );
	//void EnsureTextFitsInOneLineAndWrapIfHaveTo( int line );

	//struct cvar_s *	m_HUD_saytext;
	//struct cvar_s *	m_HUD_saytext_time;

private:
	int m_iTgaId_BG;
};

// ------------------------------------------------- [TextMessage] ------------------------------------------

class CHudTextMessage: public CHudBase
{
public:
	void Init( void );
	static char *LocaliseTextString( const char *msg, char *dst_buffer, int buffer_size );
	static char *BufferedLocaliseTextString( const char *msg );
	char *LookupString( const char *msg_name, int *msg_dest = NULL );
	int MsgFunc_TextMsg(const char *pszName, int iSize, void *pbuf);
};

// ------------------------------------------------- [Text Menu] ------------------------------------------

class CHudMenu : public CHudBase
{
public:
	CHudMenu(void);

public:
	int MsgFunc_ShowMenu( const char *pszName, int iSize, void *pbuf );

public:
	void VidInit(void);
	void Draw(void);
	void Show(void);
	void Hide(void);

	bool MenuHasShow(void);

private:
	int m_iTgaId_BG;
	int m_iWidth_BG;
	int m_iHeight_BG;

public:
	void Reset( void );
	void SelectMenuItem( int menu_item );

private:
	int m_fMenuDisplayed;
	int m_bitsValidSlots;
	float m_flShutoffTime;
	int m_fWaitingForMore;
	vgui::HFont m_font;
};

// ------------------------------------------------- [ Health ] ------------------------------------
#define FADE_TIME		0.2

#define BAR_LENGTH		180
#define BAR_HEIGHT		8

#define HEALTH_BAR_X	167
#define HEALTH_BAR_Y	667

#define ARMOR_BAR_X		167
#define ARMOR_BAR_Y		691

#define BLOOD_FADE_TIME	5.0

class CHudHealth: public CHudBase
{
public :
	CHudHealth(void);
public:
	virtual void VidInit( void );
	virtual void Draw( void );

	virtual void Hide(void);
	virtual void Show(void);

public:
	int MsgFunc_Health(const char *pszName,  int iSize, void *pbuf);
	int MsgFunc_Damage(const char *pszName,  int iSize, void *pbuf);

	void SetMaxHealth( int iMaxHealth );
	void SetMaxArmor( int iMaxArmor );

private:
	//int m_iBloodScreenType;
	//float m_flBloodScreenEndTime;
	//float m_flBloodScreenStartTime;

private:
	int m_iHealth;
	int m_iArmor;
	int m_iMaxArmor;
	int m_iMaxHealth;

	//int m_HUD_dmg_bio;
	//int m_HUD_cross;
	//float m_fAttackFront, m_fAttackRear, m_fAttackLeft, m_fAttackRight;
	//void GetPainColor( int &r, int &g, int &b );
	//float m_fFade;

private:
	int m_iTgaId_BG;
	int m_iTgaId_Bar;

	int m_iTgaId_BloodLes;
	int m_iTgaId_BloodMuch;
	
	//DAMAGE_IMAGE m_dmg[NUM_DMG_TYPES];
	//int	m_bitsDamage;
	//int DrawPain(float fTime);
	//int DrawDamage(float fTime);
	//void CalcDamageDirection(vec3_t vecFrom);
};

// ------------------------------------------------- [ Ammo ] ------------------------------------
#define AMMO_BLINK_TIME			0.1
#define GAP_TO_SCREEN_BORDER	12

#define NORMAL_FONT_H			20
#define NORMAL_FONT_W			20
#define BLINK_FONT_H			25
#define BLINK_FONT_W			25

#define WPN_IMAGE_W				150
#define WPN_IMAGE_H				38
#define PANEL_W					320


class CHudAmmo: public CHudBase
{
public :
	CHudAmmo(void);
public:
	virtual void VidInit( void );
	virtual void Draw( void );

	virtual void Hide(void);
	virtual void Show(void);

	void SetAmmo( int iClip, int iBpAmmo );

private:
	int m_iClip;
	int m_iBpAmmo;

	int m_iOldClip;
	int m_iOldBpAmmo;

	float m_flBlinkClipStartTime;
	float m_flBlinkBpAmmoStartTime;

private:
	int m_iTgaId_BG;
};

// ------------------------------------------------- [ Survivor Mode ] ------------------------------------

#define BLINK_TIME		0.2
#define SHOW_TIME		2.0
#define CENTRE_XPOS		512
#define CENTRE_YPOS		220
#define NOTICE_W		633
#define NOTICE_H		88

class CHudSuvModeUi : public CHudBase
{
public :
	CHudSuvModeUi();
public:
	virtual void VidInit(void);
	virtual void Draw(void);
	virtual void Hide(void);
	virtual void Show(void);

	void DrawCountDown( void );
	void DrawRoundEnd(void);
	void Reset(void);

	void UpdateClientDataKills( int iKill );
	void UpdateClientDataRounds( int iRounds) ;
	void SetCountDown( int iSec );
	void SetRoundEnd( bool bWin );

private:
	int m_iTgaId_SB;
	int m_iTgaId_Num[10];
	int m_iKillNum;
	int m_iRoundNum;

private:
	int m_iTgaId_CD_BG;
	int m_iTgaId_CD_Num[10];
	int m_iCountDownEndTime;

private:
	bool m_bWin;
	float m_flShowNoticeEndTime;
	int m_iTgaId_Win;
	int m_iTgaId_Lost;
};

// ------------------------------------------------- [ Timer ] ------------------------------------

class CHudTimer : public CHudBase
{
public:
	CHudTimer();
public:
	virtual void VidInit(void);
	virtual void Draw(void);
	virtual void Hide(void);
	virtual void Show(void);

public:
	void SetRoundTime( int iTime );

private:
	int m_iTgaId_BG;
	int m_iTgaId_Num[10];
	int m_iRoundTimeAmt;

	int m_iTimerNum1;
	int m_iTimerNum2;
	int m_iTimerNum3;
	int m_iTimerNum4;
	/*char m_szTimer_Num1[16];
	char m_szTimer_Num2[16];
	char m_szTimer_Num3[16];
	char m_szTimer_Num4[16];*/

	float flNextSecondTime;
};
// ------------------------------------------------- [ Night Vision ] ------------------------------------

#ifndef FFADE_IN

#define FFADE_IN			0x0000		// Just here so we don't pass 0 into the function
#define FFADE_OUT			0x0001		// Fade out (not in)
#define FFADE_MODULATE		0x0002		// Modulate (don't blend)
#define FFADE_STAYOUT		0x0004		// ignores the duration, stays faded out until new ScreenFade message received

#endif

class CHudNvg : public CHudBase
{
public:
	CHudNvg();
public:
	virtual void VidInit(void);
	virtual void Draw(void);
	virtual void Hide(void);
	virtual void Show(void);

private:
	int m_iTgaId_BG;
};


// ------------------------------------------------- [ HUD Manager ] ------------------------------------
class CHud
{
public:
	CHud(void);
	~CHud(void);

	
public:

	float m_flTime;	   // the current client time
	float m_fOldTime;  // the time at which the HUD was last redrawn
	double m_flTimeDelta; // the difference between flTime and fOldTime

	int		m_iFOV;

	int m_iIntermission;

public:
	void Init( void );
	void VidInit( void );
	//void Think(void) {}
	int Redraw( float flTime, int intermission );
	int UpdateClientData( client_data_t *cdata, float time );

	void SetGameMode( int iGameMode );

	int DrawHudString(int xpos, int ypos, int iMaxX, char *szIt, int r, int g, int b );

	
	// user messages
	int MsgFunc_Health(const char *pszName,  int iSize, void *pbuf);
	int MsgFunc_Damage(const char *pszName, int iSize, void *pbuf );
	//int MsgFunc_GameMode(const char *pszName, int iSize, void *pbuf );
	//int MsgFunc_Logo(const char *pszName,  int iSize, void *pbuf);
	//int MsgFunc_ResetHUD(const char *pszName,  int iSize, void *pbuf);
	//void MsgFunc_InitHUD( const char *pszName, int iSize, void *pbuf );
	//void MsgFunc_ViewMode( const char *pszName, int iSize, void *pbuf );
	//int MsgFunc_SetFOV(const char *pszName,  int iSize, void *pbuf);
	//int MsgFunc_Concuss( const char *pszName, int iSize, void *pbuf );
private:
	int m_iGameMode;
	int m_iTgaId_View;



public:
	CHudHealth				m_Health;
	CHudAmmo				m_Ammo;
	CHudSuvModeUi			m_SuvModeUi;
	CHudTimer				m_Timer;
	CHudNvg					m_Nvg;
	CHudSayText				m_SayText;
	CHudTextMessage			m_TextMessage;
	CHudMenu				m_Menu;

};

extern CHud gHUD;

extern int g_iPlayerClass;
extern int g_iTeamNumber;
extern int g_iUser1;
extern int g_iUser2;
extern int g_iUser3;

#endif