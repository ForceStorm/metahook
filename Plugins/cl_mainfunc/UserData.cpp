#include "base.h"
#include "UserData.h"

CUserData *g_pUserData;

CUserData::CUserData(void)
{
	m_iCode = 1;
}

void CUserData::ReadData(void)
{
	FILE *pFile;
	pFile = fopen( DATA_ADR, "rt");
	if(!pFile)
		return;

	char szLine[1024];
	char szData[5024];
	while(!feof(pFile))
	{
		fgets(szLine, sizeof(szLine)-1, pFile);
		sprintf(szData, "%s%s",szData, szLine);
	}

	fclose(pFile);

	for( int i =0; i<strlen(szData); i++ )
	{
		Decode(szData[ i]);
	}

	char szTemp[512];
	char szKey[128], szValue[256];
	int iSection = 0;
	int id = 0;

	memset( szTemp, 0, sizeof(szTemp));
	memset( szKey, 0, sizeof(szKey));
	memset( szValue, 0, sizeof(szValue));

	for( int i = 0; i<strlen( szData ); i++ )
	{
		if( szData[ i ] =='\n' )
		{
			szTemp[ id ] = '\0';
			sprintf( szValue, szTemp );
			memset( szTemp, 0, sizeof(szTemp) );

			id = 0;

			if( strstr( szValue, "[BAG_ITEM]") )
			{
				iSection = SECTION_BAGITEM;
				continue;
			}

			if( !szKey[0] )
				continue;

			switch( iSection )
			{
			case SECTION_BAGITEM:
				{
					if( strstr(szKey, "BAG1_ITEM") )
					{
						int aa = 0, aaa = 0;
						int iItems[8];
						char szBuff[16];

						for(int a=0; a < strlen(szValue); a++)
						{
							if(szValue[a] == ',')
							{
								szBuff[aa] = '\0';
								iItems[aaa] = atoi( szBuff );
								aaa++;
								aa=0;
								continue;
							}
							szBuff[aa] = szValue[a];
							aa ++;
						}

						//g_pBag->m_pBagMenu->AddItemsToBag(0, iItems);
					}
					else if( strstr(szKey, "BAG2_ITEM") )
					{
						int aa = 0, aaa = 0;
						int iItems[8];
						char szBuff[16];

						for(int a=0; a < strlen(szValue); a++)
						{
							if(szValue[a] == ',')
							{
								szBuff[aa] = '\0';
								iItems[aaa] = atoi( szBuff );
								aaa++;
								aa=0;
								continue;
							}
							szBuff[aa] = szValue[a];
							aa ++;
						}

						//g_pBag->m_pBagMenu->AddItemsToBag(1, iItems);
					}
					else if( strstr(szKey, "BAG3_ITEM") )
					{
						int aa = 0, aaa = 0;
						int iItems[8];
						char szBuff[16];

						for(int a=0; a < strlen(szValue); a++)
						{
							if(szValue[a] == ',')
							{
								szBuff[aa] = '\0';
								iItems[aaa] = atoi( szBuff );
								aaa++;
								aa=0;
								continue;
							}
							szBuff[aa] = szValue[a];
							aa ++;
						}

					//	g_pBag->m_pBagMenu->AddItemsToBag(2, iItems);
					}
					else if( strstr(szKey, "BAG4_ITEM") )
					{
						int aa = 0, aaa = 0;
						int iItems[8];
						char szBuff[16];

						for(int a=0; a < strlen(szValue); a++)
						{
							if(szValue[a] == ',')
							{
								szBuff[aa] = '\0';
								iItems[aaa] = atoi( szBuff );
								aaa++;
								aa=0;
								continue;
							}
							szBuff[aa] = szValue[a];
							aa ++;
						}

//						g_pBag->m_pBagMenu->AddItemsToBag(3, iItems);
					}
					break;
				}
			}


			memset( szKey, 0, sizeof(szKey));
			memset( szValue, 0, sizeof(szValue));
		}

		if( szData[i] == '=' )
		{
			szTemp[id] = '\0';
			sprintf(szKey, szTemp);
			memset( szTemp, 0, sizeof( szTemp ) );
			id = 0;

			continue;
		}

		if( id == 0 && szData[i] == '\n' )
			continue;
		if( id == 0 && szData[i] == 0) // the first character shouldnt be empty
			continue;
		szTemp[ id ] = szData[i];
		id ++;
		
	}
}

void CUserData::SaveData( char *pszText )
{
	FILE *pFile;
	pFile = fopen( DATA_ADR  ,"w");

	if( !pFile )
		return;

	for( int i=0; i< strlen( pszText ); i++ )
	{
		Encode(pszText[ i ] );
	}

	fprintf( pFile, "%s", pszText);


	fclose( pFile );

}

void CUserData::Encode( char & cChar )
{
	/*if(cChar == '\n')
		return;*/
	cChar += m_iCode;
}

void CUserData::Decode( char & cChar )
{
	/*if(cChar == '\n')
		return;*/
	cChar -= m_iCode;
}