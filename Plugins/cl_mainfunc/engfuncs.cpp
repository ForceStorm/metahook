#include "base.h"
#include "engfuncs.h"
#include "DrawSBPanel.h"
#include "plugins.h"
#include "UserCommands.h"

extern void View_InstallHook(void);
extern void TempEnt_InstallHook(void);


int EngFunc_AddCommand(char *szCmds, void (*pFunction)(void))
{
	if (!strcmp(szCmds, "+score") || !strcmp(szCmds, "+showscores"))
	{
		gEngfuncs.pfnAddCommand(szCmds, CmdFunc_InScoreDown);
		return 1;
	}
	else if(!strcmp(szCmds, "-score") || !strcmp(szCmds, "-showscores"))
	{
		gEngfuncs.pfnAddCommand(szCmds, CmdFunc_InScoreUp);
		return 1;
	}
	else if( !strcmp(szCmds, "slot1") )
	{
		gEngfuncs.pfnAddCommand(szCmds, __CmdFunc_Slot1);
	}
	else if( !strcmp(szCmds, "slot2") )
	{
		gEngfuncs.pfnAddCommand(szCmds, __CmdFunc_Slot2);
	}
	else if( !strcmp(szCmds, "slot3") )
	{
		gEngfuncs.pfnAddCommand(szCmds, __CmdFunc_Slot3);
	}
	else if( !strcmp(szCmds, "slot4") )
	{
		gEngfuncs.pfnAddCommand(szCmds, __CmdFunc_Slot4);
	}
	else if( !strcmp(szCmds, "slot5") )
	{
		gEngfuncs.pfnAddCommand(szCmds, __CmdFunc_Slot5);
	}
	else if( !strcmp(szCmds, "slot6") )
	{
		gEngfuncs.pfnAddCommand(szCmds, __CmdFunc_Slot6);
	}
	else if( !strcmp(szCmds, "slot7") )
	{
		gEngfuncs.pfnAddCommand(szCmds, __CmdFunc_Slot7);
	}
	else if( !strcmp(szCmds, "slot8") )
	{
		gEngfuncs.pfnAddCommand(szCmds, __CmdFunc_Slot8);
	}
	else if( !strcmp(szCmds, "slot9") )
	{
		gEngfuncs.pfnAddCommand(szCmds, __CmdFunc_Slot9);
	}
	else if( !strcmp(szCmds, "slot10") )
	{
		gEngfuncs.pfnAddCommand(szCmds, __CmdFunc_Slot10);
	}


	return gEngfuncs.pfnAddCommand(szCmds, pFunction);
}


void Engfuncs_DrawAdditive( int frame, int x, int y, const wrect_t *prc )
{
	if(frame == 0)
		return;

	gEngfuncs.pfnSPR_DrawAdditive( frame, x, y, prc );
}
void Engfuncs_FillRGBA( int x, int y, int width, int height, int r, int g, int b, int a )
{
	if( width = 1 && x > g_sScreenInfo.iWidth * 0.75 && y > g_sScreenInfo.iHeight  * 0.75 )
		return;

	gEngfuncs.pfnFillRGBA( x,  y,  width,  height,  r,  g,  b,  a);
}

void Engfuncs_SerCrosshair( HSPRITE hspr, wrect_t rc, int r, int g, int b )
{
	return;
}

void MemPatch_WideScreenLimit(void)
{
	unsigned char data[] = { 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90 };
	DWORD addr = (DWORD)g_pMetaHookAPI->SearchPattern((void *)g_dwEngineBase, g_dwEngineSize, "\x8B\x51\x08\x8B\x41\x0C\x8B\x71\x54\x8B\xFA\xC1\xE7\x04", 14);

	if (!addr)
	{
		MessageBox(NULL, "Failed to cancel WideScreen Limit!", "Warning", MB_ICONWARNING);
		return;
	}

	DWORD addr2 = addr + 11;
	DWORD addr3 =  (DWORD)g_pMetaHookAPI->SearchPattern((void *)addr, 0x60, "\xB1\x01\x8B\x7C\x24\x14", 6);

	if (addr3)
	{
		g_pMetaHookAPI->WriteMemory((void *)addr2, data, addr3 - addr2);
	}
}

void Engfunc_HookEvent( char *name, void ( *pfnEvent )( struct event_args_s *args ) )
{
	if(!strcmp(name, "events/train.sc") || !strcmp(name, "events/decal_reset.sc") || !strcmp(name, "events/vehicle.sc"))
		return gEngfuncs.pfnHookEvent (name, pfnEvent);

	Log("[Engfunc_HookEvent] Block old event hook : %s", name);
	return;
}


void Engine_InstallHook( void )
{
	MemPatch_WideScreenLimit();
	TempEnt_InstallHook();
	View_InstallHook();
}
