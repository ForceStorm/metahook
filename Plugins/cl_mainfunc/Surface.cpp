#include "base.h"
#include "plugins.h"

vgui::ISurface *g_pSurface;
#define LOADTGA_FUNC_SIG	"\x83\xEC\x18\x8B\x4C\x24\x1C\x53\x55\x56\x8D\x44\x24\x20\x57\x50"
typedef int (*type_LoadTGA)(const char *szFilename, byte *buffer, int bufferSize, int *width, int *height);
type_LoadTGA					g_pfn_LoadTGA;




void vgui::ISurface::DrawSetTextureFile2(int id, const char *filename, int hardwareFilter, bool forceReload)
{
	static byte buffer[1024 * 1024 * 4];

	char _filename[MAX_PATH];
	strcpy(_filename, filename);
	strcat(_filename, ".tga");

	int wide, tall;

	if (g_pfn_LoadTGA(_filename, buffer, sizeof(buffer), &wide, &tall))
	{
		g_pSurface->DrawSetTextureRGBA(id, buffer, wide, tall, hardwareFilter, forceReload);
	}
}



void Surface_InstallHook( vgui::ISurface *pSurface )
{
	g_pSurface = pSurface;
	g_pfn_LoadTGA = (type_LoadTGA)g_pMetaHookAPI->SearchPattern((void *)g_dwEngineBase, g_dwEngineSize, LOADTGA_FUNC_SIG, sizeof(LOADTGA_FUNC_SIG) - 1);
	//g_pfn_LoadTGA = (type_LoadTGA) GetEngfuncsAddress( 0x1D4F382 );
}