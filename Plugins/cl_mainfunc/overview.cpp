#include "base.h"
#include "overview.h"
#include "calcscreen.h"


COverview *g_pOverview;

extern void HUD_DrawPrintText(int x, int y, int r, int g, int b, int a, vgui::HFont font, wchar_t *text);

struct model_s *Engfunc_LoadMapSprite(const char *pszPath)
{
	g_pOverview->m_pSprite = gEngfuncs.LoadMapSprite(pszPath);
	return g_pOverview->m_pSprite;
}
void Engfunc_COM_FreeFile(void *pBuffer)
{
	if (g_pOverview)
		g_pOverview->m_bCanCheck = false;

	gEngfuncs.COM_FreeFile(pBuffer);
}
byte *Engfunc_COM_LoadFile(char *pszPath, int iUseHunk, int *piLength)
{
	byte *pbResult = gEngfuncs.COM_LoadFile(pszPath, iUseHunk, piLength);

	if (!pbResult)
		return NULL;

	if (g_pOverview && !strncmp(pszPath, "overviews/", 10))
	{
		g_pOverview->m_bCanCheck = true;
		g_pOverview->m_bToggle = true;
	}

	return pbResult;
}
char *Engfunc_COM_ParseFile(char *pszData, char *pszToken)
{
	static char szToken[1024];
	char *pszFile, *pszResult = gEngfuncs.COM_ParseFile(pszData, pszToken);

	if (!g_pOverview || !g_pOverview->m_bCanCheck)
		return pszResult;

	if (!_stricmp(pszToken, "zoom"))
	{
		gEngfuncs.COM_ParseFile(pszResult, szToken);
		g_pOverview->m_flZoom = (float)atof(szToken);
	}
	else if (!_stricmp(pszToken, "origin"))
	{
		pszFile = gEngfuncs.COM_ParseFile(pszResult, szToken);
		g_pOverview->m_vecOrigin[0] = (float)atof(szToken);
		pszFile = gEngfuncs.COM_ParseFile(pszFile, szToken);
		g_pOverview->m_vecOrigin[1] = (float)atof(szToken);
	}
	else if (!_stricmp(pszToken, "rotated"))
	{
		gEngfuncs.COM_ParseFile(pszResult, szToken);
		g_pOverview->m_bIsRotate = atoi(szToken) != FALSE;
	}
	/*else if (!_stricmp(pszToken, "height"))
	{	
		pszFile = gEngfuncs.COM_ParseFile(pszResult, szToken);
		g_pOverview->m_iHeight= (float)atof(szToken);
	}*/

	return pszResult;
}

COverview::COverview(void)
{
	m_bToggle = false;
	m_hFont = 0;
}
void COverview::CalcRefdef(struct ref_params_s *pParams)
{
	m_fYaw = pParams->viewangles[1];
	m_fYawSin = sin( m_fYaw *(M_PI/180.0));
	m_fYawCos = cos( m_fYaw *(M_PI/180.0));

	m_vecEyeOrigin[0] = pParams->vieworg[0];
	m_vecEyeOrigin[1] = pParams->vieworg[1];

	return;
}
void COverview::VidInit (void)
{
	m_flNextImpluseTime = 0;

	static bool bHasVidInit = false;

	if( bHasVidInit )
		return;

	bHasVidInit = true;

	m_iTgaId_BG = g_pDrawTGA->LoadTgaImage ( "resource/tga/Show_radar" , false );
	m_iTgaId_Impluse = g_pDrawTGA->LoadTgaImage("resource/tga/impluse", false);

	if( !m_hFont )
	{
		m_hFont = g_pSurface->CreateFont ();
		g_pSurface->AddGlyphSetToFont ( m_hFont, "EngineFont", ( TEAMMATE_FONT_H ) , ( TEAMMATE_FONT_W ), 0, 0, 0, 0, 1);
	}
}
void COverview::Draw(void)
{
	if( !CanDraw() )
		return;

	vec2_t vecTitles; 
	int i = static_cast<int>(sqrt(static_cast<long double>(m_pSprite->numframes / (4 * 3))));
	vecTitles[0] = i * 4;
	vecTitles[1] = i * 3;

	vec2_t vecStartPosOffset, vecScissorSize;
	vecStartPosOffset[0] = g_sScreenInfo.iWidth / 576;
	vecStartPosOffset[1] = g_sScreenInfo.iHeight / 432;
	vecScissorSize[0] = AdjustSizeW( RADAR_XPOS * 2 + RADAR_WIDTH );
	vecScissorSize[1] = AdjustSizeH( RADAR_YPOS * 2 + RADAR_HEIGHT);

	float flOverViewZoom = 10.0f;

	int iFrame = 0;
	float flScreenAspect = 4.0f / 3.0f;	
	float flAngles = (m_fYaw + 90.0) * (M_PI / 180.0);

	if (m_bIsRotate)
		flAngles -= float(M_PI / 2);

	vec2_t vecTile;

	if (m_bIsRotate)
	{
		vecTile[0] = 3 + m_flZoom * (1.0 / 1024.0) * m_vecOrigin[1] - (1.0/1024) * m_flZoom * m_vecEyeOrigin[1];
		vecTile[1] = -(-4 + m_flZoom * (1.0 / 1024.0) * m_vecOrigin[0] - (1.0/1024) * m_flZoom * m_vecEyeOrigin[0]);
	}
	else
	{
		vecTile[0] = 3 + m_flZoom * (1.0 / 1024.0) * m_vecOrigin[0] - (1.0/1024) * m_flZoom * m_vecEyeOrigin[0];
		vecTile[1] = 4 + m_flZoom * (1.0 / 1024.0) * m_vecOrigin[1] - (1.0/1024) * m_flZoom * m_vecEyeOrigin[1];
	}

	vec2_t vecStep;
	vecStep[0] = (2 * 4096.0f / flOverViewZoom) / vecTitles[0];
	vecStep[1] = -(2 * 4096.0f / (flOverViewZoom * flScreenAspect)) / vecTitles[1];

	vec2_t vecStepUp, vecStepRight;
	vec2_t vec1,vec2,vec3,vec4;

	vecStepUp[0] = cos(flAngles + (M_PI / 2)) * vecStep[1];
	vecStepUp[1] = sin(flAngles + (M_PI / 2)) * vecStep[1];
	vecStepRight[0] = cos(flAngles) * vecStep[0];
	vecStepRight[1] = sin(flAngles) * vecStep[0];

	vec2_t vecIn, vecOut;
	vecOut[0] = vecScissorSize[0] * 0.5 - vecTile[0] * vecStepRight[0] - vecTile[1] * vecStepUp[0] + vecStartPosOffset[0];
	vecOut[1] = vecScissorSize[1] * 0.5 - vecTile[0] * vecStepRight[1] - vecTile[1] * vecStepUp[1] + vecStartPosOffset[1];
	
	gEngfuncs.pfnFillRGBABlend( AdjustSizeW( RADAR_XPOS ), AdjustSizeH( RADAR_YPOS ), AdjustSizeW( RADAR_WIDTH ), AdjustSizeH( RADAR_HEIGHT ) , 0, 10, 0, 150 );
	
	glEnable(GL_SCISSOR_TEST);
	glScissor( AdjustSizeW( RADAR_XPOS ), g_sScreenInfo.iHeight - AdjustSizeH( RADAR_YPOS + RADAR_HEIGHT), AdjustSizeW( RADAR_WIDTH ), AdjustSizeH( RADAR_HEIGHT ) );

	int iWide, iTall;
	g_pSurface->DrawGetTextureSize( m_iTgaId_Impluse, iWide, iTall );
	int iImpluseStartPos = RADAR_XPOS - iWide - 5;

	if( g_flTime >= m_flNextImpluseTime )
		m_flNextImpluseTime = g_flTime + IMPLUSE_TIME_LENGTH;

	int iMoveAmt = (IMPLUSE_END_POS - iImpluseStartPos) * ( 1 - (m_flNextImpluseTime - g_flTime) /IMPLUSE_TIME_LENGTH );
	g_pDrawTGA->DrawTGA ( m_iTgaId_Impluse, AdjustSizeW( iImpluseStartPos + iMoveAmt) , AdjustSizeH(RADAR_YPOS - 3), AdjustSizeW(iWide), AdjustSizeH( RADAR_HEIGHT + 5), 200, 200, 255, 150 );

	gEngfuncs.pTriAPI->RenderMode( kRenderTransTexture );
	gEngfuncs.pTriAPI->CullFace(TRI_NONE);
	gEngfuncs.pTriAPI->Color4f( 1, 1, 1, 1);

	for (int i = 0; i < vecTitles[1]; i++)
	{
		vecIn[0] = vecOut[0];
		vecIn[1] = vecOut[1];

		for (int j = 0; j < vecTitles[0]; j++)	
		{
			gEngfuncs.pTriAPI->SpriteTexture(m_pSprite, iFrame);
			
			glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, 0x8370 );
			glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, 0x8370 );
			gEngfuncs.pTriAPI->Begin(TRI_QUADS);
			gEngfuncs.pTriAPI->TexCoord2f(0, 0);
			gEngfuncs.pTriAPI->Vertex3f(vecIn[0], vecIn[1], 0.0);
			gEngfuncs.pTriAPI->TexCoord2f(0, 1);
			gEngfuncs.pTriAPI->Vertex3f(vecIn[0] + vecStepRight[0], vecIn[1] + vecStepRight[1], 0.0);
			gEngfuncs.pTriAPI->TexCoord2f(1, 1);
			gEngfuncs.pTriAPI->Vertex3f(vecIn[0] + vecStepRight[0] + vecStepUp[0], vecIn[1] + vecStepRight[1] + vecStepUp[1], 0.0);
			gEngfuncs.pTriAPI->TexCoord2f(1, 0);
			gEngfuncs.pTriAPI->Vertex3f(vecIn[0] + vecStepUp[0], vecIn[1] + vecStepUp[1], 0.0);
			gEngfuncs.pTriAPI->End();

			iFrame++;
			vecIn[0] += vecStepUp[0];
			vecIn[1] += vecStepUp[1];
		}

		vecOut[0] += vecStepRight[0];
		vecOut[1] += vecStepRight[1];
	}
	
	glDisable(GL_SCISSOR_TEST);
	glEnd();

	g_pDrawTGA->DrawTGA ( m_iTgaId_BG, AdjustSizeW( RADAR_XPOS ), AdjustSizeH( RADAR_YPOS ), AdjustSizeW( RADAR_WIDTH ), AdjustSizeH( RADAR_HEIGHT ) );

	if( TEAMMATE_NAME_TOGGLE )
		DrawTeammateName();
}

bool COverview::CanDraw(void)
{
	return m_pSprite && m_bToggle && !gEngfuncs.GetLocalPlayer()->curstate.iuser1 && g_bAlive;
}

void COverview::DrawTeammateName(void)
{
	int iLocalIndex = gEngfuncs.GetLocalPlayer()->index;

	cl_entity_t *pEntity = NULL;
	for (int i = 1; i <= 32; i++)
	{

		if(i == iLocalIndex)
			continue;

		pEntity = gEngfuncs.GetEntityByIndex(i);

		if(!pEntity)
			continue;

		if (vPlayer[pEntity->index].team !=vPlayer[iLocalIndex].team)
			continue;

		if(vPlayer[pEntity->index].killtime < g_flTime)
			continue;

		if( !vPlayer[i].info )
			return;

		vec3_t pos;
		VectorCopy(vPlayer[i].info->origin, pos);

		pos[2] += 30.0;
		float vecScreen[3];

		if( CalcScreen(pos,vecScreen) )
		{
			hud_player_info_t hPlayer;
			gEngfuncs.pfnGetPlayerInfo(i,&hPlayer);
			wchar_t *pName = UTF8ToUnicode(hPlayer.name);
			HUD_DrawPrintText( vecScreen[0]-16, vecScreen[1], 255, 255, 255, 255, m_hFont, pName);
		}


	}
}