#ifndef _MGUI_CLASSMENU_H
#define _MGUI_CLASSMENU_H


#define MAX_PLAYER_ROLE 50

enum 
{
	DATA_TEAM = 1,
	DATA_NAME_LANG,
	DATA_INFO_LANG,
};

typedef struct PlayerRoleInfo_s
{
	char *szName;
	char *szModel;
	short iTeam;
	char *szNameLang;
	char *szInfoLang;
} PlayerRoleInfo_t;


class CMGUI_ClassMenu
{
public:
	CMGUI_ClassMenu(void);
public :
	void Init(void);
	void VidInit(void);
	void Show(  int iTeam  );
	void TurnPage(int i);
	PlayerRoleInfo_t m_PlayerRoleInfo[ MAX_PLAYER_ROLE ];

private:
	void ReadPlayerRoleInfo(void);

private:
	int m_iPlayerRoleNum;

	int m_iBtnIndexListLen;
	int *m_iBtnIndexList;

	int m_iTeam;
	int m_iPage;
	int m_iPageAmt;
	int m_iTgaId_NATO;
	int m_iTgaId_USSR;
};

extern CMGUI_ClassMenu g_MGuiClassMenu;


#endif