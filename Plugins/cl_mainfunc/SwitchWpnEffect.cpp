#include "base.h"
#include "weapon.h"
#include "SwitchWpnEffect.h"

CSwitchWpnEffect *g_pSwitchWpnFx;

CSwitchWpnEffect::CSwitchWpnEffect(void)
{
	m_flFadeTime = 0.7;
	m_iWidth_BGImage = 192;
	m_iHeight_BGImage = 48;
	m_iMovement = 60;
}
void CSwitchWpnEffect::VidInit(void)
{
	memset( m_iWpnId, 0, sizeof( m_iWpnId ) );
	memset( m_flStartTime, 0, sizeof( m_flStartTime ) );

	static bool bHasVidInit = false;

	if( bHasVidInit )
		return;

	bHasVidInit = true;

	m_iWpnBG_TgaId = g_pDrawTGA->LoadTgaImage( "resource/tga/Wpn_BackBoard", false );
}
void CSwitchWpnEffect::AddFxToWpn(int wpnid)
{
	int iChannel = FindValidChannel(wpnid);
	m_iWpnId[iChannel] = wpnid;
	m_flStartTime[iChannel] = g_flTime;
}
void CSwitchWpnEffect::Draw(void)
{
	for(int i=0; i<MAX_ITEM_FX; i++)
	{
		if(!m_iWpnId[i])
			continue;

		if(g_flTime > m_flStartTime[i] + m_flFadeTime)
		{
			m_iWpnId[i] = 0;
			continue;
		}

		int iX, iY;
		GetImagePos(i, iX, iY);
		iX = iX - (int)(m_iMovement * (g_flTime - m_flStartTime[i]) / m_flFadeTime);
		int iAlpha = (int)(255 * (1-(g_flTime - m_flStartTime[i]) / m_flFadeTime));
		if( m_iWpnId[i] < MAX_WPN && m_iWpnId[i] > 0)
		{
			g_pDrawTGA->DrawTGA( m_iWpnBG_TgaId,  AdjustSizeW(iX), AdjustSizeH(iY), AdjustSizeW(m_iWidth_BGImage), AdjustSizeH(m_iHeight_BGImage), 255, 255, 255, iAlpha );
			g_pDrawTGA->DrawTGA( g_WpnItemInfo[ m_iWpnId[i] ].iWpnTgaId,  AdjustSizeW(iX), AdjustSizeH(iY), AdjustSizeW(m_iWidth_BGImage), AdjustSizeH(m_iHeight_BGImage), 180, 180, 180, iAlpha );
		}
	}
}
int CSwitchWpnEffect::FindValidChannel(int iWpnid)
{
	int i = 0;
	for(i=0; i<MAX_ITEM_FX; i++)
	{
		if(m_iWpnId[i] == iWpnid)
			return i;
	}
	for(i=0; i<MAX_ITEM_FX; i++)
	{
		if(!m_iWpnId[i])
			break;
	}
	return i;
}
void CSwitchWpnEffect::GetImagePos(int iChannel, int &iX, int &iY)
{
	iX = 824;
	iY = 580 - iChannel * 70;
}
