#include "base.h"
#include "DrawSBPanel.h"

bool bPanelCanDraw = false;
hud_player_info_t g_PlayerInfoList[MAX_PLAYERS+1];
extra_player_info_t g_PlayerExtraInfo[MAX_PLAYERS+1];


int g_iPanelHeadTgaId;
int g_iPanelBodyTgaId;
int g_iPanelItemTgaId;
int g_iPanelItemHostTgaId;

struct vRankInfo
{
	int iFrag;
	int iId;
};
vRankInfo vAfterSort[33];

int iValidPlayer[33];

float fUpdateTime = 0;

void TAB_Panel_Init(void)
{
	memset(g_PlayerInfoList,0,sizeof(hud_player_info_t));
	memset(g_PlayerExtraInfo,0,sizeof(extra_player_info_t));

	bPanelCanDraw = false;

	static bool bHasVidInit = false;

	if( bHasVidInit )
		return;

	bHasVidInit = true;

	g_iPanelHeadTgaId = g_pDrawTGA->LoadTgaImage( "resource/tga/SBPanel_Head", true );
	g_iPanelBodyTgaId = g_pDrawTGA->LoadTgaImage( "resource/tga/SBPanel_Body", true );
	g_iPanelItemTgaId = g_pDrawTGA->LoadTgaImage( "resource/tga/SBPanel_Item", true );
	g_iPanelItemHostTgaId = g_pDrawTGA->LoadTgaImage( "resource/tga/SBPanel_ItemHost", true );

}
void TAB_Panel_SortAllPlayer(void)
{
	TAB_Panel_GetAllPlayersInfo();
	for(int i=1;i<33;i++)
	{
		vAfterSort[i].iFrag = g_PlayerExtraInfo[i].frags;
		vAfterSort[i].iId = i;
	}

	vRankInfo vTemp;
	int  i,j,n=33;
	for(i=1;i<n-1;i++) 
	{
		for(j=i+1;j<n;j++)
		{
			if(vAfterSort[i].iFrag<vAfterSort[j].iFrag)
			{
				vTemp=vAfterSort[i]; 
				vAfterSort[i]=vAfterSort[j]; 
				vAfterSort[j]=vTemp; 
			}
		}
	}
}

void TAB_Panel_Redraw(void)
{
	if(!bPanelCanDraw)
		return;

	DrawNormalPanel();

	return;
}
void DrawNormalPanel(void)
{

	// Sort Players
	if(fUpdateTime < g_flTime)
	{
		TAB_Panel_SortAllPlayer();
		fUpdateTime = g_flTime + 0.5f;
	}

	int iTeamT = 0;
	int iTeamCT = 0;
	for(int i=1;i<33;i++)
	{
		if(iValidPlayer[i])
		{
			if(vPlayer[i].team == 1) iTeamCT++;
			else if(vPlayer[i].team == 2) iTeamT++;
		}
	}

	// get the panel's size

	int iMax;

	if(iTeamCT > iTeamT)
		iMax = iTeamCT;
	else
		iMax = iTeamT;

	int iPanelBody_Height = iMax * 33 + 181;
	int iPanelBody_Width = 786;
	int iPanelBody_Xpos = 110;
	int iPanelBody_Ypos = (768 - iPanelBody_Height) / 2.0;
	int iPanelBody_CenterXpos = 504;


	// Draw Panel
	g_pDrawTGA->DrawTGA( g_iPanelBodyTgaId, AdjustSizeW(iPanelBody_Xpos), AdjustSizeH(iPanelBody_Ypos), AdjustSizeW(iPanelBody_Width), AdjustSizeH(iPanelBody_Height) );
	g_pDrawTGA->DrawTGA( g_iPanelHeadTgaId, AdjustSizeW(136), AdjustSizeH(iPanelBody_Ypos + 25), AdjustSizeW(736), AdjustSizeH(107) );
	

	// Draw Titles
	int iTextLen;

#define TITLE_FONTSIZE 53

	int iTitle_Ypos = AdjustSizeH( (107 - TITLE_FONTSIZE)/2 + (iPanelBody_Ypos + 25) + TITLE_FONTSIZE);

	char szBuffer2[16];
	wchar_t pTextTemp3[64];

	itoa( iTeamCT, szBuffer2, 10 );
	g_pLocalize->ConstructString( pTextTemp3, sizeof(pTextTemp3), g_pLocalize->Find ("FS_TabPanel_Team_NATO"), 1, ANSIToUnicode(szBuffer2) );
	iTextLen = Fonts_GetLen(pTextTemp3, TITLE_FONTSIZE);
	DrawFonts(pTextTemp3, AdjustSizeW((iPanelBody_CenterXpos - 152 -iTextLen)/2.0 + 152), iTitle_Ypos, 180,180,180,200, AdjustSizeW(TITLE_FONTSIZE), AdjustSizeH(TITLE_FONTSIZE), 1000, 1000);

	itoa( iTeamT, szBuffer2, 10 );
	g_pLocalize->ConstructString( pTextTemp3, sizeof(pTextTemp3), g_pLocalize->Find ("FS_TabPanel_Team_USSR"), 1, ANSIToUnicode(szBuffer2) );
	iTextLen = Fonts_GetLen(pTextTemp3, TITLE_FONTSIZE);
	DrawFonts(pTextTemp3, AdjustSizeW( (352 - iTextLen)/2.0 + iPanelBody_CenterXpos ), iTitle_Ypos, 180,180,180,200, AdjustSizeW(TITLE_FONTSIZE), AdjustSizeH(TITLE_FONTSIZE), 1000, 1000);

	// Tip on both sides
#define TIP_FONTSIZE 12

	int iY = AdjustSizeH(iPanelBody_Ypos + 118 + TIP_FONTSIZE);

	DrawFonts( g_pLocalize->Find ("FS_TabPanel_Mark") , AdjustSizeW(384), iY, 200, 200, 200, 255, AdjustSizeW(TIP_FONTSIZE),AdjustSizeH(TIP_FONTSIZE), 1000, 1000);
	DrawFonts( g_pLocalize->Find ("FS_TabPanel_Death") , AdjustSizeW(419), iY, 200, 200, 200, 255, AdjustSizeW(TIP_FONTSIZE),AdjustSizeH(TIP_FONTSIZE), 1000, 1000);
	DrawFonts( g_pLocalize->Find ("FS_TabPanel_Ping") , AdjustSizeW(454), iY, 200, 200, 200, 255, AdjustSizeW(TIP_FONTSIZE),AdjustSizeH(TIP_FONTSIZE), 1000, 1000);

	DrawFonts( g_pLocalize->Find ("FS_TabPanel_Mark"), AdjustSizeW(736), iY, 200, 200, 200, 255, AdjustSizeW(TIP_FONTSIZE),AdjustSizeH(TIP_FONTSIZE), 1000, 1000);
	DrawFonts( g_pLocalize->Find ("FS_TabPanel_Death"), AdjustSizeW(771), iY, 200, 200, 200, 255, AdjustSizeW(TIP_FONTSIZE),AdjustSizeH(TIP_FONTSIZE), 1000, 1000);
	DrawFonts( g_pLocalize->Find ("FS_TabPanel_Ping"), AdjustSizeW(806), iY, 200, 200, 200, 255, AdjustSizeW(TIP_FONTSIZE),AdjustSizeH(TIP_FONTSIZE), 1000, 1000);

	iY = AdjustSizeH(iPanelBody_Ypos + 118 + TIP_FONTSIZE + 2);
	gEngfuncs.pfnFillRGBA( AdjustSizeW(147), iY, AdjustSizeW(349), 1,251,201,96,255);
	gEngfuncs.pfnFillRGBA( AdjustSizeW(508), iY, AdjustSizeW(349), 1,251,201,96,255);

	// Draw Players' information

#define PLAYER_INFO_FONTSIZE 14

	int iLocal =gEngfuncs.GetLocalPlayer()->index;
	int iItemCT_Xpos = 151;
	int iItemT_Xpos = 510;
	int iY_CT = 0;
	int iY_T = 0;
	int r, g, b;
	wchar_t pText[256];
	char szBuff[32];

	for(int i=1;i<33;i++)
	{
		if( !iValidPlayer[vAfterSort[i].iId] || !vPlayer[vAfterSort[i].iId].team )
			continue;

		if(vPlayer[vAfterSort[i].iId].team == TEAM_CT)
		{
			iY = iPanelBody_Ypos + 136 + 33 * iY_CT;

			if(vAfterSort[i].iId == iLocal)
			{
				g_pDrawTGA->DrawTGA( g_iPanelItemHostTgaId, AdjustSizeW(iItemCT_Xpos), AdjustSizeH(iY), AdjustSizeW(343), AdjustSizeH(29));
	
				r = 0;
				g = 0;
				b = 0;
			}
			else
			{
				g_pDrawTGA->DrawTGA( g_iPanelItemTgaId,AdjustSizeW(iItemCT_Xpos), AdjustSizeH(iY), AdjustSizeW(343), AdjustSizeH(29));
	
				r = 173;
				g = 201;
				b = 235;
			}

			int iFont_Ypos = AdjustSizeH( iY + (29 - PLAYER_INFO_FONTSIZE)/2 + PLAYER_INFO_FONTSIZE);

			// Name
			DrawFonts(UTF8ToUnicode(g_PlayerInfoList[vAfterSort[i].iId].name), AdjustSizeW(iItemCT_Xpos + 5), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);
			
			// Attrib
			if(g_PlayerExtraInfo[vAfterSort[i].iId].iFlag & SCOREATTRIB_DEAD)
				DrawFonts( g_pLocalize->Find ("FS_TabPanel_Attrib_Dead"), AdjustSizeW(296), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);

			// Marks
			sprintf(szBuff, "%d", g_PlayerExtraInfo[vAfterSort[i].iId].frags);
			Fonts_C2W(pText, 255, szBuff);
			DrawFonts(pText,  AdjustSizeW(384), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);

			// Deaths
			sprintf(szBuff,"%d",g_PlayerExtraInfo[vAfterSort[i].iId].deaths);
			Fonts_C2W(pText, 255, szBuff);
			DrawFonts(pText,  AdjustSizeW(419), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);

			// Ping
			if(g_PlayerInfoList[vAfterSort[i].iId].ping == 0)
			{
				const char *isBotString = gEngfuncs.PlayerInfo_ValueForKey(vAfterSort[i].iId, "*bot");

				if (isBotString && atoi(isBotString) > 0)
					DrawFonts( g_pLocalize->Find ("FS_TabPanel_Attrib_Bot"), AdjustSizeW(454), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);
				else
					DrawFonts( g_pLocalize->Find ("FS_TabPanel_Attrib_Host"), AdjustSizeW(454), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);
			}
			else
			{
				sprintf(szBuff,"%d",g_PlayerInfoList[vAfterSort[i].iId].ping);
				Fonts_C2W(pText, 255, szBuff);
				DrawFonts(pText, AdjustSizeW(454), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);

			}
			iY_CT++;
		}
		else if(vPlayer[vAfterSort[i].iId].team == TEAM_T)
		{
			iY = iPanelBody_Ypos + 136 + 33 * iY_T;

			if(vAfterSort[i].iId == iLocal)
			{
				g_pDrawTGA->DrawTGA( g_iPanelItemHostTgaId, AdjustSizeW(iItemT_Xpos), AdjustSizeH(iY), AdjustSizeW(343), AdjustSizeH(29));
	
				r = 0;
				g = 0;
				b = 0;
			}
			else
			{
				g_pDrawTGA->DrawTGA( g_iPanelItemTgaId, AdjustSizeW(iItemT_Xpos), AdjustSizeH(iY), AdjustSizeW(343), AdjustSizeH(29));
	
				r = 216;
				g = 81;
				b = 80;
			}

			int iFont_Ypos = AdjustSizeH( iY + (29 - PLAYER_INFO_FONTSIZE)/2  + PLAYER_INFO_FONTSIZE);
			DrawFonts(UTF8ToUnicode(g_PlayerInfoList[vAfterSort[i].iId].name), AdjustSizeW(iItemT_Xpos + 5), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);
			
			// Attrib
			if(g_PlayerExtraInfo[vAfterSort[i].iId].iFlag & SCOREATTRIB_DEAD)
				DrawFonts( g_pLocalize->Find ("FS_TabPanel_Attrib_Dead"), AdjustSizeW(672), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);
			/*else if(g_PlayerExtraInfo[vAfterSort[i].iId].iFlag & SCOREATTRIB_BOMB && vPlayer[iLocal].team == 2)
				DrawFonts(GetLangUni("TABPANEL_ATTRIB_BOMB"), AdjustSizeW(672),iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);
			*/

			// Marks
			sprintf(szBuff, "%d", g_PlayerExtraInfo[vAfterSort[i].iId].frags);
			Fonts_C2W(pText, 255, szBuff);
			DrawFonts(pText,  AdjustSizeW(736), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);

			// Deaths
			sprintf(szBuff,"%d",g_PlayerExtraInfo[vAfterSort[i].iId].deaths);
			Fonts_C2W(pText, 255, szBuff);
			DrawFonts(pText,  AdjustSizeW(771), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);

			// Ping
			if(g_PlayerInfoList[vAfterSort[i].iId].ping == 0)
			{
				const char *isBotString = gEngfuncs.PlayerInfo_ValueForKey(vAfterSort[i].iId, "*bot");

				if (isBotString && atoi(isBotString) > 0)
					DrawFonts( g_pLocalize->Find ("FS_TabPanel_Attrib_Bot"), AdjustSizeW(806), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);
				else
					DrawFonts( g_pLocalize->Find ("FS_TabPanel_Attrib_Host"), AdjustSizeW(806), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);

			}
			else
			{
				sprintf(szBuff,"%d",g_PlayerInfoList[vAfterSort[i].iId].ping);
				Fonts_C2W(pText,255,szBuff);
				DrawFonts(pText, AdjustSizeW(806), iFont_Ypos, r, g, b, 255, AdjustSizeW(PLAYER_INFO_FONTSIZE), AdjustSizeH(PLAYER_INFO_FONTSIZE), 1000, 1000);

			}
			iY_T++;
		}
	}
}

void TAB_Panel_GetAllPlayersInfo( void )
{
	memset(vAfterSort,0,sizeof(vAfterSort));
	for ( int i = 1; i < MAX_PLAYERS; i++ )
	{
		gEngfuncs.pfnGetPlayerInfo( i, &g_PlayerInfoList[i] );
		if(g_PlayerInfoList[i].model == NULL)
		{
			iValidPlayer[i] = 0;
			g_PlayerExtraInfo[i].frags = 0;
			continue;
		}
		iValidPlayer[i] = 1;
	}
}

void CmdFunc_InScoreDown(void)
{
	bPanelCanDraw = true;
	return;
}
void CmdFunc_InScoreUp(void)
{
	bPanelCanDraw = false;
	return;
}
