#include "base.h"
#include "MGUI.h"
#include "window_teammenu.h"

int g_iTexid_NATO;
int g_iTexid_NATO_Fire;
int g_iTexid_USSR;
int g_iTexid_USSR_Fire;

CBaseLabel *g_pInfoLabel = NULL;
CBaseLabel *g_pTitleLabel = NULL;

class CTeamMenu : public CBaseWindow
{
public:
	virtual void Draw(PointPos_t *pp);
	static CTeamMenu *Instance(void);
};

void CTeamMenu::Draw(PointPos_t *pp)
{
	gEngfuncs.pfnFillRGBABlend(m_iXpos, m_iYpos, m_iWidth, m_iHeight, 0, 0, 0, 210);
	gEngfuncs.pfnFillRGBABlend(0, 0, g_sScreenInfo.iWidth, g_sScreenInfo.iHeight, 37, 31, 5, 150);
}

CTeamMenu* CTeamMenu::Instance(void)
{
	CTeamMenu* pWindow = NULL;
	pWindow = new CTeamMenu;

	if(!pWindow)
		return NULL;

	pWindow->Init();
	gMGUI.AddWindow(pWindow);

	return pWindow;
}

CTeamMenu *g_pTeamMenu = NULL;

// ============================================================================================


class CTeamButton : public CBaseButton
{
protected:
	virtual void DrawButton( MGUI_STATUS   status );
	virtual void DrawBtnCaption( MGUI_STATUS   status ){}
	virtual void OnButtonFire(void);

public:
	int m_iTeam;
	void SetTeam(int iTeam)
	{
		m_iTeam = iTeam;
	}

	static CTeamButton* Instance(CBaseMGUI *pParent);
};

CTeamButton* CTeamButton::Instance(CBaseMGUI *pParent)
{
	CTeamButton* pButton = NULL;
	pButton = new CTeamButton;

	if(!pButton)
		return NULL;

	pButton->Init();
	pButton->SetParent (pParent);
	pButton->SetClass (CLASS_BUTTON);

	return pButton;
}
void CTeamButton::OnButtonFire(void)
{
	if(g_pInfoLabel)
	{
		if(m_iTeam == 2)
			g_pInfoLabel->SetLocalString ("FS_TeamMenu_Info_NATO");
		else
			g_pInfoLabel->SetLocalString ("FS_TeamMenu_Info_USSR");
	}
}

void CTeamButton::DrawButton( MGUI_STATUS   status )
{
	int itexid, itexid_fire;

	if(m_iTeam == 2)
	{
		itexid = g_iTexid_NATO;
		itexid_fire = g_iTexid_NATO_Fire;
	}
	else
	{
		itexid = g_iTexid_USSR;
		itexid_fire = g_iTexid_USSR_Fire;
	}

	int x, y, x_parent = 0, y_parent = 0;
	if(GetParent())
		GetParent()->GetPosition(x_parent, y_parent);

	x = x_parent + m_iXpos;
	y = y_parent + m_iYpos;

	if( status == STATE_FIRE)
		gTGA.Draw(itexid_fire, x, y, m_iWidth, m_iHeight);
	else
		gTGA.Draw(itexid, x, y, m_iWidth, m_iHeight);
}


CTeamButton* g_pBtn_NATO;
CTeamButton* g_pBtn_USSR;

// ============================================================================================

void TeamMenuButtonClick(int iTeam )
{
	char szCmd[32];
	sprintf(szCmd, "jointeam %d", iTeam);
	gEngfuncs.pfnClientCmd(szCmd);
	g_pTeamMenu->Destroy();
	gMGUI.SetMouseState(MOUSE_FIXED);
}


void ShowTeamMenu(void)
{
	g_iTexid_USSR = gTGA.GetTextureId("cstrike\\resource\\tga\\Btn_USSR.tga");
	g_iTexid_USSR_Fire = gTGA.GetTextureId("cstrike\\resource\\tga\\Btn_USSR_fire.tga");

	g_iTexid_NATO = gTGA.GetTextureId("cstrike\\resource\\tga\\Btn_NATO.tga");
	g_iTexid_NATO_Fire = gTGA.GetTextureId("cstrike\\resource\\tga\\Btn_NATO_fire.tga");

	g_pTeamMenu = CTeamMenu::Instance();
	if(!g_pTeamMenu)
		return;

	g_pTeamMenu->SetPosition(0, AdjustSizeH(192));
	g_pTeamMenu->SetSize(g_sScreenInfo.iWidth, AdjustSizeH(384));
	g_pTeamMenu->SetVisible(TRUE);

	g_pBtn_NATO = CTeamButton::Instance (g_pTeamMenu);
	g_pBtn_NATO->SetTeam (2);
	g_pBtn_NATO->SetSize (AdjustSizeW(240), AdjustSizeH(216));
	g_pBtn_NATO->SetPosition( AdjustSizeW(321), AdjustSizeH(288 - 192) );
	g_pBtn_NATO->SetCallBackFunc (TeamMenuButtonClick);
	g_pBtn_NATO->SetMessage (2);

	g_pBtn_USSR = CTeamButton::Instance (g_pTeamMenu);
	g_pBtn_USSR->SetTeam (1);
	g_pBtn_USSR->SetSize (AdjustSizeW(240), AdjustSizeH(216));
	g_pBtn_USSR->SetPosition( AdjustSizeW(48), AdjustSizeH(288 - 192) );
	g_pBtn_USSR->SetCallBackFunc (TeamMenuButtonClick);
	g_pBtn_USSR->SetMessage (1);

	g_pInfoLabel = CBaseLabel::Instance (g_pTeamMenu);
	g_pInfoLabel->SetFontSize (AdjustSizeW(16));
	g_pInfoLabel->SetLocalString ("#FS_TeamMenu_Info");
	g_pInfoLabel->SetPosition (AdjustSizeW(605), AdjustSizeH(312- 192));

	g_pTitleLabel = CBaseLabel::Instance (g_pTeamMenu);
	g_pTitleLabel->SetFontSize (AdjustSizeW(30));
	g_pTitleLabel->SetLocalString ("#FS_TeamMenu_Title");
	g_pTitleLabel->SetPosition (AdjustSizeW(48), AdjustSizeH(246- 192));

	gMGUI.SetMouseState(MOUSE_FREE);
}