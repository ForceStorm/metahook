#include "base.h"
#include "window_teammenu.h"
#include "cdll_dll.h"
#include "calcscreen.h"
#include "MetaHook_Msg.h"
#include "weapon.h"
#include "SwitchWpnEffect.h"
#include "hud.h"
#include "window_classmenu.h"
#include "text_message.h"
#include "saytext.h"
#include "DrawSBPanel.h"
#include "message.h"
#include "DrawDeathmsg.h"
#include "view.h"
#include "parsemsg.h"


pfnUserMsgHook pmTeamInfo;
pfnUserMsgHook pmScoreAttrib;
pfnUserMsgHook pmDeathMsg;
pfnUserMsgHook pmSetFOV;
pfnUserMsgHook pmVGUIMenu;
pfnUserMsgHook pmScoreInfo;
pfnUserMsgHook pmShowMenu;
pfnUserMsgHook pmSayText;
pfnUserMsgHook pmHealth;
pfnUserMsgHook pmTextMsg;
pfnUserMsgHook pmResetHUD;
pfnUserMsgHook pmDamage;
pfnUserMsgHook pmNVGToggle;

int g_iGameMode = 0;


int MsgFunc_ScoreAttrib(const char *pszName, int iSize, void *pbuf)
{
	BEGIN_READ(pbuf, iSize);
	int iPlayerID = READ_BYTE();
	int iFlags = READ_BYTE();

	g_PlayerExtraInfo[iPlayerID].iFlag = iFlags;

	if (iPlayerID == gEngfuncs.GetLocalPlayer()->index)
	{
		if (iFlags & SCOREATTRIB_DEAD)
		{
			g_bAlive = false;

			g_pWeapon->SetCrosshairStatus( true );
			gHUD.m_Health.Hide();
			gHUD.m_Ammo.Hide();
		}
		else
		{
			gHUD.m_Health.Show ();
			gHUD.m_Ammo.Show();
			g_bAlive = true;
		}
	}
	return pmScoreAttrib(pszName, iSize, pbuf);
}

int MsgFunc_ScoreInfo(const char *pszName, int iSize, void *pbuf)
{
	BEGIN_READ(pbuf,iSize);
	int id = READ_BYTE();
	g_PlayerExtraInfo[id].frags= READ_SHORT();
	g_PlayerExtraInfo[id].deaths = READ_SHORT();
	READ_SHORT();
	READ_SHORT();

	return pmScoreInfo(pszName, iSize, pbuf);
}

int MsgFunc_TeamInfo(const char *pszName, int iSize, void *pbuf)
{
	BEGIN_READ(pbuf,iSize);
	int px = READ_BYTE();
	char *teamtext = READ_STRING();

	if(!*teamtext)
		vPlayer[px].team = 0;
	else
	{
		if(strstr(teamtext,"CT"))
		{
			vPlayer[px].team = 1;
		}
		else if(strstr(teamtext,"TERRORIST"))
		{
			vPlayer[px].team = 2;
		}
		else
		{
			vPlayer[px].team = 0;
		}
	}
	return pmTeamInfo(pszName, iSize, pbuf);
}
int MsgFunc_SetFOV(const char *pszName, int iSize, void *pbuf)
{
	BEGIN_READ(pbuf, iSize);

	gHUD.m_iFOV = (float) READ_BYTE();

	if( gHUD.m_iFOV == 0 )
		gHUD.m_iFOV = 90;

	if( gHUD.m_iFOV <= 55.0)
	{ 
		g_iViewEntityRenderMode = 0;
		g_iViewEntityRenderFX = 17;
		g_iViewEntityRenderAmout = 100;
	}
	else
	{
		g_iViewEntityRenderMode = 0;
		g_iViewEntityRenderFX = 0;
		g_iViewEntityRenderAmout = 0;

		g_pWeapon->ScopeUp( false );
	}
	return pmSetFOV(pszName, iSize, pbuf);
}

int MsgFunc_VGUIMenu(const char *pszName, int iSize, void *pbuf)
{
	BEGIN_READ(pbuf, iSize);
	int iMenuType = READ_BYTE();

	if(iMenuType == MENU_TEAM)
	{
		ShowTeamMenu();
		return 0;
	}
	else if( iMenuType == MENU_CLASS_T)
	{
		g_MGuiClassMenu.Show( 1 );
		return 0;
	}
	else if( iMenuType == MENU_CLASS_CT)
	{
		g_MGuiClassMenu.Show( 2 );
		return 0;
	}


	return pmVGUIMenu(pszName, iSize, pbuf);
}
int MsgFunc_ShowMenu(const char *pszName, int iSize, void *pbuf)
{
	return gHUD.m_Menu.MsgFunc_ShowMenu ( pszName, iSize, pbuf);
}

int MsgFunc_MetaHook(const char *pszName, int iSize, void *pbuf)
{
	BEGIN_READ(pbuf, iSize);

	int iFuncType = READ_BYTE();

	switch(iFuncType)
	{
	case MH_MSG_SCOPE:
		{
			int iWpnId = READ_BYTE();
			if( !iWpnId )
				g_pWeapon->ScopeUp( false );
			else
				g_pWeapon->ScopeUp( true );
			break;
		}
	case MH_MSG_CUR_WPN:
		{
			int iWpnid_new	= READ_BYTE();
			int iFire = READ_BYTE();

			g_pWeapon->SetCurrentWpn( iWpnid_new );

			if( iFire == 2 )
				g_pWeapon->SetCrosshairStatus( true );
			
			else if( iFire == 0)
			{
				if( g_WpnItemInfo[ iWpnid_new ].iCrosshair == 0 )
					g_pWeapon->SetCrosshairStatus( true );

				else 
					g_pWeapon->SetCrosshairStatus( false );
			}

			static int iWpnid_old = 0;
			if(iWpnid_old != iWpnid_new )
			{
				iWpnid_old = iWpnid_new;
				g_pSwitchWpnFx->AddFxToWpn(iWpnid_new);
			}
			break;
		}
	case MH_MSG_VIEWENT:
		{
			g_iViewEntityBody = READ_BYTE();

			break;
		}
	case MH_MSG_MAXWPN_NUM:
		{
			//int iMaxWpn = READ_BYTE();
			//g_pBag->SortOutWpn( iMaxWpn);
			break;
		}
	case MH_MSG_SET_MAXHEALTH:
		{
			int iMaxHealth = READ_LONG();
			int iMaxArmor = READ_LONG();

			if( iMaxHealth )
				gHUD.m_Health.SetMaxHealth( iMaxHealth );

			if( iMaxArmor )
				gHUD.m_Health.SetMaxArmor ( iMaxArmor);

			if( g_bAlive)
				gHUD.m_Health.Show();

			break;
		}
	case MH_MSG_DRAW_TGA:
		{
			/*
			DrawTgaItem rgTempTga;
			strcpy(rgTempTga.szName, READ_STRING());
			rgTempTga.iFullScreen = READ_BYTE();
			rgTempTga.iCenter = READ_BYTE();
			rgTempTga.r = READ_BYTE();
			rgTempTga.g = READ_BYTE();
			rgTempTga.b = READ_BYTE();
			rgTempTga.x = READ_COORD() / 100.0f * g_sScreenInfo.iWidth;
			rgTempTga.y = READ_COORD() / 100.0f * g_sScreenInfo.iHeight;
			rgTempTga.iMode = READ_BYTE();

			rgTempTga.iChanne = READ_SHORT();
			if(rgTempTga.iChanne < MAX_TGA - DRAWTGA_RETAIN_CHANNEL && rgTempTga.iChanne >= 0)
				rgTempTga.iChanne += DRAWTGA_RETAIN_CHANNEL; // 为了防止插件占用的频道与模块占用的频道混在一起
			else
				rgTempTga.iChanne = -1; // 当赋值为负值时 会选用随机通道

			rgTempTga.flStartDisplayTime = g_flTime;
			rgTempTga.flFadeinTime = READ_COORD();
			rgTempTga.flFadeoutTime  = READ_COORD();
			rgTempTga.flFlashTime = READ_COORD() / 10.0f;
			rgTempTga.flFadeIntervalTime = READ_COORD() / 10.0f;

			rgTempTga.flEndDisplayTime = READ_COORD();
			if(rgTempTga.flEndDisplayTime > 0 && rgTempTga.flEndDisplayTime < rgTempTga.flFadeinTime + rgTempTga.flFadeoutTime)
				rgTempTga.flEndDisplayTime = rgTempTga.flFadeinTime + rgTempTga.flFadeoutTime;
			if(rgTempTga.flEndDisplayTime >= 0) //如果小于0,则使图像一直显示
				rgTempTga.flEndDisplayTime = rgTempTga.flEndDisplayTime / 10.0f + g_flTime;

			rgTempTga.flFadeinTime = rgTempTga.flFadeinTime / 10.0f;
			rgTempTga.flFadeoutTime = rgTempTga.flFadeoutTime / 10.0f;
			DrawTgaAdd(rgTempTga);
			*/
			break;
		}
#if 0
	case MH_MSG_REMOVE_IMAGE:
		{
			int iChannel = READ_SHORT();
			int iType = READ_BYTE();
			switch(iType)
			{
//			case DRAW_TGA:
//				ClearTgaImage(iChannel + DRAWTGA_RETAIN_CHANNEL);
//				break;
//			case DRAW_ICON:
//				ClearFollowIcon(iChannel + DRAWICON_RETAIN_CHANNEL);
//				break;
//			case DRAW_SPR:
//				ClearSpr(iChannel + DRAWSPR_RETAIN_CHANNEL);
//				break;
//			case PLAY_BINK:
//				ClearBink(iChannel + PLAYBINK_RETAIN_CHANNEL);
//				break;
//			case DRAW_BAR:
//				ClearBar(iChannel + DRAWBAR_RETAIN_CHANNEL);
//				break;
//			case DRAW_TEXT:
//				ClearFontText( iChannel );
//				break;
			}
			break;
		}
#endif
	case MH_MSG_SET_GAMEMODE:
		{
			g_iGameMode = READ_BYTE() ;
			gHUD.SetGameMode( g_iGameMode );
			break;
		}
	case MH_MSG_DRAW_BAR:
		{
/*			DrawBarItem rgTempBar;
			sprintf(rgTempBar.szName_BG, "%s", READ_STRING());
			sprintf(rgTempBar.szName_Bar, "%s", READ_STRING());
			rgTempBar.x_BG = READ_COORD() / 100.0f * g_sScreenInfo.iWidth;
			rgTempBar.y_BG = READ_COORD() / 100.0f * g_sScreenInfo.iHeight;
			rgTempBar.x_Bar = READ_COORD() / 100.0f * g_sScreenInfo.iWidth;
			rgTempBar.y_Bar = READ_COORD() / 100.0f * g_sScreenInfo.iHeight;
			rgTempBar.iCenter = READ_BYTE();
			rgTempBar.iLength = READ_SHORT();
			rgTempBar.flStartDisplayTime = g_flTime;
			rgTempBar.flEndDisplayTime = rgTempBar.flStartDisplayTime + READ_COORD() / 10.0f;
			rgTempBar.iChannel = READ_SHORT();
			if(rgTempBar.iChannel > 0 && rgTempBar.iChannel < MAX_BAR - DRAWBAR_RETAIN_CHANNEL)
				rgTempBar.iChannel += DRAWBAR_RETAIN_CHANNEL;
			DrawBarAdd(rgTempBar);
*/
			break;
		}
	case MH_MSG_PLAY_BINK:
		{
/*			PlayBinkItem rgTempBink;
			sprintf(rgTempBink.szName, "metahook\\bik\\%s.bik", READ_STRING());
			rgTempBink.iX = READ_COORD() / 100.0f * g_sScreenInfo.iWidth;
			rgTempBink.iY = READ_COORD() / 100.0f * g_sScreenInfo.iWidth;
			rgTempBink.flPlayRate = READ_COORD()  / 10.0f;
			rgTempBink.flStayTime = READ_COORD()  / 10.0f;
			rgTempBink.flFadeoutTime = READ_COORD()  / 10.0f;
			rgTempBink.iCenter = READ_BYTE();
			rgTempBink.iLoop = READ_BYTE();
			rgTempBink.iFullScreen = READ_BYTE();
			rgTempBink.r = READ_SHORT();
			rgTempBink.g = READ_SHORT();
			rgTempBink.b = READ_SHORT();
			rgTempBink.iChannel = READ_SHORT();
			if(rgTempBink.iChannel < MAX_BINK_CHANNEL - PLAYBINK_RETAIN_CHANNEL && rgTempBink.iChannel >= 0)
				rgTempBink.iChannel += PLAYBINK_RETAIN_CHANNEL; // 为了防止插件占用的频道与模块占用的频道混在一起
			else
				rgTempBink.iChannel = -1; // 当赋值为负值时 会选用随机通道
			PlayBinkAdd(rgTempBink);*/
			break;
		}
	case MH_MSG_DRAW_FOLLOWICON:
		{
			/*
			DrawIconItem rgTemp;
			rgTemp.iType = READ_BYTE();
			char szBuffer[64];
			sprintf(szBuffer, "%s", READ_STRING());
			if(rgTemp.iType == DRAW_TGA)
			{
				sprintf(rgTemp.szTgaName, "%s", szBuffer);
			}
			else if(rgTemp.iType == DRAW_SPR)
			{
				rgTemp.hSprites = gEngfuncs.pfnSPR_Load(szBuffer);
			}
			rgTemp.iSprType = READ_BYTE();
			rgTemp.iIndex = READ_SHORT();
			rgTemp.x = READ_SHORT();
			rgTemp.y = READ_SHORT();
			rgTemp.z = READ_SHORT();
			rgTemp.iDistance = READ_BYTE();
			rgTemp.iChannel = READ_SHORT();
			if(rgTemp.iChannel >= 0 && rgTemp.iChannel < DRAWICON_MAX - DRAWICON_RETAIN_CHANNEL)
				rgTemp.iChannel += DRAWICON_RETAIN_CHANNEL; // 防止插件占用的通道与本模块所占用通道混乱.
			DrawIconAdd(rgTemp);
*/
			break;
		}
	case MH_MSG_DRAW_TEXT :
		{
			/*
			DrawFontTextItem rgTempDrawText;
			char szBuf[200];
			sprintf(rgTempDrawText.szText,"%s", READ_STRING());
			rgTempDrawText.iCenter = READ_BYTE();
			rgTempDrawText.x = READ_COORD() / 100 * g_sScreenInfo.iWidth;
			rgTempDrawText.y = READ_COORD() / 100 * g_sScreenInfo.iHeight;
			rgTempDrawText.color.r = READ_BYTE();
			rgTempDrawText.color.g = READ_BYTE();
			rgTempDrawText.color.b = READ_BYTE();
			rgTempDrawText.iScale = READ_BYTE();
			rgTempDrawText.flDisplayTime = READ_COORD() / 10 + g_flTime;
			rgTempDrawText.flStartTime = g_flTime;
			rgTempDrawText.fFadeTime = READ_COORD() / 10 ;
			rgTempDrawText.iChanne = READ_BYTE();
			rgTempDrawText.flDisplayTime += rgTempDrawText.fFadeTime*2;
			DrawFontTextAdd(rgTempDrawText);
*/
			break;
		}
	case MH_MSG_VIEWMDL_RENDER:
		{
			g_iViewEntityRenderMode = READ_BYTE();
			g_iViewEntityRenderFX = READ_BYTE();
			g_iViewEntityRenderAmout = READ_BYTE();
			g_byViewEntityRenderColor.r = READ_BYTE();
			g_byViewEntityRenderColor.g = READ_BYTE();
			g_byViewEntityRenderColor.b = READ_BYTE();

			break;
		}
	case MH_MSG_DRAW_SPR:
		{
			/*
			DrawSprItem rgTempSpr;
			char szSpr[128];
			sprintf(szSpr, "%s", READ_STRING());
			rgTempSpr.hSprites = gEngfuncs.pfnSPR_Load(szSpr);
			rgTempSpr.r = READ_SHORT();
			rgTempSpr.g = READ_SHORT();
			rgTempSpr.b = READ_SHORT();
			rgTempSpr.iDrawMode = READ_BYTE();
			rgTempSpr.x = READ_COORD() / 100.0f * g_sScreenInfo.iWidth;
			rgTempSpr.y = READ_COORD() / 100.0f * g_sScreenInfo.iHeight;
			rgTempSpr.iCenter = READ_BYTE();
			rgTempSpr.iEffect = READ_BYTE();

			rgTempSpr.flStartDisplayTime = g_flTime;
			rgTempSpr.flFadeinTime = READ_COORD();
			rgTempSpr.flFadeoutTime  = READ_COORD();
			rgTempSpr.flFlashTime = READ_COORD() / 10.0f;
			rgTempSpr.flFadeIntervalTime = READ_COORD() / 10.0f;

			rgTempSpr.flEndDisplayTime = READ_COORD();
			if(rgTempSpr.flEndDisplayTime > 0 && rgTempSpr.flEndDisplayTime < rgTempSpr.flFadeinTime + rgTempSpr.flFadeoutTime)
				rgTempSpr.flEndDisplayTime = rgTempSpr.flFadeinTime + rgTempSpr.flFadeoutTime;
			if(rgTempSpr.flEndDisplayTime >= 0) //如果小于0,则使图像一直显示
				rgTempSpr.flEndDisplayTime = rgTempSpr.flEndDisplayTime / 10.0f + g_flTime;

			rgTempSpr.flFadeinTime = rgTempSpr.flFadeinTime / 10.0f;
			rgTempSpr.flFadeoutTime = rgTempSpr.flFadeoutTime / 10.0f;
			rgTempSpr.iChannel = READ_SHORT();
			if(rgTempSpr.iChannel < MAX_DRAWSPR - DRAWSPR_RETAIN_CHANNEL && rgTempSpr.iChannel >= 0)
				rgTempSpr.iChannel += DRAWSPR_RETAIN_CHANNEL; // 为了防止插件占用的频道与模块占用的频道混在一起
			else
				rgTempSpr.iChannel = -1; // 当赋值为负值时 会选用随机通道
			DrawSprAdd(rgTempSpr);
*/
			break;
		}
	case MH_MSG_UPDATE_SUV_KILL:
		{
			int iValue = READ_LONG();
			gHUD.m_SuvModeUi.UpdateClientDataKills( iValue );
			break;
		}
	case MH_MSG_UPDATE_SUV_ROUND:
		{
			int iValue = READ_BYTE();
			gHUD.m_SuvModeUi.UpdateClientDataRounds( iValue );
			break;
		}
	case MH_MSG_UPDATE_TIME:
		{
			gHUD.m_Timer.SetRoundTime( READ_LONG() );
			break;
		}
	case MH_MSG_SET_COUNTDOWN:
		{
			gHUD.m_SuvModeUi.SetCountDown( READ_BYTE() );
			break;
		}
	case MH_MSG_SET_SUV_ROUNDEND:
		{
			int iWin = READ_BYTE();
			if( iWin )
				gHUD.m_SuvModeUi.SetRoundEnd( true );
			else
				gHUD.m_SuvModeUi.SetRoundEnd( false );

			break;
		}
	case MH_MSG_NEW_ROUND:
		{
			gHUD.m_SuvModeUi.Reset();
			break;
		}
	case MH_MSG_SET_PLAYER_WPNID:
		{
			int idx = READ_BYTE();
			int iWpnId = READ_BYTE();
			g_pWeapon->SetPlayerWpnId (idx, iWpnId);
			break;
		}

	}

	return 1;
}

int MsgFunc_Health(const char *pszName, int iSize, void *pbuf)
{	
	gHUD.MsgFunc_Health( pszName, iSize , pbuf );
	return pmHealth( pszName, iSize, pbuf );
}

int MsgFunc_ResetHUD(const char *pszName, int iSize, void *pbuf)
{
	return pmResetHUD(pszName, iSize, pbuf);
}
int MsgFunc_Damage(const char *pszName, int iSize, void *pbuf)
{
	gHUD.MsgFunc_Damage( pszName, iSize , pbuf  );
	return pmDamage( pszName, iSize, pbuf );
}
int MsgFunc_NVGToggle(const char *pszName, int iSize, void *pbuf)
{
	BEGIN_READ(pbuf, iSize);
	int iToggle = READ_BYTE();

	if( iToggle == 1 )
	{
		gHUD.m_Nvg .Show ();
	}
	else if( iToggle == 0)
	{
		gHUD.m_Nvg .Hide ();
	}

	return 0;
}

int MsgFunc_SayText(const char *pszName, int iSize, void *pbuf)
{
	return gHUD.m_SayText.MsgFunc_SayText( pszName, iSize , pbuf );
}
int MsgFunc_TextMsg(const char *pszName, int iSize, void *pbuf)
{
	return gHUD.m_TextMessage.MsgFunc_TextMsg( pszName, iSize , pbuf );
}











// -------------------  Hook Messages --------------------------

int EngFunc_HookUserMsg(char *szMsgName, pfnUserMsgHook pfn)
{
	if (!strcmp(szMsgName, "DeathMsg"))
	{
		pmDeathMsg = pfn;
		gEngfuncs.pfnHookUserMsg(szMsgName, MsgFunc_DeathMsg);
		return 0;
	}
	else if (!strcmp(szMsgName, "ScoreAttrib"))
	{
		pmScoreAttrib = pfn;
		return gEngfuncs.pfnHookUserMsg(szMsgName, MsgFunc_ScoreAttrib);
	}
	else if (!strcmp(szMsgName, "TeamInfo"))
	{
		pmTeamInfo = pfn;
		gEngfuncs.pfnHookUserMsg(szMsgName,MsgFunc_TeamInfo);
		return 0;
	}
	else if (!strcmp(szMsgName, "SetFOV"))
	{
		pmSetFOV = pfn;
		return gEngfuncs.pfnHookUserMsg(szMsgName, MsgFunc_SetFOV);
	}
	else if (!strcmp(szMsgName, "VGUIMenu"))
	{
		pmVGUIMenu = pfn;
		return gEngfuncs.pfnHookUserMsg(szMsgName, MsgFunc_VGUIMenu);
	}
	else if (!strcmp(szMsgName, "ScoreAttrib"))
	{
		pmScoreAttrib = pfn;
		gEngfuncs.pfnHookUserMsg(szMsgName,MsgFunc_ScoreAttrib);
		return 0;
	}
	else if (!strcmp(szMsgName, "ScoreInfo"))
	{
		pmScoreInfo = pfn;
		gEngfuncs.pfnHookUserMsg(szMsgName, MsgFunc_ScoreInfo);
		return 0;
	}
	else if (!strcmp(szMsgName, "ShowMenu"))
	{
		pmShowMenu = pfn;
		return gEngfuncs.pfnHookUserMsg(szMsgName, MsgFunc_ShowMenu);
	}
	else if( !strcmp(szMsgName, "SayText"))
	{
		pmSayText = pfn;
		return gEngfuncs.pfnHookUserMsg(szMsgName, MsgFunc_SayText);
	}
	else if( !strcmp(szMsgName, "TextMsg"))
	{
		pmTextMsg = pfn;
		return gEngfuncs.pfnHookUserMsg(szMsgName, MsgFunc_TextMsg);
	}
	else if (!strcmp(szMsgName, "Health"))
	{
		pmHealth = pfn;
		return gEngfuncs.pfnHookUserMsg(szMsgName, MsgFunc_Health);
	}
	else if( !strcmp(szMsgName, "ResetHUD"))
	{
		pmResetHUD = pfn;
		return gEngfuncs.pfnHookUserMsg( szMsgName, MsgFunc_ResetHUD );
	}
	else if( !strcmp(szMsgName, "Damage"))
	{
		pmDamage = pfn;
		return gEngfuncs.pfnHookUserMsg( szMsgName, MsgFunc_Damage );
	}
	else if( !strcmp(szMsgName, "NVGToggle") )
	{
		pmNVGToggle = pfn;
		return gEngfuncs.pfnHookUserMsg (szMsgName, MsgFunc_NVGToggle);
	}

	return gEngfuncs.pfnHookUserMsg(szMsgName, pfn);
}