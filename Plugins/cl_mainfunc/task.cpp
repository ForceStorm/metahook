#include "base.h"
#include "task.h"

CTask gTask;

CTask::CTask(void)
{
	m_pTask = NULL;
}

void CTask::SetTask( float flTaskTime, void (*pfnTask)(int iTaskid), int iTaskid)
{
	DeleteTask( iTaskid );

	if( !m_pTask )
	{
		m_pTask = new CTaskInfo;
		m_pTask->iTaskid = iTaskid;
		m_pTask->pfnTask = pfnTask;
		m_pTask->pNext = NULL;
		m_pTask->flEndTime = g_flTime + flTaskTime;
	}
	else
	{
		CTaskInfo *pointer = m_pTask;
		while( pointer->pNext )
			pointer = pointer->pNext ;
		pointer->pNext = new CTaskInfo;
		pointer->pNext->iTaskid = iTaskid;
		pointer->pNext->pfnTask = pfnTask;
		pointer->pNext->pNext = NULL;
		pointer->pNext->flEndTime = g_flTime + flTaskTime;
	}
}

void CTask::DeleteTask(int iTaskid) // don't use it in other place
{
	if(!m_pTask)
		return;

	CTaskInfo *pointer;
	pointer = m_pTask;
	if(pointer->iTaskid == iTaskid)
	{
		m_pTask = m_pTask->pNext ;
		delete pointer;
		return;
	}
	while( pointer->pNext && pointer->pNext->iTaskid != iTaskid)
	{
		pointer = pointer->pNext ;
	}

	if( !pointer->pNext )
		return;

	CTaskInfo *pointer2 = pointer->pNext->pNext ;
	delete pointer->pNext ;
	pointer->pNext = pointer2;
}

void CTask::Count(void)
{
	if(!m_pTask)
		return;

	CTaskInfo *pointer;
	pointer = m_pTask;
	
	do
	{
		CTaskInfo *pointer_temp = pointer->pNext ;
		if(pointer->flEndTime <= g_flTime)
		{
			pointer->pfnTask( pointer->iTaskid  );
			DeleteTask( pointer->iTaskid  );
		}
		pointer = pointer_temp;
	}
	while(pointer );
}