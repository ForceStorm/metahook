#ifndef _WEAPON_H
#define _WEAPON_H

#define MAX_WPN 100

enum DataType_e
{
	DATA_TYPE_CLASSNAME = 1,
	DATA_TYPE_SLOT,
	DATA_TYPE_SCOPEIMAGE,
	DATA_TYPE_SCOPETYPE,
	DATA_TYPE_PMODEL,
	DATA_TYPE_WMODEL,
	DATA_TYPE_PMODEL_SEQ,
	DATA_TYPE_WMODEL_SEQ,
	DATA_TYPE_CLIP,
	DATA_TYPE_BPAMMO,
	DATA_TYPE_WEIGHT,
	DATA_TYPE_CANDROP,
	DATA_TYPE_CROSSHAIR,
	DATA_TYPE_CROSSHAIR_GAP,
	DATA_TYPE_SPR_SMOKE1,
	DATA_TYPE_SPR_SMOKE2,
	DATA_TYPE_SHELL_MODEL,
	DATA_TYPE_ANIM_SHOT_START,
	DATA_TYPE_ANIM_SHOT_END,
	DATA_TYPE_ANIM_SHOT_EMPTY,
	DATA_TYPE_ANIM_END,
	DATA_TYPE_BULLET_TYPE2,
	DATA_TYPE_BULLET_DISTANCE2,
	DATA_TYPE_BULLET_PENETRATION2,
	DATA_TYPE_BULLET_TYPE,
	DATA_TYPE_BULLET_DISTANCE,
	DATA_TYPE_BULLET_PENETRATION,
	DATA_TYPE_SHOT_SOUND1,
	DATA_TYPE_SHOT_SOUND2,
	DATA_TYPE_END
};
enum ScopeType_e
{
	SCOPE_AUG = 1,
	SCOPE_SNIPER,
	SCOPE_SIGHT,
	SCOPE_ACTION,
	SCOPE_END
};
typedef struct WpnItemInfo_s
{
	struct model_s *ViewModel;
	int iSlot;
	char *szWpnName;
	char *szClassname;
	int iWpnTgaId;
	int iScopeImageTgaId;
	int iScopeType;
	char *szSprSmoke1;
	struct model_s * mdlSprSmoke1;

	int iCrosshair;
	int iCrosshairGap;

	// for events.cpp
	char *szShellModel;
	int iAnimShotStart;
	int iAnimShotEmpty;
	int iAnimShotEnd;
	int iAnimEnd;
	//float fShellVecScale[3];
	short iBulletType;
	int iBulletDistance;
	short iBulletPenetration;
	short iBulletType2;
	int iBulletDistance2;
	short iBulletPenetration2;

	float flShotSoundVolume;
	char *szShotSound1;
	char *szShotSound2;
} WpnItemInfo_t;

extern WpnItemInfo_t g_WpnItemInfo[MAX_WPN];


#define CROSSHAIR_RECOVER_TIME		0.4
#define CROSSHAIR_ENLARGE_AMT		50
#define CROSSHAIR_LENGTH			18

class CWeapon
{
public:
	CWeapon( void );
	~CWeapon( void );

public:
	void VidInit(void);
	void Draw( void );
	void SelectWpnItem( int iSlot );
	void GunFire( void );
	void SetCrosshairStatus( bool bHide );
	void ScopeUp( bool bScope );
	void SetCurrentWpn( int iWpnId );
	int GetCurrentWpn( void);
	int GetFov( void );
	void SetFov( int iFov );
	bool IsInScope(void);
	int GetPlayerWpnId(int, int iSlot);
	void SetPlayerWpnId(int idx, int iWpnId);

	int m_iClip;
	int m_iBpammo;

private:
	void ReadWpnItemInfo(void);
	void DrawScopeImage(void);
	void WpnImageInit(void);
	void DrawCrosshair( void );
	void LoadResources(void);
	void DestroyResources(void);

private:
	int m_iCurrentWpnid;
	float m_flGunFireTime;
	bool m_bHideCrosshair;
	bool m_bScopeUp;
	int m_iTgaId_Panel;
	int m_iPlayerWpnId[33][6];
};

extern CWeapon *g_pWeapon;

#endif