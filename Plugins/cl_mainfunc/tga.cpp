#include "base.h"
#include "hud.h"
#include "plugins.h"
#include "effects.h"
#include "tga.h"
#include "imagelib.h"


CTGA gTGA;
extern float mainViewAngles[3];


CTGA::CTGA ()
{
	m_vTga3DItem = NULL;
	m_vImageData = NULL;
}

int CTGA::GetTextureId (char *filename)
{
	if(m_vImageData == NULL)
		m_vImageData = CreateVector(sizeof(imagedata_t));

	size_t iSize = VectorSize(m_vImageData);
	imagedata_t *data = NULL;

	for(size_t i = 0; i < iSize; i++)
	{
		data = *(imagedata_t **)VectorGet(m_vImageData, i);
		if(data && !strcmp(filename, data->name ))
			return data->texid ;
	}
	
	imagedata_t *newdata = new imagedata_t;
	int id = g_pSurface->CreateNewTextureID();
	if(Image_LoadImage( filename, newdata, &id))
	{
		sprintf(newdata->name, "%s", filename);
		VectorPush(m_vImageData, &newdata);
		return newdata->texid ;
	}
	else
		delete newdata;

	return 0;
}

imagedata_t* CTGA::GetImageData(char *filename)
{
	GetTextureId(filename); // prevent the case that the image havent been cached
	size_t iSize = VectorSize(m_vImageData);
	imagedata_t *data = NULL;

	for(size_t i = 0; i < iSize; i++)
	{
		data = *(imagedata_t **)VectorGet(m_vImageData, i);
		if(!strcmp(filename, data->name ))
			return data;
	}

	return NULL;
}

imagedata_t* CTGA::GetImageData(int itexid)
{
	size_t iSize = VectorSize(m_vImageData);
	imagedata_t *data = NULL;

	for(size_t i = 0; i < iSize; i++)
	{
		data = *(imagedata_t **)VectorGet(m_vImageData, i);
		if(data->texid == itexid)
			return data;
	}

	return NULL;
}

void CTGA::Draw( int iTextureId, int iX, int iY, int iW , int iH)
{
	if(!iTextureId)
		return;

	Tri_Enable(GL_TEXTURE_2D);
	Tri_Enable(GL_BLEND);
	Tri_BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	

	Tri_BindTexture(GL_TEXTURE_2D, iTextureId);

	if(g_iVideoMode)
		glColor4ub(255,255,255,255);
	else
		gEngfuncs.pTriAPI->Color4ub(255,255,255,255);

	gEngfuncs.pTriAPI->Begin(TRI_QUADS);
		
		gEngfuncs.pTriAPI->TexCoord2f(0,1);
		gEngfuncs.pTriAPI->Vertex3f(iX,iY+iH,0);

		gEngfuncs.pTriAPI->TexCoord2f(1,1);
		gEngfuncs.pTriAPI->Vertex3f(iX+iW,iY+iH,0);
			
		gEngfuncs.pTriAPI->TexCoord2f(1,0);
		gEngfuncs.pTriAPI->Vertex3f(iX+iW,iY,0);
			
		gEngfuncs.pTriAPI->TexCoord2f(0,0);
		gEngfuncs.pTriAPI->Vertex3f(iX,iY,0);

	gEngfuncs.pTriAPI->End();
}


void CTGA::Draw( int iTextureId, int iX, int iY, int iW, int iH,  int r, int g, int b , int alpha)
{
	if(!iTextureId)
		return;

	Tri_Enable(GL_TEXTURE_2D);
	Tri_Enable(GL_BLEND);
	Tri_BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 

	Tri_BindTexture(GL_TEXTURE_2D, iTextureId);

	if(g_iVideoMode)
		glColor4ub(r,g,b,alpha);
	else
		gEngfuncs.pTriAPI->Color4ub(r,g,b,alpha);

	gEngfuncs.pTriAPI->Begin(TRI_QUADS);
		
		gEngfuncs.pTriAPI->TexCoord2f(0,1);
		gEngfuncs.pTriAPI->Vertex3f(iX,iY+iH,0);

		gEngfuncs.pTriAPI->TexCoord2f(1,1);
		gEngfuncs.pTriAPI->Vertex3f(iX+iW,iY+iH,0);
			
		gEngfuncs.pTriAPI->TexCoord2f(1,0);
		gEngfuncs.pTriAPI->Vertex3f(iX+iW,iY,0);
			
		gEngfuncs.pTriAPI->TexCoord2f(0,0);
		gEngfuncs.pTriAPI->Vertex3f(iX,iY,0);

	gEngfuncs.pTriAPI->End();
}

void CTGA:: HUD_DrawTransparentTriangles(void)
{
	if(!m_vTga3DItem)
		return;

	size_t iSize = VectorSize(m_vTga3DItem);
	Tga3DItem_t *p = NULL;

	// delete the invalid ones
	for(size_t i = 0; i < iSize; i++)
	{
		p = *(Tga3DItem_t **)VectorGet(m_vTga3DItem, i);
		if(!p)
			continue;

		if(gHUD.m_flTime - p->flStartTime   >=   p->flFadeInTime + p->flFadeOutTime + p->flHoldTime)
		{
			VectorEarse(m_vTga3DItem, i, i+1);
			iSize = VectorSize(m_vTga3DItem);
			--i;
			if(p->privateData )
				delete p->privateData ;

			delete p;
		}

		p = NULL;
	}

	for(size_t i = 0; i < iSize; i++)
	{
		p = *(Tga3DItem_t **)VectorGet(m_vTga3DItem, i);
		if(!p)
			continue;

		if(p->privateData )
		{
			CBaseParticle *particle = (CBaseParticle *)p->privateData;
			particle->OnThink ();
		}

		int alpha = 255;
		
		if(p->flFadeInTime &&(gHUD.m_flTime - p->flStartTime < p->flFadeInTime))
			alpha = p->iAlpha * (gHUD.m_flTime - p->flStartTime) / p->flFadeInTime;

		else if(p->flHoldTime + p->flFadeInTime >= gHUD.m_flTime - p->flStartTime >= p->flFadeInTime)
			alpha = p->iAlpha;

		else if( p->flFadeOutTime &&( p->flFadeInTime + p->flFadeOutTime + p->flHoldTime >= gHUD.m_flTime - p->flStartTime >= p->flHoldTime + p->flFadeInTime))
			alpha = p->iAlpha * (p->flFadeInTime + p->flFadeOutTime + p->flHoldTime + p->flStartTime - gHUD.m_flTime)/ p->flFadeOutTime;
		
		Tri_Enable(GL_TEXTURE_2D);
		Tri_TexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		Tri_BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
		Tri_Enable(GL_BLEND);
		glDepthMask(0);

		Tri_BindTexture(GL_TEXTURE_2D, p->imagedata->texid );

		if(g_iVideoMode)
			glColor4ub(255,255,255,alpha);
		else
			gEngfuncs.pTriAPI->Color4ub(255,255,255,alpha);

		vec3_t vecPoint[4];
		if(p->bForward)
		{
			if(!p->iMerger)
			{
				vec3_t vUp, vForward, vRight;
				gEngfuncs.pfnAngleVectors (mainViewAngles, vForward, vRight, vUp);
				VectorNormalize(vUp);
				VectorNormalize(vRight);
				VectorScale(vUp, (p->imagedata->height / 2.0) * p->scale, vUp);
				VectorScale(vRight, (p->imagedata->width / 2.0) * p->scale, vRight);

				VectorAdd(p->vecOrigin, vUp, vecPoint[0]);
				VectorSubtract(vecPoint[0], vRight, vecPoint[0]);

				VectorAdd(p->vecOrigin, vUp, vecPoint[0]);
				VectorAdd(vecPoint[0], vRight, vecPoint[0]);

				VectorSubtract(p->vecOrigin, vUp, vecPoint[0]);
				VectorAdd(vecPoint[0], vRight, vecPoint[0]);

				VectorSubtract(p->vecOrigin, vUp, vecPoint[0]);
				VectorSubtract(vecPoint[0], vRight, vecPoint[0]);

				gEngfuncs.pTriAPI->Begin(TRI_QUADS);
			
					gEngfuncs.pTriAPI->TexCoord2f(0,0);
					gEngfuncs.pTriAPI->Vertex3f(vecPoint[0][0] ,vecPoint[0][1] , vecPoint[0][2]);

					gEngfuncs.pTriAPI->TexCoord2f(1,0);
					gEngfuncs.pTriAPI->Vertex3f(vecPoint[1][0] ,vecPoint[1][1] , vecPoint[1][2]);
				
					gEngfuncs.pTriAPI->TexCoord2f(1,1);
					gEngfuncs.pTriAPI->Vertex3f(vecPoint[2][0] ,vecPoint[2][1] , vecPoint[2][2]);
				
					gEngfuncs.pTriAPI->TexCoord2f(0,1);
					gEngfuncs.pTriAPI->Vertex3f(vecPoint[3][0] ,vecPoint[3][1] , vecPoint[3][2]);

				gEngfuncs.pTriAPI->End();
			}

			else
			{
				vec3_t vUp, vForward, vRight;
				gEngfuncs.pfnAngleVectors (mainViewAngles, vForward, vRight, vUp);
				VectorNormalize(vUp);
				VectorNormalize(vRight);
				VectorScale(vUp, (p->imagedata->height/ float(p->height_amt) / 2.0) * p->scale, vUp);
				VectorScale(vRight, (p->imagedata->width/ float(p->wide_amt) / 2.0) * p->scale, vRight);

				VectorAdd(p->vecOrigin, vUp, vecPoint[0]);
				VectorSubtract(vecPoint[0], vRight, vecPoint[0]);

				VectorAdd(p->vecOrigin, vUp, vecPoint[0]);
				VectorAdd(vecPoint[0], vRight, vecPoint[0]);

				VectorSubtract(p->vecOrigin, vUp, vecPoint[0]);
				VectorAdd(vecPoint[0], vRight, vecPoint[0]);

				VectorSubtract(p->vecOrigin, vUp, vecPoint[0]);
				VectorSubtract(vecPoint[0], vRight, vecPoint[0]);

				float flw = 1.0 / float( p->wide_amt);
				float flh = 1.0 / float(p->height_amt);

				int line, item;
				line = (p->iFrame + 1) / p->wide_amt ;
				item = (p->iFrame + 1) % p->wide_amt ;
				if(item)
					line++;

				gEngfuncs.pTriAPI->Begin(TRI_QUADS);
			
					gEngfuncs.pTriAPI->TexCoord2f( (item-1)*flw, 1- line*flh);
					gEngfuncs.pTriAPI->Vertex3f(vecPoint[0][0] ,vecPoint[0][1] , vecPoint[0][2]);

					gEngfuncs.pTriAPI->TexCoord2f( item*flw, 1- line*flh);
					gEngfuncs.pTriAPI->Vertex3f(vecPoint[1][0] ,vecPoint[1][1] , vecPoint[1][2]);
				
					gEngfuncs.pTriAPI->TexCoord2f( item*flw, 1- line*flh + flh);
					gEngfuncs.pTriAPI->Vertex3f(vecPoint[2][0] ,vecPoint[2][1] , vecPoint[2][2]);
				
					gEngfuncs.pTriAPI->TexCoord2f( (item-1)*flw, 1- line*flh + flh);
					gEngfuncs.pTriAPI->Vertex3f(vecPoint[3][0] ,vecPoint[3][1] , vecPoint[3][2]);

				gEngfuncs.pTriAPI->End();

				if(gHUD.m_flTime > p->flNextFrameTime )
				{
					p->iFrame ++;
					p->flNextFrameTime = gHUD.m_flTime + p->flFrameRate ;
				}

				if(p->iFrame >= p->iFrameCount )
				{
					p->flFadeInTime = 0;
					p->flFadeOutTime =0;
					p->flHoldTime = 0;
				}
			}
		}
		else
		{
			VectorCopy( p->vecPoint[0], vecPoint[0]);
			VectorCopy( p->vecPoint[1], vecPoint[1]);
			VectorCopy( p->vecPoint[2], vecPoint[2]);
			VectorCopy( p->vecPoint[3], vecPoint[3]);

			gEngfuncs.pTriAPI->Begin(TRI_QUADS);
			
				gEngfuncs.pTriAPI->TexCoord2f(0,0);
				gEngfuncs.pTriAPI->Vertex3f(vecPoint[0][0] ,vecPoint[0][1] , vecPoint[0][2]);

				gEngfuncs.pTriAPI->TexCoord2f(1,0);
				gEngfuncs.pTriAPI->Vertex3f(vecPoint[1][0] ,vecPoint[1][1] , vecPoint[1][2]);
				
				gEngfuncs.pTriAPI->TexCoord2f(1,1);
				gEngfuncs.pTriAPI->Vertex3f(vecPoint[2][0] ,vecPoint[2][1] , vecPoint[2][2]);
				
				gEngfuncs.pTriAPI->TexCoord2f(0,1);
				gEngfuncs.pTriAPI->Vertex3f(vecPoint[3][0] ,vecPoint[3][1] , vecPoint[3][2]);

			gEngfuncs.pTriAPI->End();
		}

		
		p = NULL;

	}
}

Tga3DItem_t*  CTGA::AddTo3D (char *pName, int iAlpha, float flFadeInTime, float flHoldTime, float flFadeOutTime, vec3_t *point)
{
	if(m_vTga3DItem == NULL)
		m_vTga3DItem = CreateVector(sizeof(Tga3DItem_t));

	Tga3DItem_t *pTGA		= new Tga3DItem_t;
	pTGA->flFadeInTime		= flFadeInTime;
	pTGA->flHoldTime		= flHoldTime;
	pTGA->flFadeOutTime		= flFadeOutTime;
	pTGA->flStartTime		= gHUD.m_flTime ;
	pTGA->iAlpha			= iAlpha;
	pTGA->imagedata			= GetImageData(pName);
	pTGA->bForward			= false;
	pTGA->privateData		= NULL;

	VectorCopy(point[0], pTGA->vecPoint[0]);
	VectorCopy(point[1], pTGA->vecPoint[1]);
	VectorCopy(point[2], pTGA->vecPoint[2]);
	VectorCopy(point[3], pTGA->vecPoint[3]);
	
	VectorPush(m_vTga3DItem, &pTGA);
	return pTGA;
}

Tga3DItem_t*  CTGA::PlayTGA(char *pName, vec3_t vOrigin, float scale, float flFadeInTime, float flHoldTime, float flFadeOutTime, int iLoop, int iStartFrame )
{
	if(m_vTga3DItem == NULL)
		m_vTga3DItem = CreateVector(sizeof(Tga3DItem_t));

	Tga3DItem_t *pTGA		= new Tga3DItem_t;
	pTGA->flFadeInTime		= flFadeInTime;
	pTGA->flHoldTime		= flHoldTime;
	pTGA->flFadeOutTime		= flFadeOutTime;
	pTGA->flStartTime		= gHUD.m_flTime ;
	pTGA->iAlpha			= 255;
	pTGA->bForward			= true;
	pTGA->scale				= scale;
	pTGA->privateData		= NULL;
	VectorCopy(vOrigin, pTGA->vecOrigin);

	const char *szExt = FileExtension( pName );
	if(!strcmp(szExt,"tga"))
	{
		pTGA->imagedata	= GetImageData(pName);
	}
	else if(!strcmp(szExt,"anim"))
	{
		int iMerger = GetPrivateProfileInt( "TGA_Animation", "Merger", 0, pName);
		pTGA->iMerger = iMerger;

		if(iMerger)
		{
			char szImageAdr[256];
			GetPrivateProfileString( "TGA_Animation", "ImageFile","", szImageAdr, sizeof(szImageAdr), pName);
			pTGA->imagedata = GetImageData(szImageAdr);

			pTGA->wide_amt = GetPrivateProfileInt( "TGA_Animation", "WideAmount", 0, pName);
			pTGA->height_amt = GetPrivateProfileInt( "TGA_Animation", "HeightAmount", 0, pName);
			pTGA->iFrameCount = GetPrivateProfileInt( "TGA_Animation", "FrameCount", 0, pName);
			
			char szFrameRate[32];
			GetPrivateProfileString( "TGA_Animation", "FrameRate","", szFrameRate, sizeof(szFrameRate), pName);
			pTGA->flFrameRate = atof(szFrameRate);

			pTGA->iFrame = iStartFrame;
			pTGA->flNextFrameTime = gHUD.m_flTime + pTGA->flFrameRate;
		}
		else
		{
		}
	}

	
	VectorPush(m_vTga3DItem, &pTGA);
	return pTGA;
}

void CTGA::CreateDecal( pmtrace_t *pTrace, char *filename, float scale)
{
	imagedata_t *data = GetImageData(filename);
	vec3_t vPlayerUp, vPlayerRight, vPlayerForward;
	gEngfuncs.pfnAngleVectors(mainViewAngles, vPlayerForward, vPlayerRight, vPlayerUp);

	vec3_t vtmp ,vUp, vRight;
	if(DotProduct(vPlayerUp, pTrace->plane.normal)/ VectorLength(vPlayerUp) * VectorLength(pTrace->plane.normal) < 0.00001)
	{
		CrossProduct(vPlayerRight, pTrace->plane.normal, vUp);
		VectorNormalize(vUp);
		CrossProduct(vUp, pTrace->plane.normal , vRight);
		VectorNormalize(vRight);
	}
	else
	{
		CrossProduct(vPlayerUp, pTrace->plane.normal , vRight);
		VectorNormalize(vRight);
		CrossProduct(vRight, pTrace->plane.normal, vUp);
		VectorNormalize(vUp);
	}

	VectorScale(vUp, scale * data->height / 2.0, vUp);
	VectorScale(vRight, scale * data->width / 2.0, vRight);
	
	vec3_t vPoint[4], vNormal;
	VectorCopy( pTrace->plane.normal, vNormal);
	VectorNormalize(vNormal);
	VectorAdd(pTrace->endpos ,vNormal, vtmp);

	VectorAdd(vtmp, vUp, vPoint[0]);
	VectorSubtract(vPoint[0], vRight, vPoint[0]);

	VectorSubtract(vtmp, vUp, vPoint[1]);
	VectorSubtract(vPoint[1], vRight, vPoint[1]);

	VectorSubtract(vtmp, vUp, vPoint[2]);
	VectorAdd(vPoint[2], vRight, vPoint[2]);

	VectorAdd(vtmp, vUp, vPoint[3]);
	VectorAdd(vPoint[3], vRight, vPoint[3]);

	AddTo3D(filename, 255, 0.0, 4.0, 0.0, vPoint);

	VectorAdd(vtmp, vUp, vPoint[0]);
	VectorSubtract(vPoint[0], vRight, vPoint[0]);
	
	VectorAdd(vtmp, vUp, vPoint[1]);
	VectorAdd(vPoint[1], vRight, vPoint[1]);
	
	VectorSubtract(vtmp, vUp, vPoint[2]);
	VectorAdd(vPoint[2], vRight, vPoint[2]);

	VectorSubtract(vtmp, vUp, vPoint[3]);
	VectorSubtract(vPoint[3], vRight, vPoint[3]);

	AddTo3D(filename, 255, 0.0, 4.0, 0.0, vPoint);
}