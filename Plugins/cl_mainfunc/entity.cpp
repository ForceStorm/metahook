#include "base.h"
#include "entity.h"
#include "hud.h"
#include "DrawSBPanel.h"


cl_entity_t *cl_entities; 
int *cl_max_edicts; 


#define GETENTITYBYINDEX_MAX_EDICTS_SIG "\x3B\x05\x2A\x2A\x2A\x02\x7D"  
#define GETENTITYBYINDEX_CL_ENTITIES_SIG "\x8B\x15\x2A\x2A\x2A\x01\x8D\x04\x80"  




void Entity_Init(void)
{
	DWORD dwmaxedicts = (DWORD)g_pMetaHookAPI->SearchPattern((void *)g_pMetaSave->pEngineFuncs->GetEntityByIndex, 0x30, GETENTITYBYINDEX_MAX_EDICTS_SIG, sizeof(GETENTITYBYINDEX_MAX_EDICTS_SIG)-1);  
	if(!dwmaxedicts)  
		return;  

	cl_max_edicts = *(int **)(dwmaxedicts + 2);  
	DWORD dwclentities = (DWORD)g_pMetaHookAPI->SearchPattern((void *)g_pMetaSave->pEngineFuncs->GetEntityByIndex, 0x30, GETENTITYBYINDEX_CL_ENTITIES_SIG, sizeof(GETENTITYBYINDEX_CL_ENTITIES_SIG)-1);  

	if(!dwclentities)  
		return;

	cl_entities = *(cl_entity_t **)(dwclentities + 2); 
}




cl_entity_t *CL_FindEntityInSphere(float *org, float rad, int (*pfnFilter)(cl_entity_t *ent))  
{  
    cl_entity_t *ent;  
    float eorg;
    int i, j;  
    float distSquared;  
  
    rad *= rad;  
  
    int messagenum = gEngfuncs.GetLocalPlayer()->curstate.messagenum;  
    float msg_time = gEngfuncs.GetLocalPlayer()->curstate.msg_time;  
  
    for (i = 1; i < *cl_max_edicts; i++)  
    {  
        ent = &cl_entities[i];  
  
        if (ent->curstate.messagenum != messagenum || ent->curstate.msg_time - msg_time > 1.0f)  
            continue;  
  
        if(pfnFilter && !pfnFilter(ent))  
            continue;  
  
        distSquared = 0;  
  
        for (j = 0; j < 3; j++)  
        {  
            if (distSquared > rad)  
                break;  
  
            if (org[j] < ent->curstate.origin[i] + ent->curstate.mins[i])  
                eorg = org[i] - (ent->curstate.origin[i] + ent->curstate.mins[i]);  
            else if (org[j] > ent->curstate.origin[i] + ent->curstate.maxs[i])  
                eorg = org[i] - (ent->curstate.origin[i] + ent->curstate.maxs[i]);  
            else  
                eorg = 0;  
  
            distSquared += eorg * eorg;  
        }  
  
        if (distSquared < rad)  
            return ent;  
    }  
  
    return NULL;  
}  


void HUD_ProcessPlayerState( struct entity_state_s *dst, const struct entity_state_s *src )
{
	cl_entity_t *player = gEngfuncs.GetLocalPlayer();	// Get the local player's index
	if ( dst->number == player->index )
	{
		g_iPlayerClass = dst->playerclass;
		g_iTeamNumber = dst->team;

		g_iUser1 = src->iuser1;
		g_iUser2 = src->iuser2;
		g_iUser3 = src->iuser3;
	}

	return gExportfuncs.HUD_ProcessPlayerState( dst, src );
}


int HUD_AddEntity(int iType, struct cl_entity_s *pEntity, const char *pszModel)
{
	if(pEntity->player )  
	{
		int index = pEntity->index;
		vPlayer[index].info = pEntity;
		if(!(g_PlayerExtraInfo[index].iFlag & SCOREATTRIB_DEAD))
			vPlayer[index].killtime = gEngfuncs.GetClientTime() + 1.0;
	}

	
	return gExportfuncs.HUD_AddEntity(iType, pEntity, pszModel);
}