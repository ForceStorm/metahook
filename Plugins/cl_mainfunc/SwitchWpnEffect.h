#ifndef _SWITCHWPNEFFECT_H
#define _SWITCHWPNEFFECT_H

#define MAX_ITEM_FX 7

class CSwitchWpnEffect
{
public:
	CSwitchWpnEffect(void);
public:
	void AddFxToWpn(int wpnid);
	void Draw(void);
	void VidInit(void);

private:
	int FindValidChannel(int iWpnid);
	void GetImagePos(int iChannel, int &iX, int &iY);

private:
	float m_flFadeTime;
	int m_iMovement;
	int m_iWidth_BGImage, m_iHeight_BGImage;

private:
	int m_iWpnBG_TgaId;

private:
	int m_iWpnId[MAX_ITEM_FX];
	float m_flStartTime[MAX_ITEM_FX];
};

extern CSwitchWpnEffect *g_pSwitchWpnFx;


#endif