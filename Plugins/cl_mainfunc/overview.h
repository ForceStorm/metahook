#ifndef OVERVIEW_H
#define OVERVIEW_H

#include <com_model.h>

#define FX_PI	3.141592

#define RADAR_XPOS 20
#define RADAR_YPOS 20
#define RADAR_WIDTH 220
#define RADAR_HEIGHT 150

#define TEAMMATE_NAME_TOGGLE	1
#define TEAMMATE_FONT_W			16
#define TEAMMATE_FONT_H			16

#define IMPLUSE_END_POS			600
#define IMPLUSE_TIME_LENGTH		4.0

class COverview
{
public:
	COverview(void);

public:
	void Draw(void);
	bool CanDraw(void);
	void CalcRefdef(struct ref_params_s *pParams);
	void VidInit(void);

public:
	bool m_bCanCheck;
	bool m_bToggle;
	bool m_bIsRotate;
	model_t *m_pSprite;
	float m_flZoom;
	float m_vecOrigin[2], m_vecEyeOrigin[2];

private:
	float m_fYaw;
	bool m_bCanDraw;
	float m_fYawSin,m_fYawCos;

private:
	void DrawTeammateName(void);
	vgui::HFont m_hFont;
	int m_iTgaId_BG;
	int m_iTgaId_Impluse;
	float m_flNextImpluseTime;

};
extern COverview *g_pOverview;

struct model_s *Engfunc_LoadMapSprite(const char *pszPath);
char *Engfunc_COM_ParseFile(char *pszData, char *pszToken);
byte *Engfunc_COM_LoadFile(char *pszPath, int iUseHunk, int *piLength);
void Engfunc_COM_FreeFile(void *pBuffer);
#endif