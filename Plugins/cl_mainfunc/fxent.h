#ifndef FXENT_H
#define FXENT_H

void TempEnt_InstallHook(void);


#if 0
// from CKF metahook source
void CL_ClearTempEnts(void);
void CL_InitFXEnts(void);
void CL_FreeTempEnts(void);
void CL_PrepareTEnt( TEMPENTITY *pTemp, model_t *pmodel );
int CL_TEntAddEntity( cl_entity_t *pEntity );
BOOL CL_FreeLowPriorityTempEnt(void);
void CL_AddTempEnts( void );
TEMPENTITY *CL_TempEntAlloc( const vec3_t org, model_t *pmodel );
TEMPENTITY *CL_TempEntAllocHigh( const vec3_t org, model_t *pmodel );
TEMPENTITY *CL_TempEntAllocNoModel( const vec3_t org );
TEMPENTITY *CL_TempEntAllocCustom( const vec3_t org, model_t *model, int high, void (*pfn)( TEMPENTITY*, float, float ));
#endif


#endif