#ifndef CALCSCREEN_H
#define CALCSCREEN_H

int CalcScreen(float in[3], float out[2]);
void CalcScreen_Refdef( struct ref_params_s *pParams );
void CalcScreen_Init(void);


#endif