#ifndef _BASE_H
#define _BASE_H

#include <metahook.h>
#include <math.h>
#include "mathlib.h"

#include <triangleapi.h>
#include <r_studioint.h>
#include <cl_entity.h>
#include <ref_params.h>
#include <event_api.h>
#include <cvardef.h>

#include "exportfuncs.h"
#include "TriAPI.h"
#include "util.h"
#include "font.h"
#include "DrawFonts.h"
#include "DrawTGA.h"
#include "Surface.h"
#include "Localize.h"
#include "tga.h"



#endif