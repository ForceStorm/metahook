#include "base.h"
#include "engfuncs.h"
#include "UserData.h"
#include "weapon.h"
#include "SwitchWpnEffect.h"
#include "overview.h"
#include "playbink.h"
#include "BaseUI.h"


cl_exportfuncs_t gExportfuncs;
mh_interface_t *g_pInterface;
metahook_api_t *g_pMetaHookAPI;
mh_enginesave_t *g_pMetaSave;
IFileSystem *g_pFileSystem;

DWORD g_dwEngineBase, g_dwEngineSize;
DWORD g_dwEngineBuildnum;

DWORD g_iVideoMode;
int g_iVideoWidth, g_iVideoHeight, g_iBPP;
bool g_bWindowed;


int Initialize(struct cl_enginefuncs_s *pEnginefuncs, int iVersion);
void HUD_Init(void);
int HUD_Redraw(float time, int intermission);
int HUD_VidInit(void);
int HUD_GetStudioModelInterface(int iVersion, struct r_studio_interface_s **ppStudioInterface, struct engine_studio_api_s *pEngineStudio);
int HUD_AddEntity(int iType, struct cl_entity_s *pEntity, const char *pszModel);
void HUD_DrawTransparentTriangles(void);
void IN_MouseEvent(int);
int HUD_Key_Event(int eventcode, int keynum, const char *pszCurrentBinding);
int HUD_UpdateClientData(struct client_data_s *pcldata, float flTime);
void HUD_PlayerMove(struct playermove_s *ppmove, int server);
void V_CalcRefdef(struct ref_params_s *pParams);
void HUD_ProcessPlayerState( struct entity_state_s *dst, const struct entity_state_s *src );
int HUD_AddEntity(int iType, struct cl_entity_s *pEntity, const char *pszModel);


void IPlugins::Init(metahook_api_t *pAPI, mh_interface_t *pInterface, mh_enginesave_t *pSave)
{
	FILE * pFile = fopen(LOG_FILE_ADR, "w");
	fclose( pFile );

	Log("[ cl_mainfunc.dll Initializing... ]");

		g_pInterface = pInterface;
		g_pMetaHookAPI = pAPI;
		g_pMetaSave = pSave;

		g_pOverview			= new COverview;
		g_pSwitchWpnFx		= new CSwitchWpnEffect;
		g_pUserData			= new CUserData;
		g_pWeapon			= new CWeapon;
		g_pDrawTGA			= new CDrawTGA;
		g_pBinkManager		= new CBinkManager;

	Log("[ cl_mainfunc.dll Initialized successfully ] \n");
}



void IPlugins::Shutdown(void)
{
	Log("[ cl_mainfunc.dll Shutting down... ]");

		Fonts_Free();
		Bink_Shutdown();

		delete g_pOverview;
		delete g_pSwitchWpnFx;
		delete g_pUserData;
		delete g_pWeapon;
		delete g_pDrawTGA;
		delete g_pBinkManager;

	Log("[ cl_mainfunc.dll Shut down already !!! ] \n");
}


void IPlugins::LoadEngine(void)
{
	Log("[ cl_mainfunc.dll Load Engine... ]");

		g_pFileSystem = g_pInterface->FileSystem;
		g_dwEngineBase = g_pMetaHookAPI->GetEngineBase();
		g_dwEngineSize = g_pMetaHookAPI->GetEngineSize();

		g_iVideoMode = g_pMetaHookAPI->GetVideoMode(&g_iVideoWidth, &g_iVideoHeight, &g_iBPP, &g_bWindowed);
		g_dwEngineBuildnum = g_pMetaHookAPI->GetEngineBuildnum();

		Engine_InstallHook();
		BaseUI_InstallHook();

	Log("[ cl_mainfunc.dll Loaded Engine Successfully ] \n");
}

void IPlugins::LoadClient(cl_exportfuncs_t *pExportFunc)
{
	Log("[ cl_mainfunc.dll Load Client... ]");

		memcpy(&gExportfuncs, pExportFunc, sizeof(gExportfuncs));

		pExportFunc->Initialize						= Initialize;
		pExportFunc->HUD_Init						= HUD_Init;
		pExportFunc->HUD_Redraw						= HUD_Redraw;
		pExportFunc->HUD_VidInit					= HUD_VidInit;
		pExportFunc->HUD_GetStudioModelInterface	= HUD_GetStudioModelInterface;
		pExportFunc->HUD_AddEntity					= HUD_AddEntity;
		pExportFunc->HUD_DrawTransparentTriangles	= HUD_DrawTransparentTriangles;
		pExportFunc->HUD_Key_Event					= HUD_Key_Event;
		pExportFunc->HUD_PlayerMove					= HUD_PlayerMove;
		pExportFunc->HUD_UpdateClientData			= HUD_UpdateClientData;
		pExportFunc->V_CalcRefdef					= V_CalcRefdef;
		pExportFunc->IN_MouseEvent					= IN_MouseEvent;
		pExportFunc->HUD_ProcessPlayerState			= HUD_ProcessPlayerState;
		
	Log("[ cl_mainfunc.dll Loaded Client Successfully ] \n");
}

void IPlugins::ExitGame(int iResult)
{
	Log("[ cl_mainfunc.dll Exiting... ]");

	Log("[ cl_mainfunc.dll Exited Successfully... ] \n");
}

EXPOSE_SINGLE_INTERFACE(IPlugins, IPlugins, METAHOOK_PLUGIN_API_VERSION);