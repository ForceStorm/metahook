#ifndef _EFFECTS_H
#define _EFFECTS_H

#include "nvector.h"


enum ParticleClass_e
{
	PARTICLE_NONE,
	PARTICLE_RPG_TAIL,
	PARTICLE_FLAME,
};

class CBaseParticle
{
public:
	CBaseParticle()
	{
		m_iEnt = 0;
		m_bKillme = false;
	}

public:
	virtual void OnThink(void){}
	virtual int GetClass(void) { return PARTICLE_NONE; }

public:
	int m_iEnt;
	bool m_bKillme;

};

class CRPGTail : public CBaseParticle
{
public:
	CRPGTail();
	virtual void OnThink(void);
	virtual int GetClass(void) { return PARTICLE_RPG_TAIL; }

public:
	float m_flNextSoundTime;
	float m_flNextFxTime;
};

class CFlameParticle : public CBaseParticle
{
public:
	CFlameParticle();
	virtual void OnThink(void);
	virtual int GetClass(void) { return PARTICLE_FLAME; }
	void AddTgaItem(Tga3DItem_t *);

protected:
	float m_flNextMoveTime;
	float m_flStartTime;
	Tga3DItem_t *m_pTGA;
};

class CEffect
{
public:
	CEffect();
	void VidInit(void);
	void Redraw(void);
	void AddParticleObj(CBaseParticle *);

public:
	vector_t *m_vParticleList;
};

extern CEffect gEffect;


#endif