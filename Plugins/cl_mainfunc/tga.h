#ifndef _TGA_H
#define _TGA_H

#include "imagelib.h"
#include "pmtrace.h"
#include "nvector.h"

typedef struct Tga3DItem
{
	imagedata_t *imagedata;
	vec3_t vecPoint[4];
	int iAlpha;
	float flStartTime;
	float flFadeOutTime;
	float flFadeInTime;
	float flHoldTime;

	// if bForward is true, the tga image will always face to the local player's view like spr effect
	bool bForward;

	vec3_t vecOrigin;
	vec3_t vVelocity;
	float scale;
	bool bLoop;
	int iFrame;
	float flFrameRate;
	float flNextFrameTime;
	int iMerger;
	int wide_amt;
	int height_amt;
	int iFrameCount;
	void *privateData;
	vec3_t vecVelocity;
}
Tga3DItem_t;

class CTGA
{
private:
	vector_t  *m_vImageData;
	vector_t  *m_vTga3DItem;

public:
	CTGA();
	void HUD_DrawTransparentTriangles(void);
	int GetTextureId(char *pszName);
	imagedata_t *GetImageData(char *pszName);
	imagedata_t *GetImageData(int itexid);
	Tga3DItem_t*  AddTo3D(char *pName, int iAlpha, float flFadeInTime, float flHoldTime, float flFadeOutTime, vec3_t *point);
	Tga3DItem_t*  PlayTGA(char *pName, vec3_t vOrigin, float scale, float flFadeInTime, float flHoldTime, float flFadeOutTime, int iLoop, int iStartFrame = 0);
	void CreateDecal( pmtrace_t *pTrace,  char *pName, float scale);
	void Draw( int iTexid, int iX, int iY, int iW , int iH);
	void Draw( int iTexid, int iX, int iY, int iW , int iH, int r, int b, int g , int iAlpha);
};


extern CTGA gTGA;



#endif