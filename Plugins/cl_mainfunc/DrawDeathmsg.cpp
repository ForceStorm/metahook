#include "base.h"
#include "weapon.h"
#include "DrawSBPanel.h"
#include "playbink.h"
#include "DrawDeathmsg.h"
#include "parsemsg.h"

DeathNoticeItem rgDeathNoticeList[ MAX_DEATHNOTICES + 1 ];
int DeathMsg_iDeathTga[3][3];
int DeathMsg_iKillCount;
int iTgaIcon_Headshot, iTgaIcon_Grenade, iTgaIcon_Knife, iTgaIcon_Normal;

#define TGA_Left 0
#define TGA_Center 1
#define TGA_Right 2


float g_ColorBlue[3]	= { 0.6, 0.8, 1.0 };
float g_ColorRed[3]		= { 1.0, 0.25, 0.25 };
float g_ColorGreen[3]	= { 0.6, 1.0, 0.6 };
float g_ColorYellow[3]	= { 1.0, 0.7, 0.0 };
float g_ColorYellowish[3]	= { 1.0, 0.625, 0.0 };

float *GetClientColor( int clientIndex )
{
	const char *teamName = g_PlayerExtraInfo[ clientIndex].teamname;

	if ( !teamName || *teamName == 0 ) 
		return g_ColorYellowish;

	if ( !stricmp( "blue", teamName ) )
		return g_ColorBlue;
	else if ( !stricmp( "red", teamName ) )
		return g_ColorRed;
	else if ( !stricmp( "green", teamName ) )
		return g_ColorGreen;
	else if ( !stricmp( "yellow", teamName ) )
		return g_ColorYellow;

	return g_ColorYellowish;
}

void DeathMsg_Init(void)
{
	memset(rgDeathNoticeList,0,sizeof(rgDeathNoticeList));

	static bool bHasVidInit = false;

	if( bHasVidInit )
		return;

	bHasVidInit = true;

	DeathMsg_iDeathTga[0][TGA_Left] = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/DN_Killer_Left", false );
	DeathMsg_iDeathTga[0][TGA_Center] = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/DN_Killer_Center", false );
	DeathMsg_iDeathTga[0][TGA_Right] = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/DN_Killer_Right", false );
	DeathMsg_iDeathTga[1][TGA_Left] = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/DN_Vctim_Left", false );
	DeathMsg_iDeathTga[1][TGA_Center] = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/DN_Vctim_Center", false );
	DeathMsg_iDeathTga[1][TGA_Right] = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/DN_Vctim_Right", false );
	DeathMsg_iDeathTga[2][TGA_Left] = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/DN_Left", false );
	DeathMsg_iDeathTga[2][TGA_Center] = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/DN_Center", false );
	DeathMsg_iDeathTga[2][TGA_Right] = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/DN_Right", false );

	iTgaIcon_Headshot = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/Icon_Headshot", false );
	iTgaIcon_Grenade = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/Icon_Grenade", false );
	iTgaIcon_Knife = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/Icon_Knife", false );
	iTgaIcon_Normal = g_pDrawTGA->LoadTgaImage( "resource/tga/DeathMsg/Icon_Normal", false );
}
int MsgFunc_DeathMsg(const char *pszName, int iSize, void *pbuf)
{
	int i = 0;
	for (i = 0; i < MAX_DEATHNOTICES; i++ )
	{
		if ( !rgDeathNoticeList[i].bTakenUp )
			break;
	}
	if ( i == MAX_DEATHNOTICES )
	{
		memmove( rgDeathNoticeList, rgDeathNoticeList+1, sizeof(DeathNoticeItem) * MAX_DEATHNOTICES );
		i = MAX_DEATHNOTICES - 1;
	}

	BEGIN_READ(pbuf, iSize);
	int iKiller = READ_BYTE();
	int iVictim = READ_BYTE();
	int iHeadShot = READ_BYTE();
	sprintf(rgDeathNoticeList[i].szWpn, "d_%s",READ_STRING());

	if(iKiller && iVictim)
	{
		if(vPlayer[gEngfuncs.GetLocalPlayer()->index].team == vPlayer[iVictim].team)
		{
			vPlayer[iVictim].fDeathTime = g_flTime + 3.0f;
			vPlayer[iVictim].killtime = g_flTime + 3.0f;
		}
	}

	hud_player_info_t hPlayer;
	gEngfuncs.pfnGetPlayerInfo(iKiller, &hPlayer);
	char *killer_name = hPlayer.name;
	if ( !killer_name )
	{
		killer_name = "";
		rgDeathNoticeList[i].szKiller[0] = 0;
	}
	else
	{
		strncpy( rgDeathNoticeList[i].szKiller, killer_name, MAX_PLAYER_NAME_LENGTH );
		rgDeathNoticeList[i].szKiller[MAX_PLAYER_NAME_LENGTH-1] = 0;
	}
	// Get the Victim's name
	char *victim_name = NULL;
	// If victim is -1, the killer killed a specific, non-player object (like a sentrygun)
	if ( ((char)iVictim) != -1 )
	{
		gEngfuncs.pfnGetPlayerInfo(iVictim,&hPlayer);
		victim_name = hPlayer.name;
	}
	if ( !victim_name )
	{
		victim_name = "";
		rgDeathNoticeList[i].szVictim[0] = 0;
	}
	else
	{
		strncpy( rgDeathNoticeList[i].szVictim, victim_name, MAX_PLAYER_NAME_LENGTH );
		rgDeathNoticeList[i].szVictim[MAX_PLAYER_NAME_LENGTH-1] = 0;
	}
	// Is it a non-player object kill?
	if ( ((char)iVictim) == -1 )
	{
		rgDeathNoticeList[i].iNonPlayerKill = TRUE;

		// Store the object's name in the Victim slot (skip the d_ bit)
		strcpy( rgDeathNoticeList[i].szVictim, rgDeathNoticeList[i].szWpn+2 );
	}
	else
	{
		if ( iKiller == iVictim || iKiller == 0 )
			rgDeathNoticeList[i].iSuicide = TRUE;
	}



	rgDeathNoticeList[i].bTakenUp = true;
	rgDeathNoticeList[i].flDisplayTime = g_flTime + 6.0f;
	rgDeathNoticeList[i].iHeadShot = iHeadShot;
	rgDeathNoticeList[i].Killer = vPlayer[iKiller].team;
	rgDeathNoticeList[i].Victim = vPlayer[iVictim].team;
	
	int iIndex= gEngfuncs.GetLocalPlayer()->index;
	if(iIndex == iKiller)
	{
		rgDeathNoticeList[i].iLocal = 1;
	}
	else if(iIndex == iVictim)
	{
		rgDeathNoticeList[i].iLocal = 2;		
	}
	else rgDeathNoticeList[i].iLocal = 0;

	if(iKiller==iIndex && iVictim != iIndex )
	{
		char szCmd[128];
		if(iHeadShot)
		{
			CBink *pBink = CBink::CreateBink ( 1, AdjustSizeW(512), AdjustSizeH(589), 255, 255, 255, "cstrike\\resource\\bik\\Tip_Headshot.bik", 1, 0, 0 , 3.0, 0.5, 0.01 );
			if( pBink )
				g_pBinkManager->AddBink( pBink );

			sprintf(szCmd, "spk sound/kill/Tip_Headshot.wav");
		}
		else if(strstr(rgDeathNoticeList[i].szWpn, "knife"))
		{
			CBink *pBink = CBink::CreateBink ( 1, AdjustSizeW(512), AdjustSizeH(589), 255, 255, 255, "cstrike\\resource\\bik\\Tip_KnifeKill.bik", 1, 0, 0 , 3.0, 0.5, 0.005 );
			if( pBink )
				g_pBinkManager->AddBink( pBink );

			sprintf(szCmd, "spk sound/kill/Tip_KnifeKill.wav");
		}
		else if(strstr(rgDeathNoticeList[i].szWpn, "grenade"))
		{
			CBink *pBink = CBink::CreateBink ( 1, AdjustSizeW(512), AdjustSizeH(589), 255, 255, 255, "cstrike\\resource\\bik\\Tip_HeKill.bik", 1, 0, 0 , 3.0, 0.5, PLAYBINK_NORMAL_RATE );
			if( pBink )
				g_pBinkManager->AddBink( pBink );

			sprintf(szCmd, "spk sound/kill/Tip_HeKill.wav");
		}
		else
		{
			CBink *pBink = CBink::CreateBink ( 1, AdjustSizeW( 286 + rand()%(626-282) ), AdjustSizeH( 264 + rand()%(429-264) ), 255, 255, 255, "cstrike\\resource\\bik\\Tip_Kill.bik", 1, 0, 0 , 3.0, 0.5, 0.001 );
			if( pBink )
				g_pBinkManager->AddBink( pBink );

			sprintf(szCmd, "spk sound/kill/Tip_Kill.wav");
		}
		gEngfuncs.pfnClientCmd(szCmd);
	}
	return 0;
}
#define Y_RES	100 //80
#define X_RES	50 //30
#define BORDER	10

int DeathMsgRedraw( float flTime )
{
	static int x, y, r, g, b;
	static float flLast = 6.0f;

	for ( int i = 0; i < MAX_DEATHNOTICES; i++ )
	{
		if ( !rgDeathNoticeList[i].bTakenUp )
			break;  // we've gone through them all

		if ( rgDeathNoticeList[i].flDisplayTime < flTime )
		{ // display time has expired
			// remove the current item from the list
			memmove( &rgDeathNoticeList[i], &rgDeathNoticeList[i+1], sizeof(DeathNoticeItem) * (MAX_DEATHNOTICES - i) );
			i--;  // continue on the next item;  stop the counter getting incremented
			continue;
		}

		rgDeathNoticeList[i].flDisplayTime = min( rgDeathNoticeList[i].flDisplayTime, g_flTime +flLast );

		int iY = AdjustSizeH(Y_RES + 2 + (27 * i) );

		y = iY;
		
		int iFillX;
		
		int iLen_szKiller = Fonts_GetLen(UTF8ToUnicode(rgDeathNoticeList[i].szKiller), AdjustSizeW(14));
		int iLen_szVictim = Fonts_GetLen(UTF8ToUnicode(rgDeathNoticeList[i].szVictim), AdjustSizeW(14));
		int iW_iTgaIcon_Normal = AdjustSizeW( 58 );
		int iW_iTgaIcon_Headshot = AdjustSizeW( 28 );

		x = g_sScreenInfo.iWidth - iLen_szVictim - iW_iTgaIcon_Normal - AdjustSizeW(BORDER);
		x -= X_RES;
		iFillX = x;

		//background color
		int iWidth = iLen_szVictim + iW_iTgaIcon_Normal;

		if(rgDeathNoticeList[i].iHeadShot)
		{
			x -= iW_iTgaIcon_Headshot;
			iFillX = x;
			iWidth += iW_iTgaIcon_Headshot;
		}
		if ( !rgDeathNoticeList[i].iSuicide )
		{
			iFillX  -= (AdjustSizeW(5 + BORDER) + iLen_szKiller);
		}

		int iX_BG[3], iY_BG[3], iH_BG[3], iW_BG[3];
		int iDrawLen = rgDeathNoticeList[i].iSuicide ? (iWidth + AdjustSizeW(10 + BORDER + 5) ):(AdjustSizeW(5 + 10 + 2*BORDER + 5) + iLen_szKiller + iWidth);
		y -= AdjustSizeH(1);
		iH_BG[TGA_Left] = AdjustSizeH(20);
		iW_BG[TGA_Left] = AdjustSizeW(3);
		iX_BG[TGA_Left] = iFillX - AdjustSizeW(20+5);
		iY_BG[TGA_Left] = y;

		iH_BG[TGA_Center] = AdjustSizeH(20);
		iW_BG[TGA_Center] = iDrawLen - AdjustSizeW(6) + 2*AdjustSizeW(20);
		iX_BG[TGA_Center] = iFillX-2-AdjustSizeW(20);
		iY_BG[TGA_Center] = y;

		iH_BG[TGA_Right] = AdjustSizeH(20);
		iW_BG[TGA_Right] = AdjustSizeW(3);
		iX_BG[TGA_Right] = iFillX - AdjustSizeW(5) + iDrawLen - iW_BG[TGA_Right] + AdjustSizeW(20);
		iY_BG[TGA_Right] = y;

		if(rgDeathNoticeList[i].iLocal ==1)
		{
			g_pDrawTGA->DrawTGA( DeathMsg_iDeathTga[0][TGA_Left], iX_BG[TGA_Left], iY_BG[TGA_Left], iW_BG[TGA_Left], iH_BG[TGA_Left]);
			g_pDrawTGA->DrawTGA( DeathMsg_iDeathTga[0][TGA_Center], iX_BG[TGA_Center], iY_BG[TGA_Center], iW_BG[TGA_Center], iH_BG[TGA_Center]);
			g_pDrawTGA->DrawTGA( DeathMsg_iDeathTga[0][TGA_Right], iX_BG[TGA_Right], iY_BG[TGA_Right], iW_BG[TGA_Right], iH_BG[TGA_Right]);
		}
		else if(rgDeathNoticeList[i].iLocal ==2)
		{
			g_pDrawTGA->DrawTGA( DeathMsg_iDeathTga[1][TGA_Left], iX_BG[TGA_Left], iY_BG[TGA_Left], iW_BG[TGA_Left], iH_BG[TGA_Left]);
			g_pDrawTGA->DrawTGA( DeathMsg_iDeathTga[1][TGA_Center], iX_BG[TGA_Center], iY_BG[TGA_Center], iW_BG[TGA_Center], iH_BG[TGA_Center]);
			g_pDrawTGA->DrawTGA( DeathMsg_iDeathTga[1][TGA_Right], iX_BG[TGA_Right], iY_BG[TGA_Right], iW_BG[TGA_Right], iH_BG[TGA_Right]);
		}
		else
		{
			g_pDrawTGA->DrawTGA( DeathMsg_iDeathTga[2][TGA_Left], iX_BG[TGA_Left], iY_BG[TGA_Left], iW_BG[TGA_Left], iH_BG[TGA_Left]);
			g_pDrawTGA->DrawTGA( DeathMsg_iDeathTga[2][TGA_Center], iX_BG[TGA_Center], iY_BG[TGA_Center], iW_BG[TGA_Center], iH_BG[TGA_Center]);
			g_pDrawTGA->DrawTGA( DeathMsg_iDeathTga[2][TGA_Right], iX_BG[TGA_Right], iY_BG[TGA_Right], iW_BG[TGA_Right], iH_BG[TGA_Right]);
		}
		y = iY;
		if ( !rgDeathNoticeList[i].iSuicide )
		{
			x -= AdjustSizeW(5 + BORDER) + iLen_szKiller;
			// Draw killers name
			if(rgDeathNoticeList[i].Killer == TEAM_CT)
			{
				DrawFonts(UTF8ToUnicode(rgDeathNoticeList[i].szKiller), x, y + AdjustSizeH(14), 0, 172, 255, 255, AdjustSizeW(14), AdjustSizeH(14), 1000, 1000);
			}
			else 
				DrawFonts(UTF8ToUnicode(rgDeathNoticeList[i].szKiller), x, y + AdjustSizeH(14), 255, 83, 83, 255, AdjustSizeW(14), AdjustSizeH(14), 1000, 1000);
			x += AdjustSizeW(5+BORDER) + iLen_szKiller;
		}

		// Draw icons
		int iH, iW;

		if(strstr(rgDeathNoticeList[i].szWpn, "knife"))
		{
			iW = 59;
			iH = 20;

			g_pDrawTGA->DrawTGA(iTgaIcon_Knife, x, y, AdjustSizeW(iW), AdjustSizeH(iH) );
			
		}
		else if(strstr(rgDeathNoticeList[i].szWpn, "grenade"))
		{
			iW =  41 ;
			iH =  25 ;

			g_pDrawTGA->DrawTGA(iTgaIcon_Grenade, x, y, AdjustSizeW(iW), AdjustSizeH(iH) );
			
		}
		else
		{
			iW =  58 ;
			iH =  20 ;

			g_pDrawTGA->DrawTGA(iTgaIcon_Normal, x, y, AdjustSizeW(iW), AdjustSizeH(iH) );
			
		}
		
		x += AdjustSizeW(iW);

		if(rgDeathNoticeList[i].iHeadShot) 
		{
			iW = 28;
			iH = 25;

			g_pDrawTGA->DrawTGA( iTgaIcon_Headshot, x, y, AdjustSizeW(iW), AdjustSizeH(iH));
			x +=  AdjustSizeW(iW) ;
		}
		if (rgDeathNoticeList[i].iNonPlayerKill == FALSE)
		{
			// Draw victim's name
			if(rgDeathNoticeList[i].Victim == TEAM_CT)
			{
				DrawFonts(UTF8ToUnicode(rgDeathNoticeList[i].szVictim), x, y + AdjustSizeH(14), 0, 172, 255, 255, AdjustSizeW(14), AdjustSizeH(14), 1000, 1000);
			}
			else 
				DrawFonts(UTF8ToUnicode(rgDeathNoticeList[i].szVictim), x, y + AdjustSizeH(14), 255, 83, 83, 255, AdjustSizeW(14), AdjustSizeH(14), 1000, 1000);
			
			x += AdjustSizeW(BORDER+5);
		}
	}

	return 1;
}