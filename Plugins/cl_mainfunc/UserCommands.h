#ifndef _USERCOMMANDS_H
#define _USERCOMMANDS_H

#define HOOK_COMMAND(x, y) gEngfuncs.pfnAddCommand( x, __CmdFunc_##y );
#define DECLARE_COMMAND(y, x) void __CmdFunc_##x( void ) \
							{ \
							     UserCmd( #y );\
							}

#define DECLARE_CMDFUNC(x) void __CmdFunc_##x( void )

#define CMDFUNC_SLOT(x) void __CmdFunc_Slot##x( void ) \
						{ \
							char szBuff[32];\
							sprintf( szBuff, "slot%d", x );\
							UserCmd(szBuff);\
						}

#define DECLARE_SLOT_FUNC(x) void __CmdFunc_Slot##x( void ); 

void UserCommandInit(void);

void CmdFunc_FastrunBegin(void);
void CmdFunc_FastrunEnd(void);
void CmdFunc_BuyMenu(void);
void CmdFunc_BagEditorMenu(void);
void UserCmd( const char *pszCmd);

DECLARE_SLOT_FUNC(1)
DECLARE_SLOT_FUNC(2)
DECLARE_SLOT_FUNC(3)
DECLARE_SLOT_FUNC(4)
DECLARE_SLOT_FUNC(5)
DECLARE_SLOT_FUNC(6)
DECLARE_SLOT_FUNC(7)
DECLARE_SLOT_FUNC(8)
DECLARE_SLOT_FUNC(9)
DECLARE_SLOT_FUNC(10)



#endif