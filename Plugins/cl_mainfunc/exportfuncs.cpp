#include "base.h"
#include "MGUI.h"
#include "UserCommands.h"
#include "UserData.h"
#include "task.h"
#include "engfuncs.h"
#include "weapon.h"
#include "SwitchWpnEffect.h"
#include "hud.h"
#include "window_classmenu.h"
#include "events.h"
#include <screenfade.h>
#include "overview.h"
#include "DrawSBPanel.h"
#include "message.h"
#include "plugins.h"
#include "playbink.h"
#include "DrawDeathmsg.h"
#include "client_hook.h"
#include "resources.h"
#include "msghook.h"
#include "effects.h"

cl_enginefunc_t gEngfuncs;
engine_studio_api_t IEngineStudio;

cvar_t *cl_righthand;
cvar_t *cl_decaleffect;

// redraw time
float g_flTime, g_fOldTime;

// Player info
struct PlayerInfo vPlayer[36];
bool g_bAlive;

SCREENINFO g_sScreenInfo;
bool g_bIsWidescreen;
int g_iVideoQuality;

int Initialize(struct cl_enginefuncs_s *pEnginefuncs, int iVersion)
{
	memcpy(&gEngfuncs, pEnginefuncs, sizeof(gEngfuncs));

	pEnginefuncs->pfnHookUserMsg = &EngFunc_HookUserMsg;
	pEnginefuncs->LoadMapSprite =&Engfunc_LoadMapSprite;
	pEnginefuncs->COM_ParseFile = &Engfunc_COM_ParseFile;
	pEnginefuncs->pfnAddCommand = &EngFunc_AddCommand;
	pEnginefuncs->pfnSPR_DrawAdditive = &Engfuncs_DrawAdditive;
	pEnginefuncs->pfnFillRGBA = &Engfuncs_FillRGBA;
	pEnginefuncs->pfnSetCrosshair = &Engfuncs_SerCrosshair;
	pEnginefuncs->COM_LoadFile = &Engfunc_COM_LoadFile;
	pEnginefuncs->COM_FreeFile = &Engfunc_COM_FreeFile;

	// do not change the order
	EV_HookEvents();
	pEnginefuncs->pfnHookEvent = &Engfunc_HookEvent;

	gMGUI.Initialize();
	MSG_Init();
	Client_InstallHook();
	Bink_Init();
	Tri_Extension_InstallHook();

	return gExportfuncs.Initialize(pEnginefuncs, iVersion);
}

extern void CalcScreen_Init(void);

void HUD_Init(void)
{
	g_sScreenInfo.iSize = sizeof(g_sScreenInfo);
	gEngfuncs.pfnGetScreenInfo(&g_sScreenInfo);

	g_bIsWidescreen = g_sScreenInfo.iWidth / g_sScreenInfo.iHeight >= 4/3;

	gHUD.Init();
	g_MGuiClassMenu.Init();
	CalcScreen_Init();

	cl_decaleffect = gEngfuncs.pfnRegisterVariable("cl_decaleffect", "1", FCVAR_ARCHIVE);
	gEngfuncs.pfnHookUserMsg("MetaHook", MsgFunc_MetaHook);

	return gExportfuncs.HUD_Init();
}

int HUD_Redraw(float time, int intermission)
{

	g_fOldTime = g_flTime;
	g_flTime = time;

	g_pWeapon->Draw();
	g_pOverview->Draw();
	DeathMsgRedraw(time);
	g_pBinkManager->Redraw();
	g_pSwitchWpnFx->Draw();
	TAB_Panel_Redraw();

	gTask.Count();
	gMGUI.Redraw();
	gHUD.Redraw( time, intermission );
	gEffect.Redraw();
	
	return gExportfuncs.HUD_Redraw(time, intermission);
}

int HUD_VidInit(void)
{
	g_pOverview->m_bCanCheck = false;

	ResourceList_Reset();
	g_pDrawTGA->VidInit ();

	DeathMsg_Init();
	TAB_Panel_Init();
	UserCommandInit();

	g_pUserData->ReadData();
	g_pWeapon->VidInit();
	g_pSwitchWpnFx->VidInit();
	g_pOverview->VidInit();
	
	gMGUI.VidInit();
	gHUD.VidInit();
	gEffect.VidInit();
	g_MGuiClassMenu.VidInit();

	cl_righthand = gEngfuncs.pfnGetCvarPointer("cl_righthand");


	memset(vPlayer,0,sizeof(vPlayer));

	return gExportfuncs.HUD_VidInit();
}


int HUD_GetStudioModelInterface(int iVersion, struct r_studio_interface_s **ppStudioInterface, struct engine_studio_api_s *pEngineStudio)
{
	memcpy(&IEngineStudio, pEngineStudio, sizeof(engine_studio_api_t));

	if( g_iVideoMode == VIDEOMODE_OPENGL )
	{
		char *p = (char *)glGetString(GL_VERSION);
		char s = *p;

		Log("OpenGL Version:%s",p);

		if(s == '1')
		{
			MessageBoxA(NULL,"您的电脑的OpenGL版本过低 可能发生显示错误","武力风暴游戏提示:",MB_OK);
			g_iVideoQuality = 0;
		}
		else 
		{
			g_iVideoQuality = 1;
		}
	}

	Fonts_Init(FONT_FILE_ADR, 16,16);

	return gExportfuncs.HUD_GetStudioModelInterface(iVersion, ppStudioInterface, pEngineStudio);
}

int HUD_UpdateClientData(struct client_data_s *pcldata, float flTime)
{
	int iResult = gExportfuncs.HUD_UpdateClientData( pcldata, flTime );

	gHUD.UpdateClientData (pcldata, flTime);

	return iResult;
}

void HUD_DrawTransparentTriangles(void)
{
	gTGA.HUD_DrawTransparentTriangles ();
	return gExportfuncs.HUD_DrawTransparentTriangles();
}


void IN_MouseEvent(int x)
{
	g_mgui_oldmouseevent = g_mgui_mouseevent;
	g_mgui_mouseevent = x;

	if( gMGUI.GetMouseState() == MOUSE_FREE )
		return;

	return gExportfuncs.IN_MouseEvent( g_mgui_mouseevent );
}



#define KEY_DOT	96
#define KEY_ESC	27

int HUD_Key_Event(int eventcode, int keynum, const char *pszCurrentBinding)
{
	if( gMGUI.IsEnable() )
	{
		if(keynum != KEY_DOT &&  keynum != KEY_ESC)
			return 0;
	}

	return gExportfuncs.HUD_Key_Event(eventcode,keynum,pszCurrentBinding);
}