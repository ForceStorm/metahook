#include "ft2build.h"
#include FT_FREETYPE_H
#include FT_GLYPH_H

extern int iWidthCheck;

#define FONT_FILE_ADR "cstrike/resource/font/MainFont.ttf"

void Fonts_Init(char *FontName,int iW,int iH);
int DrawFonts(wchar_t* _strText,int x , int y, int r, int g, int b, int alpha, int iFontsSizeW, int iFontsSizeH, int maxW , int maxh);
int Fonts_GetLen(wchar_t* _strText, int iFontsSizeW);
void Fonts_C2W(wchar_t *pwstr,size_t len,const char *str);
void Fonts_Free(void);