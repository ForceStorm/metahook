#include "base.h"
#include "MGUI.h"
#include "window_classmenu.h"


CMGUI_ClassMenu g_MGuiClassMenu;

int g_iTexid_Btn;
int g_iTexid_Btn_Fire;
int g_iTexid_Class;
int g_iTexid_PreBtn;
int g_iTexid_PreBtnFire;
int g_iTexid_NextBtnFire;
int g_iTexid_NextBtn;

CBaseImage *g_pClassImage;
CBaseLabel *g_pLabel_Info;
CBaseLabel *g_pLabel_Title;


// ============================================================================================
class CClassMenu : public CBaseWindow
{
public:
	virtual void Draw(PointPos_t *pp);
	static CClassMenu *Instance(void);
};

void CClassMenu::Draw(PointPos_t *pp)
{
	gEngfuncs.pfnFillRGBABlend(m_iXpos, m_iYpos, m_iWidth, m_iHeight, 0, 0, 0, 210);
	gEngfuncs.pfnFillRGBABlend(0, 0, g_sScreenInfo.iWidth, g_sScreenInfo.iHeight, 37, 31, 5, 150);
}

CClassMenu* CClassMenu::Instance(void)
{
	CClassMenu* pWindow = NULL;
	pWindow = new CClassMenu;

	if(!pWindow)
		return NULL;

	pWindow->Init();
	gMGUI.AddWindow(pWindow);

	return pWindow;
}


CClassMenu *g_pClassMenu = NULL;

// ============================================================================================
class CPageButton : public CBaseButton
{
public:
	static CPageButton* Instance(CBaseMGUI *pParent);
protected:
	virtual void DrawButton( MGUI_STATUS   status );
	virtual void OnButtonClick(void);
	virtual void DrawBtnCaption( MGUI_STATUS   status ){}
};


CPageButton* CPageButton::Instance(CBaseMGUI *pParent)
{
	CPageButton* pButton = NULL;
	pButton = new CPageButton;

	if(!pButton)
		return NULL;

	pButton->Init();
	pButton->SetParent (pParent);
	pButton->SetClass (CLASS_BUTTON);

	return pButton;
}


void CPageButton::OnButtonClick(void)
{
	g_MGuiClassMenu.TurnPage(m_iMessage);
}

void CPageButton::DrawButton( MGUI_STATUS   status )
{
	int x, y, x_parent = 0, y_parent = 0;
	if(GetParent())
		GetParent()->GetPosition(x_parent, y_parent);

	x = x_parent + m_iXpos;
	y = y_parent + m_iYpos;

	int itexid, itexid_fire;
	if(m_iMessage == -1)
	{
		itexid = g_iTexid_PreBtn;
		itexid_fire = g_iTexid_PreBtnFire;
	}
	else if(m_iMessage == 1)
	{
		itexid = g_iTexid_NextBtn;
		itexid_fire = g_iTexid_NextBtnFire;
	}

	if( status == STATE_FIRE)
		gTGA.Draw(itexid_fire, x, y, m_iWidth, m_iHeight);
	else
		gTGA.Draw(itexid, x, y, m_iWidth, m_iHeight);
}

CPageButton *g_pPreBtn;
CPageButton *g_pNextBtn;


// ============================================================================================


class CClassButton : public CBaseButton
{
public:
	static CClassButton* Instance(CBaseMGUI *pParent);
protected:
	virtual void DrawButton( MGUI_STATUS   status );
	virtual void OnButtonFire(void);
	virtual void OnButtonClick(void);
};

CClassButton* CClassButton::Instance(CBaseMGUI *pParent)
{
	CClassButton* pButton = NULL;
	pButton = new CClassButton;

	if(!pButton)
		return NULL;

	pButton->Init();
	pButton->SetParent (pParent);
	pButton->SetClass (CLASS_BUTTON);

	return pButton;
}
void CClassButton::OnButtonFire(void)
{
	if(g_pLabel_Info)
		g_pLabel_Info->SetLocalString ( g_MGuiClassMenu.m_PlayerRoleInfo[ m_iMessage ].szInfoLang  );
}

void CClassButton::OnButtonClick(void)
{
	char szBuff[ 32 ];
	sprintf( szBuff, "joinclass %d", m_iMessage + 1 );
	ClientCmd( szBuff );

	g_pClassMenu->Destroy ();
	gMGUI.SetMouseState (MOUSE_FIXED);
}

void CClassButton::DrawButton( MGUI_STATUS   status )
{
	int x, y, x_parent = 0, y_parent = 0;
	if(GetParent())
		GetParent()->GetPosition(x_parent, y_parent);

	x = x_parent + m_iXpos;
	y = y_parent + m_iYpos;

	if( status == STATE_FIRE)
		gTGA.Draw(g_iTexid_Btn_Fire, x, y, m_iWidth, m_iHeight);
	else
		gTGA.Draw(g_iTexid_Btn, x, y, m_iWidth, m_iHeight);
}

#define MAX_PAGE			20
#define MAX_ITEM_PER_PAGE	5

CClassButton* g_pBtnPage[ MAX_PAGE ][ MAX_ITEM_PER_PAGE ];

// ============================================================================================

CMGUI_ClassMenu::CMGUI_ClassMenu( void )
{
	m_iBtnIndexList = NULL;
}

void CMGUI_ClassMenu::TurnPage(int i)
{
	m_iPage = m_iPage + i;

	for(int i = 0; i< m_iPageAmt; i++)
		for( int ii = 0; ii < MAX_ITEM_PER_PAGE; ii++)
			g_pBtnPage[i][ii]->SetVisible (FALSE);
	
	for( int ii = 0; ii < MAX_ITEM_PER_PAGE; ii++ )
		g_pBtnPage[m_iPage][ii]->SetVisible (TRUE);

}
void CMGUI_ClassMenu::Init(void)
{
	ReadPlayerRoleInfo();
}

void CMGUI_ClassMenu::VidInit(void)
{
	g_iTexid_Btn = gTGA.GetTextureId("cstrike\\resource\\tga\\Button_Normal.tga");
	g_iTexid_Btn_Fire = gTGA.GetTextureId("cstrike\\resource\\tga\\Button_Fire.tga");

	g_iTexid_PreBtn = gTGA.GetTextureId("cstrike\\resource\\tga\\prev.tga");
	g_iTexid_PreBtnFire = gTGA.GetTextureId("cstrike\\resource\\tga\\prev2.tga");
	g_iTexid_NextBtn = gTGA.GetTextureId("cstrike\\resource\\tga\\next.tga");
	g_iTexid_NextBtnFire = gTGA.GetTextureId("cstrike\\resource\\tga\\next2.tga");
}

void CMGUI_ClassMenu::Show( int iTeam )
{
	m_iTeam = iTeam;
	m_iPage = 0;

	if( m_iBtnIndexList )
		free( m_iBtnIndexList );
	
	m_iBtnIndexList = (int *)malloc( sizeof(int) *  m_iPlayerRoleNum );
	m_iBtnIndexListLen = 1;

	for( int i = 0; i<m_iPlayerRoleNum; i++ )
	{
		if( m_PlayerRoleInfo[i].iTeam == m_iTeam )
		{
			m_iBtnIndexList[ m_iBtnIndexListLen - 1] = i;
			m_iBtnIndexListLen ++;
		}
	}

	m_iBtnIndexListLen--;

	if(m_iTeam == 1)
		g_iTexid_Class = gTGA.GetTextureId("cstrike\\resource\\tga\\Team_USSR.tga");
	else
		g_iTexid_Class = gTGA.GetTextureId("cstrike\\resource\\tga\\Team_NATO.tga");

	g_pClassMenu = CClassMenu::Instance();
	if(!g_pClassMenu)
		return;

	g_pClassMenu->SetPosition(0, AdjustSizeH(168));
	g_pClassMenu->SetSize(g_sScreenInfo.iWidth, AdjustSizeH(440));
	g_pClassMenu->SetVisible(TRUE);

	g_pClassImage = CBaseImage::Instance (g_pClassMenu);
	g_pClassImage->SetTexture(g_iTexid_Class);
	g_pClassImage->SetPosition (AdjustSizeW(352), AdjustSizeH(232-168));
	g_pClassImage->SetSize (AdjustSizeW(640), AdjustSizeH(328));

	g_pLabel_Title = CBaseLabel::Instance (g_pClassMenu);
	g_pLabel_Title->SetPosition (AdjustSizeW(46), AdjustSizeH(223 -168));
	g_pLabel_Title->SetFontSize (AdjustSizeW(28));
	g_pLabel_Title->SetLocalString ("FS_ClassMenu_Title");

	g_pLabel_Info = CBaseLabel::Instance (g_pClassMenu);
	g_pLabel_Info->SetPosition (AdjustSizeW(682), AdjustSizeH(263 -168));
	g_pLabel_Info->SetFontSize (AdjustSizeW(18));
	g_pLabel_Info->SetLocalString ( "NONE" );
	
	int iPageAmt = m_iBtnIndexListLen / MAX_ITEM_PER_PAGE;
	if( m_iBtnIndexListLen % MAX_ITEM_PER_PAGE)
		iPageAmt++;

	m_iPageAmt = iPageAmt;

	int id = 0;
	for(int i = 0; i< iPageAmt; i++)
	{
		for( int ii = 0; ii < MAX_ITEM_PER_PAGE; ii++)
		{
			int x, y;
			x = AdjustSizeW(46);
			y = AdjustSizeH( ii*60 + 247-168);
			g_pBtnPage[i][ii] = CClassButton::Instance (g_pClassMenu);
			g_pBtnPage[i][ii]->SetFontSize (AdjustSizeW(17));
			g_pBtnPage[i][ii]->SetPosition (x, y);
			g_pBtnPage[i][ii]->SetSize (AdjustSizeW(232), AdjustSizeH(40));
			g_pBtnPage[i][ii]->SetMessage (id);
			g_pBtnPage[i][ii]->SetCaption (g_pLocalize->Find(m_PlayerRoleInfo[m_iBtnIndexList[id]].szNameLang));
			if(i != 0 )
				g_pBtnPage[i][ii]->SetVisible (FALSE);
			id++;
		}
	}

	if(iPageAmt > 1)
	{
		g_pPreBtn = CPageButton::Instance (g_pClassMenu);
		g_pPreBtn->SetPosition (AdjustSizeW(97), AdjustSizeH(557 - 168));
		g_pPreBtn->SetSize (AdjustSizeW(33), AdjustSizeH(32));
		g_pPreBtn->SetMessage (-1);

		g_pNextBtn = CPageButton::Instance (g_pClassMenu);
		g_pNextBtn->SetPosition (AdjustSizeW(178), AdjustSizeH(557 - 168));
		g_pNextBtn->SetSize (AdjustSizeW(33), AdjustSizeH(32));
		g_pNextBtn->SetMessage (1);
	}

	gMGUI.SetMouseState(MOUSE_FREE);
}


void CMGUI_ClassMenu::ReadPlayerRoleInfo(void)
{
	memset( m_PlayerRoleInfo, 0, sizeof( m_PlayerRoleInfo ));

	FILE* pFile;
	pFile = fopen("cstrike\\character_data\\PlayerInUse.lst", "rt");

	if(!pFile)
		return;

	char szBuff[32];
	char szAddr[128];
	FILE *pFileItem = NULL;

	int i = 0;
	while(!feof(pFile))
	{
		fgets(szBuff, sizeof(szBuff)-1, pFile);
		if(szBuff[0]==';' || !szBuff[0]  || szBuff[ 0] == '\n')
			continue;
		if(szBuff[ strlen(szBuff)-1 ] == '\n')
			szBuff[ strlen(szBuff)-1 ] = '\0';

		sprintf(szAddr, "cstrike\\character_data\\%s.data", szBuff);
		
		pFileItem = fopen(szAddr, "rt");
		if(!pFileItem)
			continue;

		char szBuff2[256], szTemp[256];
		while(!feof(pFileItem))
		{
			fgets(szBuff2, sizeof(szBuff2)-1, pFileItem);
			if(szBuff2[0]==';' || !szBuff2[0] || szBuff2[ 0] == '\n')
				continue;
			if(szBuff2[ strlen(szBuff2)-1 ] == '\n')
				szBuff2[ strlen(szBuff2)-1 ] = '\0';

			int iii = 0, iDataType = 0;
			for(int ii=0;ii<strlen(szBuff2);ii++)
			{
				if(szBuff2[ii] == '=')
				{
					szTemp[iii] = '\0';

					if(!strcmp(szTemp, "TEAM"))
						iDataType = DATA_TEAM;
					if( !strcmp( szTemp, "NAME_LANG" ) )
						iDataType = DATA_NAME_LANG;
					if( !strcmp( szTemp, "INFO_LANG" ) )
						iDataType = DATA_INFO_LANG;

					iii = 0;
					memset(szTemp,0,sizeof(szTemp));
					continue;
				}

				szTemp[iii] = szBuff2[ii];
				iii ++;
			}
			switch(iDataType)
			{
			case DATA_TEAM:
				m_PlayerRoleInfo[i].iTeam = atoi(szTemp);
				break;
			case DATA_NAME_LANG:
				m_PlayerRoleInfo[i].szNameLang = (char *)malloc( sizeof( char ) * strlen(szTemp) + 16 );
				sprintf( m_PlayerRoleInfo[i].szNameLang , szTemp );
				break;
			case DATA_INFO_LANG:
				m_PlayerRoleInfo[ i].szInfoLang = (char *)malloc( sizeof(char) * strlen(szTemp) + 16 );
				sprintf(  m_PlayerRoleInfo[i].szInfoLang , szTemp);
			
			}
			memset(szTemp, 0, sizeof(szTemp));
			memset(szBuff2, 0, sizeof(szBuff2));

		}
		fclose(pFileItem);

		i++;
	}
	fclose(pFile);

	m_iPlayerRoleNum = i;
}
