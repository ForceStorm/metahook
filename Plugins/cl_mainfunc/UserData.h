#ifndef _USERDATA_H
#define _USERDATA_H

enum Section_e
{
	SECTION_BAGITEM = 1,
};

#define DATA_ADR "cstrike\\UserData.db"

class CUserData
{
public:
	CUserData(void);
public:
	void ReadData(void);
	void SaveData( char *pszText );

private:
	void Encode(char &cChar);
	void Decode(char &cChar);


private:
	int m_iCode;
};

extern CUserData *g_pUserData;

#endif