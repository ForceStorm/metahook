#ifndef _TABPANEL_H
#define _TABPANEL_H

#include "hud.h"

#define TAB_TEAM_T	1
#define TAB_TEAM_CT	2
#define TAB_SPEC	3
#define MAX_PLAYERS	32

#define TEAM_CT		1
#define TEAM_T		2

#define SCOREATTRIB_BOMB    ( 1 << 1 )
#define SCOREATTRIB_DEAD    ( 1 << 0 )

extern hud_player_info_t g_PlayerInfoList[MAX_PLAYERS+1];
extern int iValidPlayer[33];
void TAB_Panel_Redraw(void);
void TAB_Panel_SortPlayers( int iTeam, char *team );
void TAB_Panel_SortTeams(void);
void TAB_Panel_GetAllPlayersInfo( void );
void TAB_Panel_Init(void);
void DrawNormalPanel(void);

void CmdFunc_InScoreDown(void);
void CmdFunc_InScoreUp(void);

struct extra_player_info_t 
{
	short frags;
	short deaths;
	int iFlag;
	int iFake;
	short teamnumber;
	char teamname[MAX_TEAM_NAME];
};
extern extra_player_info_t g_PlayerExtraInfo[MAX_PLAYERS+1];

extern PlayerInfo vPlayer[36];

#endif