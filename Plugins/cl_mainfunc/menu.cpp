#include "base.h"
#include "hud.h"
#include "menu.h"
#include "parsemsg.h"

#define MAX_MENU_STRING			1024
#define MAX_LINES				20
#define LINE_STRING				256

char g_szMenuStringShow[MAX_LINES][64];

extern bool bPanelCanDraw;




CHudMenu::CHudMenu(void)
{
	m_iFlags = 0;;
	m_font = 0;

}

void CHudMenu::VidInit(void)
{
	if( m_bHasVidInit )
		return;

	m_bHasVidInit = true;

	m_iTgaId_BG = g_pDrawTGA->GetIdByName( "resource/tga/Pannel" );

	if( !m_font )
	{
		m_font = g_pSurface->CreateFont ();
		g_pSurface->AddGlyphSetToFont ( m_font, "EngineFont", ( FONTSIZE_H ) , ( FONTSIZE_W ), 0, 0, 0, 0, 1);
	}
}
void CHudMenu::Show(void)
{
	m_iFlags |= HUD_ACTIVE;
}
void CHudMenu::Hide(void)
{
	m_iFlags &= ~HUD_ACTIVE;

}
void CHudMenu :: Reset( void )
{
	m_fWaitingForMore = FALSE;
}
bool CHudMenu :: MenuHasShow(void)
{
	return (m_iFlags & HUD_ACTIVE);
}


void CHudMenu :: Draw( void )		
{
	if( !(m_iFlags & HUD_ACTIVE) )
		return;

	if( bPanelCanDraw )
		return;

	// check for if menu is set to disappear
	if ( m_flShutoffTime > 0 )
	{
		if ( m_flShutoffTime <= g_flTime )
		{  // times up, shutoff
			m_fMenuDisplayed = 0;
			Hide();
			return;
		}
	}

	int y = 240;
	int x = 15;

	g_pDrawTGA->DrawTGA( m_iTgaId_BG, AdjustSizeH( x ), AdjustSizeH( y ), AdjustSizeH( m_iWidth_BG), AdjustSizeH( m_iHeight_BG), 0, 0, 0, 130);

	int iY;

	for(int i = 0; i<MAX_LINES; i++)
	{
		if( g_szMenuStringShow[i][0] == '\0' )
			continue;

		wchar_t *pText = UTF8ToUnicode( g_szMenuStringShow[i] );
		iY = i * ( FONTSIZE_H + TEXT_GAP ) + y ;
		
		// Draw Text 
		int iPos_x, iPos_y;

		iPos_x = AdjustSizeH( x + BG_BORDER );
		iPos_y = AdjustSizeH( iY + BG_TOP_BORDER );

		int startX = iPos_x;
		int fontTall = g_pSurface->GetFontTall( m_font );

		gEngfuncs.pfnDrawSetTextColor( 230 / 255.0, 230 / 255.0, 230 / 255.0);

		for (int ii = 0; ii < wcslen(pText); ii ++)
		{
			if (pText[ii] == '\n')
			{
				iPos_x = startX;
				iPos_y += fontTall;
				continue;
			}

			if( pText[ii] == '\\' )
			{
				
				if( pText[ii + 1]  ==  'Y'   ||  pText[ii + 1]  ==  'y' )
				{
					gEngfuncs.pfnDrawSetTextColor( 255 / 255.0, 242 / 255.0, 0);

					ii ++;
					continue;
				}
				else if( pText[ii + 1]  ==  'W'   ||  pText[ii + 1]  ==  'w' )
				{
					gEngfuncs.pfnDrawSetTextColor( 255 / 255.0, 255 / 255.0, 255 / 255.0);

					ii ++;
					continue;
				}
				else if( pText[ii + 1]  ==  'R'    ||  pText[ii + 1]  ==  'r' )
				{
					gEngfuncs.pfnDrawSetTextColor( 255 / 255.0, 0, 0);

					ii ++;
					continue;
				}
			}

			gEngfuncs.pfnVGUI2DrawCharacter( iPos_x, iPos_y, pText[ii], m_font);

			int w1, w2, w3;
			g_pSurface->GetCharABCwide(m_font, pText[ii], w1, w2, w3);

			iPos_x += w1 + w2 + w3;
		}
	}
	
	return;
}

void CHudMenu :: SelectMenuItem( int menu_item )
{
	if( !(m_iFlags & HUD_ACTIVE) )
		return;

	// if menu_item is in a valid slot,  send a menuselect command to the server
	if ( (menu_item > 0) && (m_bitsValidSlots & (1 << (menu_item-1))) )
	{
		char szbuf[32];
		sprintf( szbuf, "menuselect %d\n", menu_item );
		ClientCmd( szbuf );

		// remove the menu
		m_fMenuDisplayed = 0;
		Hide();
	}
}


int CHudMenu::MsgFunc_ShowMenu( const char *pszName, int iSize, void *pbuf )
{
	for(int i = 0; i<MAX_LINES; i++)
		g_szMenuStringShow[i][0] = '\0';
	
	char *temp = NULL;

	BEGIN_READ( pbuf, iSize );

	m_bitsValidSlots = READ_SHORT();
	int DisplayTime = READ_CHAR();
	int NeedMore = READ_BYTE();

	if ( !m_bitsValidSlots )
	{
		m_fMenuDisplayed = 0; // no valid slots means that the menu should be turned off
		Hide();
		return 1;
	}

	if ( DisplayTime > 0 )
		m_flShutoffTime = DisplayTime + g_flTime;
	else
		m_flShutoffTime = -1;

	

	char szMenuString[MAX_MENU_STRING];
	char szPrelocalisedMenuString[MAX_MENU_STRING];

	szMenuString[0] = '\0';
	szPrelocalisedMenuString[0] = '\0';

	if ( !m_fWaitingForMore ) // this is the start of a new menu
	{
		strncpy( szPrelocalisedMenuString, READ_STRING(), MAX_MENU_STRING );
	}
	else
	{  // append to the current menu string
		strncat( szPrelocalisedMenuString, READ_STRING(), MAX_MENU_STRING - strlen(szPrelocalisedMenuString) );
	}
	szPrelocalisedMenuString[ MAX_MENU_STRING - 1] = 0;  // ensure null termination (strncat/strncpy does not)

	if ( !NeedMore )
	{  // we have the whole string, so we can localise it now
		strcpy( szMenuString,  gHUD.m_TextMessage.BufferedLocaliseTextString( szPrelocalisedMenuString ) );

		// Swap in characters
		if ( KB_ConvertString( szMenuString, &temp ) )
		{
			strcpy( szMenuString, temp );
			if( temp )
				free( temp );
		}
	}

	m_fMenuDisplayed = 1;
	

	m_fWaitingForMore = NeedMore;

	memset( g_szMenuStringShow, 0, sizeof(szMenuString));

	int iLine = 0;
	char szTemp[512];
	int ii = 0;
	int iHighest = 0;
	int iWidest = 0;

	for(int i =0; i< strlen(szMenuString); i++)
	{
		if( szMenuString[i] == '\n'  ||   i == strlen(szMenuString) -1)
		{
			szTemp[ii] = '\0';
			sprintf( g_szMenuStringShow[ iLine ], szTemp );

			int iWide, iTall;
			g_pSurface->GetTextSize ( m_font, UTF8ToUnicode( g_szMenuStringShow[ iLine ] ) , iWide, iTall );
			
			if( iWidest < iWide )
				iWidest = iWide;

			iHighest  +=  iTall + TEXT_GAP;

			memset(szTemp, 0, sizeof(szTemp) );
			iLine ++;
			ii = 0;
			continue;
		}

		szTemp[ ii ] = szMenuString[i];
		ii ++;
	}
	m_iHeight_BG = (iHighest - TEXT_GAP) + BG_TOP_BORDER * 2;
	m_iWidth_BG = iWidest + BG_BORDER * 2;

	Show();

	return 1;
}
int KB_ConvertString( char *in, char **ppout )
{
	char sz[ 4096 ];
	char binding[ 64 ];
	char *p;
	char *pOut;
	char *pEnd;
	const char *pBinding;

	if ( !ppout )
		return 0;

	*ppout = NULL;
	p = in;
	pOut = sz;
	while ( *p )
	{
		if ( *p == '+' )
		{
			pEnd = binding;
			while ( *p && ( isalnum( *p ) || ( pEnd == binding ) ) && ( ( pEnd - binding ) < 63 ) )
			{
				*pEnd++ = *p++;
			}

			*pEnd =  '\0';

			pBinding = NULL;
			if ( strlen( binding + 1 ) > 0 )
			{
				// See if there is a binding for binding?
				pBinding = gEngfuncs.Key_LookupBinding( binding + 1 );
			}

			if ( pBinding )
			{
				*pOut++ = '[';
				pEnd = (char *)pBinding;
			}
			else
			{
				pEnd = binding;
			}

			while ( *pEnd )
			{
				*pOut++ = *pEnd++;
			}

			if ( pBinding )
			{
				*pOut++ = ']';
			}
		}
		else
		{
			*pOut++ = *p++;
		}
	}

	*pOut = '\0';

	pOut = ( char * )malloc( strlen( sz ) + 1 );
	strcpy( pOut, sz );
	*ppout = pOut;

	return 1;
}
