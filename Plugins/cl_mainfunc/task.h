#ifndef _TASK_H
#define _TASK_H


enum TaskId_e
{
	TASK_MOUSE_RECOVER = 1000,
	TASK_SEND_FOV,
};


class CTaskInfo
{
public:
	CTaskInfo(){ pNext = NULL; }
public:
	void (*pfnTask)(int iTaskid);
	int iTaskid;
	float flEndTime;
	CTaskInfo *pNext;
};


class CTask
{
public:
	CTask(void);
	void Count(void);
public:
	void SetTask( float flTaskTime, void (*pfnTask)(int iTaskid), int iTaskid);
	void DeleteTask( int iTaskid );

private:
	CTaskInfo *m_pTask;
};


extern CTask gTask;

#endif