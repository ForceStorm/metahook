#include "base.h"
#include <r_efx.h>
#include "fxent.h"
#include <com_model.h>

/*
Break Tempent amount limit
	by hzqst

Requirement:
hw.dll	ver:3266
*/

#define MAX_TEMPENT		9999
typedef void (*type_CL_InitTEnt_part2)(void);

type_CL_InitTEnt_part2			g_pfn_CL_InitTEnt_part2;
type_CL_InitTEnt_part2			g_real_CL_InitTEnt_part2;

TEMPENTITY *gTempEnts;
TEMPENTITY **gpTempEntFree;


void CL_InitTEnt_part2(void)
{
	static TEMPENTITY gTempEntsNew[MAX_TEMPENT];

	g_real_CL_InitTEnt_part2();

	memset(gTempEntsNew, 0, sizeof(gTempEntsNew));
	gTempEnts[499].next = &gTempEntsNew[0];

	for(int i = 0; i < MAX_TEMPENT - 1; ++i)
		gTempEntsNew[i].next = &gTempEntsNew[i+1];

	gTempEntsNew[MAX_TEMPENT - 1].next = NULL;
}


void TempEnt_InstallHook(void)
{
	g_pfn_CL_InitTEnt_part2 = (type_CL_InitTEnt_part2)0x1D295F0;
	g_pMetaHookAPI->InlineHook((void *)g_pfn_CL_InitTEnt_part2, CL_InitTEnt_part2, (void *&)g_real_CL_InitTEnt_part2);
	gTempEnts = (TEMPENTITY *)0x1EF9EB0;
}

/*
#define SHARD_VOLUME 12.0

void R_BreakModel(float *pos, float *size, float *dir, float random, float life, int count, model_t model, char flags)
{
	int i, frameCount;
	TEMPENTITY *pTemp;
	model_t *pModel;
	char type;
	vec3_t vecSpot;

	if (!modelIndex)
		return;

	type = flags & BREAK_TYPEMASK;

	pModel = CL_GetModelByIndex(modelIndex);

	if (!pModel)
		return;

	frameCount = ModelFrameCount(pModel);

	if (count == 0)
		count = (size[0] * size[1] + size[1] * size[2] + size[2] * size[0]) / (3 * SHARD_VOLUME * SHARD_VOLUME);

	if (count > 100)
		count = 100;

	for (i = 0; i < count; i++)
	{
		vecSpot[0] = pos[0] + RandomFloat(-0.5, 0.5) * size[0];
		vecSpot[1] = pos[0] + RandomFloat(-0.5, 0.5) * size[1];
		vecSpot[2] = pos[0] + RandomFloat(-0.5, 0.5) * size[2];

		pTemp = efx.CL_TempEntAlloc(vecSpot, pModel);

		if (!pTemp)
			break;

		pTemp->hitSound = type;

		if (pModel->type == mod_sprite)
			pTemp->entity.curstate.frame = RandomLong(0, frameCount - 1);
		else if (pModel->type == mod_studio)
			pTemp->entity.curstate.body = RandomLong(0, frameCount - 1);

		pTemp->flags |= FTENT_COLLIDEWORLD | FTENT_FADEOUT | FTENT_SLOWGRAVITY;

		if (RandomLong(0, 255) < 200)
		{
			pTemp->flags |= FTENT_ROTATE;
			pTemp->entity.baseline.angles[0] = RandomFloat(-256, 255);
			pTemp->entity.baseline.angles[1] = RandomFloat(-256, 255);
			pTemp->entity.baseline.angles[2] = RandomFloat(-256, 255);
		}

		if (RandomLong(0, 255) < 100 && (flags & BREAK_SMOKE))
			pTemp->flags |= FTENT_SMOKETRAIL;

		if ((type == BREAK_GLASS) || (flags & BREAK_TRANS))
		{
			pTemp->entity.curstate.rendermode = kRenderTransTexture;
			pTemp->entity.baseline.renderamt = pTemp->entity.curstate.renderamt = 128;
		}
		else
		{
			pTemp->entity.curstate.rendermode = kRenderNormal;
			pTemp->entity.baseline.renderamt = 255;
		}

		pTemp->entity.baseline.origin[0] = dir[0] + RandomFloat(-random, random);
		pTemp->entity.baseline.origin[1] = dir[1] + RandomFloat(-random, random);
		pTemp->entity.baseline.origin[2] = dir[2] + RandomFloat(0, random);
		pTemp->die = cl.time + life + RandomFloat(0, 1);
	}
}


*/

#if 0
// from CKF metahook source

#include "base.h"
#include "fxent.h"
#include "util.h"

TEMPENTITY	*cl_active_tents;
TEMPENTITY	*cl_free_tents;
TEMPENTITY	*cl_tempents = NULL;// entities pool

#define ET_TEMPENTITY	2

#define max_tents 9999

void CL_InitFXEnts(void)//CL_InitTempEnts();
{
	cl_tempents = (TEMPENTITY *)malloc(sizeof(TEMPENTITY)*max_tents);
	CL_ClearTempEnts();
}

void CL_ClearTempEnts( void )
{
	int	i;

	if( !cl_tempents ) return;

	for( i = 0; i < max_tents - 1; i++ )
		cl_tempents[i].next = &cl_tempents[i+1];

	cl_tempents[max_tents-1].next = NULL;
	cl_free_tents = cl_tempents;
	cl_active_tents = NULL;
}

void CL_FreeTempEnts(void)//in cl_game.c(3767):void CL_UnloadProgs( void )
{
	if( cl_tempents ) free(cl_tempents);
	cl_tempents = NULL;
}

void CL_PrepareTEnt( TEMPENTITY *pTemp, model_t *pmodel )
{
	int	frameCount = 0;
	int	modelIndex = 0;

	memset( pTemp, 0, sizeof( *pTemp ));

	// use these to set per-frame and termination conditions / actions
	pTemp->flags = FTENT_NONE;		
	pTemp->die = g_flTime + 0.75f;

	if( pmodel )
	{
		modelIndex = gEngfuncs.pEventAPI->EV_FindModelIndex(pmodel->name);
		frameCount = pmodel->numframes;
	}
	else
	{
		pTemp->flags |= FTENT_NOMODEL;
	}

	pTemp->entity.curstate.modelindex = modelIndex;
	pTemp->entity.curstate.rendermode = kRenderNormal;
	pTemp->entity.curstate.renderfx = kRenderFxNone;
	pTemp->entity.curstate.rendercolor.r = 0;
	pTemp->entity.curstate.rendercolor.g = 0;
	pTemp->entity.curstate.rendercolor.b = 0;
	pTemp->frameMax = max( 0, frameCount - 1 );
	pTemp->entity.curstate.renderamt = 255;
	pTemp->entity.curstate.body = 0;
	pTemp->entity.curstate.skin = 0;
	pTemp->entity.model = pmodel;
	pTemp->fadeSpeed = 0.5f;
	pTemp->hitSound = 0;
	pTemp->clientIndex = 0;
	pTemp->bounceFactor = 1;
	pTemp->entity.curstate.scale = 1.0f;
}

int CL_TEntAddEntity(cl_entity_t *pEntity)
{
	if(!pEntity) return -1;

	static char szModel[64];
	strcpy(szModel, pEntity->model->name);

	if(strstr(szModel, ".spr"))
	{
		pEntity->origin[0]=pEntity->origin[0]+pEntity->curstate.velocity[0]*g_flTimeDelta;
		pEntity->origin[1]=pEntity->origin[1]+pEntity->curstate.velocity[1]*g_flTimeDelta;
		pEntity->origin[2]=pEntity->origin[2]+pEntity->curstate.velocity[2]*g_flTimeDelta;
		//grenade
		if(pEntity->curstate.iuser1 == FXENT_GRENADEFLARE_ADD)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 1.5f;
			pEntity->curstate.renderamt = int(120 - 120*fNow/fTotal);
			if(fNow > fTotal * 0.25 && g_cvarGrenadeLevel->value >= 4)
			{
				if(pEntity->curstate.skin == 0)
				{
					pEntity->curstate.rendercolor.r = max(pEntity->curstate.rendercolor.r - gEngfuncs.pfnRandomLong(0,7), 0);
					pEntity->curstate.rendercolor.g = min(pEntity->curstate.rendercolor.g + gEngfuncs.pfnRandomLong(0,7), 255);
					pEntity->curstate.rendercolor.b = min(pEntity->curstate.rendercolor.b + gEngfuncs.pfnRandomLong(0,10), 255);
				}
				else
				{
					pEntity->curstate.rendercolor.r = min(pEntity->curstate.rendercolor.r + gEngfuncs.pfnRandomLong(0,10), 255);
					pEntity->curstate.rendercolor.g = min(pEntity->curstate.rendercolor.g + gEngfuncs.pfnRandomLong(0,7), 255);
					pEntity->curstate.rendercolor.b = max(pEntity->curstate.rendercolor.b - gEngfuncs.pfnRandomLong(0,7), 0);
				}
			}
		}
		else if(pEntity->curstate.iuser1 == FXENT_GRENADEFLARE_INDEX)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.2f;
			pEntity->curstate.scale = 0.06 + 0.06*fNow/fTotal;
			pEntity->curstate.renderamt = int( (1.0f-fNow/fTotal)*180 );
			cl_entity_t *pOwner = gEngfuncs.GetEntityByIndex(pEntity->curstate.owner);
			if(pOwner) VectorCopy(pOwner->origin, pEntity->origin);
		}
		//rocket
		else if(pEntity->curstate.iuser1 == FXENT_ROCKETSMOKE || pEntity->curstate.iuser1 == FXENT_MULTIJUMP_SMOKE)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 1.8f;
			pEntity->curstate.scale = 0.03 + min(fNow / fTotal * 1.2, 0.04);
			if(pEntity->curstate.animtime < g_flTime)
			{
				pEntity->curstate.frame ++;
				if(pEntity->curstate.frame > pEntity->curstate.team) pEntity->curstate.frame = pEntity->curstate.team;
				pEntity->curstate.animtime = g_flTime+pEntity->curstate.framerate;
			}
		}
		else if(pEntity->curstate.iuser1 == FXENT_ROCKETFLARE_ADD_LV3)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.10f;
			pEntity->curstate.scale = 0.10 + 0.05*sin(M_PI*(fNow)/fTotal);
			if(pEntity->curstate.animtime < g_flTime)
			{
				pEntity->curstate.frame ++;
				if(pEntity->curstate.frame > pEntity->curstate.team) pEntity->curstate.frame = 0;
				pEntity->curstate.animtime = g_flTime+pEntity->curstate.framerate;
			}
		}
		else if(pEntity->curstate.iuser1 == FXENT_ROCKETFLARE_ADD_LV2 || pEntity->curstate.iuser1 == FXENT_GRENADE_CRITFLARE)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.10f;
			pEntity->curstate.scale = 0.15 - 0.12*fNow/fTotal;
		}
		else if(pEntity->curstate.iuser1 == FXENT_ROCKETFLARE_INDEX_LV3)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.10f;
			if(fNow > fTotal*0.5) fNow = fTotal*0.5;
			pEntity->curstate.scale = 0.20 + 0.10*cos(M_PI*(fNow)/fTotal);
		}
		//explode
		else if(pEntity->curstate.iuser1 == FXENT_EXPLODE_BRIGHT1)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.20f;
			pEntity->curstate.renderamt = 100+100*sin(M_PI*(fNow-0.025)/fTotal);
			if(fNow > 0.5*fTotal && pEntity->curstate.team == 0) {pEntity->curstate.team = 1;DrawExplode(pEntity->origin, 1);}
		}
		else if(pEntity->curstate.iuser1 == FXENT_EXPLODE_BRIGHT2)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.15f;
			pEntity->curstate.scale = 1.0 + 1.0*fNow/fTotal;
			pEntity->curstate.renderamt = 200-150*fNow/fTotal;
		}
		//minigun
		else if(pEntity->curstate.iuser1 == FXENT_MINIGUN_FIRE)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.25f;
			pEntity->curstate.renderamt = 255 - 100*fNow/fTotal;
			pEntity->curstate.scale = 0.75 + 1.0 * fabs(0.5-fNow/fTotal);
			if(pEntity->curstate.iuser2 == 998) VectorCopy(g_pViewEnt->attachment[0], pEntity->origin);
		}
		else if(pEntity->curstate.iuser1 == FXENT_CRITICAL_PARTICLE)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.15f;
			pEntity->curstate.renderamt = 255-128*fNow/fTotal;
		}
		else if(pEntity->curstate.iuser1 == FXENT_FLAME)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.6f;
			pEntity->curstate.scale = max(3*pEntity->curstate.fov * sin(M_PI*fNow/fTotal), 0.03);
			pEntity->curstate.renderamt = 255-200*fNow/fTotal;
			if(pEntity->curstate.animtime < g_flTime)
			{
				pEntity->curstate.frame ++;
				if(pEntity->curstate.frame > pEntity->curstate.team) pEntity->curstate.frame = pEntity->curstate.team;
				pEntity->curstate.animtime = g_flTime+pEntity->curstate.framerate;
			}
		}
		else if(pEntity->curstate.iuser1 == FXENT_STICKYKILL)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 2.0f;
			pEntity->curstate.renderamt = 255-255*fNow/fTotal;
		}
		else if(pEntity->curstate.iuser1 == FXENT_MEDIBEAM)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.05f;
			pEntity->curstate.renderamt = 128-128*fNow/fTotal;
		}
		else if(pEntity->curstate.iuser1 == FXENT_HEALCROSS)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.5f;
			pEntity->curstate.renderamt = 255-128*fNow/fTotal;
		}
		else if(pEntity->curstate.iuser1 == FXENT_IGNITEGLOW)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.5f;
			pEntity->curstate.renderamt = 255-255*fNow/fTotal;
		}
		else if(pEntity->curstate.iuser1 == FXENT_IGNITEFLAME)
		{
			float fNow = g_flTime - pEntity->curstate.fuser1;
			float fTotal = 0.8f;
			pEntity->curstate.scale = max(2*pEntity->curstate.fov * sin(M_PI*fNow/fTotal), 0.03);
			pEntity->curstate.renderamt = 255-128*fNow/fTotal;
			if(pEntity->curstate.animtime < g_flTime)
			{
				pEntity->curstate.frame ++;
				if(pEntity->curstate.frame > pEntity->curstate.team) pEntity->curstate.frame = pEntity->curstate.team;
				pEntity->curstate.animtime = g_flTime+pEntity->curstate.framerate;
			}
		}
	}
	return gEngfuncs.CL_CreateVisibleEntity(ET_TEMPENTITY, pEntity);
}

BOOL CL_FreeLowPriorityTempEnt(void)
{
	TEMPENTITY	*pActive = cl_active_tents;
	TEMPENTITY	*pPrev = NULL;

	while( pActive )
	{
		if( pActive->priority == TENTPRIORITY_LOW )
		{
			// remove from the active list.
			if( pPrev ) pPrev->next = pActive->next;
			else cl_active_tents = pActive->next;

			// add to the free list.
			pActive->next = cl_free_tents;
			cl_free_tents = pActive;

			return TRUE;
		}

		pPrev = pActive;
		pActive = pActive->next;
	}
	return FALSE;
}

void CL_AddTempEnts( void )
{
	gExportfuncs.HUD_TempEntUpdate( g_flTimeDelta, g_flTime, g_fGravity, &cl_free_tents, &cl_active_tents, CL_TEntAddEntity, Engfunc_Call_TempEntPlaySound );	// SB
}

TEMPENTITY *CL_TempEntAlloc( const vec3_t org, model_t *pmodel )
{
	TEMPENTITY	*pTemp;

	if(!cl_free_tents)
	{
		gEngfuncs.pfnConsolePrint( "Overflow 9999 temporary ents!\n" );
		return NULL;
	}

	pTemp = cl_free_tents;
	cl_free_tents = pTemp->next;

	CL_PrepareTEnt( pTemp, pmodel );

	pTemp->priority = TENTPRIORITY_LOW;
	if(org) VectorCopy( org, pTemp->entity.origin );

	pTemp->next = cl_active_tents;
	cl_active_tents = pTemp;

	//gEngfuncs.CL_CreateVisibleEntity(ET_TEMPENTITY, &(pTemp->entity));

	return pTemp;
}

TEMPENTITY *CL_TempEntAllocHigh( const vec3_t org, model_t *pmodel )
{
	TEMPENTITY	*pTemp;

	if(!cl_free_tents)
	{
		// no temporary ents free, so find the first active low-priority temp ent 
		// and overwrite it.
		CL_FreeLowPriorityTempEnt();
	}

	if( !cl_free_tents )
	{
		// didn't find anything? The tent list is either full of high-priority tents
		// or all tents in the list are still due to live for > 10 seconds. 
		gEngfuncs.pfnConsolePrint( "Couldn't alloc a high priority TENT!\n" );
		return NULL;
	}

	// Move out of the free list and into the active list.
	pTemp = cl_free_tents;
	cl_free_tents = pTemp->next;

	CL_PrepareTEnt( pTemp, pmodel );

	pTemp->priority = TENTPRIORITY_HIGH;
	if( org ) VectorCopy( org, pTemp->entity.origin );

	pTemp->next = cl_active_tents;
	cl_active_tents = pTemp;

	gEngfuncs.CL_CreateVisibleEntity(ET_TEMPENTITY, &(pTemp->entity));

	return pTemp;
}

TEMPENTITY *CL_TempEntAllocNoModel( const vec3_t org )
{
	return CL_TempEntAlloc( org, NULL );
}

TEMPENTITY *CL_TempEntAllocCustom( const vec3_t org, model_t *model, int high, void (*pfn)( TEMPENTITY*, float, float ))
{
	TEMPENTITY	*pTemp;

	if( high )
	{
		pTemp = CL_TempEntAllocHigh( org, model );
	}
	else
	{
		pTemp = CL_TempEntAlloc( org, model );
	}

	if( pTemp && pfn )
	{
		pTemp->flags |= FTENT_CLIENTCUSTOM;
		pTemp->callback = pfn;
	}

	gEngfuncs.CL_CreateVisibleEntity(ET_TEMPENTITY, &(pTemp->entity));

	return pTemp;
}

#endif