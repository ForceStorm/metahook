#include "base.h"
#include <pm_defs.h>

vec3_t g_vecCurVelocity;


void HUD_PlayerMove(struct playermove_s *ppmove, int server)
{
	VectorCopy(ppmove->velocity, g_vecCurVelocity);
	gExportfuncs.HUD_PlayerMove ( ppmove, server );
}