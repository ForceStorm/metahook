#include "base.h"
#include "hud.h"
#include "saytext.h"
#include "parsemsg.h"

#include <string.h>
#include <stdio.h>

extern float *GetClientColor( int clientIndex );
extern hud_player_info_t	g_PlayerInfoList[MAX_PLAYERS+1];
extern bool bPanelCanDraw;

#define MAX_LINES	4
#define MAX_CHARS_PER_LINE	256  /* it can be less than this, depending on char size */

#define MAX_LINE_WIDTH  (g_sScreenInfo.iWidth - 40)

static char g_szLineBuffer[ MAX_LINES + 1 ][ MAX_CHARS_PER_LINE ];
static char g_szLineFadeOut[ MAX_CHARS_PER_LINE ];
static float g_flLineRemoveTime;
static float flScrollTime = 0;  // the time at which the lines next scroll up

void CHudSayText :: Init( void )
{
	InitHUDData();
	return;
}


void CHudSayText :: InitHUDData( void )
{
	memset( g_szLineFadeOut, 0, sizeof( g_szLineFadeOut ) );
	memset( g_szLineBuffer, 0, sizeof g_szLineBuffer );

	g_flLineRemoveTime = 0;
	flScrollTime = 0;

}

void CHudSayText :: VidInit( void )
{
	InitHUDData();

	if( m_bHasVidInit )
		return;

	m_bHasVidInit = true;

	m_iTgaId_BG = g_pDrawTGA->GetIdByName ( "resource/tga/Pannel" );

	return;
}


int ScrollTextUp( void )
{
	ConsolePrint( g_szLineBuffer[0] ); // move the first line into the console buffer

	sprintf( g_szLineFadeOut, g_szLineBuffer[0] );
	g_flLineRemoveTime = g_flTime + SAYTEXT_LINE_FADE_TIME;

	g_szLineBuffer[MAX_LINES][0] = 0;
	memmove( g_szLineBuffer[0], g_szLineBuffer[1], sizeof(g_szLineBuffer) - sizeof(g_szLineBuffer[0]) );
	g_szLineBuffer[MAX_LINES-1][0] = 0;


	return 1;
}

void CHudSayText :: Draw( void )
{
	if( bPanelCanDraw )
		return;
	
	if ( flScrollTime <= g_flTime )
	{
		if ( *g_szLineBuffer[0] )
		{
			flScrollTime = g_flTime + SAYTEXT_SCROLL_TIME;
			ScrollTextUp();
		}
	}

	if( g_flLineRemoveTime >= g_flTime )
	{
		wchar_t *pText = UTF8ToUnicode( g_szLineFadeOut );
		int iTextLen = 0;
		iTextLen = Fonts_GetLen( pText, AdjustSizeH( SAYTEXT_FONT_SIZE_W) );

		int iX = g_sScreenInfo.iWidth / 2.0 - iTextLen / 2.0;
		int iY = SAYTEXT_TOP_Y - 1 * ( SAYTEXT_GAP_Y + SAYTEXT_FONT_SIZE_H );

		int iAlphaTGA = ( (g_flLineRemoveTime - g_flTime) / SAYTEXT_LINE_FADE_TIME ) * 70;
		int iAlphaText = ( (g_flLineRemoveTime - g_flTime) / SAYTEXT_LINE_FADE_TIME ) * 255 ;

		g_pDrawTGA->DrawTGA ( m_iTgaId_BG, iX - 10, AdjustSizeH( iY -SAYTEXT_FONT_SIZE_H  )-SAYTEXT_TEXT_GAP, iTextLen + 10 * 2, AdjustSizeH( SAYTEXT_FONT_SIZE_H) +SAYTEXT_TEXT_GAP*2, 0, 0, 0, iAlphaTGA );
		DrawFonts( pText, iX, AdjustSizeH( iY ), 255,255,255,iAlphaText, AdjustSizeH( SAYTEXT_FONT_SIZE_W ), AdjustSizeH( SAYTEXT_FONT_SIZE_H ), iTextLen + 10, 1000);
	}

	for ( int i = 0; i < MAX_LINES; i++ )
	{
		if ( *g_szLineBuffer[i] )
		{
			// normal draw
			wchar_t *pText = UTF8ToUnicode( g_szLineBuffer[i] );
			int iTextLen = 0;
			iTextLen = Fonts_GetLen( pText, AdjustSizeH( SAYTEXT_FONT_SIZE_W) );

			int iX = g_sScreenInfo.iWidth / 2.0 - iTextLen / 2.0;
			int iY = SAYTEXT_TOP_Y + i * ( SAYTEXT_GAP_Y + SAYTEXT_FONT_SIZE_H );

			g_pDrawTGA->DrawTGA ( m_iTgaId_BG, iX - 10, AdjustSizeH( iY - SAYTEXT_FONT_SIZE_H  ) -SAYTEXT_TEXT_GAP, iTextLen + 10 * 2, AdjustSizeH( SAYTEXT_FONT_SIZE_H )+ SAYTEXT_TEXT_GAP *2.5, 0, 0, 0, 70 );
			DrawFonts( pText, iX, AdjustSizeH( iY ), 255,255,255,255, AdjustSizeH( SAYTEXT_FONT_SIZE_W ), AdjustSizeH( SAYTEXT_FONT_SIZE_H ), iTextLen + 10, 1000);
			
		}
	}

	return;
}

int CHudSayText :: MsgFunc_SayText( const char *pszName, int iSize, void *pbuf )
{
	BEGIN_READ( pbuf, iSize );

	int client_index = READ_BYTE();		// the client who spoke the message
	SayTextPrint( READ_STRING(), iSize - 1,  client_index );
	
	return 1;
}
void CHudSayText :: SayTextPrint( const char *pszBuf, int iBufSize, int clientIndex )
{
	// find an empty string slot
	int i;
	for ( i = 0; i < MAX_LINES; i++ )
	{
		if ( ! *g_szLineBuffer[i] )
			break;
	}
	if ( i == MAX_LINES )
	{
		// force scroll buffer up
		ScrollTextUp();
		i = MAX_LINES - 1;
	}

	strncpy( g_szLineBuffer[i], pszBuf, max(iBufSize -1, MAX_CHARS_PER_LINE-1) );

	// Set scroll time
	if ( i == 0 )
	{
		flScrollTime = gHUD.m_flTime + SAYTEXT_SCROLL_TIME;
	}

	gEngfuncs.pfnPlaySoundByName( "misc/talk.wav", 1 );

}
