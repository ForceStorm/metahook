#include "base.h"
#include <entity_state.h>
#include <usercmd.h>
#include "weapon.h"
#include "weaponinfo.h"



void (*g_pfnHUD_WeaponsPostThink)(local_state_s *, local_state_s *, usercmd_t *, double, unsigned int) = (void (*)(local_state_s *, local_state_s *, usercmd_t *, double, unsigned int))0x190DC30;

void HUD_WeaponsPostThink( local_state_s *from, local_state_s *to, usercmd_t *cmd, double time, unsigned int random_seed )
{
	// Current weapon
	int iWpnClass = from->client.m_iId;
	weapon_data_t *pfrom;

	pfrom = &from->weapondata[ iWpnClass ];
	g_pWeapon->m_iClip = pfrom->m_iClip;
	g_pWeapon->m_iBpammo = (int)from->client.vuser4[1];

	return g_pfnHUD_WeaponsPostThink(from, to, cmd, time, random_seed);
}

void HL_Weapons_InstallHook(void)
{
	g_pMetaHookAPI->InlineHook((void *)g_pfnHUD_WeaponsPostThink, HUD_WeaponsPostThink, (void *&)g_pfnHUD_WeaponsPostThink);
}


