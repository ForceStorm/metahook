#include "base.h"
#include "saytext.h"
#include "text_message.h"
#include "hud.h"
#include "weapon.h"
#include <screenfade.h>
#include "parsemsg.h"

CHud gHUD;

int g_iPlayerClass;
int g_iTeamNumber;
int g_iUser1;
int g_iUser2;
int g_iUser3;



CHud::CHud(void)
{
	m_iGameMode = 0;
	m_iFOV = 90;
}

CHud::~CHud(void)
{
}

void CHud::Init( void )
{
	m_Health.Init();
	m_Ammo.Init();
	m_SuvModeUi.Init();
	m_Timer.Init();
	m_SayText.Init();
	m_TextMessage.Init();
	m_Menu.Init ();
}

void CHud::VidInit( void )
{
	m_Health.VidInit();
	m_Ammo.VidInit();
	m_SuvModeUi.VidInit();
	m_Timer.VidInit();
	m_Nvg.VidInit();
	m_SayText.VidInit();
	m_TextMessage.VidInit();
	m_Menu.VidInit ();


	static bool bHasVidInit = false;

	if( bHasVidInit )
		return;

	bHasVidInit = true;

	m_iTgaId_View = g_pDrawTGA->LoadTgaImage( "resource/tga/NewView", false );
}
int CHud :: Redraw( float flTime, int intermission )
{
	m_fOldTime = m_flTime;	// save time of previous redraw
	m_flTime = flTime;
	m_flTimeDelta = (double)m_flTime - m_fOldTime;
	
	// Clock was reset, reset delta
	if ( m_flTimeDelta < 0 )
		m_flTimeDelta = 0;

	m_iIntermission = intermission;

	g_pDrawTGA->DrawTGA( m_iTgaId_View, 0, 0, g_sScreenInfo.iWidth, g_sScreenInfo.iHeight );

	m_Health.Draw();
	m_Ammo.Draw();
	m_SuvModeUi.Draw();
	m_Timer.Draw();
	m_Nvg.Draw ();
	m_SayText.Draw();
	m_TextMessage.Draw();
	m_Menu.Draw();

	return 1;
}
void CHud::SetGameMode( int iGameMode )
{
	m_iGameMode = iGameMode;

	if( m_iGameMode == GAME_MODE_SURVIVOR )
		m_SuvModeUi.Show ();
}
int CHud::DrawHudString(int xpos, int ypos, int iMaxX, char *szIt, int r, int g, int b )
{
	// draw the string until we hit the null character or a newline character
	for ( ; *szIt != 0 && *szIt != '\n'; szIt++ )
	{
		int next = xpos + g_sScreenInfo.charWidths[ *szIt ]; // variable-width fonts look cool
		if ( next > iMaxX )
			return xpos;

		TextMessageDrawChar( xpos, ypos, *szIt, r, g, b );
		xpos = next;		
	}

	return xpos;
}

int CHud :: UpdateClientData( client_data_t *cdata, float time )
{
	cdata->fov = m_iFOV;

	return 1;
}

int CHud::MsgFunc_Damage(const char *pszName, int iSize, void *pbuf )
{
	return m_Health.MsgFunc_Damage(  pszName,  iSize,  pbuf );
}
int CHud::MsgFunc_Health(const char *pszName,  int iSize, void *pbuf)
{
	return m_Health.MsgFunc_Health( pszName,   iSize,  pbuf);
}

CHudBase::CHudBase()
{
	m_bHasVidInit = false;
}

//  CHudHealth

CHudHealth::CHudHealth(void)
{
	m_pos.x = 7;
	m_pos.y = 569;

	m_iFlags = 0;

	m_iMaxHealth = 100;
	m_iMaxArmor = 100;

	m_iHealth = 0;
	m_iArmor = 0;

	//m_flBloodScreenEndTime = 0.0;
	//m_flBloodScreenStartTime = 0.0;
}


void CHudHealth::VidInit(void)
{
	if( m_bHasVidInit )
		return;

	m_bHasVidInit = true;

	m_iTgaId_BG = g_pDrawTGA->LoadTgaImage( "resource/tga/Hud_Health", true );
	m_iTgaId_Bar = g_pDrawTGA->LoadTgaImage( "resource/tga/Bar", false );

	m_iTgaId_BloodLes = g_pDrawTGA->LoadTgaImage( "resource/tga/hp25", true );
	m_iTgaId_BloodMuch = g_pDrawTGA->LoadTgaImage( "resource/tga/hp15", true );
}


void CHudHealth::Show(void)
{
	m_iFlags |= HUD_ACTIVE;
}

void CHudHealth::Hide(void)
{
	m_iFlags &= ~HUD_ACTIVE;
}


int CHudHealth:: MsgFunc_Health(const char *pszName,  int iSize, void *pbuf )
{
	// TODO: update local health data
	BEGIN_READ( pbuf, iSize );
	int x = READ_BYTE();

	
	//m_iFlags |= HUD_ACTIVE;

	
	// Only update the fade if we've changed health
	if (x != m_iHealth)
	{
		//m_fFade = FADE_TIME;
		m_iHealth = x;
	}

	if( g_bAlive )
		Show();

	return 1;
}

int CHudHealth:: MsgFunc_Damage(const char *pszName,  int iSize, void *pbuf )
{
	BEGIN_READ( pbuf, iSize );

	int armor = READ_BYTE();	// armor
	int damageTaken = READ_BYTE();	// health
	long bitsDamage = READ_LONG(); // damage bits

	vec3_t vecFrom;

	for ( int i = 0 ; i < 3 ; i++)
		vecFrom[i] = READ_COORD();

	// Actually took damage?
	if ( damageTaken > 0 || armor > 0 )
	{
		/*enum 
		{
			BLOOD_MUCH = 1,
			BLOOD_LITTLE,
		};

		if( damageTaken >= 40 )
		{
			m_iBloodScreenType = BLOOD_MUCH;
			m_flBloodScreenEndTime = g_flTime + BLOOD_FADE_TIME;
			m_flBloodScreenStartTime = g_flTime;
		}
		else if ( damageTaken > 15 && damageTaken < 40 )
		{
			if( m_iBloodScreenType == 0 )
				m_iBloodScreenType = BLOOD_LITTLE;

			m_flBloodScreenEndTime = g_flTime + BLOOD_FADE_TIME;
			m_flBloodScreenStartTime = g_flTime;
		}*/
		//CalcDamageDirection(vecFrom);
	}

	return 1;
}
void CHudHealth::SetMaxHealth ( int iMaxHealth)
{
	m_iMaxHealth = iMaxHealth;
}
void CHudHealth::SetMaxArmor( int iMaxArmor )
{
	m_iMaxArmor = iMaxArmor;
}

void CHudHealth::Draw( void )
{

	//if( !( m_iFlags & HUD_ACTIVE ))
		//return;
	if( !g_bAlive )
		return;

	if ( gEngfuncs.IsSpectateOnly() )
		return;

	g_pDrawTGA->DrawTGA( m_iTgaId_BG, AdjustSizeW( m_pos.x ), AdjustSizeH( m_pos.y  ), AdjustSizeW( 352 ), AdjustSizeH( 190 ));
	
	int iW, iH;
	int iR, iG, iB;

	iW = BAR_LENGTH * ( (float)m_iHealth ) / ( m_iMaxHealth );
	iH = BAR_HEIGHT;
	
	iR = iG = iB = 158;

	if( ( (float)m_iHealth ) / ( m_iMaxHealth )   <   0.15 )
	{
		iR = 255;
		iG =iB = 0;
	}

	g_pDrawTGA->DrawTGA( m_iTgaId_Bar, AdjustSizeW( HEALTH_BAR_X ), AdjustSizeH( HEALTH_BAR_Y ), AdjustSizeW( iW ), AdjustSizeH( iH ), iR, iG, iB, 255);

	iW = BAR_LENGTH * ( (float)m_iArmor ) / ( m_iMaxArmor );
	iH = BAR_HEIGHT;

	iR = iG = iB = 158;

	g_pDrawTGA->DrawTGA( m_iTgaId_Bar, AdjustSizeW( ARMOR_BAR_X ), AdjustSizeH( ARMOR_BAR_Y ), AdjustSizeW( iW ), AdjustSizeH( iH ), iR, iG, iB, 255);

	/*if( m_flBloodScreenEndTime && g_flTime > m_flBloodScreenEndTime )
	{
		m_flBloodScreenEndTime = 0.0;
		m_iBloodScreenType = 0;
	}
	else if( m_flBloodScreenEndTime && m_flBloodScreenEndTime >  g_flTime &&  m_iBloodScreenType )
	{
		enum 
		{
			BLOOD_MUCH = 1,
			BLOOD_LITTLE,
		};


		int iAlpha = (int) ( (m_flBloodScreenEndTime - g_flTime) / m_flBloodScreenStartTime) * 255;

		if( m_iBloodScreenType == BLOOD_LITTLE )
			DrawTGA_Alpha( g_MHTga[ m_iTgaId_BloodLes ].texid, 0 ,0, g_sScreenInfo.iWidth ,  g_sScreenInfo.iHeight , 255, 255, 255, iAlpha);
	
		else if( m_iBloodScreenType == BLOOD_MUCH )
			DrawTGA_Alpha( g_MHTga[ m_iTgaId_BloodMuch].texid, 0 ,0, g_sScreenInfo.iWidth ,g_sScreenInfo.iHeight , 255, 255, 255, iAlpha);
	}*/


	if( m_iHealth < 40 && m_iHealth >= 20 )
		g_pDrawTGA->DrawTGA( m_iTgaId_BloodLes, 0 ,0, g_sScreenInfo.iWidth ,  g_sScreenInfo.iHeight);

	else if( m_iHealth < 20 )
		g_pDrawTGA->DrawTGA( m_iTgaId_BloodMuch, 0 ,0, g_sScreenInfo.iWidth ,  g_sScreenInfo.iHeight);
	
	
	/*
	// Has health changed? Flash the health #
	if (m_fFade)
	{
		m_fFade -= (gHUD.m_flTimeDelta * 20);
		if (m_fFade <= 0)
		{
			a = MIN_ALPHA;
			m_fFade = 0;
		}

		// Fade the health number back to dim

		a = MIN_ALPHA +  (m_fFade/FADE_TIME) * 128;

	}
	else
		a = MIN_ALPHA;


	
	// If health is getting low, make it bright red
	if (m_iHealth <= 15)
		a = 255;
		
	GetPainColor( r, g, b );
	ScaleColors(r, g, b, a );

	// Only draw health if we have the suit.
	if (gHUD.m_iWeaponBits & (1<<(WEAPON_SUIT)))
	{
		HealthWidth = gHUD.GetSpriteRect(gHUD.m_HUD_number_0).right - gHUD.GetSpriteRect(gHUD.m_HUD_number_0).left;
		int CrossWidth = gHUD.GetSpriteRect(m_HUD_cross).right - gHUD.GetSpriteRect(m_HUD_cross).left;

		y = ScreenHeight - gHUD.m_iFontHeight - gHUD.m_iFontHeight / 2;
		x = CrossWidth /2;

		SPR_Set(gHUD.GetSprite(m_HUD_cross), r, g, b);
		SPR_DrawAdditive(0, x, y, &gHUD.GetSpriteRect(m_HUD_cross));

		x = CrossWidth + HealthWidth / 2;

		x = gHUD.DrawHudNumber(x, y, DHN_3DIGITS | DHN_DRAWZERO, m_iHealth, r, g, b);

		x += HealthWidth/2;

		int iHeight = gHUD.m_iFontHeight;
		int iWidth = HealthWidth/10;
		FillRGBA(x, y, iWidth, iHeight, 255, 160, 0, a);
	}

	DrawDamage(flTime);
	return DrawPain(flTime);*/


}







/*
void CHudHealth::CalcDamageDirection(vec3_t vecFrom)
{
	
	vec3_t	forward, right, up;
	float	side, front;
	vec3_t vecOrigin, vecAngles;

	if (!vecFrom[0] && !vecFrom[1] && !vecFrom[2])
	{
		m_fAttackFront = m_fAttackRear = m_fAttackRight = m_fAttackLeft = 0;
		return;
	}


	memcpy(vecOrigin, gHUD.m_vecOrigin, sizeof(vec3_t));
	memcpy(vecAngles, gHUD.m_vecAngles, sizeof(vec3_t));


	VectorSubtract (vecFrom, vecOrigin, vecFrom);

	float flDistToTarget = vecFrom.Length();

	vecFrom = vecFrom.Normalize();
	AngleVectors (vecAngles, forward, right, up);

	front = DotProduct (vecFrom, right);
	side = DotProduct (vecFrom, forward);

	if (flDistToTarget <= 50)
	{
		m_fAttackFront = m_fAttackRear = m_fAttackRight = m_fAttackLeft = 1;
	}
	else 
	{
		if (side > 0)
		{
			if (side > 0.3)
				m_fAttackFront = max(m_fAttackFront, side);
		}
		else
		{
			float f = fabs(side);
			if (f > 0.3)
				m_fAttackRear = max(m_fAttackRear, f);
		}

		if (front > 0)
		{
			if (front > 0.3)
				m_fAttackRight = max(m_fAttackRight, front);
		}
		else
		{
			float f = fabs(front);
			if (f > 0.3)
				m_fAttackLeft = max(m_fAttackLeft, f);
		}
	}
}

int CHudHealth::DrawPain(float flTime)
{
	if (!(m_fAttackFront || m_fAttackRear || m_fAttackLeft || m_fAttackRight))
		return 1;

	int r, g, b;
	int x, y, a, shade;

	// TODO:  get the shift value of the health
	a = 255;	// max brightness until then

	float fFade = gHUD.m_flTimeDelta * 2;
	
	// SPR_Draw top
	if (m_fAttackFront > 0.4)
	{
		GetPainColor(r,g,b);
		shade = a * max( m_fAttackFront, 0.5 );
		ScaleColors(r, g, b, shade);
		SPR_Set(m_hSprite, r, g, b );

		x = ScreenWidth/2 - SPR_Width(m_hSprite, 0)/2;
		y = ScreenHeight/2 - SPR_Height(m_hSprite,0) * 3;
		SPR_DrawAdditive(0, x, y, NULL);
		m_fAttackFront = max( 0, m_fAttackFront - fFade );
	} else
		m_fAttackFront = 0;

	if (m_fAttackRight > 0.4)
	{
		GetPainColor(r,g,b);
		shade = a * max( m_fAttackRight, 0.5 );
		ScaleColors(r, g, b, shade);
		SPR_Set(m_hSprite, r, g, b );

		x = ScreenWidth/2 + SPR_Width(m_hSprite, 1) * 2;
		y = ScreenHeight/2 - SPR_Height(m_hSprite,1)/2;
		SPR_DrawAdditive(1, x, y, NULL);
		m_fAttackRight = max( 0, m_fAttackRight - fFade );
	} else
		m_fAttackRight = 0;

	if (m_fAttackRear > 0.4)
	{
		GetPainColor(r,g,b);
		shade = a * max( m_fAttackRear, 0.5 );
		ScaleColors(r, g, b, shade);
		SPR_Set(m_hSprite, r, g, b );

		x = ScreenWidth/2 - SPR_Width(m_hSprite, 2)/2;
		y = ScreenHeight/2 + SPR_Height(m_hSprite,2) * 2;
		SPR_DrawAdditive(2, x, y, NULL);
		m_fAttackRear = max( 0, m_fAttackRear - fFade );
	} else
		m_fAttackRear = 0;

	if (m_fAttackLeft > 0.4)
	{
		GetPainColor(r,g,b);
		shade = a * max( m_fAttackLeft, 0.5 );
		ScaleColors(r, g, b, shade);
		SPR_Set(m_hSprite, r, g, b );

		x = ScreenWidth/2 - SPR_Width(m_hSprite, 3) * 3;
		y = ScreenHeight/2 - SPR_Height(m_hSprite,3)/2;
		SPR_DrawAdditive(3, x, y, NULL);

		m_fAttackLeft = max( 0, m_fAttackLeft - fFade );
	} else
		m_fAttackLeft = 0;

	return 1;
}

int CHudHealth::DrawDamage(float flTime)
{
	int r, g, b, a;
	DAMAGE_IMAGE *pdmg;

	if (!m_bitsDamage)
		return 1;

	UnpackRGB(r,g,b, RGB_YELLOWISH);
	
	a = (int)( fabs(sin(flTime*2)) * 256.0);

	ScaleColors(r, g, b, a);

	// Draw all the items
	for (int i = 0; i < NUM_DMG_TYPES; i++)
	{
		if (m_bitsDamage & giDmgFlags[i])
		{
			pdmg = &m_dmg[i];
			SPR_Set(gHUD.GetSprite(m_HUD_dmg_bio + i), r, g, b );
			SPR_DrawAdditive(0, pdmg->x, pdmg->y, &gHUD.GetSpriteRect(m_HUD_dmg_bio + i));
		}
	}


	// check for bits that should be expired
	for ( int i = 0; i < NUM_DMG_TYPES; i++ )
	{
		DAMAGE_IMAGE *pdmg = &m_dmg[i];

		if ( m_bitsDamage & giDmgFlags[i] )
		{
			pdmg->fExpire = min( flTime + DMG_IMAGE_LIFE, pdmg->fExpire );

			if ( pdmg->fExpire <= flTime		// when the time has expired
				&& a < 40 )						// and the flash is at the low point of the cycle
			{
				pdmg->fExpire = 0;

				int y = pdmg->y;
				pdmg->x = pdmg->y = 0;

				// move everyone above down
				for (int j = 0; j < NUM_DMG_TYPES; j++)
				{
					pdmg = &m_dmg[j];
					if ((pdmg->y) && (pdmg->y < y))
						pdmg->y += giDmgHeight;

				}

				m_bitsDamage &= ~giDmgFlags[i];  // clear the bits
			}
		}
	}

	return 1;
}
 */
CHudTimer::CHudTimer ()
{
}
void CHudTimer::SetRoundTime (int iRoundTime)
{
	m_iRoundTimeAmt = iRoundTime;

	if( m_iRoundTimeAmt < 0 )
		m_iRoundTimeAmt = 0;

	if(  !m_iRoundTimeAmt )
		return;

	m_iTimerNum1 = (iRoundTime /60) /10;
	m_iTimerNum2 = (iRoundTime /60) %10;
	m_iTimerNum3 = (iRoundTime %60) /10;
	m_iTimerNum4 = (iRoundTime %60) %10 ;
}

void CHudTimer::VidInit(void)
{
	if( m_bHasVidInit )
		return;

	m_bHasVidInit = true;

	m_iTgaId_BG = g_pDrawTGA->LoadTgaImage( "resource/tga/Pannel", false );

	for( int i = 0; i< 10; i++)
	{
		char szBuff[ MAX_PATH ];
		sprintf( szBuff, "resource/tga/time%d", i);
		m_iTgaId_Num[i] = g_pDrawTGA->LoadTgaImage( szBuff, false );
	}
}

void CHudTimer::Hide (void)
{
	m_iFlags &= ~HUD_ACTIVE;
}

void CHudTimer::Show (void)
{
	m_iFlags |= HUD_ACTIVE;
}

void CHudTimer::Draw (void)
{
	if( m_iFlags & HUD_ACTIVE )
		return;

	if( !g_bAlive )
		return;

	if( g_flTime > flNextSecondTime )
	{
		m_iRoundTimeAmt --;
		SetRoundTime( m_iRoundTimeAmt );
		flNextSecondTime = g_flTime + 1.0;
	}

	g_pDrawTGA->DrawTGA( m_iTgaId_BG, AdjustSizeW( 464 ), AdjustSizeH( 736 ), AdjustSizeW( 104 ), AdjustSizeH( 24 ), 0, 0, 0, 100);
	
	g_pDrawTGA->DrawTGA( m_iTgaId_Num[m_iTimerNum1], AdjustSizeW( 469 ), AdjustSizeH( 740 ), AdjustSizeW(13), AdjustSizeH(16) );
	g_pDrawTGA->DrawTGA( m_iTgaId_Num[m_iTimerNum2], AdjustSizeW( 490 ), AdjustSizeH( 740 ), AdjustSizeW(13), AdjustSizeH(16) );

	g_pDrawTGA->DrawTGA( m_iTgaId_Num[m_iTimerNum3], AdjustSizeW( 527 ), AdjustSizeH( 740 ), AdjustSizeW(13), AdjustSizeH(16) );
	g_pDrawTGA->DrawTGA( m_iTgaId_Num[m_iTimerNum4], AdjustSizeW( 548 ), AdjustSizeH( 740 ), AdjustSizeW(13), AdjustSizeH(16) );
}

CHudAmmo::CHudAmmo (void)
{
	m_iClip = 0;
	m_iBpAmmo = 0;
	m_iOldClip = 0;
	m_iOldBpAmmo = 0;

	m_flBlinkClipStartTime = 0;
	m_flBlinkBpAmmoStartTime = 0;
}

void CHudAmmo::VidInit( void )
{
	if( m_bHasVidInit )
		return;

	m_bHasVidInit = true;

	m_iTgaId_BG = g_pDrawTGA->LoadTgaImage( "resource/tga/Pannel", false );
}
void CHudAmmo::Draw(void)
{
	if( !( m_iFlags & HUD_ACTIVE ) )
		return;

	int iWpnId = g_pWeapon->GetCurrentWpn() ;
	if(  iWpnId < 1 ||  iWpnId >= MAX_WPN)
		return;

	int iX, iY, iW, iH;

	// Draw a Panel
	iW = PANEL_W;
	iH = NORMAL_FONT_H + 2 * 5;

	iX = 1024 - GAP_TO_SCREEN_BORDER - PANEL_W;
	iY = 768 - GAP_TO_SCREEN_BORDER + 5 - iH;

	g_pDrawTGA->DrawTGA( m_iTgaId_BG, AdjustSizeW( iX ), AdjustSizeH( iY ), AdjustSizeW( iW ), AdjustSizeH( iH ), 0, 0, 0, 100);
	
	// Draw Wpn Background image

	iH = WPN_IMAGE_H;
	iW = WPN_IMAGE_W;

	iX = 1024 - GAP_TO_SCREEN_BORDER - WPN_IMAGE_W - 5;
	iY = 768 - GAP_TO_SCREEN_BORDER - iH;

	int iTgaId =  g_WpnItemInfo[  g_pWeapon->GetCurrentWpn()  ].iWpnTgaId;
	g_pDrawTGA->DrawTGA( iTgaId,  AdjustSizeW( iX ), AdjustSizeH( iY  ), AdjustSizeW( iW ), AdjustSizeH( iH ) );

	if( g_WpnItemInfo[ iWpnId].szClassname && !strcmp( g_WpnItemInfo[ iWpnId ].szClassname, "weapon_knife") )
		return;

	// Draw ammo
	char szClip[16] , szBpAmmo[16];
	
	SetAmmo(g_pWeapon->m_iClip, g_pWeapon->m_iBpammo);
	itoa( g_pWeapon->m_iClip , szClip, 10);
	itoa( g_pWeapon->m_iBpammo , szBpAmmo, 10 );

	int iTextLen = 0;

	iY = 768 - GAP_TO_SCREEN_BORDER;

	wchar_t *pTextBpAmmo = UTF8ToUnicode( szBpAmmo );

	if( gHUD.m_flTime < m_flBlinkBpAmmoStartTime + AMMO_BLINK_TIME )
	{
		iTextLen = Fonts_GetLen( pTextBpAmmo, BLINK_FONT_W );
		iX = iX - iTextLen - 10;
		DrawFonts( pTextBpAmmo, AdjustSizeW( iX ), AdjustSizeH( iY ), 255,255,255,255, AdjustSizeW( BLINK_FONT_W ), AdjustSizeH( BLINK_FONT_H ), 1000, 1000);
	}
	else
	{
		iTextLen = Fonts_GetLen( pTextBpAmmo, NORMAL_FONT_W );
		iX = iX - iTextLen - 10;
		DrawFonts( pTextBpAmmo, AdjustSizeW( iX ), AdjustSizeH( iY ), 255,255,255,255, AdjustSizeW( NORMAL_FONT_W ), AdjustSizeH( NORMAL_FONT_H ), 1000, 1000);
	}

	if( !strcmp( g_WpnItemInfo[ iWpnId ].szClassname, "weapon_hegrenade") ||  !strcmp( g_WpnItemInfo[ iWpnId ].szClassname, "weapon_flashbang") ||  !strcmp( g_WpnItemInfo[ iWpnId ].szClassname, "weapon_smokegrenade")  )
		return;

	wchar_t *pTextBuff = UTF8ToUnicode(  "/" );
	
	iTextLen = Fonts_GetLen( pTextBuff, NORMAL_FONT_W );

	iX = iX - iTextLen;
	DrawFonts( pTextBuff, AdjustSizeW( iX ), AdjustSizeH( iY ), 255,255,255,255, AdjustSizeW( NORMAL_FONT_W ), AdjustSizeH( NORMAL_FONT_H ), 1000, 1000);
	
	wchar_t *pTextClip = UTF8ToUnicode( szClip );

	if( gHUD.m_flTime < m_flBlinkClipStartTime + AMMO_BLINK_TIME )
	{
		iTextLen = Fonts_GetLen( pTextClip, BLINK_FONT_W );

		iX = iX - iTextLen;
		DrawFonts( pTextClip, AdjustSizeW( iX ), AdjustSizeH( iY ), 255,255,255,255, AdjustSizeW( BLINK_FONT_W ), AdjustSizeH( BLINK_FONT_H ), 1000, 1000);
	}
	else
	{
		iTextLen = Fonts_GetLen( pTextClip, NORMAL_FONT_W );

		iX = iX - iTextLen;
		DrawFonts( pTextClip, AdjustSizeW( iX ), AdjustSizeH( iY ), 255,255,255,255, AdjustSizeW( NORMAL_FONT_W ), AdjustSizeH( NORMAL_FONT_H ), 1000, 1000);
	}
}

void CHudAmmo::Show(void)
{
	m_iFlags |= HUD_ACTIVE;
}
void CHudAmmo::Hide( void )
{
	m_iFlags &= ~HUD_ACTIVE;
}
void CHudAmmo::SetAmmo(int iClip, int iBpAmmo)
{
	if( m_iClip != iClip )
	{
		m_iOldClip = m_iClip;
		m_iClip = iClip;

		m_flBlinkClipStartTime = gHUD.m_flTime ;
	}

	if( m_iBpAmmo != iBpAmmo )
	{
		m_iOldBpAmmo = m_iBpAmmo;
		m_iBpAmmo = iBpAmmo;

		m_flBlinkBpAmmoStartTime = gHUD.m_flTime ;
	}

}
CHudSuvModeUi::CHudSuvModeUi()
{
	m_iFlags &= ~HUD_ACTIVE;
	m_iKillNum = 0;
	m_iRoundNum = 0;
	m_iCountDownEndTime = 0.0;
	m_flShowNoticeEndTime = 0.0;
}
void CHudSuvModeUi::VidInit(void)
{
	if( m_bHasVidInit )
		return;

	m_bHasVidInit = true;

	m_iTgaId_SB = g_pDrawTGA->LoadTgaImage( "resource/tga/Suv_SB", true );
	for( int i = 0; i< 10; i++)
	{
		char szBuff[64];
		sprintf( szBuff, "resource/tga/time%d", i);
		m_iTgaId_Num[i] = g_pDrawTGA->LoadTgaImage( szBuff, false );
	}

	for( int i = 1; i< 10; i++)
	{
		char szBuff[64];
		sprintf( szBuff, "resource/tga/survive_%d", i);
		m_iTgaId_CD_Num[i] = g_pDrawTGA->LoadTgaImage( szBuff, false );
	}

	m_iTgaId_CD_BG =  g_pDrawTGA->LoadTgaImage( "resource/tga/suv_notice", true );

	m_iTgaId_Win = g_pDrawTGA->LoadTgaImage( "resource/tga/survive", true );
	m_iTgaId_Lost = g_pDrawTGA->LoadTgaImage( "resource/tga/lost", true );
}
void CHudSuvModeUi::Show(void)
{
	m_iFlags |= HUD_ACTIVE;
}
void CHudSuvModeUi::Hide(void)
{
	m_iFlags &= ~HUD_ACTIVE;
}
void CHudSuvModeUi::DrawCountDown(void)
{
	if( !m_iCountDownEndTime )
		return;

	if( g_flTime > m_iCountDownEndTime )
		m_iCountDownEndTime = 0;

	int iTime = m_iCountDownEndTime - g_flTime;
	if( iTime > 9 || iTime <= 0 )
		return;

	g_pDrawTGA->DrawTGA( m_iTgaId_CD_BG, AdjustSizeW(176), AdjustSizeH(304), AdjustSizeW(662), AdjustSizeH(36));
	g_pDrawTGA->DrawTGA( m_iTgaId_CD_Num[iTime],  AdjustSizeW(431), AdjustSizeH(192), AdjustSizeW(169), AdjustSizeH(114));
}
void CHudSuvModeUi::Reset(void)
{
	m_iCountDownEndTime = 0;
}
void CHudSuvModeUi::DrawRoundEnd(void)
{
	if( !m_flShowNoticeEndTime )
		return;

	if( g_flTime > m_flShowNoticeEndTime )
	{
		m_flShowNoticeEndTime = 0.0;
		return;
	}
	int iTgaId = m_bWin ? m_iTgaId_Win : m_iTgaId_Lost;

	float flBlinkTime = m_flShowNoticeEndTime - g_flTime - SHOW_TIME;

	if( flBlinkTime > 0 )
	{
		int iW = (   flBlinkTime / BLINK_TIME + 1 ) * NOTICE_W;
		int iH = (   flBlinkTime / BLINK_TIME + 1 ) * NOTICE_H;

		int iX = CENTRE_XPOS - iW / 2;
		int iY = CENTRE_YPOS - iH / 2;

		g_pDrawTGA->DrawTGA( iTgaId, AdjustSizeW(iX), AdjustSizeH(iY), AdjustSizeW(iW), AdjustSizeH(iH));
	}
	else if( flBlinkTime <= 0)
	{
		int iW = NOTICE_W;
		int iH = NOTICE_H;

		int iX = CENTRE_XPOS - iW / 2;
		int iY = CENTRE_YPOS - iH / 2;

		g_pDrawTGA->DrawTGA( iTgaId, AdjustSizeW(iX), AdjustSizeH(iY), AdjustSizeW(iW), AdjustSizeH(iH));
	}
	
}
void CHudSuvModeUi::SetRoundEnd( bool bWin )
{
	m_bWin = bWin;
	m_flShowNoticeEndTime = g_flTime + BLINK_TIME + SHOW_TIME ;
}
void CHudSuvModeUi::Draw(void)
{
	if( !(m_iFlags & HUD_ACTIVE) )
		return;

	DrawCountDown();
	DrawRoundEnd();

	if( !g_bAlive )
		return;

	g_pDrawTGA->DrawTGA( m_iTgaId_SB , AdjustSizeW(378), 0, AdjustSizeW(268), AdjustSizeH(67));

	int iNum_Round1 = m_iRoundNum / 10;
	int iNum_Round2 = m_iRoundNum % 10;

	int iKillNum1 = m_iKillNum / 10000;
	int iKillNum2 = m_iKillNum % 10000 / 1000;
	int iKillNum3 = m_iKillNum % 10000 % 1000 /100;
	int iKillNum4 = m_iKillNum % 10000 % 1000 % 100 /10;
	int iKillNum5 = m_iKillNum % 10000 % 1000 % 100 % 10;

	g_pDrawTGA->DrawTGA( m_iTgaId_Num[iNum_Round1] , AdjustSizeW(397), AdjustSizeH(30), AdjustSizeW(22), AdjustSizeH(25));
	g_pDrawTGA->DrawTGA( m_iTgaId_Num[iNum_Round2] , AdjustSizeW(422), AdjustSizeH(30), AdjustSizeW(22), AdjustSizeH(25));
	g_pDrawTGA->DrawTGA( m_iTgaId_Num[iKillNum1] , AdjustSizeW(521), AdjustSizeH(21), AdjustSizeW(22), AdjustSizeH(25));
	g_pDrawTGA->DrawTGA( m_iTgaId_Num[iKillNum2] , AdjustSizeW(545), AdjustSizeH(21), AdjustSizeW(22), AdjustSizeH(25));
	g_pDrawTGA->DrawTGA( m_iTgaId_Num[iKillNum3] , AdjustSizeW(569), AdjustSizeH(21), AdjustSizeW(22), AdjustSizeH(25));
	g_pDrawTGA->DrawTGA( m_iTgaId_Num[iKillNum4] , AdjustSizeW(593), AdjustSizeH(21), AdjustSizeW(22), AdjustSizeH(25));
	g_pDrawTGA->DrawTGA( m_iTgaId_Num[iKillNum5] , AdjustSizeW(617), AdjustSizeH(21), AdjustSizeW(22), AdjustSizeH(25));

}
void CHudSuvModeUi::UpdateClientDataKills(  int iKill )
{
		m_iKillNum = iKill;
}
void CHudSuvModeUi::UpdateClientDataRounds( int iRounds) 
{
	m_iRoundNum = iRounds;
}
void CHudSuvModeUi::SetCountDown( int iSec )
{
	m_iCountDownEndTime = g_flTime + iSec + 0.2;
}



CHudNvg::CHudNvg()
{
}

void CHudNvg::VidInit (void)
{
	if( !m_bHasVidInit )
	{
		m_bHasVidInit = true;
		m_iTgaId_BG = g_pDrawTGA->LoadTgaImage( "resource/tga/nvg_bg", true );
	}

	Hide();
}

void CHudNvg::Draw (void)
{
	if (!(m_iFlags & HUD_ACTIVE))
		return;

	g_pDrawTGA->DrawTGA( m_iTgaId_BG, 0, 0, g_sScreenInfo.iWidth , g_sScreenInfo.iHeight );
}
void CHudNvg::Hide (void)
{
	m_iFlags &= ~HUD_ACTIVE;

	static screenfade_t sf;
	sf.fader = 0;
	sf.fadeg = 0;
	sf.fadeb = 0;
	sf.fadealpha = 0;
	gEngfuncs.pfnSetScreenFade( &sf ); 
}
void CHudNvg::Show (void)
{
	m_iFlags |= HUD_ACTIVE;

	static screenfade_t sf;
	sf.fader = 0;
	sf.fadeg = 255;
	sf.fadeb = 0;//255;
	sf.fadealpha = 80;
	sf.fadeFlags = FFADE_STAYOUT | FFADE_OUT;
	gEngfuncs.pfnSetScreenFade( &sf ); 
}