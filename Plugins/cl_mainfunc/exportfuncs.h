#ifndef _EXPORTFUNCS_H
#define _EXPORTFUNCS_H



extern cl_enginefunc_t gEngfuncs;

// redraw time
extern float g_flTime, g_fOldTime;

// video info
extern engine_studio_api_t IEngineStudio;
extern SCREENINFO g_sScreenInfo;
extern bool g_bIsWidescreen;

// Player info
struct PlayerInfo
{
	int alive;
	cl_entity_s* info;
	float killtime;
	int team;
	float fZBTime;
	vec3_t vLastPos;
	int iHero;
	float fAudioTime;
	float fDeathTime;
	float fInfectTime;
};
extern struct PlayerInfo vPlayer[36];
extern bool g_bAlive;



#endif