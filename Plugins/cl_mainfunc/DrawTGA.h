#ifndef _DRAW_TGA_H
#define _DRAW_TGA_H

#define MH_MAX_TGA		600


typedef struct
{
	char *pszName;
	int index;
}
TgaInfo_t;




class CDrawTGA
{
public:
	CDrawTGA();
	~CDrawTGA(){}

public :
	void VidInit(void);
	

	int LoadTgaImage( char *filename , bool bLargeSize );
	void DrawTGA( int iTextureId, int iX, int iY, int iW , int iH);
	void DrawTGA( int iTextureId, int iX, int iY, int iW , int iH, int r, int b, int g , int iAlpha);
	int GetIdByName( char *pszName );

private:
	void LoadList(void);
	
private:
	TgaInfo_t						m_TgaInfo[ MH_MAX_TGA ];
	bool							m_bHasVidInit;

};

extern CDrawTGA *g_pDrawTGA;

#endif