#ifndef _VIEW_H
#define _VIEW_H

void View_InstallHook(void);
void View_V_CalcRefdef(struct ref_params_s *pParams);

extern int g_iViewEntityBody, g_iViewEntityRenderMode, g_iViewEntityRenderFX, g_iViewEntityRenderAmout;
extern color24 g_byViewEntityRenderColor;
extern int g_iSetupSkin;

#endif