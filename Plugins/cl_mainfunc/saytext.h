#ifndef _SAYTEXT_H
#define _SAYTEXT_H

#define SAYTEXT_SCROLL_TIME					5.0
#define SAYTEXT_FONT_SIZE_W					12
#define SAYTEXT_FONT_SIZE_H					12
#define SAYTEXT_LINE_FADE_TIME				1.0
#define SAYTEXT_TEXT_GAP					3

#define SAYTEXT_TOP_Y						600
#define SAYTEXT_GAP_Y						12


#endif