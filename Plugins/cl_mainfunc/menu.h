#ifndef _MENU_H
#define _MENU_H


#define FONTSIZE_W		13
#define FONTSIZE_H		13
#define BG_BORDER		25
#define BG_TOP_BORDER	20
#define TEXT_GAP		6


int KB_ConvertString( char *in, char **ppout );

#endif