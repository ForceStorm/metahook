#ifndef DEATHMSG_H
#define DEATHMSG_H

#define MAX_DEATHNOTICES	5
#define MAX_PLAYER_NAME_LENGTH 32
#define DEATHNOTICE_TOP		32

#define TEAM_CT		1
#define TEAM_T		2

struct DeathNoticeItem {
	char szKiller[32];
	char szVictim[32];
	char szWpn[32];
	bool bTakenUp;
	int iSuicide;
	int iNonPlayerKill;
	float flDisplayTime;
	int Killer;
	int Victim;
	int iHeadShot;
	int iLocal;
};

void DeathMsg_Init(void);
int MsgFunc_DeathMsg(const char *pszName, int iSize, void *pbuf);
int DeathMsgRedraw( float flTime );


#endif