#include "base.h"
#include <event_api.h>
#include <r_efx.h>
#include <pm_materials.h>
#include "pm_defs.h"
#include "events.h"
#include "weapon.h"
#include "client_hook.h"
#include "hud.h"
#include "msghook.h"
#include "effects.h"


typedef enum
{
	BULLET_NONE = 0,
	BULLET_PLAYER_9MM,
	BULLET_PLAYER_MP5,
	BULLET_PLAYER_357,
	BULLET_PLAYER_BUCKSHOT,
	BULLET_PLAYER_CROWBAR,

	BULLET_MONSTER_9MM,
	BULLET_MONSTER_MP5,
	BULLET_MONSTER_12MM,

	BULLET_PLAYER_45ACP,
	BULLET_PLAYER_338MAG,
	BULLET_PLAYER_762MM,
	BULLET_PLAYER_556MM,
	BULLET_PLAYER_50AE,
	BULLET_PLAYER_57MM,
	BULLET_PLAYER_357SIG,
	BULLET_RPG,
	BULLET_M79,
	BULLET_M32
}
Bullet;

int g_iShotsFired;
int g_iAnim = 0;
extern cvar_t *cl_righthand;
extern cvar_t *cl_decaleffect;

vector_t *g_vRpgTail = NULL;


// Function Declarations
void EV_Incendiary( event_args_t *args );
void EV_RealExplode( event_args_t *args );
void EV_RocketExplode( event_args_t *args );
void EV_BoomerExplode( event_args_t *args );
void EV_SmokeProduce( event_args_t *args );
void EV_FireRifle( event_args_t *args );
void EV_FireRocket( event_args_t *args );
void EV_FireEliteLeft(struct event_args_s *args);
void EV_FireEliteRight(struct event_args_s *args);
void EV_FireFamas(struct event_args_s *args);
void EV_FireGlock18(struct event_args_s *args);
void EV_FireM32(struct event_args_s *args);
void EV_FireM4A1(struct event_args_s *args);
void EV_FirePistol(struct event_args_s *args);
void EV_FireShotgun(struct event_args_s *args);
void EV_FireSniper(struct event_args_s *args);
void EV_FireSniper2(struct event_args_s *args);
void EV_FireUSP(struct event_args_s *args);

void EV_HLDM_CreateSmoke(float *origin, float *dir, int speed, float scale, int r, int g, int b, int iSmokeType, float *base_velocity, bool bWind, int framerate);
void EV_GetDefaultShellInfo(event_args_t *args, float *origin, float *velocity, float *ShellVelocity, float *ShellOrigin, float *forward, float *right, float *up, float forwardScale, float upScale, float rightScale, bool bReverseDirection);
void EV_EjectBrass(float *origin, float *velocity, float rotation, int model, int soundtype, int idx, int angle_velocity);
void EV_HLDM_FireBullets(int idx, float *forward, float *right, float *up, int cShots, float *vecSrc, float *vecDirShooting, float *vecSpread, float flDistance, int iBulletType, int iTracerFreq, int *tracerCount, int iPenetration);
void EV_HLDM_DecalGunshot( pmtrace_t *pTrace, int iBulletType, float scale, int r, int g, int b, bool bCreateSparks, char cTextureType );

void (*g_pfnEV_HLDM_FireBullets)(int idx, float *forward, float *right, float *up, int cShots, float *vecSrc, float *vecDirShooting, float *vecSpread, float flDistance, int iBulletType, int iTracerFreq, int *tracerCount, int iPenetration) = (void (*)(int, float *, float *, float *, int, float *, float *, float *, float, int, int, int *, int))0x1902460;
void (*g_pfnEV_EjectBrass)(float *origin, float *velocity, float rotation, int model, int soundtype, int idx, int angle_velocity) = (void (*)(float *, float *, float, int, int, int, int))0x1945180;
void (*g_pfnEV_HLDM_DecalGunshot)(pmtrace_t *pTrace, int iBulletType, float scale, int r, int g, int b, bool bCreateSparks, char cTextureType) = (void (*)(pmtrace_t *, int, float, int, int, int, bool, char))0x19020B0;
void (*g_pfnEV_HLDM_CreateSmoke)(float *origin, float *dir, int speed, float scale, int r, int g, int b, int iSmokeType, float *base_velocity, bool bWind, int framerate) = (void (*)(float *, float *, int, float, int, int, int, int, float *, bool, int))0x19017D0;
void (*g_pfnEV_GetDefaultShellInfo)(event_args_t *args, float *origin, float *velocity, float *ShellVelocity, float *ShellOrigin, float *forward, float *right, float *up, float forwardScale, float upScale, float rightScale, bool bReverseDirection) = (void (*)(event_args_t *, float *, float *, float *, float *, float *, float *, float *, float, float, float, bool))0x1945260;
int  (*g_pfnEV_IsLocal)(int) = (int (*)(int))0x1945040;
int  (*g_pfnEV_GetGunPosition)(event_args_t *args, float *pos, float *origin ) = (int (*)(event_args_t *args, float *pos, float *origin ))0x1945090;

void EV_HookEvents( void )
{
	gEngfuncs.pfnHookEvent( "events/incendiary.sc",				EV_Incendiary );
	gEngfuncs.pfnHookEvent( "events/realexplode.sc",			EV_RealExplode );
	gEngfuncs.pfnHookEvent( "events/rocketexplode.sc",			EV_RocketExplode );
	gEngfuncs.pfnHookEvent( "events/boomer_explode.sc",			EV_BoomerExplode );
	gEngfuncs.pfnHookEvent( "events/smokeproduce.sc",			EV_SmokeProduce );
	gEngfuncs.pfnHookEvent( "events/rifle.sc",					EV_FireRifle );
	gEngfuncs.pfnHookEvent( "events/rocketfire.sc",				EV_FireRocket );
	gEngfuncs.pfnHookEvent("events/elite_left.sc",				EV_FireEliteLeft);
	gEngfuncs.pfnHookEvent("events/elite_right.sc",				EV_FireEliteRight);
	gEngfuncs.pfnHookEvent("events/famas.sc",					EV_FireFamas);
	gEngfuncs.pfnHookEvent("events/glock18.sc",					EV_FireGlock18);
	gEngfuncs.pfnHookEvent("events/m32.sc",						EV_FireM32);
	gEngfuncs.pfnHookEvent("events/m4a1.sc",					EV_FireM4A1);
	gEngfuncs.pfnHookEvent("events/pistol.sc",					EV_FirePistol);
	gEngfuncs.pfnHookEvent("events/shotgun.sc",					EV_FireShotgun);
	gEngfuncs.pfnHookEvent("events/sniper.sc",					EV_FireSniper);
	gEngfuncs.pfnHookEvent("events/sniper2.sc",					EV_FireSniper2);
	gEngfuncs.pfnHookEvent("events/usp.sc",						EV_FireUSP);
}

void EV_Init( void )
{
	g_pMetaHookAPI->InlineHook( (void *)g_pfnEV_HLDM_DecalGunshot, EV_HLDM_DecalGunshot, (void *&)g_pfnEV_HLDM_DecalGunshot);
}


// ------------------------------ Util ---------------------------------------------
#define random_num (*gEngfuncs.pfnRandomLong)
#define random_float (*gEngfuncs.pfnRandomFloat)
#define GetViewEntity (*gEngfuncs.GetViewModel)
extern void GetAimOrigin( float forw, float right, float up, struct cl_entity_s *pEnt, vec3_t &vecReturn);


void ProduceSmoke(vec3_t vecOrigin, int size, int deep)
{
	TEMPENTITY *entSmoke = gEngfuncs.pEfxAPI->R_DefaultSprite (vecOrigin, gEngfuncs.pEventAPI->EV_FindModelIndex("sprites/exsmoke.spr"), 10.5);
	entSmoke->entity.curstate.rendermode = kRenderTransAdd;
	entSmoke->entity.curstate .renderamt = 65;
	entSmoke->entity.curstate .scale = size;
	entSmoke->die = gEngfuncs.GetClientTime() + 4.0;
}

void get_spherical_coord( vec3_t vecOrigin, float flRadius, float flLevelAngle, float flVerticalAngle, vec3_t & vecReturn)
{
	float flLength;
	flLength = flRadius * cos(flVerticalAngle * M_PI / 180.0);
	vecReturn[0] = vecOrigin[0] + flLength * cos(flLevelAngle * M_PI / 180.0);
	vecReturn[1] = vecOrigin[1] + flLength * sin(flLevelAngle * M_PI / 180.0);
	vecReturn[2] = vecOrigin[2] + flLength * sin(flVerticalAngle * M_PI / 180.0);
}


void get_speed_vector(vec3_t origin1, vec3_t origin2, float speed, vec3_t &new_velocity)
{
	VectorSubtract(origin2, origin1, new_velocity);
	float num = sqrt(speed*speed / (new_velocity[0]*new_velocity[0] + new_velocity[1]*new_velocity[1] + new_velocity[2]*new_velocity[2]));
	VectorScale(new_velocity, num, new_velocity);
	return ;
}

void MakeDebris(vec3_t vecOrigin, vec3_t vecVelocity, short iModelIndex)
{
	vec3_t vSize;
	vSize[0] = vSize[1] = vSize[2] = 1.0;
	gEngfuncs.pEfxAPI->R_BreakModel( vecOrigin, vSize, vecVelocity, 10, 5, 10, iModelIndex, 0x40 );
}

void make_smoke2( vec3_t vecOrigin, short iSpriteIndex, int size, int light)
{
	gEngfuncs.pEfxAPI->R_Explosion( vecOrigin, iSpriteIndex, size, 8.5, (TE_EXPLFLAG_NODLIGHTS | TE_EXPLFLAG_NOSOUND | TE_EXPLFLAG_NOPARTICLES) );
}

void make_smoke( vec3_t vecOrigin, short iSpriteIndex, int size, int light)
{
	TEMPENTITY *entSmoke = gEngfuncs.pEfxAPI->R_DefaultSprite ( vecOrigin, iSpriteIndex, 8.0);
	entSmoke->entity.curstate.rendermode = kRenderTransAdd;
	entSmoke->entity.curstate .renderamt = light;
	entSmoke->entity.curstate .scale = size;
	entSmoke->die = gEngfuncs.GetClientTime() + 4.0;
}

void makegibs( vec3_t origin_end, vec3_t vnormal, float damage, int minnum, int maxnum, int gibs, float minspeed, float maxspeed)
{
	int counst = gEngfuncs.pfnRandomLong(minnum, maxnum);

	for(int i = 0; i<=counst; i++)
	{
		float speed = max(min(damage*5.0, minspeed), maxspeed);
		vec3_t vRandom;
		for(int j=0; i<3; j++)
			vRandom[j] = random_float(0.5, 1.5);
		vec3_t vnormal2, velocity, vSize;
		VectorAdd(vRandom, vnormal, vnormal2);
		VectorScale(vnormal2, speed * 0.15 *random_float(0.8, 1.8), velocity);
		vSize[0] = vSize[1] = vSize[2] = 0.1;
		gEngfuncs.pEfxAPI->R_BreakModel( origin_end, vSize, velocity, 7, 1, 6, gibs, 0 );
	}
}

void normaleffect( vec3_t origin_end, vec3_t vnormal , char cTextureType)
{

	vec3_t fOrigin[7], vtmp;

	VectorScale(vnormal, 2, vtmp);
	VectorAdd(origin_end, vtmp, fOrigin[0]);
	VectorScale(vnormal, 5, vtmp);
	VectorAdd(origin_end, vtmp, fOrigin[1]);
	VectorScale(vnormal, 10, vtmp);
	VectorAdd(origin_end, vtmp, fOrigin[2]);
	VectorScale(vnormal, 15, vtmp);
	VectorAdd(origin_end, vtmp, fOrigin[3]);
	VectorScale(vnormal, 25, vtmp);
	VectorAdd(origin_end, vtmp, fOrigin[4]);
	VectorScale(vnormal, 38, vtmp);
	VectorAdd(origin_end, vtmp, fOrigin[5]);
	VectorScale(vnormal, 40, vtmp);
	VectorAdd(origin_end, vtmp, fOrigin[6]);

	for(int i=0; i<7; i++)
	{
		int imodel = 0;
		switch(random_num(0, 3))
		{
		case 0:imodel = gEngfuncs.pEventAPI->EV_FindModelIndex("sprites/smokeeffect.spr");break;
		case 1:imodel = gEngfuncs.pEventAPI->EV_FindModelIndex("sprites/smokeeffect2.spr");break;
		case 2:imodel = gEngfuncs.pEventAPI->EV_FindModelIndex("sprites/smokeeffect3.spr");break;
		case 3:imodel = gEngfuncs.pEventAPI->EV_FindModelIndex("sprites/smokeeffect4.spr");break;
		}

		TEMPENTITY *entSmoke = gEngfuncs.pEfxAPI->R_DefaultSprite ( fOrigin[i], imodel, 8.5);
		entSmoke->entity.curstate.rendermode = kRenderTransAdd;
		entSmoke->entity.curstate .renderamt = 5.28571428571429 *(8-i);
		entSmoke->entity.curstate .scale = (i+1)*0.1428571428571429;
		entSmoke->die = gEngfuncs.GetClientTime() + 4.0;

		if(cTextureType == CHAR_TEX_WOOD)
		{
			color24 color;
			color.r = 185;
			color.g = 122;
			color.b = 87;
			entSmoke->entity.curstate.rendercolor = color;
		}
		else
		{
			color24 color;
			int i = random_num(190, 255);
			color.r = i;
			color.g = i;
			color.b = i;
			entSmoke->entity.curstate.rendercolor = color;
		}

	}

	gEngfuncs.pEfxAPI->R_SparkShower(origin_end);
}

void metaleffect( vec3_t origin_end, vec3_t vNormal , float damage)
{
	float value = min(max((int)damage * 2, 120), 240);
	gEngfuncs.pEfxAPI->R_StreakSplash( origin_end, vNormal, 10, 100, value, value*0.75, value*1.75 );

	dlight_t *dl = gEngfuncs.pEfxAPI->CL_AllocDlight (0);
	VectorCopy(origin_end, dl->origin);
	dl->radius = 4;
	dl->color.r = 255;
	dl->color.g = 120;
	dl->color.b = 100;
	dl->die = gEngfuncs.GetClientTime () + 0.01;
}

int GetYaw(event_args_t *a1)
{
	// copy from the IDA decompile source
	float v56 = *(float *)(a1 + 24) + (double)*(signed int *)(a1 + 60) * 0.01;
	return (int)v56;
}

// ===================================================================================================================================

void EV_HLDM_CreateSmoke(float *origin, float *dir, int speed, float scale, int r, int g, int b, int iSmokeType, float *base_velocity, bool bWind, int framerate)
{
	if(gHUD.m_iFOV > 55.0)
	{
		//for(int i=0; i<2; i++)
			g_pfnEV_HLDM_CreateSmoke(origin, dir, speed, scale, r, g, b, iSmokeType, base_velocity, bWind, framerate);
	}
}
void EV_GetDefaultShellInfo(event_args_t *args, float *origin, float *velocity, float *ShellVelocity, float *ShellOrigin, float *forward, float *right, float *up, float forwardScale, float upScale, float rightScale, bool bReverseDirection)
{
	g_pfnEV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, forwardScale, upScale, rightScale, bReverseDirection);
}
void EV_EjectBrass(float *origin, float *velocity, float rotation, int model, int soundtype, int idx, int angle_velocity)
{
	g_pfnEV_EjectBrass(origin, velocity, rotation, model, soundtype, idx, angle_velocity);
}
void EV_HLDM_FireBullets(int idx, float *forward, float *right, float *up, int cShots, float *vecSrc, float *vecDirShooting, float *vecSpread, float flDistance, int iBulletType, int iTracerFreq, int *tracerCount, int iPenetration)
{
	g_pfnEV_HLDM_FireBullets(idx, forward, right, up, cShots, vecSrc, vecDirShooting, vecSpread, flDistance, iBulletType, iTracerFreq, tracerCount, iPenetration);
}
int EV_IsLocal(int idx)
{
	return g_pfnEV_IsLocal(idx);
}
int EV_GetGunPosition( event_args_t *args, float *pos, float *origin )
{
	return g_pfnEV_GetGunPosition(args, pos, origin);
}

void EV_MuzzleFlash(void)
{
	cl_entity_t *ent = gEngfuncs.GetViewModel ();

	if (!ent)
		return;

	ent->curstate.effects |= EF_MUZZLEFLASH;
}

void EV_DecalEffect(pmtrace_t *pTrace, char cTextureType)
{
	float damage = 40;
	int bEntity = 0;

	vec3_t endpos;
	VectorCopy( pTrace->endpos, endpos);
	vec3_t angles, start, end, vNormal, vNormal2;

	VectorCopy( pTrace->plane.normal, vNormal);

	int gibs[5];
	gibs[0] = gEngfuncs.pEventAPI->EV_FindModelIndex( "models/gibs.mdl");
	gibs[1] = gEngfuncs.pEventAPI->EV_FindModelIndex("models/gibs2.mdl");
	gibs[2] = gEngfuncs.pEventAPI->EV_FindModelIndex("models/gibs3.mdl");
	gibs[3] = gEngfuncs.pEventAPI->EV_FindModelIndex("models/gibs4.mdl");
	gibs[4] = gEngfuncs.pEventAPI->EV_FindModelIndex( "models/gibs5.mdl");

	switch(cTextureType)
	{
	case CHAR_TEX_CONCRETE: makegibs(endpos, vNormal, damage, 5, 10, gibs[2], 100.0, 230.0);break;
	case CHAR_TEX_DIRT: makegibs(endpos, vNormal, damage, 5, 10, gibs[2], 100.0, 230.0);break;
	case CHAR_TEX_GRATE: makegibs(endpos, vNormal, damage, 5, 10, gibs[2], 100.0, 230.0);break;
	case CHAR_TEX_TILE: makegibs(endpos, vNormal, damage, 5, 10, gibs[2], 100.0, 230.0);break;
	case CHAR_TEX_METAL: metaleffect(endpos, vNormal, damage);return;
	case CHAR_TEX_VENT: metaleffect(endpos, vNormal, damage);return;
	case CHAR_TEX_COMPUTER: metaleffect(endpos, vNormal, damage);return;
	case CHAR_TEX_SLOSH: makegibs(endpos, vNormal, damage, 5, 10, gibs[1], 100.0, 230.0);break;
	case CHAR_TEX_WOOD: makegibs(endpos, vNormal, damage, 5, 10, gibs[0], 100.0, 230.0);break;
	case CHAR_TEX_GLASS: makegibs(endpos, vNormal, damage, 5, 10, gibs[3], 100.0, 230.0);break;
	case 'N':makegibs(endpos, vNormal, damage, 5, 10, gibs[1], 100.0, 230.0);break;
	case 'X':makegibs(endpos, angles, damage, 5, 10, gibs[4], 100.0, 230.0);break;
	}

	normaleffect(endpos, vNormal, cTextureType);
}


void EV_HLDM_GunshotDecalTrace(pmtrace_t *pTrace, char *decalName)
{
	int iRand;
	physent_t *pe;

	gEngfuncs.pEfxAPI->R_BulletImpactParticles(pTrace->endpos);

	iRand = gEngfuncs.pfnRandomLong(0, 0x7FFF);

	if (iRand < (0x7fff / 2))
	{
		switch(iRand % 5)
		{
			case 0:	gEngfuncs.pEventAPI->EV_PlaySound(-1, pTrace->endpos, 0, "weapons/ric1.wav", 1.0, ATTN_NORM, 0, PITCH_NORM); break;
			case 1:	gEngfuncs.pEventAPI->EV_PlaySound(-1, pTrace->endpos, 0, "weapons/ric2.wav", 1.0, ATTN_NORM, 0, PITCH_NORM); break;
			case 2:	gEngfuncs.pEventAPI->EV_PlaySound(-1, pTrace->endpos, 0, "weapons/ric3.wav", 1.0, ATTN_NORM, 0, PITCH_NORM); break;
			case 3:	gEngfuncs.pEventAPI->EV_PlaySound(-1, pTrace->endpos, 0, "weapons/ric4.wav", 1.0, ATTN_NORM, 0, PITCH_NORM); break;
			case 4:	gEngfuncs.pEventAPI->EV_PlaySound(-1, pTrace->endpos, 0, "weapons/ric5.wav", 1.0, ATTN_NORM, 0, PITCH_NORM); break;
		}
	}

	pe = gEngfuncs.pEventAPI->EV_GetPhysent(pTrace->ent);

	if (decalName && decalName[0] && pe && (pe->solid == SOLID_BSP || pe->movetype == MOVETYPE_PUSHSTEP))
	{
		/*
		if(random_num(0, 1))
			gTGA.CreateDecal (pTrace, "M4A1-S_JEWELRY_1.tga", 0.1);
		else
			gTGA.CreateDecal (pTrace, "AXE_2.tga", 0.1);
		*/
		gEngfuncs.pEfxAPI->R_DecalShoot(gEngfuncs.pEfxAPI->Draw_DecalIndex(gEngfuncs.pEfxAPI->Draw_DecalIndexFromName(decalName)), gEngfuncs.pEventAPI->EV_IndexFromTrace( pTrace ), 0, pTrace->endpos, 0);
	}
}


char *EV_HLDM_DamageDecal(physent_t *pe)
{
	static char decalname[32];
	int idx;

	if (pe->classnumber == 1)
	{
		idx = gEngfuncs.pfnRandomLong(0, 2);
		sprintf(decalname, "{break%i", idx + 1);
	}
	else if (pe->rendermode != kRenderNormal)
	{
		sprintf(decalname, "{bproof1");
	}
	else
	{
		idx = gEngfuncs.pfnRandomLong(0, 4);
		sprintf(decalname, "{shot%i", idx + 1);
	}

	return decalname;
}


void EV_HLDM_DecalGunshot( pmtrace_t *pTrace, int iBulletType, float scale, int r, int g, int b, bool bCreateSparks, char cTextureType )
{
	if(!cl_decaleffect->value )
		return g_pfnEV_HLDM_DecalGunshot(pTrace, iBulletType, scale, r, g, b, bCreateSparks, cTextureType);

	physent_t *pe;

	pe = gEngfuncs.pEventAPI->EV_GetPhysent(pTrace->ent);

	if (pe && pe->solid == SOLID_BSP)
	{
		switch (iBulletType)
		{
			case BULLET_PLAYER_9MM:
			case BULLET_MONSTER_9MM:
			case BULLET_PLAYER_MP5:
			case BULLET_MONSTER_MP5:
			case BULLET_PLAYER_BUCKSHOT:
			case BULLET_PLAYER_357:
			default:
			{
				char *decalName = EV_HLDM_DamageDecal(pe);
				EV_HLDM_GunshotDecalTrace(pTrace, decalName);

				if (decalName && decalName[0] && pe && (pe->solid == SOLID_BSP || pe->movetype == MOVETYPE_PUSHSTEP))
				{
					EV_DecalEffect(pTrace, cTextureType);
				}

				break;
			}
		}
	}
}


// ===================================================================================================================================

void EV_FireRocket( event_args_t *args )
{
	int idx;
	int weaponid;

	idx = args->entindex ;
	weaponid = args->iparam1 ;
	int iEnt = args->iparam2 ;
	vec3_t vecOrigin;
	VectorCopy(args->origin, vecOrigin);
	vecOrigin[2] -= 15;

	CRPGTail *particle = new CRPGTail;
	particle->m_flNextFxTime = gHUD.m_flTime;
	particle->m_iEnt = iEnt;
	gEffect.AddParticleObj (particle);

	if(!g_vRpgTail)
		g_vRpgTail = CreateVector(sizeof(CRPGTail));
	VectorPush(g_vRpgTail, &particle);

	TEMPENTITY *entSmoke = gEngfuncs.pEfxAPI->R_DefaultSprite (vecOrigin, gEngfuncs.pEventAPI->EV_FindModelIndex("sprites/rockefire.spr"), 10.5);
	entSmoke->entity.curstate.rendermode = kRenderTransAdd;
	entSmoke->entity.curstate .renderamt = 75;
	entSmoke->entity.curstate .scale = 1;
	entSmoke->die = gEngfuncs.GetClientTime() + 4.0;

	vec3_t newOrigin[5], newOrigin2[5];
	get_spherical_coord(vecOrigin, 20.0, 30.0, 5.0, newOrigin[0]);
	get_spherical_coord(vecOrigin, 20.0, -20.0, -5.0, newOrigin[1]);
	get_spherical_coord(vecOrigin, -14.0, 30.0, 7.0, newOrigin[2]);
	get_spherical_coord(vecOrigin, 25.0, 10.0, -8.0, newOrigin[3]);
	get_spherical_coord(vecOrigin, -17.0, 17.0, 0.0, newOrigin[4]);

	GetAimOrigin( -80.0, 4.0, 1.0, gEngfuncs.GetEntityByIndex (idx), vecOrigin);

	get_spherical_coord(vecOrigin, 30.0, 40.0, 2.0, newOrigin2[0]);
	get_spherical_coord(vecOrigin, 30.0, -30.0, -3.0, newOrigin2[1]);
	get_spherical_coord(vecOrigin, -24.0, 40.0, 4.0, newOrigin2[2]);
	get_spherical_coord(vecOrigin, 35.0, 20.0, -4.0, newOrigin2[3]);
	get_spherical_coord(vecOrigin, -27.0, 27.0, 0.0, newOrigin2[4]);

	for(int i = 0; i < 5; i++)
	{
		ProduceSmoke(newOrigin[i], 1, 50);
		ProduceSmoke(newOrigin2[i], 1, gEngfuncs.pfnRandomLong (50, 100));
	}

	if(EV_IsLocal(idx))
	{
		g_pWeapon->GunFire ();

		MSG_BeginWrite("ScreenShake");
			MSG_WriteShort((1<<12)*2);
			MSG_WriteShort((1<<12));
			MSG_WriteShort((1<<12));
		MSG_EndWrite();
	}

}

void EV_Incendiary( event_args_t *args )
{

	if( args->bparam1 == TRUE )
	{
		gEngfuncs.pEventAPI->EV_StopSound(  args->entindex, CHAN_WEAPON, "ambience/burning1.wav" );
		return;
	}

	int ModelIndex_Fire = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/fire_burning.spr");
	int ModelIndex_Smoke = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/steam1.spr");

	vec3_t origin;
	VectorCopy( args->origin, origin );

	int iFireRange = args->iparam1;
	int iFirstTime = args->iparam2;

	if(iFirstTime == 1)
	{
		gEngfuncs.pEventAPI->EV_PlaySound( args->entindex, origin, CHAN_WEAPON, "ambience/burning1.wav", 1.0, ATTN_NORM, 0, 100 );

		int iRange = (int)( iFireRange / 5.0 );
		int iMax = iRange * iRange * M_PI / 2500;

		for(int i=0; i<iMax; i++)
		{
			vec3_t vecFirePos;
			vecFirePos[0] = origin[0] - iRange + rand() % (iRange * 2);
			vecFirePos[1] = origin[1] - iRange + rand() % (iRange * 2);
			vecFirePos[2] = origin[2] - 20;

			gEngfuncs.pEfxAPI->R_Explosion( vecFirePos, ModelIndex_Fire, 3, 8.5, (TE_EXPLFLAG_NODLIGHTS | TE_EXPLFLAG_NOSOUND | TE_EXPLFLAG_NOPARTICLES) );
		}

	}
	else
	{
		// Draw Fire

		int iRange = iFireRange;
		int iMax = iRange * iRange * M_PI / 2500;

		for(int i=0; i< iMax; i++)
		{
			vec3_t vecFirePos;
			vecFirePos[0] = origin[0] - iRange + rand() % (iRange * 2);
			vecFirePos[1] = origin[1] - iRange + rand() % (iRange * 2);
			vecFirePos[2] = origin[2] - 20;

			gEngfuncs.pEfxAPI->R_Explosion( vecFirePos, ModelIndex_Fire, 3, 8.5, (TE_EXPLFLAG_NODLIGHTS | TE_EXPLFLAG_NOSOUND | TE_EXPLFLAG_NOPARTICLES) );
		}

		
		for(int i=0; i< iMax / 3; i++)
		{
			vec3_t vecSmokePos;
			vecSmokePos[0] = origin[0] - iRange + rand() % (iRange * 2);
			vecSmokePos[1] = origin[1] - iRange + rand() % (iRange * 2);
			vecSmokePos[2] = origin[2] + 70.0 ;

			TEMPENTITY *entSmoke = gEngfuncs.pEfxAPI->R_DefaultSprite (vecSmokePos, ModelIndex_Smoke, 10.0);
			entSmoke->entity.curstate.rendermode = kRenderTransAlpha;
			entSmoke->entity.curstate .renderfx =  kRenderFxNone;
			entSmoke->entity .curstate .rendercolor .r = 20;
			entSmoke->entity .curstate .rendercolor .g = 20;
			entSmoke->entity .curstate .rendercolor .b = 20;
			entSmoke->entity.curstate .renderamt = 190;
			entSmoke->entity.curstate .scale = 4;
			entSmoke->die = gEngfuncs.GetClientTime() + 6.0;

		}

		
	}

}


void EV_RealExplode( event_args_t *args )
{
	vec3_t origin;
	VectorCopy( args->origin, origin );

	int ModelIndex_FireBall_1 = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/fexplo.spr");
	int ModelIndex_FireBall_2 = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/eexplo.spr");
	int ModelIndex_Smoke_1 = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/gas_smoke1.spr");
	int ModelIndex_Smoke_2 = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/exsmoke.spr");

	vec3_t vOrigin;

	VectorCopy(origin ,vOrigin);
	vOrigin[2] += 20;

	gEngfuncs.pEfxAPI->R_Explosion( vOrigin, ModelIndex_FireBall_1, 3.0, 25, TE_EXPLFLAG_NONE );

	VectorCopy(origin ,vOrigin);
	vOrigin[0]  +=  rand()% 128  - 64;
	vOrigin[1]  +=  rand()% 128  - 64;
	vOrigin[2]  +=  rand() % 60  - 30;

	gEngfuncs.pEfxAPI->R_Explosion( vOrigin, ModelIndex_FireBall_2, 2.0, 10, TE_EXPLFLAG_NONE );

	IEngineStudio.Mod_ForName ("models/gibs_wallbrown.mdl", 1);
	IEngineStudio.Mod_ForName ("models/gibs_woodplank.mdl", 1);
	IEngineStudio.Mod_ForName ("models/gibs_wallbrown.mdl", 1);
	IEngineStudio.Mod_ForName ("models/gibs_brickred.mdl", 1);
	IEngineStudio.Mod_ForName ("models/gibs_wallbrown.mdl", 1);

	for(int i = 0; i < 5; i++)
	{
		int iModelIndex_Debris;
		int ii = gEngfuncs.pfnRandomLong(0, 3);
		switch(ii)
		{
		case 0:
			iModelIndex_Debris = gEngfuncs.pEventAPI->EV_FindModelIndex ( "models/gibs_wallbrown.mdl" );
			break;
		case 1:
			iModelIndex_Debris = gEngfuncs.pEventAPI->EV_FindModelIndex ( "models/gibs_woodplank.mdl" );
			break;
		case 2:
			iModelIndex_Debris = gEngfuncs.pEventAPI->EV_FindModelIndex ( "models/gibs_brickred.mdl" );
			break;
		default :
			iModelIndex_Debris = gEngfuncs.pEventAPI->EV_FindModelIndex ( "models/gibs_wallbrown.mdl" );
			break;
		}

		vec3_t velocity;
		
		velocity[0]  = gEngfuncs.pfnRandomLong(200, 500);
		velocity[1]  = gEngfuncs.pfnRandomLong(200, 500);
		velocity[2]  = gEngfuncs.pfnRandomLong(200, 500);

		vec3_t vecDebrisOrigin;
		VectorCopy(origin , vecDebrisOrigin);
		vecDebrisOrigin[2] += 10;

		vec3_t vSize;
		vSize[0] = vSize[1] = vSize[2] = 0.5;
		gEngfuncs.pEfxAPI->R_BreakModel( vecDebrisOrigin, vSize, velocity, 10, 15, 10, iModelIndex_Debris, FTENT_FLICKER/*0x40*/ );
	}

	// make smoke
	vec3_t vecOrigin[12];
	get_spherical_coord( origin, 40.0, 0.0, -15.0, vecOrigin[0]);
	get_spherical_coord( origin, 40.0, 90.0, -15.0, vecOrigin[1]);
	get_spherical_coord( origin, 40.0, 180.0, -15.0, vecOrigin[2]);
	get_spherical_coord( origin, 40.0, 270.0, -15.0, vecOrigin[3]);
	get_spherical_coord( origin, 100.0, 0.0, -15.0, vecOrigin[4]);
	get_spherical_coord( origin, 100.0, 45.0, -15.0, vecOrigin[5]);
	get_spherical_coord( origin, 100.0, 90.0, -15.0, vecOrigin[6]);
	get_spherical_coord( origin, 100.0, 135.0, -15.0, vecOrigin[7]);
	get_spherical_coord( origin, 100.0, 180.0, -15.0, vecOrigin[8]);
	get_spherical_coord( origin, 100.0, 225.0, -15.0, vecOrigin[9]);
	get_spherical_coord( origin, 100.0, 270.0, -15.0, vecOrigin[10]);
	get_spherical_coord( origin, 100.0, 315.0, -15.0, vecOrigin[11]);

	for(int e = 0; e < 12; e++)
	{
		make_smoke( vecOrigin[e], ModelIndex_Smoke_1, 2, 100);
	}

	vec3_t vecPos;
	VectorCopy(origin ,vecPos);
	vecPos[2] += 120;
	get_spherical_coord(vecPos, 0.0, 0.0, 135.0, vecOrigin[0]);
	get_spherical_coord(vecPos, 0.0, 80.0, 80.0, vecOrigin[1]);
	get_spherical_coord(vecPos, 41.0, 43.0, 60.0, vecOrigin[2]);
	get_spherical_coord(vecPos, 90.0, 90.0, 40.0, vecOrigin[3]);
	get_spherical_coord(vecPos, 80.0, 25.0, 135.0, vecOrigin[4]);
	get_spherical_coord(vecPos, 101.0, 100.0, 112.0, vecOrigin[5]);
	get_spherical_coord(vecPos, 68.0, 35.0, 139.0, vecOrigin[6]);
	get_spherical_coord(vecPos, 0.0, 95.0, 105.0, vecOrigin[7]);

	for(int c = 0; c < 8; c++)
	{
		make_smoke(vecOrigin[c], ModelIndex_Smoke_2, 3, 30);
	}
	

	float flShakeRadius = 380 * 2;
	float iMaxShakeAmount = 6;

	vec3_t vecOff;
	VectorSubtract(args->origin , gEngfuncs.GetLocalPlayer()->origin, vecOff);
	if( VectorLength(vecOff) > flShakeRadius )
		return;

	iMaxShakeAmount = 8;
	int iShakeAmount = 0;

	float falloff = iMaxShakeAmount / flShakeRadius;
	iShakeAmount = (int)(iMaxShakeAmount - VectorLength(vecOff) * falloff);

	if(iShakeAmount <= 0)
		iShakeAmount = 1;

	MSG_BeginWrite("ScreenShake");
		MSG_WriteShort((1<<12)*iShakeAmount);
		MSG_WriteShort((1<<12));
		MSG_WriteShort((1<<12));
	MSG_EndWrite();
}


void EV_RocketExplode( event_args_t *args )
{
	size_t iSize = VectorSize(g_vRpgTail);
	CBaseParticle *particle = NULL;

	for(size_t i = 0; i < iSize; i++)
	{
		particle = *(CBaseParticle **)VectorGet(g_vRpgTail, i);
		if(particle->m_iEnt == args->entindex )
		{
			particle->m_bKillme = true;
			VectorEarse(g_vRpgTail, i, i+1);
			break;
		}
	}

	vec3_t origin;
	VectorCopy( args->origin, origin );
	origin[2] += 30.0;

	dlight_t *dl;
	dl = gEngfuncs.pEfxAPI->CL_AllocDlight ( 0 );
	VectorCopy ( origin, dl->origin );
	dl->radius = 280;
	dl->die = gEngfuncs.GetClientTime() + 0.2;
	dl->dark = true;
	dl->color.r = 200;
	dl->color.g = 200;
	dl->color.b = 20;

	int ModelIndex_FireBall_1 = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/fexplo.spr");
	int ModelIndex_FireBall_2 = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/eexplo.spr");
	int ModelIndex_Smoke_1 = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/rockefire.spr");
	int ModelIndex_Smoke_2 = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/exsmoke.spr");

	vec3_t vOrigin;

	VectorCopy(origin, vOrigin);
	vOrigin[2] += 20;

	gEngfuncs.pEfxAPI->R_Explosion( vOrigin, ModelIndex_FireBall_1, 3.0, 25, TE_EXPLFLAG_NONE );

	VectorCopy(origin, vOrigin);
	vOrigin[0]  +=  rand()% 128  - 64;
	vOrigin[1]  +=  rand()% 128  - 64;
	vOrigin[2]  +=  rand() % 60  - 30;

	gEngfuncs.pEfxAPI->R_Explosion( vOrigin, ModelIndex_FireBall_2, 2.0, 10, TE_EXPLFLAG_NONE );

	for(int i = 0; i < 5; i++)
	{
		int iModelIndex_Debris;
		int ii = gEngfuncs.pfnRandomLong(0, 3);
		switch(ii)
		{
		case 0:
			iModelIndex_Debris = gEngfuncs.pEventAPI->EV_FindModelIndex ( "models/gibs_wallbrown.mdl" );
			break;
		case 1:
			iModelIndex_Debris = gEngfuncs.pEventAPI->EV_FindModelIndex ( "models/gibs_woodplank.mdl" );
			break;
		case 2:
			iModelIndex_Debris = gEngfuncs.pEventAPI->EV_FindModelIndex ( "models/gibs_brickred.mdl" );
			break;
		default :
			iModelIndex_Debris = gEngfuncs.pEventAPI->EV_FindModelIndex ( "models/gibs_wallbrown.mdl" );
			break;
		}

		vec3_t velocity;
		
		velocity[0]  = gEngfuncs.pfnRandomLong(200, 700);
		velocity[1]  = gEngfuncs.pfnRandomLong(200, 700);
		velocity[2]  = gEngfuncs.pfnRandomLong(200, 700);

		vec3_t vecDebrisOrigin;
		VectorCopy(origin, vecDebrisOrigin);
		vecDebrisOrigin[2] += 10;

		vec3_t vSize;
		vSize[0] = vSize[1] = vSize[2] = 0.5;
		gEngfuncs.pEfxAPI->R_BreakModel( vecDebrisOrigin, vSize, velocity, 10, 15, 10, iModelIndex_Debris, FTENT_FLICKER/*0x40*/ );
	}

	// make smoke
	vec3_t vecOrigin[12];
	
	get_spherical_coord( origin, 20.0, 0.0, -15.0, vecOrigin[0]);
	get_spherical_coord( origin, 20.0, 90.0, -15.0, vecOrigin[1]);
	get_spherical_coord( origin, 20.0, 180.0, -15.0, vecOrigin[2]);
	get_spherical_coord( origin, 20.0, 270.0, -15.0, vecOrigin[3]);
	get_spherical_coord( origin, 50.0, 0.0, -15.0, vecOrigin[4]);
	get_spherical_coord( origin, 50.0, 45.0, -15.0, vecOrigin[5]);
	get_spherical_coord( origin, 50.0, 90.0, -15.0, vecOrigin[6]);
	get_spherical_coord( origin, 50.0, 135.0, -15.0, vecOrigin[7]);
	get_spherical_coord( origin, 50.0, 180.0, -15.0, vecOrigin[8]);
	get_spherical_coord( origin, 50.0, 225.0, -15.0, vecOrigin[9]);
	get_spherical_coord( origin, 50.0, 270.0, -15.0, vecOrigin[10]);
	get_spherical_coord( origin, 50.0, 315.0, -15.0, vecOrigin[11]);


	for(int e = 0; e < 12; e++)
	{
		make_smoke( vecOrigin[e], ModelIndex_Smoke_1, 1, 100);
	}

	vec3_t vecPos;
	VectorCopy(origin, vecPos);
	vecPos[2] += 120;
	get_spherical_coord(vecPos, 0.0, 0.0, 135.0, vecOrigin[0]);
	get_spherical_coord(vecPos, 0.0, 80.0, 80.0, vecOrigin[1]);
	get_spherical_coord(vecPos, 41.0, 43.0, 60.0, vecOrigin[2]);
	get_spherical_coord(vecPos, 90.0, 90.0, 40.0, vecOrigin[3]);
	get_spherical_coord(vecPos, 80.0, 25.0, 135.0, vecOrigin[4]);
	get_spherical_coord(vecPos, 101.0, 100.0, 112.0, vecOrigin[5]);
	get_spherical_coord(vecPos, 68.0, 35.0, 139.0, vecOrigin[6]);
	get_spherical_coord(vecPos, 0.0, 95.0, 105.0, vecOrigin[7]);


	for(int c = 0; c < 8; c++)
	{
		make_smoke(vecOrigin[c], ModelIndex_Smoke_2, 3, 30);
	}

	float flShakeRadius = 420 * 2;
	float iMaxShakeAmount = 6;

	vec3_t vecOff;
	VectorSubtract(args->origin, gEngfuncs.GetLocalPlayer()->origin, vecOff);
	if( VectorLength(vecOff) > flShakeRadius )
		return;

	iMaxShakeAmount = 8;
	int iShakeAmount = 0;

	float falloff = iMaxShakeAmount / flShakeRadius;
	iShakeAmount = (int)(iMaxShakeAmount - VectorLength(vecOff) * falloff);

	if(iShakeAmount <= 0)
		iShakeAmount = 1;

	MSG_BeginWrite("ScreenShake");
		MSG_WriteShort((1<<12)*iShakeAmount);
		MSG_WriteShort((1<<12));
		MSG_WriteShort((1<<12));
	MSG_EndWrite();
}


void EV_BoomerExplode( event_args_t *args )
{
	int iRadius = (int)args->fparam1 ;
	vec3_t vecOrigin;
	VectorCopy(args->origin, vecOrigin);

	int ModelIndexBoomerSpr = gEngfuncs.pEventAPI->EV_FindModelIndex ("sprites/Survivor/spr_boomer.spr");

	for( int i = 0; i < 15; i++ )
	{
		vec3_t origin;
		VectorCopy(vecOrigin , origin);
		origin[0]  += rand() % (iRadius *2) - iRadius;
		origin[1]  += rand() % (iRadius *2) - iRadius;
		origin[2]  -= gEngfuncs.pfnRandomLong ( 10, 50);

		gEngfuncs.pEfxAPI->R_Explosion( origin, ModelIndexBoomerSpr, 0.5, 8.5, (TE_EXPLFLAG_NODLIGHTS | TE_EXPLFLAG_NOSOUND | TE_EXPLFLAG_NOPARTICLES) );

	}
}


void EV_SmokeProduce( event_args_t *args )
{
	int iFirstTime = args->bparam1 ;
	int iWpnId = args->iparam1 ;
	vec3_t vecSmokePos;
	VectorCopy(args->origin, vecSmokePos);

	int iModelIndexSmokeSpr = gEngfuncs.pEventAPI->EV_FindModelIndex ( g_WpnItemInfo[iWpnId].szSprSmoke1  );

	if( iFirstTime == TRUE )
	{

		int iSmokeAmount = args->iparam2 ;
		float flSmokeTime = args->fparam1 ;
		float flRadius = args->fparam2 ;

		for( int i = 0; i < iSmokeAmount; i++)
		{
			vec3_t vecOrigin;
			VectorCopy(vecSmokePos, vecOrigin);
			vecOrigin[0] = gEngfuncs.pfnRandomLong ( vecOrigin[0] - flRadius,    vecOrigin[0] + flRadius );
			vecOrigin[1] = gEngfuncs.pfnRandomLong ( vecOrigin[1] - flRadius,    vecOrigin[1] + flRadius );
			vecOrigin[2] = gEngfuncs.pfnRandomLong ( vecOrigin[2] - 10.0,    vecOrigin[2] + 60.0 );

			int iAlpha = gEngfuncs.pfnRandomLong ( 170, 255 );

			TEMPENTITY *entSmoke = gEngfuncs.pEfxAPI->CL_TempEntAlloc( vecOrigin, g_WpnItemInfo[iWpnId].mdlSprSmoke1 );
			entSmoke->entity.curstate.rendermode = kRenderTransAlpha;
			entSmoke->entity.curstate .scale = gEngfuncs.pfnRandomFloat(1.5, 4.0);
			entSmoke->entity.curstate.renderamt = iAlpha;
			entSmoke->entity.baseline.renderamt = iAlpha;
			entSmoke->flags |= FTENT_FADEOUT;
			entSmoke->fadeSpeed = 0.2;
			entSmoke->die = gEngfuncs.GetClientTime() + flSmokeTime + gEngfuncs.pfnRandomFloat(1.0, 3.5);
		}
	}
	else
	{
		TEMPENTITY *entSmoke = gEngfuncs.pEfxAPI->CL_TempEntAlloc( vecSmokePos, g_WpnItemInfo[iWpnId].mdlSprSmoke1 );
		entSmoke->entity.curstate.rendermode = kRenderTransAlpha;
		entSmoke->entity.curstate .scale = 0.5;
		entSmoke->entity.curstate.renderamt = 200;
		entSmoke->entity.baseline.renderamt = 200;
		entSmoke->flags |= FTENT_FADEOUT;
		entSmoke->flags &= ~FTENT_GRAVITY;
		entSmoke->fadeSpeed = 0.2;
		entSmoke->die = gEngfuncs.GetClientTime()  + gEngfuncs.pfnRandomFloat(1.0, 2.5);
	}
	
}


enum elite_e
{
	ELITE_IDLE,
	ELITE_IDLE_LEFTEMPTY,
	ELITE_SHOOTLEFT1,
	ELITE_SHOOTLEFT2,
	ELITE_SHOOTLEFT3,
	ELITE_SHOOTLEFT4,
	ELITE_SHOOTLEFT5,
	ELITE_SHOOTLEFTLAST,
	ELITE_SHOOTRIGHT1,
	ELITE_SHOOTRIGHT2,
	ELITE_SHOOTRIGHT3,
	ELITE_SHOOTRIGHT4,
	ELITE_SHOOTRIGHT5,
	ELITE_SHOOTRIGHTLAST,
	ELITE_RELOAD,
	ELITE_DRAW
};


void EV_FireEliteLeft(struct event_args_s *args)
{
	args->bparam1 = true;

	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	int lefthand;
	float bullets_left;
	float time_diff;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	vec3_t vSpread;
	int weaponid;

	ent = GetViewEntity();
	idx = args->entindex;
	lefthand = args->bparam1;
	bullets_left = args->iparam2;
	time_diff = args->fparam1;

	weaponid = g_pWeapon->GetPlayerWpnId(idx, 2);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);
	shell = gEngfuncs.pEventAPI->EV_FindModelIndex(g_WpnItemInfo[weaponid].szShellModel );

	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire();
		EV_MuzzleFlash();

		if (lefthand)
		{
			if (bullets_left <= 1)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTLEFTLAST, 2);
			else if (time_diff >= 0.5)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTLEFT5, 2);
			else if (time_diff >= 0.4)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTLEFT4, 2);
			else if (time_diff >= 0.3)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTLEFT3, 2);
			else if (time_diff >= 0.2)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTLEFT2, 2);
			else if (time_diff >= 0.0)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTLEFT1, 2);

			EV_HLDM_CreateSmoke(ent->attachment[0], forward, 0, 0.25, 10, 10, 10, 3, velocity, false, 35);
			EV_HLDM_CreateSmoke(ent->attachment[0], forward, 25, 0.3, 15, 15, 15, 2, velocity, false, 35);
			EV_HLDM_CreateSmoke(ent->attachment[0], forward, 50, 0.2, 25, 25, 25, 2, velocity, false, 35);
		}
		else
		{
			if (bullets_left <= 1)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHTLAST, 2);
			else if (time_diff >= 0.5)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHT5, 2);
			else if (time_diff >= 0.4)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHT4, 2);
			else if (time_diff >= 0.3)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHT3, 2);
			else if (time_diff >= 0.2)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHT2, 2);
			else if (time_diff >= 0.0)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHT1, 2);

			EV_HLDM_CreateSmoke(ent->attachment[1], forward, 0, 0.25, 10, 10, 10, 3, velocity, false, 35);
			EV_HLDM_CreateSmoke(ent->attachment[1], forward, 25, 0.3, 15, 15, 15, 2, velocity, false, 35);
			EV_HLDM_CreateSmoke(ent->attachment[1], forward, 50, 0.2, 25, 25, 25, 2, velocity, false, 35);
			
		}
	}

	if (EV_IsLocal(idx))
	{
		if (lefthand)
		{
			EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 35.0, -11.0, -16.0, false);
			VectorCopy(ent->attachment[2], ShellOrigin);
		}
		else
		{
			EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 35.0, -11.0, 16.0, false);
			VectorCopy(ent->attachment[3], ShellOrigin);
		}

		VectorScale(ShellVelocity, 0.75, ShellVelocity);
		ShellVelocity[2] -= 25;
	}
	else
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 20.0, -12.0, 4.0, false);

	EV_EjectBrass(ShellOrigin, ShellVelocity, angles[GetYaw(args)], shell, TE_BOUNCE_SHELL, idx, 5);

	
	gEngfuncs.pEventAPI->EV_PlaySound(
		idx, 
		origin, 
		CHAN_WEAPON, 
		args->bparam1?g_WpnItemInfo[weaponid].szShotSound2:g_WpnItemInfo[weaponid].szShotSound1, 
		1.0, 
		ATTN_NORM, 
		0, 
		94 + gEngfuncs.pfnRandomLong(0, 0xf));

	EV_GetGunPosition(args, vecSrc, origin);

	VectorCopy(forward, vecAiming);

	vSpread[0] = args->fparam2;
	vSpread[1] = args->fparam1 / 100.0;
	vSpread[2] = 0;

	if (lefthand)
	{
		vecSrc[0] -= right[0] * 5;
		vecSrc[1] -= right[1] * 5;
		vecSrc[2] -= right[2] * 5;
	}
	else
	{
		vecSrc[0] += right[0] * 5;
		vecSrc[1] += right[1] * 5;
		vecSrc[2] += right[2] * 5;
	}

	EV_HLDM_FireBullets(idx, forward, right, up, 1, vecSrc, vecAiming, vSpread, g_WpnItemInfo[weaponid].iBulletDistance, g_WpnItemInfo[weaponid].iBulletType, 0, 0, g_WpnItemInfo[weaponid].iBulletPenetration  );
}

void EV_FireEliteRight(struct event_args_s *args)
{
	args->bparam1 = false;

	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	float bullets_left;
	float time_diff;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	vec3_t vSpread;
	int weaponid;

	ent = GetViewEntity();
	idx = args->entindex;
	bullets_left = args->iparam2;
	time_diff = args->fparam1;
	weaponid = g_pWeapon->GetPlayerWpnId(idx, 2);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);
	shell = gEngfuncs.pEventAPI->EV_FindModelIndex(g_WpnItemInfo[weaponid].szShellModel );

	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire();
		EV_MuzzleFlash();

		if (bullets_left <= 1)
			gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHTLAST, 2);
		else if (time_diff >= 0.5)
			gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHT5, 2);
		else if (time_diff >= 0.4)
			gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHT4, 2);
		else if (time_diff >= 0.3)
			gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHT3, 2);
		else if (time_diff >= 0.2)
			gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHT2, 2);
		else if (time_diff >= 0.0)
			gEngfuncs.pEventAPI->EV_WeaponAnimation(ELITE_SHOOTRIGHT1, 2);


		EV_HLDM_CreateSmoke(ent->attachment[1], forward, 0, 0.25, 10, 10, 10, 3, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[1], forward, 25, 0.3, 15, 15, 15, 2, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[1], forward, 50, 0.2, 25, 25, 25, 2, velocity, false, 35);
		
	}

	if (EV_IsLocal(idx))
	{
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 35.0, -11.0, 16.0, false);

		VectorCopy(ent->attachment[3], ShellOrigin);
		VectorScale(ShellVelocity, 0.75, ShellVelocity);
		ShellVelocity[2] -= 25;
	}
	else
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 20.0, -12.0, 4.0, false);

	EV_EjectBrass(ShellOrigin, ShellVelocity, angles[GetYaw(args)], shell, TE_BOUNCE_SHELL, idx, 5);

	gEngfuncs.pEventAPI->EV_PlaySound(
		idx, 
		origin, 
		CHAN_WEAPON, 
		args->bparam1? g_WpnItemInfo[weaponid].szShotSound2 :g_WpnItemInfo[weaponid].szShotSound1, 
		1.0, 
		ATTN_NORM, 
		0, 
		94 + gEngfuncs.pfnRandomLong(0, 0xf));

	EV_GetGunPosition(args, vecSrc, origin);

	VectorCopy(forward, vecAiming);

	vSpread[0] = args->fparam2;
	vSpread[1] = args->fparam1 / 100.0;
	vSpread[2] = 0;

	vecSrc[0] += right[0] * 5;
	vecSrc[1] += right[1] * 5;
	vecSrc[2] += right[2] * 5;

	EV_HLDM_FireBullets(idx, forward, right, up, 1, vecSrc, vecAiming, vSpread, g_WpnItemInfo[weaponid].iBulletDistance , g_WpnItemInfo[weaponid].iBulletType, 0, 0, g_WpnItemInfo[weaponid].iBulletPenetration  );
}


void EV_FireFamas(struct event_args_s *args)
{
	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	int lefthand;
	vec3_t vSpread;
	int weaponid;
		
	ent = GetViewEntity();
	idx = args->entindex;
	lefthand = cl_righthand->value;
	weaponid = g_pWeapon->GetPlayerWpnId (idx, 1);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	angles[0] += args->iparam1 / 10000000.0;
	angles[1] += args->iparam2 / 10000000.0;

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);
	shell = gEngfuncs.pEventAPI->EV_FindModelIndex(g_WpnItemInfo[weaponid].szShellModel );
	
	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire ();
		EV_MuzzleFlash();

		gEngfuncs.pEventAPI->EV_WeaponAnimation(gEngfuncs.pfnRandomLong(g_WpnItemInfo[weaponid].iAnimShotStart, g_WpnItemInfo[weaponid].iAnimShotEnd), 2);

	}

	if (EV_IsLocal(idx))
	{
		float vecScale[3] = {17.0, -8.0, -14.0};
		if (lefthand)
			vecScale[2] = 0 - vecScale[2];
		
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, vecScale[0], vecScale[1], vecScale[2], true);

		VectorCopy(ent->attachment[1], ShellOrigin);
		VectorScale(ShellVelocity, 1.25, ShellVelocity);
		ShellVelocity[2] -= 122;
	}
	else
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 20.0, -12.0, -4.0, false);

	EV_HLDM_CreateSmoke(ent->attachment[0], forward, 3, 0.5, 20, 20, 20, 3, velocity, false, 35);
	EV_EjectBrass(ShellOrigin, ShellVelocity, angles[GetYaw(args)], shell, TE_BOUNCE_SHELL, idx, 8);

	gEngfuncs.pEventAPI->EV_PlaySound(
		idx, 
		origin, 
		CHAN_WEAPON, 
		args->bparam1? g_WpnItemInfo[weaponid].szShotSound2 : g_WpnItemInfo[weaponid].szShotSound1, 
		1.0, 
		0.48, 
		0, 
		94 + gEngfuncs.pfnRandomLong(0, 0xf));

	EV_GetGunPosition(args, vecSrc, origin);

	VectorCopy(forward, vecAiming);

	vSpread[0] = args->fparam1;
	vSpread[1] = args->fparam2;
	vSpread[2] = 0;

	EV_HLDM_FireBullets(idx, forward, right, up, 1, vecSrc, vecAiming, vSpread, g_WpnItemInfo[weaponid].iBulletDistance, g_WpnItemInfo[weaponid].iBulletType, 0, 0, g_WpnItemInfo[weaponid].iBulletPenetration  );
	
	/*
	// flamethrower
	Tga3DItem_t *pTGA = gTGA.PlayTGA ("cstrike\\flame.anim", ent->attachment[0], 1.0, 0.0, 15.0, 0.0, 0, random_num(0, 24));
	pTGA->vecVelocity = forward.Normalize () * 3.0;
	CFlameParticle *flame_particle = new CFlameParticle;
	flame_particle->AddTgaItem(pTGA);
	pTGA->privateData = (void *)flame_particle;

	pTGA = gTGA.PlayTGA ("cstrike\\flame.anim", ent->attachment[0], 1.0, 0.0, 15.0, 0.0, 0, random_num(0, 24));
	pTGA->vecVelocity = forward.Normalize () * 3.0;
	flame_particle = new CFlameParticle;
	flame_particle->AddTgaItem(pTGA);
	pTGA->privateData = (void *)flame_particle;
	*/
	
}


void EV_FireGlock18(struct event_args_s *args)
{
	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	int empty;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	int lefthand;
	vec3_t vSpread;
	int weaponid;


	ent = GetViewEntity();
	idx = args->entindex;
	empty = args->bparam1 != false;
	lefthand = cl_righthand->value;
	weaponid = g_pWeapon->GetPlayerWpnId(idx, 2);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	angles[0] += args->iparam1 / 100.0;
	angles[1] += args->iparam2 / 100.0;

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);
	shell = gEngfuncs.pEventAPI->EV_FindModelIndex(g_WpnItemInfo[weaponid].szShellModel );
	
	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire ();
		EV_MuzzleFlash();


		if (empty)
			gEngfuncs.pEventAPI->EV_WeaponAnimation(g_WpnItemInfo[weaponid].iAnimShotEmpty, 2);
		else		
			gEngfuncs.pEventAPI->EV_WeaponAnimation(gEngfuncs.pfnRandomLong(g_WpnItemInfo[weaponid].iAnimShotStart, g_WpnItemInfo[weaponid].iAnimShotEnd), 2);
		
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 0, 0.2, 10, 10, 10, 3, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 40, 0.4, 20, 20, 20, 2, velocity, false, 35);

	}

	if (EV_IsLocal(idx))
	{
		float vecScale[3] = {36.0, -14.0, -14.0};

		if (lefthand)
			vecScale[2] = 0 - vecScale[2];		

		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, vecScale[0], vecScale[1], vecScale[2], true);

		VectorCopy(ent->attachment[1], ShellOrigin);
		VectorScale(ShellVelocity, 0.65, ShellVelocity);
		ShellVelocity[2] += 25.0;
	}
	else
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 20.0, -12.0, -4.0, false);

	EV_EjectBrass(ShellOrigin, ShellVelocity, angles[GetYaw(args)], shell, TE_BOUNCE_SHELL, idx, 4);

	gEngfuncs.pEventAPI->EV_PlaySound(
		idx, 
		origin,
		CHAN_WEAPON, 
		args->bparam1 ? g_WpnItemInfo[weaponid].szShotSound2 :g_WpnItemInfo[weaponid].szShotSound1, 
		1.0, 
		ATTN_NORM, 
		0, 
		94 + gEngfuncs.pfnRandomLong(0, 0xf));

	EV_GetGunPosition(args, vecSrc, origin);

	VectorCopy(forward, vecAiming);

	vSpread[0] = args->fparam1;
	vSpread[1] = args->fparam2;
	vSpread[2] = 0;

	EV_HLDM_FireBullets(idx, forward, right, up, 1, vecSrc, vecAiming, vSpread, g_WpnItemInfo[weaponid].iBulletDistance, g_WpnItemInfo[weaponid].iBulletType, 0, 0, g_WpnItemInfo[weaponid].iBulletPenetration  );
	
}



void EV_FireRifle( event_args_t *args )
{
	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	int lefthand;
	vec3_t vSpread;
	int weaponid;
		
	ent = gEngfuncs.GetViewModel();
	idx = args->entindex;
	lefthand = cl_righthand->value;
	weaponid = g_pWeapon->GetPlayerWpnId (idx, 1);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	angles[0] += args->iparam1 / 100.0;
	angles[1] += args->iparam2 / 100.0;

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);
	shell = gEngfuncs.pEventAPI->EV_FindModelIndex(g_WpnItemInfo[weaponid].szShellModel);
	
	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		EV_MuzzleFlash();
		g_pWeapon->GunFire();

		//danger The second param is the body
		gEngfuncs.pEventAPI->EV_WeaponAnimation(gEngfuncs.pfnRandomLong(g_WpnItemInfo[weaponid].iAnimShotStart, g_WpnItemInfo[weaponid].iAnimShotEnd), 2);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 3, 0.35, 20, 20, 20, 4, velocity, false, 35);
	}

	if (EV_IsLocal(idx))
	{
		float vecScale[3] = {20.0, -8.0, -13.0};
		if (lefthand)
			vecScale[2] = 0 - vecScale[2];
		
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, vecScale[0], vecScale[1], vecScale[2], true);

		VectorCopy(ent->attachment[1], ShellOrigin);
		ShellVelocity[2] -= 75;
	}
	else
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 20.0, -12.0, -4.0, false);

	EV_EjectBrass(ShellOrigin, ShellVelocity, angles[GetYaw(args)], shell, TE_BOUNCE_SHELL, idx, 9);

	gEngfuncs.pEventAPI->EV_PlaySound(
		idx, 
		origin, 
		CHAN_WEAPON, 
		args->bparam1 ? g_WpnItemInfo[weaponid].szShotSound1 : g_WpnItemInfo[weaponid].szShotSound2, 
		ATTN_NORM, 
		0.4, 
		0, 
		94 + gEngfuncs.pfnRandomLong(0, 0xf));

	EV_GetGunPosition(args, vecSrc, origin);

	VectorCopy(forward, vecAiming);

	vSpread[0] = args->fparam1;
	vSpread[1] = args->fparam2;
	vSpread[2] = 0;

	
	EV_HLDM_FireBullets(
		idx, 
		forward, 
		right, 
		up, 
		1, 
		vecSrc, 
		vecAiming, 
		vSpread, 
		g_WpnItemInfo[weaponid].iBulletDistance , 
		g_WpnItemInfo[weaponid].iBulletType , 
		0, 
		0, 
		g_WpnItemInfo[weaponid].iBulletPenetration );
}

void EV_FireM32(struct event_args_s *args)
{
	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	int lefthand;
	vec3_t vSpread;
	int weaponid;
		
	ent = GetViewEntity();
	idx = args->entindex;
	lefthand = cl_righthand->value;
	weaponid = g_pWeapon->GetPlayerWpnId (idx, 1);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	angles[0] += args->iparam1 / 100.0;
	angles[1] += args->iparam2 / 100.0;

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);

	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire ();
		EV_MuzzleFlash();

		gEngfuncs.pEventAPI->EV_WeaponAnimation(random_num(g_WpnItemInfo[weaponid].iAnimShotStart ,g_WpnItemInfo[weaponid].iAnimShotEnd), 2);
		
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 3, 0.45, 15, 15, 15, 3, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 40, 0.35, 9, 9, 9, 2, velocity, false, 35);
	}

	gEngfuncs.pEventAPI->EV_PlaySound(
		idx, 
		origin, 
		CHAN_WEAPON, 
		args->bparam1 ? g_WpnItemInfo[weaponid].szShotSound2: g_WpnItemInfo[weaponid].szShotSound1, 
		1.0, 
		0.52, 
		0, 
		94 + gEngfuncs.pfnRandomLong(0, 0xf));
}

void EV_FireM4A1(struct event_args_s *args)
{
	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	int silencer_on;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	int lefthand;
	vec3_t vSpread;
	int weaponid;

	ent = GetViewEntity();
	idx = args->entindex;
	silencer_on = args->bparam1;
	lefthand = cl_righthand->value;
	weaponid = g_pWeapon->GetPlayerWpnId (idx, 1);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	angles[0] += args->iparam1 / 100.0;
	angles[1] += args->iparam2 / 100.0;

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);
	shell = gEngfuncs.pEventAPI->EV_FindModelIndex(g_WpnItemInfo[weaponid].szShellModel );

	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire ();

		if (silencer_on)
		{
			gEngfuncs.pEventAPI->EV_WeaponAnimation(gEngfuncs.pfnRandomLong(g_WpnItemInfo[weaponid].iAnimShotStart, g_WpnItemInfo[weaponid].iAnimShotEnd), 2);
		}
		else
		{		
			EV_MuzzleFlash();
			int iAnim = random_num(g_WpnItemInfo[weaponid].iAnimShotStart, g_WpnItemInfo[weaponid].iAnimShotEnd) + g_WpnItemInfo[weaponid].iAnimEnd;
			gEngfuncs.pEventAPI->EV_WeaponAnimation(iAnim, 2);
		}

		if (silencer_on)
			EV_HLDM_CreateSmoke(ent->attachment[0], forward, 3, 0.3, 16, 16, 16, 3, velocity, false, 35);
		else
			EV_HLDM_CreateSmoke(ent->attachment[2], forward, 3, 0.2, 16, 16, 16, 4, velocity, false, 35);
		
	}

	if (EV_IsLocal(idx))
	{
		float vecScale[3] = {20.0, -8.0, -10.0};

		if (lefthand)
			vecScale[2] = 0 - vecScale[2];
		
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, vecScale[0], vecScale[1], vecScale[2], true);

		VectorCopy(ent->attachment[1], ShellOrigin);
		ShellVelocity[2] -= 45;
	}
	else
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 20.0, -12.0, -4.0, false);

	EV_EjectBrass(ShellOrigin, ShellVelocity, angles[GetYaw(args)], shell, TE_BOUNCE_SHELL, idx, 10);

	if (silencer_on)
	{
		gEngfuncs.pEventAPI->EV_PlaySound(
			idx, 
			origin, 
			CHAN_WEAPON, 
			g_WpnItemInfo[weaponid].szShotSound1, 
			1.0, 
			1.4, 
			0, 
			94 + gEngfuncs.pfnRandomLong(0, 0xf));
	}
	else
	{
		gEngfuncs.pEventAPI->EV_PlaySound(
			idx, 
			origin, 
			CHAN_WEAPON, 
			g_WpnItemInfo[weaponid].szShotSound2, 
			1.0, 
			1.4, 
			0, 
			94 + gEngfuncs.pfnRandomLong(0, 0xf));
	}

	EV_GetGunPosition(args, vecSrc, origin);

	VectorCopy(forward, vecAiming);

	vSpread[0] = args->fparam1;
	vSpread[1] = args->fparam2;
	vSpread[2] = 0;

	EV_HLDM_FireBullets(idx, forward, right, up, 1, vecSrc, vecAiming, vSpread, g_WpnItemInfo[weaponid].iBulletDistance, g_WpnItemInfo[weaponid].iBulletType, 0, 0, g_WpnItemInfo[weaponid].iBulletPenetration);
}



void EV_FirePistol(struct event_args_s *args)
{
	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	int empty;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	int lefthand;
	vec3_t vSpread;
	int weaponid;

	ent = GetViewEntity();
	idx = args->entindex;
	empty = args->bparam1 != false;
	lefthand = cl_righthand->value;
	weaponid = g_pWeapon->GetPlayerWpnId (idx, 2);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	angles[0] += args->iparam1 / 100.0;
	angles[1] += args->iparam2 / 100.0;

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);
	shell = gEngfuncs.pEventAPI->EV_FindModelIndex(g_WpnItemInfo[weaponid].szShellModel );

	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire ();
		EV_MuzzleFlash();


		if (empty)
		{
			gEngfuncs.pEventAPI->EV_WeaponAnimation(g_WpnItemInfo[weaponid].iAnimShotEmpty , 2);
		}
		else
		{		
			int iAnim = random_num(g_WpnItemInfo[weaponid].iAnimShotStart , g_WpnItemInfo[weaponid].iAnimShotEnd);
			gEngfuncs.pEventAPI->EV_WeaponAnimation(iAnim , 2);
		}

		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 0, 0.25, 10, 10, 10, 3, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 25, 0.3, 15, 15, 15, 2, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 50, 0.2, 25, 25, 25, 2, velocity, false, 35);

	}

	if (EV_IsLocal(idx))
	{
		float vecScale[3] = {35.0, -11.0, -16.0};

		if (lefthand)
			vecScale[2] = 0 - vecScale[2];
		
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, vecScale[0], vecScale[1], vecScale[2], true);

		VectorCopy(ent->attachment[1], ShellOrigin);
		VectorScale(ShellVelocity, 0.75, ShellVelocity);
		ShellVelocity[2] += 25;
	}
	else
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 20.0, -12.0, -4.0, false);

	EV_EjectBrass(ShellOrigin, ShellVelocity, angles[GetYaw(args)], shell, TE_BOUNCE_SHELL, idx, 5);

	gEngfuncs.pEventAPI->EV_PlaySound(
		idx, 
		origin, 
		CHAN_WEAPON, 
		args->bparam1 ? g_WpnItemInfo[weaponid].szShotSound2 : g_WpnItemInfo[weaponid].szShotSound1, 
		1.0, 0.6, 0, 94 + gEngfuncs.pfnRandomLong(0, 0xf));

	EV_GetGunPosition(args, vecSrc, origin);

	VectorCopy(forward, vecAiming);

	vSpread[0] = args->fparam1;
	vSpread[1] = args->fparam2;
	vSpread[2] = 0;

	EV_HLDM_FireBullets(idx, forward, right, up, 1, vecSrc, vecAiming, vSpread, g_WpnItemInfo[weaponid].iBulletDistance, g_WpnItemInfo[weaponid].iBulletType , 0, 0, g_WpnItemInfo[weaponid].iBulletPenetration);
}


void EV_FireShotgun(struct event_args_s *args)
{
	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	int lefthand;
	vec3_t vSpread;
	int weaponid;
		
	ent = GetViewEntity();
	idx = args->entindex;
	lefthand = cl_righthand->value;
	weaponid = g_pWeapon->GetPlayerWpnId (idx, 1);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	angles[0] += args->iparam1 / 100.0;
	angles[1] += args->iparam2 / 100.0;

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);
	shell = gEngfuncs.pEventAPI->EV_FindModelIndex(g_WpnItemInfo[weaponid].szShellModel );

	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire ();
		EV_MuzzleFlash();

		gEngfuncs.pEventAPI->EV_WeaponAnimation(random_num(g_WpnItemInfo[weaponid].iAnimShotStart , g_WpnItemInfo[weaponid].iAnimShotEnd), 2);
		
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 3, 0.45, 15, 15, 15, 3, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 40, 0.35, 9, 9, 9, 2, velocity, false, 35);
		
	}

	if (EV_IsLocal(idx))
	{
		float vecScale[3] = {22.0, -9.0, -11.0};
		

		if (lefthand)
			vecScale[2] = 0 - vecScale[2];
		
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, vecScale[0], vecScale[1], vecScale[2], true);

		VectorCopy(ent->attachment[1], ShellOrigin);
		VectorScale(ShellVelocity, 1.25, ShellVelocity);
		ShellVelocity[2] -= 50;
	}
	else
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 20.0, -12.0, -4.0, false);

	EV_EjectBrass(ShellOrigin, ShellVelocity, angles[GetYaw(args)], shell, TE_BOUNCE_SHOTSHELL, idx, 3);

	gEngfuncs.pEventAPI->EV_PlaySound(
		idx, 
		origin, 
		CHAN_WEAPON, 
		args->bparam1 ? g_WpnItemInfo[weaponid].szShotSound2:g_WpnItemInfo[weaponid].szShotSound1, 
		1.0, 0.52, 0, 94 + gEngfuncs.pfnRandomLong(0, 0xf));

	EV_GetGunPosition(args, vecSrc, origin);

	VectorCopy(forward, vecAiming);

	vSpread[0] = 0.0725;
	vSpread[1] = 0.0725;
	vSpread[2] = 0;

	if (args->fparam1)
	{
		vSpread[0] = args->fparam1;
		vSpread[1] = args->fparam2;
	}

	int shots = args->iparam1 ? args->iparam1 : 6;

	EV_HLDM_FireBullets(idx, forward, right, up, shots, vecSrc, vecAiming, vSpread, g_WpnItemInfo[weaponid].iBulletDistance, g_WpnItemInfo[weaponid].iBulletType , 0, 0, 1);
}

void EV_FireSniper(struct event_args_s *args)
{
	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	int lefthand;
	vec3_t vSpread;
	int weaponid;
		
	ent = GetViewEntity();
	idx = args->entindex;
	lefthand = cl_righthand->value;
	weaponid = g_pWeapon->GetPlayerWpnId (idx , 1);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	angles[0] += args->iparam1 / 100.0;
	angles[1] += args->iparam2 / 100.0;

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);

	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire ();
		EV_MuzzleFlash();


		gEngfuncs.pEventAPI->EV_WeaponAnimation(random_num(g_WpnItemInfo[weaponid].iAnimShotStart ,g_WpnItemInfo[weaponid].iAnimShotEnd), 2);

		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 3, 0.5, 20, 20, 20, 3, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 40, 0.5, 15, 15, 15, 2, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 80, 0.5, 10, 10, 10, 2, velocity, false, 35);		
	}

	gEngfuncs.pEventAPI->EV_PlaySound(idx, origin, CHAN_WEAPON, args->bparam1?g_WpnItemInfo[weaponid].szShotSound2:g_WpnItemInfo[weaponid].szShotSound1, 1.0, 0.28, 0, 94 + gEngfuncs.pfnRandomLong(0, 0xf));

	EV_GetGunPosition(args, vecSrc, origin);

	vSpread[0] = args->fparam1 / 1000.0;
	vSpread[1] = args->fparam2 / 1000.0;
	vSpread[2] = 0;

	VectorCopy(forward, vecAiming);

	EV_HLDM_FireBullets(idx, forward, right, up, 1, vecSrc, vecAiming, vSpread, g_WpnItemInfo[weaponid].iBulletDistance, g_WpnItemInfo[weaponid].iBulletType , 0, 0, g_WpnItemInfo[weaponid].iBulletPenetration);
}


void EV_FireSniper2(struct event_args_s *args)
{
	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	int lefthand;
	vec3_t vSpread;
	int weaponid;
		
	ent = GetViewEntity();
	idx = args->entindex;
	lefthand = cl_righthand->value;
	weaponid = g_pWeapon->GetPlayerWpnId (idx, 1);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	angles[0] += args->iparam1 / 100.0;
	angles[1] += args->iparam2 / 100.0;

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);
	shell = gEngfuncs.pEventAPI->EV_FindModelIndex(g_WpnItemInfo[weaponid].szShellModel );

	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire ();
		EV_MuzzleFlash();

		gEngfuncs.pEventAPI->EV_WeaponAnimation(random_num(g_WpnItemInfo[weaponid].iAnimShotStart, g_WpnItemInfo[weaponid].iAnimShotEnd), 2);

		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 3, 0.3, 35, 35, 35, 4, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 35, 0.35, 30, 30, 30, 2, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 70, 0.3, 25, 25, 25, 2, velocity, false, 35);
		
	}

	if (EV_IsLocal(idx))
	{
		float vecScale[3] = {20.0, -8.0, -10.0};

		if (lefthand)
			vecScale[2] = 0 - vecScale[2];

		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, vecScale[0], vecScale[1], vecScale[2], true);
		
		VectorCopy(ent->attachment[1], ShellOrigin);
		VectorScale(ShellVelocity, 1.5, ShellVelocity);
		ShellVelocity[2] -= 50.0;
	}
	else
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 20.0, -12.0, -4.0, false);

	EV_EjectBrass(ShellOrigin, ShellVelocity, angles[GetYaw(args)], shell, TE_BOUNCE_SHELL, idx, 17);
	gEngfuncs.pEventAPI->EV_PlaySound(idx, origin, CHAN_WEAPON, args->bparam1?g_WpnItemInfo[weaponid].szShotSound2:g_WpnItemInfo[weaponid].szShotSound1, 1.0, 0.6, 0, 94 + gEngfuncs.pfnRandomLong(0, 0xf));

	EV_GetGunPosition(args, vecSrc, origin);

	VectorCopy(forward, vecAiming);

	vSpread[0] = args->fparam1;
	vSpread[1] = args->fparam2;
	vSpread[2] = 0;

	EV_HLDM_FireBullets(idx, forward, right, up, 1, vecSrc, vecAiming, vSpread, g_WpnItemInfo[weaponid].iBulletDistance , g_WpnItemInfo[weaponid].iBulletType , 0, 0, g_WpnItemInfo[weaponid].iBulletPenetration );
}


void EV_FireUSP(struct event_args_s *args)
{
	int idx;
	vec3_t origin;
	vec3_t angles;
	vec3_t velocity;
	int empty;
	int silencer_on;
	vec3_t ShellVelocity;
	vec3_t ShellOrigin;
	int shell;
	vec3_t vecSrc, vecAiming;
	vec3_t up, right, forward;
	cl_entity_t *ent;
	int lefthand;
	vec3_t vSpread;
	vec3_t smoke_origin;
	float base_scale;
	int weaponid;

	ent = GetViewEntity();
	idx = args->entindex;
	empty = args->bparam1 != false;
	silencer_on = args->bparam2;
	lefthand = cl_righthand->value;
	weaponid = g_pWeapon->GetPlayerWpnId (idx, 2);

	VectorCopy(args->origin, origin);
	VectorCopy(args->angles, angles);
	VectorCopy(args->velocity, velocity);

	angles[0] += args->iparam1 / 100.0;

	gEngfuncs.pfnAngleVectors(angles, forward, right, up);
	shell = gEngfuncs.pEventAPI->EV_FindModelIndex(g_WpnItemInfo[weaponid].szShellModel );

	if (EV_IsLocal(idx))
	{
		g_iShotsFired++;
		g_pWeapon->GunFire ();

		if (silencer_on)
		{
			if (!empty)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(random_num(g_WpnItemInfo[weaponid].iAnimShotStart , g_WpnItemInfo[weaponid].iAnimShotEnd ), 2);
			else
				gEngfuncs.pEventAPI->EV_WeaponAnimation(g_WpnItemInfo[weaponid].iAnimShotEmpty , 2);
		}
		else
		{
			EV_MuzzleFlash();

			int a = g_WpnItemInfo[weaponid].iAnimEnd ;

			if (!empty)
				gEngfuncs.pEventAPI->EV_WeaponAnimation(random_num(a + g_WpnItemInfo[weaponid].iAnimShotStart, a + g_WpnItemInfo[weaponid].iAnimShotEnd ), 2);
			else
				gEngfuncs.pEventAPI->EV_WeaponAnimation(a + g_WpnItemInfo[weaponid].iAnimShotEmpty, 2);
		}

		VectorCopy(ent->attachment[0], smoke_origin);

		smoke_origin[0] -= forward[0] * 3;
		smoke_origin[1] -= forward[1] * 3;
		smoke_origin[2] -= forward[2] * 3;

		base_scale = gEngfuncs.pfnRandomFloat(0.1, 0.25);

		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 20, base_scale + 0.1, 10, 10, 10, 2, velocity, false, 35);
		EV_HLDM_CreateSmoke(ent->attachment[0], forward, 40, base_scale, 13, 13, 13, 2, velocity, false, 35);
		
	}

	if (EV_IsLocal(idx))
	{
		if (lefthand == 0)
			EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 36.0, -14.0, -14.0, false);
		else
			EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 36.0, -14.0, 14.0, false);

		VectorCopy(ent->attachment[1], ShellOrigin);
		VectorScale(ShellVelocity, 0.5, ShellVelocity);
		ShellVelocity[2] += 45.0;
	}
	else
		EV_GetDefaultShellInfo(args, origin, velocity, ShellVelocity, ShellOrigin, forward, right, up, 20.0, -12.0, 4.0, false);

	EV_EjectBrass(ShellOrigin, ShellVelocity, angles[GetYaw(args)], shell, TE_BOUNCE_SHELL, idx, 5);

	if (silencer_on)
	{
		gEngfuncs.pEventAPI->EV_PlaySound(idx, origin, CHAN_WEAPON, g_WpnItemInfo[weaponid].szShotSound1, 1.0, ATTN_NORM, 0, 94 + gEngfuncs.pfnRandomLong(0, 0xf));
	}
	else
	{
		gEngfuncs.pEventAPI->EV_PlaySound(idx, origin, CHAN_WEAPON, g_WpnItemInfo[weaponid].szShotSound2, 1.0, 2.0, 0, 94 + gEngfuncs.pfnRandomLong(0, 0xf));
	}

	EV_GetGunPosition(args, vecSrc, origin);

	VectorCopy(forward, vecAiming);

	vSpread[0] = args->fparam1;
	vSpread[1] = args->fparam2;
	vSpread[2] = 0;

	EV_HLDM_FireBullets(idx, forward, right, up, 1, vecSrc, vecAiming, vSpread, g_WpnItemInfo[weaponid].iBulletDistance , g_WpnItemInfo[weaponid].iBulletType , 0, 0, g_WpnItemInfo[weaponid].iBulletPenetration );
}

