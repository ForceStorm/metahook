#ifndef _ENGFUNCS_H
#define _ENGFUNCS_H

int EngFunc_AddCommand(char *szCmds, void (*pFunction)(void));
void Engfuncs_DrawAdditive( int frame, int x, int y, const wrect_t *prc );
void Engfuncs_FillRGBA( int x, int y, int width, int height, int r, int g, int b, int a );
void Engfuncs_SerCrosshair( HSPRITE hspr, wrect_t rc, int r, int g, int b );
void Engfunc_HookEvent( char *name, void ( *pfnEvent )( struct event_args_s *args ) );
void Engine_InstallHook( void );


#endif