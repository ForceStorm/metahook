#include "base.h"
#include "Localize.h"
#include "plugins.h"

vgui::ILocalize *g_pLocalize;

void Localize_InstallHook( vgui::ILocalize *pLocalize )
{
	g_pLocalize = pLocalize;

	g_pLocalize->AddFile(g_pFileSystem,"Resource/forcestorm_%language%.txt");
}