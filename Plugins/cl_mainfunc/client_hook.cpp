#include "base.h"
#include "client_hook.h"


extern void GameUI_Init(void);
extern void EV_Init(void);
extern void Entity_Init(void);
extern void HL_Weapons_InstallHook(void);
extern void Resource_InstallHook(void);


void MemPatch_BlockSniperScope(void)
{
	unsigned char data[] = { 0xB8, 0x01, 0x00, 0x00, 0x00, 0xC2, 0x04, 0x00 };
	DWORD addr = (DWORD)g_pMetaHookAPI->SearchPattern((void *)g_pMetaSave->pExportFuncs->Initialize, 0x100000, "\x83\xEC\x50\xA1\x2A\x2A\x2A\x2A\x89\x4C\x24\x00\x85\xC0", 14);

	if (!addr)
	{
		MessageBox(NULL, "BlockSniperScope patch failed!", "Warning", MB_ICONWARNING);
		return;
	}

	g_pMetaHookAPI->WriteMemory((void *)addr, data, sizeof(data));
}




void Client_InstallHook( void )
{
	MemPatch_BlockSniperScope();
	GameUI_Init();
	EV_Init();
	Entity_Init();
	Resource_InstallHook();
	HL_Weapons_InstallHook();
}