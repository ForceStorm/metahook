/*====================================================================================================
	MetaHook Graphical User Interface (MGUI)
	built by Niko 2015.2.4
	Email : kvcsgame@hotmail.com

	Pay attention to the followings:
	1.you should call gMGUI.Redraw() in HUD_Redraw(){}
	2.you should call gMGUI.VidInit() in HUD_VidInit(){}
	3.you should call gMGUI.Initialize() in Initialize(){}
=====================================================================================================*/

#include "base.h"
#include "MGUI.h"
#include "hud.h"
#include "window_teammenu.h"

CMGUI gMGUI;
CANDIDATELIST *CandidateList;
vgui::HFont g_hImeFont;
CBaseTextEntry *g_pTextEntryOnFocus = NULL;

bool g_bDrawIme;
int g_iMouseTexid;
int  g_mgui_mouseevent;
int  g_mgui_oldmouseevent;
int  g_iIndex = 0;


/*
   =====================================================================================================

								[	Window Message And utils   ]

   =====================================================================================================
*/

WNDPROC g_MainWndProc;

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if( !gMGUI.IsEnable ())
		return CallWindowProc(g_MainWndProc, hWnd, message, wParam, lParam);

	CBaseTextEntry *pTextEntry = g_pTextEntryOnFocus;
	if( !pTextEntry )
		return CallWindowProc(g_MainWndProc, hWnd, message, wParam, lParam);

	int iwidth, iheight;
	pTextEntry->GetSize(iwidth, iheight);
	gFont.SetSize (pTextEntry->GetFontSize(), pTextEntry->GetFontSize());

	switch (message)
	{
	case WM_CHAR:
		{

			if (wParam == VK_BACK) // Backspace Key
			{
				size_t len = strlen( pTextEntry->m_szText );
				
				if (pTextEntry->m_szText[len - 1] < 0)
					pTextEntry->m_szText[len - 2] = '\0';
				else
					pTextEntry->m_szText[len - 1] = '\0';
			}
			else if(wParam == VK_RETURN)
			{
				pTextEntry->m_pfnTextOutput( pTextEntry->m_szText );
			}
			else
			{
				wchar_t *pWchar;
				pWchar = UTF8ToUnicode( pTextEntry->m_szText );
				if( gFont.GetLen(pWchar) >= iwidth - AdjustSizeW(30) )
					break;

				size_t len = strlen( pTextEntry->m_szText );
				pTextEntry->m_szText[len] = wParam;
				pTextEntry->m_szText[len + 1] = '\0';
			}
			break;
		}
	case WM_IME_CHAR:
		{
			wchar_t *pWchar;
			pWchar = UTF8ToUnicode( pTextEntry->m_szText );
			if( gFont.GetLen(pWchar)  >=  iwidth - AdjustSizeW(30) )
				break;

			char buf[3];
			buf[0] = wParam >> 8;
			buf[1] = (byte)wParam;
			buf[2] = '\0';
			strcat( pTextEntry->m_szText, UnicodeToUTF8( ANSIToUnicode(buf))  );
			return 1;
			break;
		}
	case WM_IME_NOTIFY:
		{
			HIMC hImc = ImmGetContext(hWnd);
			if(!hImc)
				break;
			if(wParam == IMN_CHANGECANDIDATE)
			{
				DWORD dwSize;
				dwSize = ImmGetCandidateList(hImc,0,NULL,0);
				if(dwSize > 0)
				{
					CandidateList = (LPCANDIDATELIST)GlobalAlloc( GPTR, dwSize );
					g_bDrawIme = true;
					ImmGetCandidateList(hImc,0,CandidateList,dwSize);
				}
				else memset(CandidateList,0,sizeof(CandidateList));
			}
			else if(wParam == IMN_CLOSECANDIDATE)
			{
				g_bDrawIme = false;
			}
			ImmReleaseContext( hWnd, hImc );
			break;
		}
	}

	return CallWindowProc(g_MainWndProc, hWnd, message, wParam, lParam);
}


void HUD_DrawPrintText(int x, int y, int r, int g, int b, int a, vgui::HFont font, wchar_t *text)
{
	int startX = x;
	int fontTall = g_pSurface->GetFontTall(font);

	gEngfuncs.pfnDrawSetTextColor(r / 255.0, g / 255.0, b / 255.0);

	for (int i = 0; i < wcslen(text); i++)
	{
		if (text[i] == '\n')
		{
			x = startX;
			y += fontTall;
			continue;
		}

		gEngfuncs.pfnVGUI2DrawCharacter(x, y, text[i], font);

		int w1, w2, w3;
		g_pSurface->GetCharABCwide(font, text[i], w1, w2, w3);

		x += w1 + w2 + w3;
	}
}


/*
   =====================================================================================================

								[	CMGUI   ]

   =====================================================================================================
*/

CMGUI::CMGUI()
{
	m_pWindows = NULL;
}
CMGUI::~CMGUI()
{
	DestroyAll();
}

bool CMGUI::IsEnable(void)
{
	if(m_pWindows == NULL)
		return false;

	CBaseWindow *pWindow = NULL;
	size_t iSize = VectorSize(m_pWindows);

	for(size_t i = 0; i < iSize; i++)
	{
		pWindow = *(CBaseWindow **)VectorGet(m_pWindows, i);
		if(pWindow && pWindow->GetVisible())
			return true;
	}
	return false;
}

void CMGUI::DestroyAll(void)
{
	if(m_pWindows == NULL)
		return;

	CBaseWindow *pWindow = NULL;
	size_t iSize = VectorSize(m_pWindows);

	for(size_t i = 0; i < iSize; i++)
	{
		pWindow = *(CBaseWindow **)VectorGet(m_pWindows, i);
		if(pWindow)
		{
			delete pWindow;
			pWindow = NULL;
		}
	}

	nVectorClear(m_pWindows);
	DestroyVector(m_pWindows);
	m_pWindows = NULL;
}

void CMGUI::AddWindow(CBaseWindow *pWindow) 
{
	if(m_pWindows == NULL)
		m_pWindows = CreateVector(sizeof(CBaseWindow));
	
	VectorPush(m_pWindows, &pWindow);
}

int CMGUI::Redraw (void)
{
	PointPos_t pp;
	gEngfuncs.GetMousePosition(&pp.x, &pp.y);

	if(m_iMouseState == MOUSE_FIXED)
		gExportfuncs.IN_ActivateMouse();
	else if(m_iMouseState == MOUSE_FREE)
		gExportfuncs.IN_DeactivateMouse();


	if(m_pWindows == NULL)
		return -1;

	size_t iSize;
	CBaseWindow *pWindow;

	iSize = VectorSize(m_pWindows);
	pWindow = NULL;

	// pre processing
	// to destroy the dead windows
	for(size_t i = 0; i < iSize; i++)
	{
		pWindow = *(CBaseWindow **)VectorGet(m_pWindows, i);
		if(pWindow != NULL)
		{
			if(pWindow->GetDead ())
			{
				VectorEarse(m_pWindows, i, i+1);
				iSize = VectorSize(m_pWindows);
				--i;

				delete pWindow;
			}
		}

		pWindow = NULL;
	}

	iSize = VectorSize(m_pWindows);
	pWindow = NULL;

	for(size_t i = 0; i < iSize; i++)
	{
		pWindow = *(CBaseWindow **)VectorGet(m_pWindows, i);
		if(pWindow != NULL)
		{
			pWindow->PreDraw ();
			pWindow->Draw (&pp);
			pWindow->PostDraw (&pp);
		}

		pWindow = NULL;
	}

	if(m_iMouseState == MOUSE_FREE)
		gTGA.Draw (g_iMouseTexid, pp.x, pp.y, 17, 25 );

	return 1;
}

void CMGUI::VidInit(void)
{
	DestroyAll();


	if(!g_hImeFont)
	{
		g_pSurface->AddGlyphSetToFont ( g_hImeFont, "EngineFont", AdjustSizeH( IME_FONT_SIZE ) , AdjustSizeW( IME_FONT_SIZE ), 0, 0, 0, 0, 1);
		g_iMouseTexid = gTGA.GetTextureId ("cstrike\\resource\\tga\\cursor.tga");
	}
}
void CMGUI::Initialize(void)
{
	HWND hWnd = FindWindow("Valve001", NULL);
	g_MainWndProc = (WNDPROC)GetWindowLong( hWnd, GWL_WNDPROC );
	SetWindowLong(hWnd, GWL_WNDPROC, (LONG)MainWndProc);


	char szTitle[128];
	FILE *pFile = fopen("version_num", "r");

	if(pFile)
	{
		char szBuff[10];
		fgets(szBuff, sizeof(szBuff)-1, pFile);

		if( szBuff[ strlen( szBuff )-1] == '\n')
			szBuff[ strlen(szBuff) -1] = '\0';

		sprintf( szTitle, "�����籩 %s", szBuff );
		fclose(pFile);
	}

	SendMessage( hWnd, WM_SETTEXT, 0, (LPARAM)szTitle);

	gEngfuncs.pfnClientCmd ("echo \"\" ");
	gEngfuncs.pfnClientCmd ("echo \"     Force Storm\" ");
	gEngfuncs.pfnClientCmd ("echo \"     The metahook client has been loaded \" ");
	gEngfuncs.pfnClientCmd ("echo \"     When you meet a bug, you can send a mail to <kvcsgame@gmail.com> \" ");
	gEngfuncs.pfnClientCmd ("echo \"     Or to <kvcsgame@hotmail.com> \" ");
	gEngfuncs.pfnClientCmd ("echo \"     Your bug reports will help improve the game \" ");
	gEngfuncs.pfnClientCmd ("echo \"     Want more details? Please goto our website:\" ");
	gEngfuncs.pfnClientCmd ("echo \"     http://tieba.baidu.com/f?kw=k%B0%E6cs&fr=home\" ");
	gEngfuncs.pfnClientCmd ("echo \"\" ");
	gEngfuncs.pfnClientCmd ("echo \"              By FreeCreater Team 2014.9.14 \" ");
	gEngfuncs.pfnClientCmd ("echo \"\" ");
	gEngfuncs.pfnClientCmd ("echo \"\" ");

}
void CMGUI::InputKey(int iKey)
{
	if(m_pWindows == NULL)
		return;

	size_t iSize = VectorSize(m_pWindows);
	CBaseWindow *pWin = NULL;

	for(size_t i = 0; i < iSize; i++)
	{
		pWin = *(CBaseWindow **)VectorGet(m_pWindows, i);
		if(pWin && pWin->GetFocus())
		{
			pWin->InputKey(iKey);
			return;
		}
	}
}
int CMGUI::GetWindowIndex(CBaseWindow *pWindow_Specific)
{
	if(m_pWindows == NULL)
		return -1;

	size_t iSize = VectorSize(m_pWindows);
	CBaseWindow *pWindow = NULL;

	for(size_t i = 0; i < iSize; i++)
	{
		pWindow = *(CBaseWindow **)VectorGet(m_pWindows, i);
		if(pWindow == pWindow_Specific)
			return (int)i;
	}
	return -1;
}
vector_t* CMGUI::GetWindows(void)
{
	return m_pWindows;
}

void CMGUI::SetMouseState(int iState)
{
	m_iMouseState = iState;
}

int CMGUI::GetMouseState(void)
{
	return m_iMouseState;
}


/*
=====================================================================================================

								[	CBaseMGUI   ]

 =====================================================================================================
 */

int MGUI_SortComparer(const void *a, const void *b)
{
	return (*(CBaseMGUI **)a)->GetPriority() - (*(CBaseMGUI **)b)->GetPriority();
}

CBaseMGUI::CBaseMGUI()
{
	g_iIndex++;
	m_iIndex = g_iIndex;
}
CBaseMGUI::~CBaseMGUI()
{
	DestroyChild();
}

void CBaseMGUI::Init (void)
{
	m_iXpos = 0;
	m_iYpos = 0;
	m_iWidth = 0;
	m_iHeight = 0;
	m_bKillme = FALSE;
	m_bOnFocus = FALSE;
	m_bVisible = TRUE;
	m_pParent = NULL;
	m_pChildren = NULL;
	m_bSortOut = FALSE;
	m_iPriority = 0;
}
/*
CBaseMGUI* CBaseMGUI::Instance(CBaseMGUI *pParent, int iClass)
{
	CBaseMGUI* pMGUI = NULL;
	pMGUI = new CBaseMGUI;

	if(!pMGUI)
		return NULL;

	pMGUI->Init();
	pMGUI->SetParent (pParent);
	pMGUI->SetClass (iClass);

	return pMGUI;
}
*/

void CBaseMGUI::GetPosition(int &iXpos, int &iYpos)
{
	int x_parent = 0, y_parent = 0;
	if(GetParent())
		GetParent()->GetPosition(x_parent, y_parent);

	iXpos = x_parent + m_iXpos;
	iYpos = y_parent + m_iYpos;
}


CBaseMGUI* CBaseMGUI::GetParent(void)
{
	return m_pParent;
}

vector_t*  CBaseMGUI::GetChildren(void)
{
	return m_pChildren;
}

int CBaseMGUI::GetClass(void) 
{
	return m_iClass;
}

bool CBaseMGUI::GetFocus (void)
{
	return m_bOnFocus;
}
bool CBaseMGUI::GetDead(void)
{
	return m_bKillme;
}

int CBaseMGUI::GetIndex(void) 
{
	return m_iIndex;
}

void CBaseMGUI::GetSize(int &iWidth, int &iHeight)
{
	iWidth = m_iWidth;
	iHeight = m_iHeight;
}

bool CBaseMGUI::GetVisible(void) 
{
	return m_bVisible;
}

int CBaseMGUI::GetPriority(void)
{
	return m_iPriority;
}

bool CBaseMGUI::IsInRect(PointPos_t *pp)
{
	int ix_rect, iy_rect, iw_rect, ih_rect;
	GetPosition(ix_rect, iy_rect);
	GetSize(iw_rect, ih_rect);

	if(pp->x  > ix_rect && pp->x  < ix_rect + iw_rect && pp->y > iy_rect && pp->y  < iy_rect + ih_rect)
		return true;
	else
		return false;
}

bool CBaseMGUI::IsClick(void)
{
	bool b =  (!(g_mgui_mouseevent & ME_LEFTCLICK) && (g_mgui_oldmouseevent & ME_LEFTCLICK));
	if(b)
	{
		g_mgui_mouseevent = 0;
		g_mgui_oldmouseevent = 0;
	}
	return b;
}

void CBaseMGUI::SetPosition(int iXpos, int iYpos)
{
	m_iXpos = iXpos;
	m_iYpos = iYpos;
}

void CBaseMGUI::AddChild(CBaseMGUI* pChild)
{
	if(m_pChildren == NULL)
		m_pChildren = CreateVector(sizeof(CBaseMGUI));
	
	VectorPush(m_pChildren, &pChild);
}

void CBaseMGUI::SetParent(CBaseMGUI* pParent)
{
	m_pParent = pParent;
	pParent->AddChild (this);
}

void CBaseMGUI::SetClass(int iClass) 
{
	m_iClass = iClass;
}

void CBaseMGUI::SetFocus (bool bFocus)
{
	m_bOnFocus = bFocus;

	if(bFocus && GetParent())
	{
		GetParent()->SetFocus (TRUE);
		vector_t *pChildren = GetParent()->GetChildren();
		if(pChildren == NULL)
			return;

		size_t iSize = VectorSize(pChildren);
		CBaseMGUI *pMGUI = NULL;

		for(size_t i = 0; i < iSize; i++)
		{
			pMGUI = *(CBaseMGUI **)VectorGet(pChildren, i);
			if(pMGUI && pMGUI != this)
				pMGUI->SetFocus(false);
		}
	}
}

void CBaseMGUI::SetSize(int iWidth, int iHeight)
{
	m_iWidth = iWidth;
	m_iHeight = iHeight;
}

void CBaseMGUI::SetVisible(bool bVisible) 
{
	m_bVisible = bVisible;
}

void CBaseMGUI::DestroyChild(void)
{
	if(m_pChildren == NULL)
		return;

	CBaseMGUI *pMGUI;
	size_t iSize = VectorSize(m_pChildren);

	for(size_t i = 0; i < iSize; i++)
	{
		pMGUI = *(CBaseMGUI **)VectorGet(m_pChildren, i);
		if(pMGUI)
		{
			delete pMGUI;
			pMGUI = NULL;
		}
	}

	nVectorClear(m_pChildren);
	DestroyVector(m_pChildren);
	m_pChildren = NULL;
}

void CBaseMGUI::Destroy(void)
{
	m_bKillme = TRUE;
}

int CBaseMGUI::FindChild(CBaseMGUI *pChild)
{
	if(m_pChildren == NULL)
		return -1;

	size_t iSize = VectorSize(m_pChildren);
	CBaseMGUI *pMGUI = NULL;

	for(size_t i = 0; i < iSize; i++)
	{
		pMGUI = *(CBaseMGUI **)VectorGet(m_pChildren, i);
		if(pMGUI == pChild)
			return (int)i;
	}
	return -1;
}

void CBaseMGUI::SetFree(CBaseMGUI *pChild) 
{
	if(m_pChildren == NULL || pChild== NULL)
		return;

	int i = FindChild(pChild);
	if( i >= 0 )
	{
		VectorEarse(m_pChildren, i, i+1);
		pChild->SetParent (NULL);
	}
}

void CBaseMGUI::SortAll (void)
{
	m_bSortOut = TRUE;
}

void CBaseMGUI::PreDraw(void)
{
	if(m_bSortOut)
	{
		m_bSortOut = FALSE;

		if(m_pChildren)
			VectorSort(m_pChildren, MGUI_SortComparer);
	}
}

void CBaseMGUI::SetPriority(int iPriority)
{
	m_iPriority = iPriority;
}

void CBaseMGUI::PostDraw(PointPos_t *pp)
{
	if(m_pChildren == NULL)
		return ;

	size_t iSize;
	CBaseMGUI *pMGUI;

	iSize = VectorSize(m_pChildren);
	pMGUI = NULL;

	// pre processing
	// to destroy the dead windows
	for(size_t i = 0; i < iSize; i++)
	{
		pMGUI = *(CBaseMGUI **)VectorGet(m_pChildren, i);
		if(pMGUI != NULL)
		{
			if(pMGUI->GetDead ())
			{
				VectorEarse(m_pChildren, i, i+1);
				iSize = VectorSize(m_pChildren);
				--i;

				delete pMGUI;
			}
		}

		pMGUI = NULL;
	}

	iSize = VectorSize(m_pChildren);
	pMGUI = NULL;

	for(size_t i = 0; i < iSize; i++)
	{
		pMGUI = *(CBaseMGUI **)VectorGet(m_pChildren, i);
		if(pMGUI != NULL)
		{
			int x, y;
			GetPosition(x, y);

			glEnable(GL_SCISSOR_TEST);
			glScissor( x, g_sScreenInfo.iHeight-( y + m_iHeight),  m_iWidth, m_iHeight );

				pMGUI->PreDraw ();
				if(pMGUI->GetVisible())
				{
					pMGUI->Draw (pp);
					pMGUI->PostDraw (pp);
				}

			glDisable(GL_SCISSOR_TEST);
			glEnd();
		}

		pMGUI = NULL;
	}
	return ;
}


/*
 =====================================================================================================

								[	CBaseWindow   ]

 =====================================================================================================
*/
/*
CBaseWindow* CBaseWindow::Instance(void)
{
	CBaseWindow* pWindow = NULL;
	pWindow = new CBaseWindow;

	if(!pWindow)
		return NULL;

	pWindow->Init();
	gMGUI.AddWindow(pWindow);

	return pWindow;
}
*/

void CBaseWindow::SetFocus (bool bFocus)
{
	m_bOnFocus  = bFocus;
	if(bFocus)
	{
		vector_t *windows = gMGUI.GetWindows();
		if(windows == NULL)
			return;

		size_t iSize = VectorSize(windows);
		CBaseWindow *pWin = NULL;

		for(size_t i = 0; i < iSize; i++)
		{
			pWin = *(CBaseWindow **)VectorGet(windows, i);
			if(pWin && pWin != this)
				pWin->SetFocus(false);
		}
	}
}




/*
 =====================================================================================================

								[	CBaseLabel   ]

 =====================================================================================================
*/
void CBaseLabel::Init( )
{
	BaseClass::Init();

	m_bLocalStr = FALSE;
	m_szLocalString[0] = '\0';
	memset(m_pwText, 0, sizeof(m_pwText));
	m_iSize = 20;

	for(int i = 0; i<4; i++)
		m_iColor[i] = 255;
}

CBaseLabel* CBaseLabel::Instance( CBaseMGUI *pParent )
{
	CBaseLabel* pLabel = NULL;
	pLabel = new CBaseLabel;

	if(!pLabel)
		return NULL;

	pLabel->Init();
	pLabel->SetParent (pParent);
	pLabel->SetClass (CLASS_LABEL);

	return pLabel;
}

void CBaseLabel::SetColor(int r, int g, int b)
{
	m_iColor[0] = r;
	m_iColor[1] = g;
	m_iColor[2] = b;
}

void CBaseLabel::SetAlpha(int a)
{
	m_iColor[3] = a;
}

int CBaseLabel::GetAlpha( void )
{
	return m_iColor[3];
}

void CBaseLabel::SetFontSize(int iSize)
{
	m_iSize = iSize;
}

void CBaseLabel::SetString (wchar_t *pText)
{
	m_bLocalStr = FALSE;
	wsprintfW(m_pwText, pText);
}

void CBaseLabel::SetLocalString (char *pText)
{
	m_bLocalStr = TRUE;
	sprintf(m_szLocalString, "%s", pText);
}

void CBaseLabel::Draw(PointPos_t *pp)
{
	if(!m_pwText[0] && !m_szLocalString[0])
		return;

	int x, y;
	GetPosition(x, y);

	gFont.SetSize (m_iSize, m_iSize);
	gFont.SetColor(m_iColor[0], m_iColor[1], m_iColor[2]);
	gFont.SetAlpha (m_iColor[3]);

	if(m_bLocalStr)
		gFont.DrawString (g_pLocalize->Find(m_szLocalString), x, y, 1000);
	else
		gFont.DrawString (m_pwText, x, y, 1000);
	
}


/*
 =====================================================================================================

								[	CBaseImage   ]

 =====================================================================================================
*/

void CBaseImage::Draw(PointPos_t *pp)
{
	int x, y;
	GetPosition(x, y);
	gTGA.Draw(m_iTextureId, x, y, m_iWidth, m_iHeight, m_iColor[0], m_iColor[1], m_iColor[2], m_iColor[3]);
}

void CBaseImage::SetColor(int r, int g, int b)
{
	m_iColor[0] = r;
	m_iColor[1] = g;
	m_iColor[2] = b;
}

void CBaseImage::SetAlpha(int a)
{
	m_iColor[3] = a;
}
int CBaseImage::GetAlpha( void )
{
	return m_iColor[3];
}

void CBaseImage::SetTexture(int iTexid)
{
	m_iTextureId = iTexid;
}

void CBaseImage::Init(void)
{
	BaseClass::Init();

	m_iTextureId = 0;
	for(int i = 0; i<4; i++)
		m_iColor[i] = 255;
}

CBaseImage* CBaseImage::Instance( CBaseMGUI *pParent )
{
	CBaseImage* pImage = NULL;
	pImage = new CBaseImage;

	if(!pImage)
		return NULL;

	pImage->Init();
	pImage->SetParent (pParent);
	pImage->SetClass (CLASS_IMAGE);

	return pImage;
}


/*
 =====================================================================================================

								[	CBaseButton   ]

 =====================================================================================================
*/

void CBaseButton::Init(void)
{
	BaseClass::Init();

	m_pfnBtnClick = NULL;
	m_szCmd[0] = '\0';
	memset(m_pwCaption, 0, sizeof(m_pwCaption));
	m_iTextSize = 14;
	m_iTex_Normal = 0;
	m_iTex_Click = 0;
	m_iTex_Fire = 0;
	m_pwCaption[0] = 0;

	for(int i = 0; i<4; i++)
		m_iTextColor[i] = 255;
}

void CBaseButton::SetCommand(char *pCmd)
{
	sprintf(m_szCmd, "%s", pCmd);
}

void CBaseButton::SetCallBackFunc(void (*pfn)(int))
{
	m_pfnBtnClick = pfn;
}

void CBaseButton::SetMessage(int imsg)
{
	m_iMessage = imsg;
}

void CBaseButton::SetCaptionColor(int r, int g, int b, int a)
{
	m_iTextColor[0] = r;
	m_iTextColor[1] = g;
	m_iTextColor[2] = b;
	m_iTextColor[3] = a;
}

void CBaseButton::SetCaption(wchar_t *pText)
{
	wsprintfW(m_pwCaption, pText);
}

void CBaseButton::SetFontSize(int iSize)
{
	m_iTextSize = iSize;
}

void CBaseButton::OnButtonClick(void)
{
	if(m_szCmd[0] != '\0')
		ClientCmd(m_szCmd);

	if(m_pfnBtnClick != NULL)
		m_pfnBtnClick( m_iMessage );

	SetFocus(true);
}

void CBaseButton::DrawBtnCaption( MGUI_STATUS status )
{
	if(!m_pwCaption[0])
		return;

	int x, y;
	GetPosition(x, y);

	gFont.SetSize(m_iTextSize, m_iTextSize);
	gFont.SetColor(m_iTextColor[0], m_iTextColor[1], m_iTextColor[2]);
	gFont.SetAlpha (m_iTextColor[3]);

	int iTextlen = gFont.GetLen(m_pwCaption);
	int ix_text = m_iWidth/2.0 + x - iTextlen/2;
	int iy_text = m_iHeight/2.0 + y + m_iTextSize/2.0;

	gFont.DrawString(m_pwCaption, ix_text, iy_text, 1000);
}

void CBaseButton::SetTexture( MGUI_STATUS status, int itex)
{
	switch(status)
	{
	case STATE_CLICK:
		m_iTex_Click = itex;
		break;
	case STATE_FIRE:
		m_iTex_Fire = itex;
		break;
	case STATE_NORMAL:
		m_iTex_Normal = itex;
		break;
	default:
		m_iTex_Normal = itex;
		break;
	}
}


void CBaseButton::DrawButton( MGUI_STATUS status )
{
	int r, g, b;
	int x, y;
	GetPosition(x, y);
	
	switch(status)
	{
	case STATE_NORMAL:
		{
			if( m_iTex_Normal )
			{
				gTGA.Draw (m_iTex_Normal, x, y, m_iWidth, m_iHeight);
			}
			else
			{
				r = 0;
				g = 0;
				b = 0;
				gEngfuncs.pfnFillRGBABlend(x, y, m_iWidth, m_iHeight, r, g, b, 200);
			}
			break;
		}
	case STATE_FIRE:
		{
			if( !m_iTex_Normal && !m_iTex_Fire)
			{
				r =255;
				g = 0;
				b = 0;
				gEngfuncs.pfnFillRGBABlend(x, y, m_iWidth, m_iHeight, r, g, b, 200);
			}
			else if (m_iTex_Fire)
			{
				gTGA.Draw (m_iTex_Fire, x, y, m_iWidth, m_iHeight);
			}
			else if(m_iTex_Normal)
			{
				gTGA.Draw (m_iTex_Normal, x, y, m_iWidth, m_iHeight);
			}
			break;
		}
	case STATE_CLICK:
		{
			if( !m_iTex_Normal && !m_iTex_Click)
			{
				r = 0;
				g = 0;
				b = 255;
				gEngfuncs.pfnFillRGBABlend(x, y, m_iWidth, m_iHeight, r, g, b, 200);
			}
			else if (m_iTex_Click)
			{
				gTGA.Draw (m_iTex_Click, x, y, m_iWidth, m_iHeight);
			}
			else if(m_iTex_Normal)
			{
				gTGA.Draw (m_iTex_Normal, x, y, m_iWidth, m_iHeight);
			}

			break;
		}
	}
}

void CBaseButton::Draw(PointPos_t *pp)
{
	MGUI_STATUS status;
	if( IsInRect(pp) )
	{
		if(IsClick())	status = STATE_CLICK;
		else			status = STATE_FIRE;
	}
	else				status = STATE_NORMAL;

	if(status == STATE_CLICK)
		OnButtonClick();
	else if(status == STATE_FIRE)
		OnButtonFire();

	DrawButton(status);
	DrawBtnCaption(status );
}


CBaseButton* CBaseButton::Instance( CBaseMGUI *pParent )
{
	CBaseButton* pButton = NULL;
	pButton = new CBaseButton;

	if(!pButton)
		return NULL;

	pButton->Init();
	pButton->SetParent (pParent);
	pButton->SetClass (CLASS_BUTTON);

	return pButton;
}




/*
 =====================================================================================================

								[	CBaseTextEntry   ]

 =====================================================================================================
*/

CBaseTextEntry::~CBaseTextEntry()
{
	if(this == g_pTextEntryOnFocus)
		g_pTextEntryOnFocus = NULL;
}

void CBaseTextEntry::Init(void)
{
	BaseClass::Init();

	m_pfnTextOutput = NULL;
	m_szText[0] = '\0';
}

void CBaseTextEntry::SetCallBackFunc( void (*pfn)(char *) )
{
	m_pfnTextOutput  =  pfn;
}


void CBaseTextEntry::Draw (PointPos_t *pp)
{
	if(IsClick() && IsInRect(pp))
		SetFocus(TRUE);

	MGUI_STATUS state;
	state = GetFocus() ? STATE_ONFOCUS : STATE_NORMAL;

	DrawBG(state);
	int x_end = DrawBGText(state);

	if(state == STATE_ONFOCUS)
		DrawImeList(x_end);
}

int CBaseTextEntry::DrawBGText( MGUI_STATUS  state)
{
	wchar_t *pWchar;
	pWchar = UTF8ToUnicode(m_szText);

	int x, y;
	GetPosition(x, y);

	gFont.SetSize(m_iFontSize, m_iFontSize);
	gFont.SetColor(m_iTextColor[0], m_iTextColor[1], m_iTextColor[2]);
	gFont.SetAlpha (m_iTextColor[3]);

	int iTextlen = gFont.GetLen(pWchar);
	int ix_text = x + AdjustSizeW(5);
	int iy_text = y + m_iHeight /2.0 + m_iFontSize/2.0;

	int x_end = gFont.DrawString(pWchar, ix_text, iy_text, 1000);

	if( state == STATE_ONFOCUS)
	{
		float fDelta = gHUD.m_flTime;
		fDelta -= (int)fDelta;

		if(fDelta > 0.5)
			gFont.DrawString(L"|", x_end, iy_text, 500);
	}
	
	return x_end;
}

void CBaseTextEntry::SetFontSize(int iSize)
{
	m_iFontSize = iSize;
}

void CBaseTextEntry::SetFontColor(int r, int g, int b, int a)
{
	m_iTextColor[0] = r;
	m_iTextColor[1] = g;
	m_iTextColor[2] = b;
	m_iTextColor[3] = a;
}

void CBaseTextEntry::DrawImeList(int x_End)
{
	if(  !CandidateList || !g_bDrawIme  || CandidateList->dwCount <= 1 )
		return;

	// Get the longest text's length
	int iLongest = 0;
	for( int i = 0; i < CandidateList->dwCount; i++)
	{
		char *pText = (char*) CandidateList + CandidateList->dwOffset[i];
		if(!pText)
			continue;

		wchar_t *pWText;
		pWText = ANSIToUnicode(pText);

		int iWide, iTall;
		g_pSurface->GetTextSize ( g_hImeFont, pWText , iWide, iTall );

		if( iWide > iLongest)
			iLongest = iWide;
	}

	int iWidth_BG, iHeight_BG;
	iWidth_BG = iLongest + 10*2;
	iHeight_BG = CandidateList->dwCount * ( IME_FONT_SIZE + 8 * 2 );

	int x, y;
	GetPosition(x, y);

	// draw the background
	gEngfuncs.pfnFillRGBABlend( x_End, (y-5) -iHeight_BG, iWidth_BG, iHeight_BG, 0, 0, 0, 150);

	// draw the ime text
	for(int i = 0; i < CandidateList->dwCount; i++)
	{
		int x_ime, y_ime;
		x_ime = x_End + 10;
		y_ime = (y-5) -(i-1)*( IME_FONT_SIZE +8*2 ) -8;

		char *pText = (char*)CandidateList + CandidateList->dwOffset[i];
		if( pText == NULL )
			continue;

		char Content[64];
		sprintf(Content, "%d %s", i+1, pText);
		wchar_t *pwText = ANSIToUnicode(Content);

		int iWide, iTall;
		g_pSurface->GetTextSize ( g_hImeFont, pwText , iWide, iTall );
		HUD_DrawPrintText( x_ime, y_ime, 240, 240, 240, 255, g_hImeFont, pwText);
	}
}

void CBaseTextEntry::SetFocus(bool bFocus)
{
	m_bOnFocus = bFocus;

	if (!bFocus && g_pTextEntryOnFocus == this)
		g_pTextEntryOnFocus = NULL;

	if(bFocus && GetParent())
	{
		g_pTextEntryOnFocus = this;
		GetParent()->SetFocus (TRUE);
		vector_t *pChildren = GetParent()->GetChildren();
		if(pChildren == NULL)
			return;

		size_t iSize = VectorSize(pChildren);
		CBaseMGUI *pMGUI = NULL;

		for(size_t i = 0; i < iSize; i++)
		{
			pMGUI = *(CBaseMGUI **)VectorGet(pChildren, i);
			if(pMGUI && pMGUI != this)
				pMGUI->SetFocus(false);
		}
	}
}

int CBaseTextEntry:: GetFontSize(void)
{
	return m_iFontSize;
}

CBaseTextEntry* CBaseTextEntry::Instance(CBaseMGUI *pParent)
{
	CBaseTextEntry* pTextEntry = NULL;
	pTextEntry = new CBaseTextEntry;

	if(!pTextEntry)
		return NULL;

	pTextEntry->Init();
	pTextEntry->SetParent (pParent);
	pTextEntry->SetClass (CLASS_BUTTON);

	return pTextEntry;
}




