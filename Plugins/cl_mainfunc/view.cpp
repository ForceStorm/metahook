#include "base.h"
#include <r_efx.h>
#include "view.h"
#include "engfuncs.h"
#include "hud.h"
#include "DrawSBPanel.h"
#include "overview.h"
#include "plugins.h"
#include "calcscreen.h"
#include "exportfuncs.h"

#define R_DRAWVIEW_SIG	"\x83\xEC\x50\xD9\x05\xD0\x75\xEC\x01\xD8\x1D\x8C\x70\xE8\x01\x56"

extern int g_iGameMode;



int g_iViewEntityBody, g_iViewEntityRenderMode, g_iViewEntityRenderFX, g_iViewEntityRenderAmout;
color24 g_byViewEntityRenderColor;
int g_iSetupSkin = 0;

void (__fastcall *g_pfn_R_DrawViewModel)(void);
void (__fastcall *g_pfn_real_DrawViewModel)(void);
void R_DrawViewModel(void);

TEMPENTITY *g_entViewEnt = NULL;

vec3_t g_vecZero = {0.0f, 0.0f, 0.0f};



void View_InstallHook(void)
{
	g_pfn_R_DrawViewModel = (void (__fastcall *)(void))g_pMetaHookAPI->SearchPattern((void *)g_dwEngineBase, g_dwEngineSize, R_DRAWVIEW_SIG, sizeof(R_DRAWVIEW_SIG) - 1);
	//g_pfn_R_DrawViewModel = (void (__fastcall *)(void)) GetEngfuncsAddress(0x1D45660);
	g_pMetaHookAPI->InlineHook( (void *)g_pfn_R_DrawViewModel, R_DrawViewModel, (void *&)g_pfn_real_DrawViewModel);
}


void R_DrawViewModel(void)
{
	return g_pfn_real_DrawViewModel();

}
void RefreshViewTempEnt( void )
{
	if( g_iGameMode != GAME_MODE_GHOST )
		return;
/*
	int iIndex = gEngfuncs.GetLocalPlayer()->index;

	if( vPlayer[ iIndex ].team == TEAM_CT )
		return ;

	static cl_entity_t *viewent;
	viewent = gEngfuncs.GetViewModel();

	if( !viewent  ||  !viewent->model )
	{
		if( g_entViewEnt )
		{
			g_entViewEnt->die = gEngfuncs.GetClientTime();
			g_entViewEnt = NULL;
		}

		return;
	}

	if( g_entViewEnt == NULL )
	{
		g_entViewEnt = gEngfuncs.pEfxAPI->CL_TempEntAlloc( viewent->origin, viewent->model );
	}

	VectorCopy( viewent->angles,		g_entViewEnt->entity.angles );
	VectorCopy( viewent->angles,		g_entViewEnt->entity.angles );
	g_entViewEnt->entity.model			= viewent->model ;
	g_entViewEnt->entity.curstate		= viewent->curstate ;
	g_entViewEnt->die					= gEngfuncs.GetClientTime() + 999.0;

	int iVelocity = vecCurVelocity.Length2D ();
	iVelocity = max( 0 , iVelocity );
	iVelocity = min( 270, iVelocity );
	int iAmt = iVelocity * 70.0 / 270.0;

	g_entViewEnt->entity.curstate.rendermode = kRenderTransAlpha;
	g_entViewEnt->entity.curstate.renderfx = kRenderFxGlowShell;
	g_entViewEnt->entity.curstate.renderamt = iAmt;
*/
}

#define ViewModel_Offset_X				0.0
#define ViewModel_Offset_Y				0.0
#define ViewModel_Offset_Z				0.0

#define ViewModel_Lag					1
#define ViewModel_LagMax				0.9
#define ViewModel_LagSpeed				1.0

void MakeViewEffect( struct ref_params_s *pparams )
{
	static vec3_t v_lastFacing;

	if ( g_bIsWidescreen )
	{
		vec3_t forward, right, up;
		vec3_t newOrigin;
		
		VectorCopy(pparams->vieworg, newOrigin);
		gEngfuncs.pfnAngleVectors (pparams->viewangles, forward, right, up);

		VectorScale(forward, -1.8, forward);
		VectorAdd(newOrigin, forward, newOrigin);
		VectorCopy(newOrigin, pparams->vieworg);
	}

	cl_entity_t *view;
	view = gEngfuncs.GetViewModel();

	view->origin[0] += ViewModel_Offset_X;
	view->origin[1] += ViewModel_Offset_Y;
	view->origin[2] += ViewModel_Offset_Z;
	
	if ( ViewModel_Lag  && pparams->frametime != 0.0f)
	{
		float speed;
		float diff;
		float max;
		vec3_t newOrigin;
		vec3_t vecDiff;
		vec3_t forward, right, up;

		//danger
		gEngfuncs.pfnAngleVectors (pparams->viewangles, forward, right, up);
		
		VectorSubtract(forward, v_lastFacing, vecDiff);

		speed = 5.0f;
		
		diff = VectorLength(vecDiff);

		if ((diff > ViewModel_LagSpeed) && ( ViewModel_LagSpeed > 0.0f))
			speed *= diff * ViewModel_LagSpeed;

		v_lastFacing[0] += vecDiff[0] * (speed * pparams->frametime);
		v_lastFacing[1] += vecDiff[1] * (speed * pparams->frametime);
		v_lastFacing[2] += vecDiff[2] * (speed * pparams->frametime);

		VectorNormalize(v_lastFacing);

		newOrigin[0] = (vecDiff[0] * -1.0f) * speed;
		newOrigin[1] = (vecDiff[1] * -1.0f) * speed;
		newOrigin[2] = (vecDiff[2] * -1.0f) * speed;

		max = ViewModel_LagMax;

		if (newOrigin[0] > max) newOrigin[0] = max;
		if (newOrigin[0] < -max) newOrigin[0] = -max;
		if (newOrigin[1] > max) newOrigin[1] = max;
		if (newOrigin[1] < -max) newOrigin[1] = -max;
		if (newOrigin[2] > max) newOrigin[2] = max;
		if (newOrigin[2] < -max) newOrigin[2] = -max;

		VectorAdd(newOrigin, view->origin, newOrigin);
		VectorCopy(newOrigin,  view->origin);
	}
}


void View_V_CalcRefdef(struct ref_params_s *pParams)
{
/*	static cl_entity_t *viewent;
	viewent = gEngfuncs.GetViewModel();

	if (viewent)
	{
		if (g_iViewEntityBody >= 0)
		{
			viewent->curstate.body = g_iViewEntityBody;
		}

		if (g_iViewEntityRenderMode >= 0)
		{
			viewent->curstate.rendermode = g_iViewEntityRenderMode;
			viewent->curstate.renderfx = g_iViewEntityRenderFX;
			viewent->curstate.renderamt = g_iViewEntityRenderAmout;
			viewent->curstate.rendercolor = g_byViewEntityRenderColor;
		}
		viewent->curstate.skin = g_iSetupSkin; 
	}

	//MakeViewEffect( pParams );
	RefreshViewTempEnt();*/
}




void V_CalcRefdef(struct ref_params_s *pParams)
{

	gExportfuncs.V_CalcRefdef(pParams);

	View_V_CalcRefdef( pParams );
	g_pOverview->CalcRefdef(pParams);
	CalcScreen_Refdef( pParams );
	MakeViewEffect( pParams );

	return ;
}

