#include "base.h"
#include "weapon.h"
#include "task.h"
#include "hud.h"



#define SET_FOV					"ASD4ASD21"

WpnItemInfo_t g_WpnItemInfo[MAX_WPN];
extern bool bPanelCanDraw;

CWeapon *g_pWeapon;


CWeapon::CWeapon( void )
{
	m_iCurrentWpnid = 0;
	m_flGunFireTime = 0;
	m_iTgaId_Panel = 0;
	m_bScopeUp = false;
}
CWeapon::~CWeapon( void )
{
	DestroyResources();
}

void CWeapon::VidInit(void)
{
	static bool bHasVidInit = false;

	if( bHasVidInit )
		return;

	bHasVidInit = true;
	

	ReadWpnItemInfo();
	WpnImageInit();
	LoadResources();

	m_iTgaId_Panel = g_pDrawTGA->GetIdByName ("resource/tga/Pannel");
}

void CWeapon::Draw( void )
{
	DrawScopeImage();
	DrawCrosshair();
}
void CWeapon::LoadResources( void )
{
	for(int i = 1;i < MAX_WPN; i++)
	{
		if( !g_WpnItemInfo[i].szWpnName )
			break;

		char szBuff[128];

		sprintf( szBuff, "models/weapon/v_%s.mdl", g_WpnItemInfo[i].szWpnName );
		//g_WpnItemInfo[i].ViewModel = IEngineStudio.Mod_ForName (szBuff, 1);

		if( g_WpnItemInfo[i].szSprSmoke1 )
			g_WpnItemInfo[i].mdlSprSmoke1 = IEngineStudio.Mod_ForName (g_WpnItemInfo[i].szSprSmoke1, 1);
		
	}
}

void CWeapon::SelectWpnItem( int iSlot )
{
	char szBuff[ 32 ];
	memset( szBuff, 0, sizeof(szBuff) );
	sprintf( szBuff, "ChooseSlot %d", iSlot);
	ClientCmd( szBuff );
}
void CWeapon::SetCurrentWpn( int iWpnId )
{
	m_iCurrentWpnid = iWpnId;
}
int CWeapon::GetCurrentWpn( void)
{
	return m_iCurrentWpnid;
}
void CWeapon::GunFire( void )
{
	m_flGunFireTime = g_flTime;
}
void CWeapon::SetCrosshairStatus( bool bHide )
{
	m_bHideCrosshair = bHide;
}
void CWeapon::ScopeUp(  bool bScope )
{
	m_bScopeUp = bScope;
}
void CWeapon::WpnImageInit(void)
{
	for( int i =1 ; i< MAX_WPN ; i++)
	{
		if( !g_WpnItemInfo[ i ].szWpnName )
			break;

		char szBuff[ MAX_PATH ];
		sprintf( szBuff, "resource/tga/weapons/%s" , g_WpnItemInfo[ i ].szWpnName );
		g_WpnItemInfo[i].iWpnTgaId = g_pDrawTGA->LoadTgaImage( szBuff, false );
	}
}


void CWeapon::ReadWpnItemInfo(void)
{
	memset(g_WpnItemInfo, 0, sizeof(g_WpnItemInfo));


	FILE* pFile;
	pFile = fopen("cstrike\\weapons\\WeaponsInUse.lst", "rt");

	if(!pFile)
		return;

	char szBuff[32];
	char szAddr[128];
	FILE *pFileItem = NULL;

	int i = 0;
	while(!feof(pFile))
	{
		i++;

		fgets(szBuff, sizeof(szBuff)-1, pFile);
		if(szBuff[0]==';' || !szBuff[0])
			continue;
		if(szBuff[ strlen(szBuff)-1 ] == '\n')
			szBuff[ strlen(szBuff)-1 ] = '\0';

		sprintf(szAddr, "cstrike\\weapons\\items\\%s.data", szBuff);
		
		pFileItem = fopen(szAddr, "rt");
		if(!pFileItem)
			continue;

		// read weapon item
		g_WpnItemInfo[ i ].szWpnName  = ( char *)malloc( sizeof( char ) * 32 );
		sprintf(g_WpnItemInfo[i].szWpnName ,szBuff);

		char szBuff2[1024], szTemp[512];
		while(!feof(pFileItem))
		{
			fgets(szBuff2, sizeof(szBuff2)-1, pFileItem);
			if(szBuff2[0]==';' || !szBuff2[0])
				continue;
			if(szBuff2[ strlen(szBuff2)-1 ] == '\n')
				szBuff2[ strlen(szBuff2)-1 ] = '\0';

			int iii = 0, iDataType = 0;
			for(int ii=0;ii<strlen(szBuff2);ii++)
			{
				if(szBuff2[ii] == '=')
				{
					szTemp[iii] = '\0';

					if(!strcmp(szTemp, "SLOT"))
						iDataType = DATA_TYPE_SLOT;
					else if(!strcmp(szTemp, "SCOPE_TYPE"))
						iDataType = DATA_TYPE_SCOPETYPE;
					else if(!strcmp(szTemp, "SCOPE_IMAGE"))
						iDataType = DATA_TYPE_SCOPEIMAGE;
					else if(!strcmp(szTemp, "CLASSNAME"))
						iDataType = DATA_TYPE_CLASSNAME;
					else if( !strcmp( szTemp, "CROSSHAIR" ) )
						iDataType = DATA_TYPE_CROSSHAIR;
					else if( !strcmp( szTemp, "CROSSHAIR_GAP" ) )
						iDataType = DATA_TYPE_CROSSHAIR_GAP;
					else if(!strcmp(szTemp, "SMOKE_SPR"))
						iDataType = DATA_TYPE_SPR_SMOKE1;
					else if(!strcmp(szTemp, "SHELL_MODEL"))
						iDataType = DATA_TYPE_SHELL_MODEL;
					else if(!strcmp(szTemp, "ANIM_SHOT_START"))
						iDataType = DATA_TYPE_ANIM_SHOT_START;
					else if(!strcmp(szTemp, "ANIM_SHOT_END"))
						iDataType = DATA_TYPE_ANIM_SHOT_END;
					else if(!strcmp(szTemp, "ANIM_SHOT_EMPTY"))
						iDataType = DATA_TYPE_ANIM_SHOT_EMPTY;
					else if(!strcmp(szTemp, "BULLET_TYPE"))
						iDataType = DATA_TYPE_BULLET_TYPE;
					else if(!strcmp(szTemp, "BULLET_DISTACNE"))
						iDataType = DATA_TYPE_BULLET_DISTANCE;
					else if(!strcmp(szTemp, "BULLET_PENETRATION"))
						iDataType = DATA_TYPE_BULLET_PENETRATION;
					else if(!strcmp(szTemp, "BULLET_TYPE2"))
						iDataType = DATA_TYPE_BULLET_TYPE2;
					else if(!strcmp(szTemp, "BULLET_DISTACNE2"))
						iDataType = DATA_TYPE_BULLET_DISTANCE2;
					else if(!strcmp(szTemp, "BULLET_PENETRATION2"))
						iDataType = DATA_TYPE_BULLET_PENETRATION2;
					else if(!strcmp(szTemp, "SHOT_SOUND1"))
						iDataType = DATA_TYPE_SHOT_SOUND1;
					else if(!strcmp(szTemp, "SHOT_SOUND2"))
						iDataType = DATA_TYPE_SHOT_SOUND2;
					else if(!strcmp(szTemp, "ANIM_END"))
						iDataType = DATA_TYPE_ANIM_END;

					iii = 0;
					memset(szTemp,0,sizeof(szTemp));
					continue;
				}

				szTemp[iii] = szBuff2[ii];
				iii ++;
			}

			switch(iDataType)
			{
			case DATA_TYPE_SLOT:
				g_WpnItemInfo[i].iSlot = atoi(szTemp);
				break;
			case DATA_TYPE_SCOPEIMAGE:
				g_WpnItemInfo[i].iScopeImageTgaId = g_pDrawTGA->LoadTgaImage( szTemp, true );
				break;
			case DATA_TYPE_SCOPETYPE:
				if(!strcmp(szTemp, "SCOPE_SNIPER"))
					g_WpnItemInfo[i].iScopeType = SCOPE_SNIPER;
				else if(!strcmp(szTemp, "SCOPE_AUG"))
					g_WpnItemInfo[i].iScopeType = SCOPE_AUG;
				else if(!strcmp(szTemp, "SCOPE_SIGHT"))
					g_WpnItemInfo[i].iScopeType = SCOPE_SIGHT;
				else if(!strcmp(szTemp, "SCOPE_ACTION"))
					g_WpnItemInfo[i].iScopeType = SCOPE_ACTION;
				break;
			case DATA_TYPE_CROSSHAIR:
				g_WpnItemInfo[i].iCrosshair = atoi( szTemp );
				break;
			case DATA_TYPE_CROSSHAIR_GAP:
				g_WpnItemInfo[i].iCrosshairGap = atoi( szTemp );
				break;
			case DATA_TYPE_CLASSNAME:
				g_WpnItemInfo[i].szClassname = (char *)malloc( sizeof(char) * 32 );
				sprintf(g_WpnItemInfo[i].szClassname , szTemp);
				break;
			case DATA_TYPE_SPR_SMOKE1:
				g_WpnItemInfo[i].szSprSmoke1 = (char *)malloc( sizeof(char) * 64 );
				sprintf(g_WpnItemInfo[i].szSprSmoke1  , szTemp);
				break;
			case DATA_TYPE_SHELL_MODEL:
				g_WpnItemInfo[i].szShellModel = (char *)malloc( sizeof(char) * 64 );
				sprintf(g_WpnItemInfo[i].szShellModel  , szTemp);
				break;
			case DATA_TYPE_ANIM_SHOT_START:
				g_WpnItemInfo[i].iAnimShotStart = atoi( szTemp );
				break;
			case DATA_TYPE_ANIM_SHOT_END:
				g_WpnItemInfo[i].iAnimShotEnd = atoi( szTemp );
				break;
			case DATA_TYPE_ANIM_SHOT_EMPTY:
				g_WpnItemInfo[i].iAnimShotEmpty = atoi(szTemp);
				break;
			case DATA_TYPE_ANIM_END:
				g_WpnItemInfo[i].iAnimEnd = atoi(szTemp);
				break;
			case DATA_TYPE_BULLET_TYPE:
				g_WpnItemInfo[i].iBulletType  = atoi(szTemp);
				break;
			case DATA_TYPE_BULLET_DISTANCE:
				g_WpnItemInfo[i].iBulletDistance  = atoi(szTemp);
				break;
			case DATA_TYPE_BULLET_PENETRATION:
				g_WpnItemInfo[i].iBulletPenetration  = atoi(szTemp);
				break;
			case DATA_TYPE_BULLET_TYPE2:
				g_WpnItemInfo[i].iBulletType2  = atoi(szTemp);
				break;
			case DATA_TYPE_BULLET_DISTANCE2:
				g_WpnItemInfo[i].iBulletDistance2  = atoi(szTemp);
				break;
			case DATA_TYPE_BULLET_PENETRATION2:
				g_WpnItemInfo[i].iBulletPenetration2  = atoi(szTemp);
				break;
			case DATA_TYPE_SHOT_SOUND1:
				g_WpnItemInfo[i].szShotSound1 = (char *)malloc( sizeof(char) * 64 );
				sprintf(g_WpnItemInfo[i].szShotSound1  , szTemp);
				break;
			case DATA_TYPE_SHOT_SOUND2:
				g_WpnItemInfo[i].szShotSound2 = (char *)malloc( sizeof(char) * 64 );
				sprintf(g_WpnItemInfo[i].szShotSound2  , szTemp);
				break;
			
			
			}
			memset(szTemp, 0, sizeof(szTemp));
			memset(szBuff2, 0, sizeof(szBuff2));

		}
		fclose(pFileItem);
	}
	fclose(pFile);
}

void CWeapon::DestroyResources(void)
{

	for(int i=1; i<MAX_WPN; i++)
	{
		if(!g_WpnItemInfo[i].szWpnName)
			break;

		if(g_WpnItemInfo[i].szWpnName )
			free(g_WpnItemInfo[i].szWpnName);

		if(g_WpnItemInfo[i].szClassname )
			free(g_WpnItemInfo[i].szClassname);
	}

}
bool CWeapon::IsInScope(void)
{
	if( !m_bScopeUp )
		return false;

	if( !g_WpnItemInfo[ m_iCurrentWpnid ].iScopeType )
		return false;

	return true;
}
void CWeapon::SetPlayerWpnId(int idx, int iWpnId)
{
	m_iPlayerWpnId[idx][g_WpnItemInfo[iWpnId].iSlot ] = iWpnId;
}
int CWeapon::GetPlayerWpnId(int idx, int iSlot)
{
	return m_iPlayerWpnId[idx][iSlot];
}
void CWeapon::DrawScopeImage(void)
{
	if( !m_bScopeUp )
		return;

	if( !g_WpnItemInfo[ m_iCurrentWpnid ].iScopeType )
		return;

	if( m_iCurrentWpnid < 1 || m_iCurrentWpnid > MAX_WPN )
		return;

	int iX, iY, iW, iH;

	if(g_WpnItemInfo[ m_iCurrentWpnid ].iScopeType == SCOPE_AUG)
	{
		iX = 0, iY = 0;
		iW = g_sScreenInfo.iWidth;
		iH = g_sScreenInfo.iHeight;
	}
	else if(g_WpnItemInfo[ m_iCurrentWpnid ].iScopeType == SCOPE_SNIPER || g_WpnItemInfo[ m_iCurrentWpnid ].iScopeType == SCOPE_ACTION)
	{
		int iScopeWidth, iScopeHeight;
		g_pSurface->DrawGetTextureSize( g_WpnItemInfo[ m_iCurrentWpnid ].iScopeImageTgaId , iScopeWidth, iScopeHeight );
		ScaleToScreen( iScopeWidth, iScopeHeight, 1024, 768 );

		iW = iScopeWidth;
		iH = iScopeHeight;
		iX = (g_sScreenInfo.iWidth - iW)/2.0;
		iY = (g_sScreenInfo.iHeight - iH)/2.0;

		g_pDrawTGA->DrawTGA( m_iTgaId_Panel, 0, 0, g_sScreenInfo.iWidth, iY, 0, 0, 0, 255);
		g_pDrawTGA->DrawTGA( m_iTgaId_Panel, 0, iY, iX, iH, 0, 0, 0, 255);
		g_pDrawTGA->DrawTGA( m_iTgaId_Panel, iX + iW, iY, iX, iH, 0, 0, 0, 255);
		g_pDrawTGA->DrawTGA( m_iTgaId_Panel, 0, iY + iH, g_sScreenInfo.iWidth, iY, 0, 0, 0, 255);
	}

	g_pDrawTGA->DrawTGA( g_WpnItemInfo[ m_iCurrentWpnid ].iScopeImageTgaId, iX, iY, iW, iH);
}
void CWeapon::DrawCrosshair(void) 
{
	if( m_bHideCrosshair )
		return;

	if( bPanelCanDraw )
		return;

	int iScreenCenterX = 1024.0 / 2.0;
	int iScreenCenterY = 768.0 / 2.0;

	int iGap;

	if( g_flTime >= m_flGunFireTime + CROSSHAIR_RECOVER_TIME ) 
	{
		iGap = g_WpnItemInfo[ m_iCurrentWpnid ].iCrosshairGap;
		
	}
	else
	{
		iGap = g_WpnItemInfo[ m_iCurrentWpnid ].iCrosshairGap + CROSSHAIR_ENLARGE_AMT * (1 - (g_flTime - m_flGunFireTime) / CROSSHAIR_RECOVER_TIME );
	}

	int iX, iY, iW, iH;
	iX = AdjustSizeW( iScreenCenterX - iGap - CROSSHAIR_LENGTH );
	iY = AdjustSizeH( iScreenCenterY);
	iW = AdjustSizeW( CROSSHAIR_LENGTH );
	iH = 1;

	gEngfuncs.pfnFillRGBA( iX , iY, iW, iH , 255, 255, 255, 200);

	iX = AdjustSizeW( iScreenCenterX + iGap );
	iY = AdjustSizeH( iScreenCenterY);
	iW = AdjustSizeW( CROSSHAIR_LENGTH );
	iH = 1;

	gEngfuncs.pfnFillRGBA( iX , iY, iW, iH , 255, 255, 255, 200);

	iX = AdjustSizeW( iScreenCenterX );
	iY = AdjustSizeH( iScreenCenterY - iGap - CROSSHAIR_LENGTH );
	iW = 1;
	iH = AdjustSizeW( CROSSHAIR_LENGTH );

	gEngfuncs.pfnFillRGBA( iX , iY, iW, iH , 255, 255, 255, 200);

	iX = AdjustSizeW( iScreenCenterX );
	iY = AdjustSizeH( iScreenCenterY + iGap);
	iW = 1;
	iH = AdjustSizeW( CROSSHAIR_LENGTH );

	gEngfuncs.pfnFillRGBA( iX , iY, iW, iH , 255, 255, 255, 200);
}

void pfnSendFovToServer( int iTaskid )
{
	char szCmd[64];
	sprintf( szCmd, "%s%d", SET_FOV, gHUD.m_iFOV );
	gEngfuncs.pfnClientCmd ( szCmd );
}

void CWeapon::SetFov( int iFov )
{
	if( IsInScope() )
	{
		gHUD.m_iFOV = iFov;

		gTask.DeleteTask ( TASK_SEND_FOV );
		gTask.SetTask ( 0.5, pfnSendFovToServer, TASK_SEND_FOV );
	}
}

int CWeapon::GetFov( void )
{
	return gHUD.m_iFOV;
}