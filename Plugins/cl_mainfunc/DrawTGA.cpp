#include "base.h"
#include "engfuncs.h"
#include "plugins.h"

CDrawTGA *g_pDrawTGA;

CDrawTGA::CDrawTGA ()
{
	for( int ii = 0; ii< MH_MAX_TGA; ii++)
	{
		m_TgaInfo[ii].index = 0;
		m_TgaInfo[ii].pszName = NULL;
	}
}
int CDrawTGA::LoadTgaImage( char *filename , bool bLargeSize )
{
	int id = g_pSurface->CreateNewTextureID();

	if( bLargeSize )
		g_pSurface->DrawSetTextureFile2( id, filename, true, true );
	else
		g_pSurface->DrawSetTextureFile( id, filename, true, true );

	return id;
}

void CDrawTGA::VidInit(void)
{
	static bool bHasVidInit = false;

	if( !bHasVidInit )
	{
		bHasVidInit = true;
		LoadList();
	}
}

/*
void CDrawTGA::Redraw(void)
{
}*/

void CDrawTGA::DrawTGA( int iTextureId, int iX, int iY, int iW , int iH)
{
	gEngfuncs.pTriAPI->RenderMode( kRenderTransTexture );
	g_pSurface->DrawSetColor( 255, 255, 255, 255);
	g_pSurface->DrawSetTexture( iTextureId );
	g_pSurface->DrawTexturedRect( iX,   iY,   iX+iW,   iY+iH);
	//gEngfuncs.pTriAPI->End();
	
}


void CDrawTGA::DrawTGA( int iTextureId, int iX, int iY, int iW, int iH,  int r, int g, int b , int alpha)
{
	gEngfuncs.pTriAPI->RenderMode( kRenderTransTexture );
	g_pSurface->DrawSetColor( r, g, b, alpha);
	g_pSurface->DrawSetTexture( iTextureId );
	g_pSurface->DrawTexturedRect( iX,   iY,   iX+iW,   iY+iH);
}

int CDrawTGA::GetIdByName ( char *pszName )
{
	int i;
	for( i = 0; i< MH_MAX_TGA; i++)
	{
		if( m_TgaInfo[i].index == 0 )
			break;

		if( !strcmp( pszName, m_TgaInfo[i].pszName ) )
			return m_TgaInfo[i].index;
	}

	m_TgaInfo[i].pszName = (char *)malloc( sizeof(char) * MAX_PATH );
	sprintf( m_TgaInfo[i].pszName, "%s", pszName );
	m_TgaInfo[i].index = LoadTgaImage(  m_TgaInfo[i].pszName , true );

	return m_TgaInfo[i].index;
}


void CDrawTGA::LoadList(void)
{

	FILE* pTgaList;
	pTgaList = fopen( "cstrike\\resource\\tga\\TgaFilesList.lst", "rt");

	if(pTgaList == NULL)
		return;
	
	int i = 0;
	for( i = 0; i< MH_MAX_TGA; i++)
	{
		if( m_TgaInfo[i].index == 0 )
			break;
	}

	char szBuff[ MAX_PATH ];

	while( !feof(pTgaList) )
	{
		fgets( szBuff,  sizeof(szBuff)-1,  pTgaList);

		if(szBuff[strlen(szBuff)-1] =='\n')
			szBuff[strlen(szBuff)-1] = '\0';

		if(szBuff[0]==';'|| !szBuff[0] || szBuff[0]=='/')
			continue;

		m_TgaInfo[i].pszName = (char *)malloc( sizeof(char) * MAX_PATH );
		sprintf( m_TgaInfo[i].pszName , "%s", szBuff );

		m_TgaInfo[i].index = LoadTgaImage( m_TgaInfo[i].pszName , false );

		i ++;
	}

	fclose( pTgaList );
}