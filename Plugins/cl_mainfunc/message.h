#ifndef MESSAGE_H
#define MESSAGE_H


#define DRAW_SPR 1
#define DRAW_TGA 2
#define DRAW_ICON 3
#define PLAY_BINK 4
#define DRAW_BAR 5
#define DRAW_TEXT 6


int EngFunc_HookUserMsg(char *szMsgName, pfnUserMsgHook pfn);
int MsgFunc_MetaHook(const char *pszName, int iSize, void *pbuf);



#endif