#include "base.h"
#include "effects.h"
#include "hud.h"
#include <pm_defs.h>

CEffect gEffect;

void GetAimOrigin( float forw, float right, float up, struct cl_entity_s *pEnt, vec3_t &vecReturn)
{
	vec3_t vecOrigin, vecForward, vecRight, vecUp;

	VectorCopy(pEnt->curstate.origin, vecOrigin);
	gEngfuncs.pfnAngleVectors (pEnt->curstate.angles, vecForward, vecRight, vecUp);
	
	vecReturn[0]  = vecOrigin[0]  + vecForward[0] * forw + vecRight[0] * right + vecUp[0] * up;
	vecReturn[1]  = vecOrigin[1]  + vecForward[1] * forw + vecRight[1] * right + vecUp[1] * up;
	vecReturn[2]  = vecOrigin[2]  + vecForward[2] * forw + vecRight[2] * right + vecUp[2] * up;
}

//==========================================================================================

CRPGTail::CRPGTail()
{
	m_flNextSoundTime = 0;
	m_flNextFxTime = 0;
}

void CRPGTail::OnThink(void)
{
	if(m_flNextFxTime >= gHUD.m_flTime)
		return;

	cl_entity_t *pObj = gEngfuncs.GetEntityByIndex (m_iEnt);
	if(pObj == NULL)
	{
		m_bKillme = true;
		return;
	}

	m_flNextFxTime = gHUD.m_flTime + 0.01;
	
	vec3_t vecOrigin, vecAngle;
	VectorCopy(pObj->origin ,vecOrigin);

	dlight_t *dl = gEngfuncs.pEfxAPI->CL_AllocDlight (0);
	VectorCopy(vecOrigin, dl->origin);
	dl->radius = 15;
	dl->color.r = 255;
	dl->color.g = 127;
	dl->color.b = 100;
	dl->die = gEngfuncs.GetClientTime () + 0.08;

	GetAimOrigin( -10.0, 0.0, 0.0, pObj, vecOrigin);

	gEngfuncs.pEfxAPI->R_Explosion( 
		vecOrigin,
		gEngfuncs.pEventAPI->EV_FindModelIndex("sprites/hotglow.spr"), 
		1, 
		8.5, 
		(TE_EXPLFLAG_NODLIGHTS | TE_EXPLFLAG_NOSOUND | TE_EXPLFLAG_NOPARTICLES) );

	gEngfuncs.pEfxAPI->R_Explosion( 
		vecOrigin,
		gEngfuncs.pEventAPI->EV_FindModelIndex("sprites/rockefire.spr"), 
		gEngfuncs.pfnRandomFloat (0.2, 1.3), 
		8.5, 
		(TE_EXPLFLAG_NODLIGHTS | TE_EXPLFLAG_NOPARTICLES) );
}

//==========================================================================================

CFlameParticle::CFlameParticle()
{
	m_flNextMoveTime = 0.0f;
	m_flStartTime = 0.0f;
	m_pTGA = NULL;
}

void CFlameParticle::AddTgaItem(Tga3DItem_t *p)
{
	m_pTGA = p;
	m_pTGA->scale = 0.05;
	m_flStartTime = gHUD.m_flTime ;
	m_flNextMoveTime = gHUD.m_flTime + 0.02;
}

void CFlameParticle::OnThink(void)
{
	if(gHUD.m_flTime > m_flNextMoveTime)
	{
		m_pTGA->scale += 0.0084;
		m_flNextMoveTime = gHUD.m_flTime + 0.01;
		/*
		vec3_t vEnd = m_pTGA->vecOrigin + m_pTGA->vecVelocity ;
		pmtrace_t pmtrace;
		
		gEngfuncs.pEventAPI->EV_SetTraceHull( 2 );
		gEngfuncs.pEventAPI->EV_PlayerTrace( m_pTGA->vecOrigin, vEnd, PM_WORLD_ONLY, -1, &pmtrace );

		if( pmtrace.fraction != 1)
		{
			vec3_t vVel , vtmp;
			vtmp = pmtrace.plane.normal;
			vtmp[0] += 1;
			vtmp[1] += 2;
			vtmp[2] += 3;
			vtmp = CrossProduct(pmtrace.plane.normal , vtmp);
			m_pTGA->vecVelocity = vtmp.Normalize() * m_pTGA->vecVelocity.Length ();
		}
		*/
		VectorAdd(m_pTGA->vecOrigin , m_pTGA->vecVelocity, m_pTGA->vecOrigin );
	}
}

// =========================== manager ========================

CEffect::CEffect()
{
	m_vParticleList = NULL;
}

void CEffect::VidInit(void)
{
}

void CEffect::AddParticleObj(CBaseParticle *pParticle)
{
	if(m_vParticleList == NULL)
		m_vParticleList = CreateVector(sizeof(CBaseParticle));

	VectorPush(m_vParticleList, &pParticle);
}

void CEffect::Redraw(void)
{
	if(!m_vParticleList)
		return;

	size_t iSize;
	CBaseParticle *pObj = NULL;
	iSize = VectorSize(m_vParticleList);

	// pre processing; destroy the invalid objects
	for(size_t i = 0; i < iSize; i++)
	{
		pObj = *(CBaseParticle **)VectorGet(m_vParticleList, i);
		if(pObj != NULL)
		{
			if(pObj->m_bKillme)
			{
				VectorEarse(m_vParticleList, i, i+1);
				iSize = VectorSize(m_vParticleList);
				--i;

				delete pObj;
			}
		}

		pObj = NULL;
	}

	pObj = NULL;
	iSize = VectorSize(m_vParticleList);

	for(size_t i = 0; i < iSize; i++)
	{
		pObj = *(CBaseParticle **)VectorGet(m_vParticleList, i);
		if(pObj != NULL)
		{
			 pObj->OnThink ();
		}

		pObj = NULL;
	}
}
