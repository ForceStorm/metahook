#include <metahook.h>
#include "surface.h"
#include "engfuncs.h"
#include "GameUI_Interface.h"

void (__fastcall *g_pfnCGameUI_LoadingStarted)(void *pthis, const char *resourceType, const char *resourceName);

CGameUI *g_pGameUI;


void CGameUI::LoadingStarted ( const char *resourceType, const char *resourceName )
{
	g_pfnCGameUI_LoadingStarted( this, resourceType, resourceName );
}

void GameUI_InstallHook()
{
	DWORD *pVFTable = *(DWORD **)g_pGameUI;

	g_pMetaHookAPI->VFTHook(g_pGameUI, 0, 12, (void *)pVFTable[12], (void *&)g_pfnCGameUI_LoadingStarted);
}



void GameUI_Init(void)
{
	CreateInterfaceFn fnCreateInterface = (CreateInterfaceFn)GetProcAddress( GetModuleHandle("GameUI.dll"), "CreateInterface");
	g_pGameUI = (CGameUI *)fnCreateInterface(GAMEUI_INTERFACE_VERSION, NULL);

	if( g_pGameUI )
		GameUI_InstallHook();
}