#ifndef _PLAYBINK_H
#define _PLAYBINK_H


#include "Bink.h"


#define PLAYBINK_NORMAL_RATE -1.0


class CBink
{
public:
	CBink();
	~CBink();

public:
	int m_index;

	int m_r, m_b, m_g;
	int m_iCenter;
	int m_iFullScreen;
	int m_iX, m_iY;
	int m_iLoop;

	float m_flStayTime;
	float m_flFadeoutTime;
	float m_flEndDisplayTime;
	float m_flPlayRate;
	float m_flNextFrameTime;

	int m_iTextureId;
	unsigned int m_iFrame;

	static CBink *CreateBink( int index, int x, int y, int r, int g, int b, char *szName, int iCenter, int iFullScreen, int iLoop, float flStayTime, float flFadeoutTime, float flPlayRate, int bitsFlags = 135266304 );

public:
	HBINK m_hBink;
	byte *m_pPixel;

public:
	CBink *m_pNext;
};








class CBinkManager
{
public:
	CBinkManager();

	void VidInit(void);
	void Redraw(void);

	void AddBink( CBink *pBink );
	void DeleteBink( int index );
	

private:
	void DeleteBink( CBink *pBink );
	CBink *m_pBink;
};

extern CBinkManager *g_pBinkManager;



#endif