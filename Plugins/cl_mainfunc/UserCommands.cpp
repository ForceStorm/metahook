#include "base.h"
#include "UserCommands.h"
#include "weapon.h"
#include "hud.h"
#include "MGUI.h"


CMDFUNC_SLOT(1)
CMDFUNC_SLOT(2)
CMDFUNC_SLOT(3)
CMDFUNC_SLOT(4)
CMDFUNC_SLOT(5)
CMDFUNC_SLOT(6)
CMDFUNC_SLOT(7)
CMDFUNC_SLOT(8)
CMDFUNC_SLOT(9)
CMDFUNC_SLOT(10)


void __CmdFunc_FastrunBegin(void)
{
	gEngfuncs.pfnClientCmd( "fastrun_begin" );
}

void __CmdFunc_FastrunEnd(void)
{
	gEngfuncs.pfnClientCmd( "fastrun_end" );
}

void __CmdFunc_BuyMenu(void)
{
}
void __CmdFunc_BagEditorMenu(void)
{
}

void __CmdFunc_FovDown(void)
{
	int iFov = g_pWeapon->GetFov ();

	if( iFov <= 40 && iFov - 1 > 10)
	{
		g_pWeapon->SetFov( iFov - 1 );
	}
}


void __CmdFunc_FovUp(void)
{
	int iFov = g_pWeapon->GetFov ();

	if( iFov <= 40 && iFov + 1 < 40 )
	{
		g_pWeapon->SetFov( iFov + 1 );
	}
}


void UserCommandInit(void)
{
	HOOK_COMMAND("+fastrun", FastrunBegin);
	HOOK_COMMAND("-fastrun", FastrunEnd);
	HOOK_COMMAND("bag", BuyMenu);
	HOOK_COMMAND("bag_editor", BagEditorMenu);

	//HOOK_COMMAND("cancelselect", Close);
	HOOK_COMMAND("fov_down", FovDown);
	HOOK_COMMAND("fov_up", FovUp);
}

void UserCmd( const char *pszCmd)
{
	if(!strcmp( pszCmd, "slot1" ))
	{
		if( gHUD.m_Menu.MenuHasShow () )
			gHUD.m_Menu.SelectMenuItem( 1 );
		else
			g_pWeapon->SelectWpnItem( 1 );

		gMGUI.InputKey(KEY_1);
	}
	else if(!strcmp( pszCmd, "slot2" ))
	{
		if( gHUD.m_Menu.MenuHasShow () )
			gHUD.m_Menu.SelectMenuItem( 2 );
		else
			g_pWeapon->SelectWpnItem( 2 );

		gMGUI.InputKey(KEY_2);
	}
	else if(!strcmp( pszCmd, "slot3" ))
	{
		if( gHUD.m_Menu.MenuHasShow () )
			gHUD.m_Menu.SelectMenuItem( 3 );
		else
			g_pWeapon->SelectWpnItem( 3 );

		gMGUI.InputKey(KEY_3);
	}
	else if(!strcmp( pszCmd, "slot4" ))
	{
		if( gHUD.m_Menu.MenuHasShow () )
			gHUD.m_Menu.SelectMenuItem( 4 );
		else
			g_pWeapon->SelectWpnItem( 4 );

		gMGUI.InputKey(KEY_4);
	}
	else if(!strcmp( pszCmd, "slot5" ))
	{
		if( gHUD.m_Menu.MenuHasShow () )
			gHUD.m_Menu.SelectMenuItem( 5 );
		else
			g_pWeapon->SelectWpnItem( 5 );

		gMGUI.InputKey(KEY_5);
	}
	else if(!strcmp( pszCmd, "slot6" ))
	{
		if( gHUD.m_Menu.MenuHasShow () )
			gHUD.m_Menu.SelectMenuItem( 6 );
		else
			g_pWeapon->SelectWpnItem( 6 );

		gMGUI.InputKey(KEY_6);
	}
	else if(!strcmp( pszCmd, "slot7" ))
	{
		if( gHUD.m_Menu.MenuHasShow () )
			gHUD.m_Menu.SelectMenuItem( 7 );
		else
			g_pWeapon->SelectWpnItem( 7 );

		gMGUI.InputKey(KEY_7);
	}
	else if(!strcmp( pszCmd, "slot8" ))
	{
		if( gHUD.m_Menu.MenuHasShow () )
			gHUD.m_Menu.SelectMenuItem( 8 );
		else
			g_pWeapon->SelectWpnItem( 8 );

		gMGUI.InputKey(KEY_8);
	}
	else if(!strcmp( pszCmd, "slot9" ))
	{
		if( gHUD.m_Menu.MenuHasShow () )
			gHUD.m_Menu.SelectMenuItem( 9 );
		else
			g_pWeapon->SelectWpnItem( 9 );

		gMGUI.InputKey(KEY_9);
	}
	else if(!strcmp( pszCmd, "slot10" ))
	{
		if( gHUD.m_Menu.MenuHasShow () )
			gHUD.m_Menu.SelectMenuItem( 10 );
		else
			g_pWeapon->SelectWpnItem( 10 );

		gMGUI.InputKey(KEY_10);
	}
	else if(!strcmp( pszCmd, "Close" ))
	{
		
	}
}
