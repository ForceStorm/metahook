#include "base.h"
#include "resources.h"
#include <custom.h>
#include <com_model.h>
#include "plugins.h"

#define EXTRA_LIST_OFFSET		9999
#define CL_GETMODELBYINDEX_SIG "\x83\xEC\x10\x56\x57\x8B\x7C\x24\x1C\x8B\x34\xBD\x2A\x2A\x2A\x2A\x85\xF6"
#define CL_ADDTORESOURCELIST_SIG "\x8B\x44\x24\x04\x8B\x88\x84\x00\x00\x00\x85\xC9"

struct model_s *(*g_pfnCL_GetModelByIndex)(int index);
void (*g_pfnCL_AddToResourceList)(resource_t *pResource, resource_t *pList);
int	(*g_pfnEV_FindModelIndex)(const char *pmodel );

int g_iModelIndex = 0;
int g_iModelIndex_ExtraList = 0;


typedef struct model_res_s
{
	char szName[ MAX_QPATH ];
	int index;
	struct model_s *model;
	struct model_res_s *pNext;

public:
	model_res_s()
	{
		szName[0] = '\0';
		index = -1;
		model = NULL;
		pNext = NULL;
	}
} model_res_t;


model_res_t g_ModelResList;
model_res_t g_ModelResExtraList;

int EV_FindModelIndex( const char *pmodel )
{
	int index = g_pfnEV_FindModelIndex(pmodel);

	if(index)
		return index;

	else
	{
		model_t *_pModel = IEngineStudio.Mod_ForName(pmodel, TRUE);
		if(!_pModel)
			gEngfuncs.Con_Printf ("MetaHook EV_FindModelIndex: failed to load %s", pmodel);


		// Add the model resource to extra list
		model_res_t * pointer = &g_ModelResExtraList;

		if(g_ModelResExtraList.model == _pModel)
			return g_ModelResExtraList.index ;

		else if( !g_ModelResExtraList.pNext  && !g_ModelResExtraList.model )
		{
			g_ModelResExtraList.model = _pModel;
			strcpy( g_ModelResExtraList.szName , pmodel);
			g_ModelResExtraList.index = EXTRA_LIST_OFFSET;
			return g_ModelResExtraList.index ;
		}

		while(pointer->pNext)
		{
			if(pointer->pNext->model == _pModel)
				return pointer->pNext->index;
			pointer = pointer->pNext ;
		}

		model_res_t * pNewRes = new model_res_s;
		strcpy( pNewRes->szName , pmodel);
		pNewRes->model = _pModel;
		pNewRes->index = EXTRA_LIST_OFFSET + g_iModelIndex_ExtraList;
		pointer->pNext = pNewRes;
		g_iModelIndex_ExtraList ++;

		return pNewRes->index ;
		
	}
}

struct model_s *CL_GetModelByIndex(int index)
{
	if (index == -1)
		return NULL;

	if(index >= EXTRA_LIST_OFFSET)
	{
		// search the model in the Extra List
		model_res_t * pointer = &g_ModelResExtraList;
		while( pointer )
		{
			if( pointer->index == index )
				return pointer->model ;
			pointer = pointer->pNext ;
		}
	}
	else
	{

		model_res_t * pointer = &g_ModelResList;
		while( pointer )
		{
			if( pointer->index == index )
				return pointer->model ;
			pointer = pointer->pNext ;
		}
	}

	return g_pfnCL_GetModelByIndex(index);
}


void CL_AddToResourceList(resource_t *pResource, resource_t *pList)
{
	if (pResource->type == t_model)
	{
		g_iModelIndex ++;
		if( strstr(pResource->szFileName, "models/") )
		{
			if (!strcmp( g_ModelResList.szName , pResource->szFileName))
				return;

			model_res_t * pointer = &g_ModelResList;

			while( pointer->pNext )
			{
				if (!strcmp( pointer->pNext->szName , pResource->szFileName))
					return;
				pointer = pointer->pNext ;
			}

			model_res_t * pNewRes = new model_res_s;
			strcpy( pNewRes->szName , pResource->szFileName);
			pNewRes->model = IEngineStudio.Mod_ForName(pResource->szFileName, TRUE);
			pNewRes->index = g_iModelIndex;
			pointer->pNext = pNewRes;
			return;
		}
	}

	return g_pfnCL_AddToResourceList(pResource, pList);
}
void ResourceList_Reset(void)
{
	// Clear the resources list
	g_iModelIndex = 0;

	while( g_ModelResList.pNext )
	{
		model_res_t *pointer = &g_ModelResList;
		while( pointer->pNext->pNext )
			pointer = pointer->pNext ;

		delete pointer->pNext;
		pointer->pNext = NULL;
	}


	// Clear the extra resources list
	g_iModelIndex_ExtraList = 1;

	while( g_ModelResExtraList.pNext )
	{
		model_res_t *pointer = &g_ModelResExtraList;
		while( pointer->pNext->pNext )
			pointer = pointer->pNext ;

		delete pointer->pNext;
		pointer->pNext = NULL;
	}
	g_ModelResExtraList.index = EXTRA_LIST_OFFSET;
	g_ModelResExtraList.model = NULL;
	g_ModelResExtraList.szName[0] = '\0';
}

void Resource_InstallHook(void)
{
	g_pfnCL_GetModelByIndex = (struct model_s *(*)(int))g_pMetaHookAPI->SearchPattern((void *)g_dwEngineBase, g_dwEngineSize, CL_GETMODELBYINDEX_SIG, sizeof(CL_GETMODELBYINDEX_SIG) - 1);
	g_pfnCL_AddToResourceList = (void (*)(resource_t *, resource_t *))g_pMetaHookAPI->SearchPattern((void *)g_dwEngineBase, g_dwEngineSize, CL_ADDTORESOURCELIST_SIG, sizeof(CL_ADDTORESOURCELIST_SIG) - 1);
	
	g_pMetaHookAPI->InlineHook(g_pfnCL_AddToResourceList, CL_AddToResourceList, (void *&)g_pfnCL_AddToResourceList);
	g_pMetaHookAPI->InlineHook(g_pfnCL_GetModelByIndex, CL_GetModelByIndex, (void *&)g_pfnCL_GetModelByIndex);
	g_pMetaHookAPI->InlineHook(gEngfuncs.pEventAPI->EV_FindModelIndex , EV_FindModelIndex, (void *&)g_pfnEV_FindModelIndex);
}