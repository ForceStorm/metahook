#include "base.h" 
#include "plugins.h"

void Fonts_SetSize(int iW,int iH)
{
	gFont.SetSize (iW, iH);
}

int Fonts_GetLen(wchar_t* _strText , int iW)
{
	gFont.SetSize (iW, iW);
	return gFont.GetLen (_strText);
}

void Fonts_SetColor(int r,int g,int b,int a)
{
	gFont.SetColor (r, g, b);
	gFont.SetAlpha (a);
}

void Fonts_Init(char *FontName,int iW,int iH)
{
	gFont.Init();
}

void Fonts_C2W(wchar_t *pwstr,size_t len,const char *str)
{
	gFont.C2W(pwstr, len, str);
}

void Fonts_Free(void)
{
	gFont.Shutdown();
}

int DrawFonts(wchar_t* _strText,int x , int y, int r, int g, int b, int alpha, int iFontsSizeW, int iFontsSizeH, int maxW , int h)
{
	gFont.SetColor (r,g,b);
	gFont.SetAlpha (alpha);
	gFont.SetSize (iFontsSizeW, iFontsSizeH);
	return gFont.DrawString (_strText, x, y, maxW);
}
