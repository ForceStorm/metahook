#ifndef _EVENTS_H
#define _EVENTS_H


#ifndef M_PI
#define M_PI		3.14159265358979323846	// matches value in gcc v2 math.h
#endif

void EV_HookEvents( void );
void EV_Init( void );

#endif