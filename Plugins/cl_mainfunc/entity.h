#ifndef _ENTITY_H
#define _ENTITY_H

#include <cl_entity.h>
#include <entity_state.h>


void Entity_Init(void);
cl_entity_t *CL_FindEntityInSphere(float *org, float rad, int (*pfnFilter)(cl_entity_t *ent));


#endif