#ifndef _MGUI_H
#define _MGUI_H

#include "nvector.h"


#define KEY_1 (1<<0)
#define KEY_2 (1<<1)
#define KEY_3 (1<<2)
#define KEY_4 (1<<3)
#define KEY_5 (1<<4)
#define KEY_6 (1<<5)
#define KEY_7 (1<<6)
#define KEY_8 (1<<7)
#define KEY_9 (1<<8)
#define KEY_0 (1<<9)
#define KEY_10 (1<<10)


#define ME_LEFTCLICK	1
#define ME_RIGHTCLICK	2
#define ME_WHEELCLICK	4


enum MGUI_TYPE
{
	CLASS_NONE = 0,
	CLASS_IMAGE,
	CLASS_LABEL,
	CLASS_BUTTON,
	CLASS_TEXTENTRY
};

enum MGUI_STATUS
{
	STATE_NORMAL,
	STATE_ONFOCUS,
	STATE_FIRE,
	STATE_CLICK,
	STATE_FREEZE,
};

enum MOUSE_STATE
{
	MOUSE_FIXED = 0,
	MOUSE_FREE,
};


typedef struct
{
    int  x;
    int  y;
} PointPos_t;


// =====================================================================================================
class CBaseMGUI
{

public :
	CBaseMGUI();
	virtual ~CBaseMGUI();

protected:
	int m_iXpos;
	int m_iYpos;
	int m_iWidth;
	int m_iHeight;

	int m_iIndex;
	int m_iClass;
	int m_iPriority;	// the draw order

	bool m_bKillme;
	bool m_bOnFocus;
	bool m_bVisible;
	bool m_bSortOut;

	// bind this to a controller; 
	// when the parent controller is destroyed, this will be destroyed too
	CBaseMGUI *m_pParent;	

	// bind a child.
	// when this is destroyed. the child controller will be destroyed too
	vector_t *m_pChildren;

protected:
	virtual int FindChild(CBaseMGUI* pChild);

public:
	virtual void Init(void);
	virtual void PreDraw(void);
	virtual void PostDraw(PointPos_t *pp);
	virtual void Draw(PointPos_t *pp){}
	virtual int  GetIndex(void);
	virtual bool GetVisible(void);
	virtual int  GetClass(void);
	virtual void GetPosition(int &iXpos, int &iYpos);
	virtual void GetSize(int &iWidth, int &iHeight);
	virtual bool GetFocus(void);
	virtual bool GetDead(void);
	virtual int  GetPriority(void);
	virtual bool IsInRect( PointPos_t *pp);
	virtual bool IsClick(void);
	virtual CBaseMGUI* GetParent(void);
	virtual vector_t* GetChildren(void);

public:
	virtual void SetFocus(bool);
	virtual void SetVisible(bool);
	virtual void SetPosition(int , int);
	virtual void SetClass(int);
	virtual void SetSize(int iWidth, int iHeight);
	virtual void Destroy(void);
	virtual void DestroyChild(void);
	virtual void SetParent(CBaseMGUI *pParent);
	virtual void AddChild(CBaseMGUI *pChild);
	virtual void SetFree(CBaseMGUI *pChild);
	virtual void SortAll(void);
	virtual void SetPriority(int);
	//static CBaseMGUI* Instance(CBaseMGUI *pParent, int iClass);
};







// =====================================================================================================
class CBaseWindow : public CBaseMGUI
{
	typedef CBaseMGUI  BaseClass;

public:
	virtual void SetFocus(bool);
	virtual void Think(void){}
	//static CBaseWindow* Instance(void);
	virtual void InputKey(int){};
};






// =====================================================================================================
class CBaseLabel : public CBaseMGUI
{
	typedef CBaseMGUI  BaseClass;

public :
	virtual void Init(void);
	virtual void Draw(PointPos_t *pp);
	virtual void SetColor(int , int , int);
	virtual void SetAlpha(int);
	virtual void SetFontSize(int);
	virtual int  GetAlpha(void);
	virtual void SetString(wchar_t *);
	virtual void SetLocalString(char *);
	static CBaseLabel *Instance( CBaseMGUI *pParent );

protected:
	int m_iSize;
	int m_iColor[4];
	bool m_bLocalStr;
	char m_szLocalString[64];
	wchar_t m_pwText[256];
};






// =====================================================================================================
class CBaseImage : public CBaseMGUI
{
	typedef CBaseMGUI  BaseClass;

public:
	virtual void Init(void);
	virtual void SetTexture(int iTexid);
	virtual void SetColor(int , int , int);
	virtual void SetAlpha(int);
	virtual int  GetAlpha(void);
	virtual void Draw(PointPos_t *pp);
	static CBaseImage *Instance(CBaseMGUI *pParent);

protected:
	int m_iColor[4];
	int m_iTextureId;
};







// =====================================================================================================

class CBaseButton : public CBaseMGUI
{
	typedef CBaseMGUI  BaseClass;

public:
	virtual void Init(void);
	virtual void Draw(PointPos_t *pp);
	virtual void SetCommand(char *szCmd);
	virtual void SetCallBackFunc( void (*pfn)(int) );
	virtual void SetMessage(int);
	virtual void SetFontSize(int);
	virtual void SetCaption(wchar_t *);
	virtual void SetCaptionColor(int, int ,int ,int);
	virtual void SetTexture(MGUI_STATUS status, int iTex);
	static CBaseButton *Instance(CBaseMGUI *pParent);

protected:
	virtual void DrawButton( MGUI_STATUS   status );
	virtual void DrawBtnCaption( MGUI_STATUS   status );
	virtual void OnButtonClick(void);
	virtual void OnButtonFire(void){};
	
	int m_iTex_Click;
	int m_iTex_Fire;
	int m_iTex_Normal;

	int m_iTextColor[4];
	int m_iTextSize;
	wchar_t m_pwCaption[64];

	int m_iMessage;
	char m_szCmd[64];
	void (*m_pfnBtnClick)(int imsg);
};






// =====================================================================================================

#define IME_FONT_SIZE			14

class CBaseTextEntry : public CBaseMGUI
{
	typedef CBaseMGUI  BaseClass;

public:
	virtual void Init(void);
	virtual void Draw(PointPos_t *pp);
	virtual void SetCallBackFunc( void (*pfn)(char *) );
	virtual void SetFontSize(int);
	virtual void SetFontColor(int, int ,int ,int);
	virtual void SetFocus(bool);
	virtual int  GetFontSize(void);

	virtual ~CBaseTextEntry();
	static CBaseTextEntry* Instance(CBaseMGUI *pParent);

	void (*m_pfnTextOutput)(char *pText);
	char m_szText[256];

protected:
	virtual void DrawImeList( int x_end );
	virtual void DrawBG( MGUI_STATUS  state){};
	virtual int  DrawBGText( MGUI_STATUS  state);

	int m_iTextColor[4];
	int m_iFontSize;
};






// =====================================================================================================


class CMGUI
{
public:
	CMGUI();
	~CMGUI();

	void Initialize(void);
	int  Redraw(void);
	void VidInit(void);

	bool IsEnable(void);
	void InputKey(int);
	void SetMouseState(int);
	int  GetMouseState(void);
	void AddWindow(CBaseWindow *pWindow);
	vector_t * GetWindows(void);

private:
	void DestroyAll(void);
	int  GetWindowIndex(CBaseWindow *pWindow);

	vector_t*	m_pWindows;
	int			m_iMouseState;
};






extern CMGUI gMGUI;
extern int  g_mgui_mouseevent;
extern int  g_mgui_oldmouseevent;

#endif