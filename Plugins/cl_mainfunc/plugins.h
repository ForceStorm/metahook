#ifndef _PLUGINS_H
#define _PLUGINS_H

extern IFileSystem *g_pFileSystem;

extern DWORD g_dwEngineBase, g_dwEngineSize;
extern DWORD g_dwEngineBuildnum;

extern DWORD g_iVideoMode;
extern int g_iVideoWidth, g_iVideoHeight, g_iBPP;
extern bool g_bWindowed;

#define GetEngfuncsAddress(addr) (g_dwEngineBase+addr-0x1D01000)

#endif