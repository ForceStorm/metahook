#include "base.h"

wchar_t *ANSIToUnicode( const char* str )
{
   int textlen ;
   static wchar_t result[1024];
   textlen = MultiByteToWideChar( CP_ACP, 0, str,-1, NULL,0 ); 
   memset(result, 0, sizeof(char) * ( textlen + 1 ) );
   MultiByteToWideChar(CP_ACP, 0,str,-1,(LPWSTR)result,textlen ); 
   return result; 
}


char *UnicodeToANSI( const wchar_t* str )
{
     static char result[1024];
     int textlen;
     textlen = WideCharToMultiByte( CP_ACP, 0, str, -1, NULL, 0, NULL, NULL );
     memset(result, 0, sizeof(char) * ( textlen + 1 ) );
     WideCharToMultiByte( CP_ACP, 0, str, -1, result, textlen, NULL, NULL );
     return result;
}


wchar_t *UTF8ToUnicode( const char* str )
{
     int textlen ;
     static wchar_t result[1024];
     textlen = MultiByteToWideChar( CP_UTF8, 0, str,-1, NULL,0 ); 
     memset(result, 0, sizeof(char) * ( textlen + 1 ) );
     MultiByteToWideChar(CP_UTF8, 0,str,-1,(LPWSTR)result,textlen ); 
     return result; 
}


char *UnicodeToUTF8( const wchar_t* str )
{
	static char result[1024];
	int textlen;
	textlen = WideCharToMultiByte( CP_UTF8, 0, str, -1, NULL, 0, NULL, NULL );
	memset(result, 0, sizeof(char) * ( textlen + 1 ) );
	WideCharToMultiByte( CP_UTF8, 0, str, -1, result, textlen, NULL, NULL );
	return result;
}


const char *FileExtension( const char *in )
{
	const char *separator, *backslash, *colon, *dot;

	separator = strrchr( in, '/' );
	backslash = strrchr( in, '\\' );
	if( !separator || separator < backslash ) separator = backslash;
	colon = strrchr( in, ':' );
	if( !separator || separator < colon ) separator = colon;

	dot = strrchr( in, '.' );
	if( dot == NULL || ( separator && ( dot < separator )))
		return "";
	return dot + 1;
}

int FileExist(const char *szFile)
{
	DWORD dAttrib = GetFileAttributes(szFile);

	if (dAttrib == INVALID_FILE_ATTRIBUTES || dAttrib == FILE_ATTRIBUTE_DIRECTORY)
		return 0;

	return 1;
}

int Log(char *szLogText, ...)
{
	FILE *fp;

	if (!(fp = fopen(LOG_FILE_ADR, "a")))
		return 0;
	
	va_list vArgptr;
	char szText[1024];

	va_start(vArgptr, szLogText);
	vsprintf(szText, szLogText, vArgptr);
	va_end(vArgptr);

	SYSTEMTIME systime;
	::GetLocalTime(&systime);


	fprintf(fp, "[%02d:%02d:%02d.%03d] %s\n", systime.wHour,systime.wMinute,systime.wSecond,systime.wMilliseconds,szText);
	fclose(fp);
	return 1;
}

void UnpackRGB(int &r, int &g, int &b, unsigned long ulRGB)
{
	r = (ulRGB & 0xFF0000) >>16;
	g = (ulRGB & 0xFF00) >> 8;
	b = ulRGB & 0xFF;
}

void ScaleColors(int &r, int &g, int &b, int a)
{
	float x = (float)a / 255;
	r = (int)(r * x);
	g = (int)(g * x);
	b = (int)(b * x);
}

void StringSeparate(char *pszSrc, char cChar, char **pszDest)
{
	int iLine = 0;
	char szTemp[512];
	int ii = 0;

	for(int i =0; i< strlen(pszSrc); i++)
	{
		if( pszSrc[i] == cChar)
		{
			szTemp[ii] = '\0';
			sprintf( pszDest[ iLine ], szTemp );

			memset(szTemp, 0, sizeof(szTemp) );
			iLine ++;
			ii = 0;
			continue;
		}

		szTemp[ ii ] = pszSrc[i];
		ii ++;
	}
}

void ScaleToScreen( int &iWidth, int & iHeight, int iScreenWidth, int iScreenHeight )
{
	if( iWidth <= iScreenWidth && iHeight <= iScreenHeight )
		return;


	int iW, iH;
	iW = iWidth;
	iH = iHeight;


	float flScaleW = iWidth / ((float) iScreenWidth );
	float flScaleH = iHeight / ((float) iScreenHeight );
	float flScaleRect = iWidth / ( (float)iHeight );

	if( flScaleW > flScaleH )
	{
		iW = iScreenWidth;
		iH = iW / flScaleRect;
	}
	else
	{
		iH = iScreenHeight;
		iW = iH * flScaleRect;
	}

	iWidth = iW;
	iHeight = iH;
}

void angle_vector(float* vecAngle, int Type, float* vecReturn)
{
	vec3_t v_forward, v_right, v_up;
	gEngfuncs.pfnAngleVectors (vecAngle, v_forward, v_right, v_up);

	switch (Type)
	{
	case ANGLEVECTORS_FORWARD:
		VectorCopy(v_forward, vecReturn);
		break;
	case ANGLEVECTORS_RIGHT:
		VectorCopy(v_right, vecReturn);
		break;
	case ANGLEVECTORS_UP:
		VectorCopy(v_up, vecReturn);
		break;
	}
}
