#ifndef UTIL_H
#define UTIL_H


#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#define LOG_FILE_ADR "launcher.log"


const char *FileExtension( const char *in );
int FileExist(const char *szFile);
int Log(char *szLogText, ...);
void UnpackRGB(int &r, int &g, int &b, unsigned long ulRGB);
void ScaleColors(int &r, int &g, int &b, int a);

#define ANGLEVECTORS_FORWARD	1
#define ANGLEVECTORS_RIGHT		2
#define ANGLEVECTORS_UP			3
void angle_vector(float* vecAngle, int Type, float* vecReturn);



wchar_t *UTF8ToUnicode( const char* str );
wchar_t *ANSIToUnicode( const char* str );
char *UnicodeToANSI( const wchar_t* str );
char *UnicodeToUTF8( const wchar_t* str );


inline int AdjustSizeW(int iSize) { return (int)(iSize * g_sScreenInfo.iWidth/1024.0f); }
inline int AdjustSizeH(int iSize) { return (int)(iSize * g_sScreenInfo.iHeight/768.0f); }
void ScaleToScreen( int &iWidth, int & iHeight, int iScreenWidth, int iScreenHeight );



#define ConsolePrint			(*gEngfuncs.pfnConsolePrint)
#define CenterPrint				(*gEngfuncs.pfnCenterPrint)
#define GetPlayerInfo			(*gEngfuncs.pfnGetPlayerInfo)
#define ServerCmd				(*gEngfuncs.pfnServerCmd)
#define ClientCmd				(*gEngfuncs.pfnClientCmd)
#define TextMessageGet			(*gEngfuncs.pfnTextMessageGet)
#define TextMessageDrawChar		(*gEngfuncs.pfnDrawCharacter)
#define GetConsoleStringSize	(*gEngfuncs.pfnDrawConsoleStringLen)

void StringSeparate(char *pszSrc, char cChar, char **pszDest);

#endif
