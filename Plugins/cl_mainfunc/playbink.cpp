#include "base.h"
#include "plugins.h"
#include "playbink.h"

CBinkManager *g_pBinkManager;



CBink::CBink()
{
	m_pNext = NULL;
	m_index = 0;

	m_hBink = 0;
}
CBink::~CBink()
{
	if( m_hBink )
	{
		BinkClose( m_hBink );
		free( m_pPixel );
	}
}

CBink * CBink::CreateBink( int index, int x, int y, int r, int g, int b, char *szName, int iCenter, int iFullScreen, int iLoop, float flStayTime, float flFadeoutTime, float flPlayRate , int bitsFlags )
{
	CBink *pBink = new CBink;

	pBink->m_hBink				= BinkOpen( szName,  bitsFlags);

	if( !pBink->m_hBink )
	{
		Log( "[Bink]Failed to load bink file�� %s", szName);
		delete pBink;
		return NULL;
	}

	int iPixelSize = 4 * pBink->m_hBink->Width   *   pBink->m_hBink->Height;

	pBink->m_index				= index;
	pBink->m_iTextureId			= g_pSurface->CreateNewTextureID();
	pBink->m_pPixel				= (byte *)malloc(iPixelSize);
	pBink->m_iFrame				= 0;
	pBink->m_iX					= x;
	pBink->m_iY					= y;
	pBink->m_r					= r;
	pBink->m_g					= g;
	pBink->m_b					= b;
	pBink->m_iCenter			= iCenter;
	pBink->m_iFullScreen		= iFullScreen;
	pBink->m_iLoop				= iLoop;
	pBink->m_flStayTime			= flStayTime;
	pBink->m_flFadeoutTime		= flFadeoutTime;
	pBink->m_flPlayRate			= flPlayRate;


	return pBink;
}


CBinkManager::CBinkManager()
{
	m_pBink = NULL;
}

void CBinkManager::AddBink(CBink *pBink) 
{
	if( !pBink )
		return;

	if( m_pBink )
	{
		CBink *pointer;
		pointer = m_pBink ;
		
		while(pointer->m_pNext)
			pointer = pointer->m_pNext;

		pointer->m_pNext = pBink;
	}
	else
		m_pBink = pBink;
}

void CBinkManager::DeleteBink ( int index )
{
	CBink *pointer = m_pBink;

	if( !pointer )
		return;

	while( pointer )
	{
		if( pointer->m_index == index )
		{
			pointer->m_index = 0;
			return;
		}
		pointer = pointer->m_pNext ;
	}

}


void CBinkManager::DeleteBink ( CBink *pBink )
{
	CBink *pointer = m_pBink;

	if( !pointer )
		return;

	if( pointer == pBink )
	{
		m_pBink = pointer->m_pNext;
		delete pointer;
		return;

	}

	while( pointer->m_pNext && pointer->m_pNext != pBink )
		pointer = pointer->m_pNext;

	if( !pointer->m_pNext )
		return;

	pointer->m_pNext = pBink->m_pNext;
	delete pBink;
}
void CBinkManager::Redraw (void)
{
	CBink *pointer = m_pBink;

	while( pointer )
	{
		if( !pointer->m_hBink )
			continue;

		if( !pointer->m_index )
		{
			if( m_pBink == pointer)
			{
				delete m_pBink;
				m_pBink = NULL;
				return;
			}
			CBink *pointer2 = m_pBink;
			while( pointer2->m_pNext && pointer2->m_pNext != pointer )
				pointer2 = pointer2->m_pNext ;
			if( pointer2->m_pNext && pointer2->m_pNext == pointer )
			{
				pointer2->m_pNext = pointer->m_pNext ;
				delete pointer;
				pointer = pointer2->m_pNext ;
			}
		}

		int iAlpha = 255;
		
		if( pointer->m_flPlayRate != PLAYBINK_NORMAL_RATE && pointer->m_iFrame < pointer->m_hBink->Frames)
		{
			if(g_flTime < pointer->m_flNextFrameTime)
			{
				goto DISPLAY;
			}
			else
			{
				pointer->m_flNextFrameTime = g_flTime + pointer->m_flPlayRate;
			}
		}
		
		pointer->m_iFrame++;
		if( pointer->m_iFrame > pointer->m_hBink->Frames)
		{
			if( pointer->m_iLoop )
			{
				pointer->m_iFrame = 1;
			}
			else if( pointer->m_iFrame - pointer->m_hBink->Frames == 1)
			{
				pointer->m_flEndDisplayTime = g_flTime;
			}
			else
			{
				if( pointer->m_flStayTime && g_flTime < pointer->m_flStayTime + pointer->m_flEndDisplayTime)
				{
					goto DISPLAY;
				}
				else if( pointer->m_flFadeoutTime && g_flTime < pointer->m_flFadeoutTime + pointer->m_flStayTime + pointer->m_flEndDisplayTime)
				{
					iAlpha = (int)( 255 * ( pointer->m_flFadeoutTime + pointer->m_flStayTime + pointer->m_flEndDisplayTime - g_flTime)/pointer->m_flFadeoutTime );
					goto DISPLAY;
				}
				else
				{
					CBink *pointer2;
					pointer2 = pointer->m_pNext;
					DeleteBink( pointer );
					pointer = pointer2;

					continue;
				}
			}
			
		}
		
		BinkGoto( pointer->m_hBink, pointer->m_iFrame, 0);
		BinkDoFrame( pointer->m_hBink );
		BinkCopyToBuffer( pointer->m_hBink, pointer->m_pPixel, pointer->m_hBink->Width * 4, pointer->m_hBink->Height, 0, 0, BINKSURFACE32RA);

		Tri_BindTexture(GL_TEXTURE_2D, pointer->m_iTextureId );
		Tri_SetTextureParam();
		Tri_TexImage2D(GL_TEXTURE_2D, 0, 4, pointer->m_hBink->Width, pointer->m_hBink->Height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pointer->m_pPixel);

DISPLAY:

		int iX ,iY,iWidth,iHeight;
		if(pointer->m_iFullScreen)
		{
			iWidth = g_sScreenInfo.iWidth;
			iHeight = g_sScreenInfo.iHeight;
			iX = iY =0;
		}
		else
		{
			iWidth = AdjustSizeW( pointer->m_hBink->Width);
			iHeight = AdjustSizeH( pointer->m_hBink->Height);
			if(pointer->m_iCenter)
			{
				iX = pointer->m_iX - iWidth/2;
				iY = pointer->m_iY - iHeight/2;
			}
			else
			{
				iX = pointer->m_iX;
				iY = pointer->m_iY;
			}
		}

		Tri_Enable(GL_TEXTURE_2D);
		Tri_BindTexture(GL_TEXTURE_2D, pointer->m_iTextureId);

		if( g_iVideoMode == VIDEOMODE_OPENGL)
			glColor4ub(  pointer->m_r, pointer->m_g, pointer->m_b, iAlpha );
		else if ( g_iVideoMode == VIDEOMODE_D3D)
			gEngfuncs.pTriAPI->Color4ub(  pointer->m_r, pointer->m_g, pointer->m_b, iAlpha );

		Tri_Enable(GL_BLEND);
		Tri_Enable(GL_ALPHA_TEST);
		Tri_BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
		Tri_AlphaFunc(GL_GREATER, 0.0);
		gEngfuncs.pTriAPI->Begin(TRI_QUADS);
			
		gEngfuncs.pTriAPI->TexCoord2f(0,1);
		gEngfuncs.pTriAPI->Vertex3f( iX, iY+iHeight, 0);

		gEngfuncs.pTriAPI->TexCoord2f(1,1);
		gEngfuncs.pTriAPI->Vertex3f( iX+iWidth,  iY+iHeight,  0);
		
		gEngfuncs.pTriAPI->TexCoord2f(1,0);
		gEngfuncs.pTriAPI->Vertex3f( iX+iWidth,  iY,  0);
			
		gEngfuncs.pTriAPI->TexCoord2f(0,0);
		gEngfuncs.pTriAPI->Vertex3f( iX,  iY,  0);
		gEngfuncs.pTriAPI->End();


		pointer = pointer->m_pNext;
	}
}